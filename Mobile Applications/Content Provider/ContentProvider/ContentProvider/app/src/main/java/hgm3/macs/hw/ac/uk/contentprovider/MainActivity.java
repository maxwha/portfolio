package hgm3.macs.hw.ac.uk.contentprovider;

import android.app.Activity;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    public void onClickInsert(View view) {
        ContentValues values = new ContentValues();

        values.put("cName", ((EditText) findViewById(R.id.txt_cName))
                .getText().toString());

        values.put("ccity", ((EditText) findViewById(R.id.txt_ccity))
                .getText().toString());

        String area1 = ((EditText) findViewById(R.id.txt_area)).getText().toString();
        int area = Integer.parseInt(area1);
        values.put("area", area);

        String popula1 = ((EditText) findViewById(R.id.txt_popula)).getText().toString();
        int popula = Integer.parseInt(popula1);
        values.put("popula", popula);

        values.put("olang", ((EditText) findViewById(R.id.txt_olang)).getText().toString());

        Uri uri = getContentResolver().insert(Provider.CONTENT_URI, values);
        Toast.makeText(getBaseContext(), "New record inserted", Toast.LENGTH_LONG)
                .show();

        onClickClear(null);
    }

    public void onClickExit(View v) {
        finish();
    }

    public void onClickClear(View v) {
        EditText name = (EditText) findViewById(R.id.txt_cName);
        EditText ccity = (EditText) findViewById(R.id.txt_ccity);
        EditText area = (EditText) findViewById(R.id.txt_area);
        EditText population = (EditText) findViewById(R.id.txt_popula);
        EditText olang = (EditText) findViewById(R.id.txt_olang);

        name.setText("");
        ccity.setText("");
        area.setText("");
        population.setText("");
        olang.setText("");

    }
}