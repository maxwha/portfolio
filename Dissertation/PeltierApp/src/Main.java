import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


public class Main 
{
	
	static int user;
	Font titleFont = new Font("Veranda", Font.BOLD, 25);
	
	public static void main(String[] args) {
		
		PeltierControl.init();
		
		// Create and display a window that asks for a user ID
		final JFrame userNumberFrame = new JFrame("User Number");
		userNumberFrame.getContentPane().setPreferredSize(new Dimension(200,100));
		userNumberFrame.setResizable(false);
		userNumberFrame.setAlwaysOnTop(true);
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3,1));
		panel.add(new JLabel("Enter User Number"));
		final JTextField userField = new JTextField();
		panel.add(userField);
		
		// Start button logs the user ID and then opens the main window of the app.
		JButton startButton = new JButton("Start");
		startButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					user = Integer.parseInt(userField.getText());
					userNumberFrame.dispose();
					MainWindow.main(args);
				}
				catch (Exception e)
				{
					userField.setText("");

				}
			}
			
		});
		
		panel.add(startButton);
		userNumberFrame.getContentPane().add(panel);
		userNumberFrame.pack();
		userNumberFrame.setVisible(true);
	}
}
