import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JSlider;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.BoxLayout;
import javax.swing.SpringLayout;
import javax.swing.DefaultComboBoxModel;

public class MainWindow {

	JFrame tempWindow;
	static JLabel peltierStatus;
	String[][] results = new String[12][2];
	static private int questionNum = 1;
	long time_windowOpened, time_firstTempChange = 0, time_lastTempChange = 0;
	Thread tempTimerThread;
	final int cooldownTime = 45;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.tempWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * @wbp.parser.entryPoint
	 */
	public MainWindow()
	{
		initialize();
	}
	
	public static void write (String filename, String[][] s)
	{
		try {
		BufferedWriter br = new BufferedWriter(new FileWriter(filename));
		StringBuilder sb = new StringBuilder();
		
		 for (int x = 0; x < 12; x++)
	      {
	         String content = "";
	         for (int y = 0; y < 2; y++)
	         {
	            content += s[x][y]+ ",";
	         }
	        br.write(content);
	        br.newLine();
	      }
		br.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void initialize() {
		
		tempWindow = new JFrame();
		tempWindow.setTitle("Peltier Control - "+questionNum+"/12");
		tempWindow.setAlwaysOnTop(true);
		tempWindow.setBounds(100, 100, 510, 193);
		tempWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		tempWindow.getContentPane().setLayout(springLayout);
		tempTimerThread = new Thread();
		time_windowOpened = System.currentTimeMillis();
		
		tempWindow.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	if (time_firstTempChange != 0){
		    		PeltierControl.cooldown((long) 30.0);
		    	}
		    }
		});
		
		/*Reconnect Peltier*/
		
		JButton reconnect = new JButton("Reconnect");
		reconnect.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				PeltierControl.init();
			}
		});
		tempWindow.getContentPane().add(reconnect);
		
		/*Peltier Status*/

		peltierStatus = new JLabel("Connecting...");
		springLayout.putConstraint(SpringLayout.WEST, peltierStatus, 20, SpringLayout.WEST, tempWindow.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, peltierStatus, -37, SpringLayout.SOUTH, tempWindow.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, reconnect, 6, SpringLayout.SOUTH, peltierStatus);
		springLayout.putConstraint(SpringLayout.WEST, reconnect, 0, SpringLayout.WEST, peltierStatus);
		peltierStatus.setHorizontalAlignment(SwingConstants.CENTER);
		tempWindow.getContentPane().add(peltierStatus);

		/*Temperature Status*/
		
		JLabel change = new JLabel("Temperature Set");
		springLayout.putConstraint(SpringLayout.WEST, change, 20, SpringLayout.WEST, tempWindow.getContentPane());
		change.setHorizontalAlignment(SwingConstants.CENTER);
		tempWindow.getContentPane().add(change);
		
		/*Drop-down Box*/
		
		JComboBox range = new JComboBox();
		springLayout.putConstraint(SpringLayout.EAST, range, -8, SpringLayout.EAST, tempWindow.getContentPane());
		range.setModel(new DefaultComboBoxModel(new String[] {"-----", "Very Warm", "Warm", "Neutral", "Cold", "Very Cold"}));
		tempWindow.getContentPane().add(range);
		
		/*Temperature Slider*/
		
		JSlider temp_slider = new JSlider();
		springLayout.putConstraint(SpringLayout.WEST, temp_slider, 12, SpringLayout.WEST, tempWindow.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, temp_slider, -6, SpringLayout.NORTH, change);
		springLayout.putConstraint(SpringLayout.EAST, temp_slider, 0, SpringLayout.EAST, range);
		temp_slider.setMinorTickSpacing(1);
		temp_slider.setMajorTickSpacing(10);
		temp_slider.setMinimum(220);
		temp_slider.setMaximum(380);
		temp_slider.setValue(300);
		System.out.println("starting val: " + Integer.toString(temp_slider.getValue()));
		final Runnable tempControl = new Runnable()
		{
				public void run() {
					//SET PELTIER TEMPERATURE
					System.out.println("setting temp to: "+Integer.toString(temp_slider.getValue()));
					PeltierControl.setPeltierTemperature((temp_slider.getValue()));
					for (int i = 10; i > 0; i--){
						System.out.println("changing temp");
						change.setText("Setting temperature...");
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							return;
						}
					}
					//RESET PELTIER TEMPERATURE
					System.out.println("finished changing temp to "+temp_slider.getValue());
					change.setText("Temperature Set");
					//PeltierControl.setPeltierTemperature(300);
					//temp_slider.setValue(30);
				}
				
			};
			temp_slider.addMouseListener(new MouseListener(){

				public void mouseClicked(MouseEvent arg0) {
					// TODO Auto-generated method stub
				}

				public void mouseEntered(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				public void mouseExited(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				public void mousePressed(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				public void mouseReleased(MouseEvent arg0) {
					// TODO Auto-generated method stub
					if (time_firstTempChange == 0){
						time_firstTempChange = System.currentTimeMillis();
						time_lastTempChange = System.currentTimeMillis();
					}
					if (tempTimerThread.isAlive()){
						tempTimerThread.interrupt();
					}
					tempTimerThread = new Thread(tempControl);
					tempTimerThread.start();
				}
				
			});
			temp_slider.addChangeListener(new ChangeListener(){

				public void stateChanged(ChangeEvent arg0) {
					// TODO Auto-generated method stub
					time_lastTempChange = System.currentTimeMillis();
				}
				
			});
		tempWindow.getContentPane().add(temp_slider);
		
		/*Submit Results*/
		
		JButton q_submit = new JButton("Submit");
		springLayout.putConstraint(SpringLayout.NORTH, q_submit, 6, SpringLayout.SOUTH, range);
		springLayout.putConstraint(SpringLayout.EAST, q_submit, -10, SpringLayout.EAST, tempWindow.getContentPane());
		q_submit.addActionListener(new ActionListener()
		{
			//TODO add submit conditions
			public void actionPerformed(ActionEvent arg0) 
			{
				/*while(range.getSelectedItem().toString() == "-----")
				{
					q_submit.setEnabled(false);
				}	
				while (range.getSelectedItem().toString() != "-----")
				{
					q_submit.setEnabled(true);
				}*/
				if (range.getSelectedItem().toString() == "-----"){};
				
				if (range.getSelectedItem().toString() != "-----")
				{
					String final_range = range.getSelectedItem().toString();
					String final_temp = Integer.toString(PeltierControl.getPeltierTemperature());
					//add results
					results[questionNum-1][0] = final_temp;
					results[questionNum-1][1] = final_range;
					//reset range
					range.setSelectedIndex(0);
					final_range = "";
					//reset temp
					final_temp = "";
					//reset GUI
					PeltierControl.setPeltierTemperature(300);
					temp_slider.setValue(300);
					//increase question number
					questionNum++;
					tempWindow.setTitle("Peltier Control - "+questionNum+"/12");
				}
				if (questionNum > 12)
				{
					String resultsFile = "user"+Main.user+"results.csv";
					write(resultsFile, results);
					questionNum = 1;
					tempWindow.dispose();
				}
			}
			
		});	
		tempWindow.getContentPane().add(q_submit);
		
		JLabel lblPleaseChooseA = new JLabel("Please choose a range \r\nfor your chosen temperature");
		springLayout.putConstraint(SpringLayout.WEST, lblPleaseChooseA, 20, SpringLayout.WEST, tempWindow.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblPleaseChooseA, -164, SpringLayout.EAST, tempWindow.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, range, -3, SpringLayout.NORTH, lblPleaseChooseA);
		springLayout.putConstraint(SpringLayout.WEST, range, 6, SpringLayout.EAST, lblPleaseChooseA);
		springLayout.putConstraint(SpringLayout.SOUTH, lblPleaseChooseA, -26, SpringLayout.NORTH, peltierStatus);
		springLayout.putConstraint(SpringLayout.SOUTH, change, -6, SpringLayout.NORTH, lblPleaseChooseA);
		tempWindow.getContentPane().add(lblPleaseChooseA);
	}
}
