--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.0
-- Dumped by pg_dump version 9.4.0
-- Started on 2015-01-19 23:55:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 250 (class 1259 OID 27192)
-- Name: feed_amex_security; Type: TABLE; Schema: public; Owner: admin; Tablespace: 
--

CREATE TABLE feed_amex_security (
    id integer NOT NULL,
    symbol character varying(255),
    name character varying(255),
    lastsale character varying(255),
    marketcap character varying(255),
    ipoyear character varying(255),
    sector character varying(255),
    industry character varying(255),
    summaryquote character varying(255)
);


ALTER TABLE feed_amex_security OWNER TO admin;

--
-- TOC entry 249 (class 1259 OID 27190)
-- Name: feed_amex_security_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE feed_amex_security_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_amex_security_id_seq OWNER TO admin;

--
-- TOC entry 2255 (class 0 OID 0)
-- Dependencies: 249
-- Name: feed_amex_security_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE feed_amex_security_id_seq OWNED BY feed_amex_security.id;


--
-- TOC entry 2127 (class 2604 OID 27195)
-- Name: id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY feed_amex_security ALTER COLUMN id SET DEFAULT nextval('feed_amex_security_id_seq'::regclass);


--
-- TOC entry 2250 (class 0 OID 27192)
-- Dependencies: 250
-- Data for Name: feed_amex_security; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY feed_amex_security (id, symbol, name, lastsale, marketcap, ipoyear, sector, industry, summaryquote) FROM stdin;
1	XXII	22nd Century Group, Inc	1.48	$95.2M	n/a	Consumer Non-Durables	Farming/Seeds/Milling	http://www.nasdaq.com/symbol/xxii
2	FAX	Aberdeen Asia-Pacific Income Fund Inc	5.67	$1.51B	1986	n/a	n/a	http://www.nasdaq.com/symbol/fax
3	IAF	Aberdeen Australia Equity Fund Inc	6.99	$160.34M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/iaf
4	CH	Aberdeen Chile Fund, Inc.	7.35	$68.99M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/ch
5	ETF	Aberdeen Emerging Markets Smaller Company Opportunities Fund I	12.97	$126.65M	1992	n/a	n/a	http://www.nasdaq.com/symbol/etf
6	FCO	Aberdeen Global Income Fund, Inc.	9.71	$89.29M	1992	n/a	n/a	http://www.nasdaq.com/symbol/fco
7	IF	Aberdeen Indonesia Fund, Inc.	8.16	$77.4M	1990	n/a	n/a	http://www.nasdaq.com/symbol/if
8	ISL	Aberdeen Israel Fund, Inc.	16.88	$69.36M	1992	n/a	n/a	http://www.nasdaq.com/symbol/isl
9	ACU	Acme United Corporation.	19.06	$62.7M	1988	Capital Goods	Industrial Machinery/Components	http://www.nasdaq.com/symbol/acu
10	ATNM	Actinium Pharmaceuticals, Inc.	5.17	$155.13M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/atnm
11	AE	Adams Resources & Energy, Inc.	52.15	$219.95M	n/a	Energy	Oil Refining/Marketing	http://www.nasdaq.com/symbol/ae
12	ADK	Adcare Health Systems Inc	4	$75.51M	n/a	Health Care	Hospital/Nursing Management	http://www.nasdaq.com/symbol/adk
13	ADK^A	Adcare Health Systems Inc	27.6464	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/adk^a
14	API	Advanced Photonix, Inc.	0.32	$11.96M	n/a	Technology	Semiconductors	http://www.nasdaq.com/symbol/api
15	ACY	AeroCentury Corp.	7.82	$12.07M	n/a	Technology	Diversified Commercial Services	http://www.nasdaq.com/symbol/acy
16	WGA	AG&E Holdings, Inc.	0.66	$7.74M	n/a	Technology	Radio And Television Broadcasting And Communications Equipment	http://www.nasdaq.com/symbol/wga
17	AIRI	Air Industries Group	10.1	$71.71M	n/a	Capital Goods	Military/Government/Technical	http://www.nasdaq.com/symbol/airi
18	AA^	Alcoa Inc.	85.14	n/a	n/a	Capital Goods	Metal Fabrications	http://www.nasdaq.com/symbol/aa^
19	AXX	Alderon Iron Ore Corp.	0.3575	$47.24M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/axx
20	AXU	Alexco Resource Corp	0.542	$37.72M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/axu
21	ANV	Allied Nevada Gold Corp.	1.19	$149.92M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/anv
22	AAU	Almaden Minerals, Ltd.	1.17	$80.41M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/aau
23	APT	Alpha Pro Tech, Ltd.	2.74	$50.14M	n/a	Health Care	Industrial Specialties	http://www.nasdaq.com/symbol/apt
24	ALTV	Alteva (D/B/A)	7.05	$42.13M	n/a	Public Utilities	Telecommunications Equipment	http://www.nasdaq.com/symbol/altv
25	AAMC	Altisource Asset Management Corp	321.81	$704.72M	n/a	Finance	Real Estate	http://www.nasdaq.com/symbol/aamc
26	DIT	AMCON Distributing Company	78.001	$48.01M	n/a	Consumer Non-Durables	Food Distributors	http://www.nasdaq.com/symbol/dit
27	APP	American Apparel Inc	0.8	$139.82M	n/a	Finance	Business Services	http://www.nasdaq.com/symbol/app
28	ADGE	American DG Energy Inc.	0.575	$30.17M	n/a	Energy	Electric Utilities: Central	http://www.nasdaq.com/symbol/adge
29	AMZG	American Eagle Energy Corporation.	0.614	$18.7M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/amzg
30	ALN	American Lorain Corporation	1.05	$36.66M	n/a	Consumer Non-Durables	Packaged Foods	http://www.nasdaq.com/symbol/aln
31	AMS	American Shared Hospital Services	2.7	$14.48M	n/a	Health Care	Medical Specialities	http://www.nasdaq.com/symbol/ams
32	AQQ	American Spectrum Realty, Inc.	1.68	$6.22M	n/a	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/aqq
33	AMPE	Ampio Pharmaceuticals, Inc.	3.66	$190.22M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/ampe
34	AXN	Aoxing Pharmaceutical Company, Inc.	0.3301	$21.87M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/axn
35	AMCO	Armco Metals Holdings, Inc.	1.9	$10.64M	n/a	Consumer Durables	Steel/Iron Ore	http://www.nasdaq.com/symbol/amco
36	HRT	Arrhythmia Research Technology Inc.	7.739	$21.34M	n/a	Health Care	Biotechnology: Electromedical & Electrotherapeutic Apparatus	http://www.nasdaq.com/symbol/hrt
37	AKG	Asanko Gold Inc.	1.67	$290.71M	n/a	Basic Industries	Mining & Quarrying of Nonmetallic Minerals (No Fuels)	http://www.nasdaq.com/symbol/akg
38	AINC	Ashford Inc.	103.2	$206.4M	2014	Consumer Services	Professional Services	http://www.nasdaq.com/symbol/ainc
39	ASB/WS	Associated Banc-Corp	2.15	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/asb/ws
40	AST	Asterias Biotherapeutics, Inc.	4.8	$148.33M	n/a	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/ast
41	ATL	Atlatsa Resources Corporation	0.1858	$102.99M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/atl
42	AWX	Avalon Holdings Corporation	3.15	$11.98M	n/a	Public Utilities	Environmental Services	http://www.nasdaq.com/symbol/awx
43	AVL	Avalon Rare Metals, Inc.	0.182	$24.88M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/avl
44	ASM	Avino Silver	1.6	$56.6M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/asm
45	BTG	B2Gold Corp	2.05	$1.88B	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/btg
46	BONE	Bacterin International Holdings, Inc.	2.93	$19.56M	n/a	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/bone
47	BTN	Ballantyne Strong, Inc	4.25	$61.29M	n/a	Miscellaneous	Industrial Machinery/Components	http://www.nasdaq.com/symbol/btn
48	BKJ	Bancorp of New Jersey, Inc	11.06	$59.39M	n/a	Finance	Major Banks	http://www.nasdaq.com/symbol/bkj
49	BCV	Bancroft Fund Limited	19.95	$104.58M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/bcv
50	BAA	BANRO CORPORATION	0.17	$42.86M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/baa
51	BHB	Bar Harbor Bankshares, Inc.	32.0201	$190.16M	n/a	Finance	Major Banks	http://www.nasdaq.com/symbol/bhb
52	BRN	Barnwell Industries, Inc.	2.83	$23.42M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/brn
53	BIR^A	Berkshire Income Realty, Inc.	28.6522	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/bir^a
54	BGSF	BG Staffing Inc	13	$72.91M	n/a	Technology	Professional Services	http://www.nasdaq.com/symbol/bgsf
55	BTX	BioTime, Inc.	4.5	$374.05M	n/a	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/btx
56	BTX/WS	BioTime, Inc.	1.35	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/btx/ws
57	BGI	Birks Group Inc.	1.55	$27.79M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/bgi
58	BZM	BlackRock Maryland Municipal Bond Trust	14.8799	$30.87M	2002	n/a	n/a	http://www.nasdaq.com/symbol/bzm
59	MHE	BlackRock Massachusetts Tax-Exempt Trust	14.2699	$33.72M	1993	n/a	n/a	http://www.nasdaq.com/symbol/mhe
60	BLE	BlackRock Municipal Income Trust II	16.18	$379.47M	2002	n/a	n/a	http://www.nasdaq.com/symbol/ble
61	BLJ	Blackrock New Jersey Municipal Bond Trust	15.12	$35.15M	2002	n/a	n/a	http://www.nasdaq.com/symbol/blj
62	BFY	BlackRock New York Municipal Income Trust II	15.03	$75.13M	2002	n/a	n/a	http://www.nasdaq.com/symbol/bfy
63	BPS	BlackRock Pennsylvania Strategic Municipal Trust (The)	13.1133	$26.65M	1999	n/a	n/a	http://www.nasdaq.com/symbol/bps
64	BHV	BlackRock Virginia Municipal Bond Trust	16.2	$25.77M	2002	n/a	n/a	http://www.nasdaq.com/symbol/bhv
65	BDR	Blonder Tongue Laboratories, Inc.	2.31	$14.39M	n/a	Technology	Radio And Television Broadcasting And Communications Equipment	http://www.nasdaq.com/symbol/bdr
66	BRG	Bluerock Residential Growth REIT, Inc.	13.45	$115.56M	2014	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/brg
67	BVX	Bovie Medical Corporation	3.78	$67.43M	n/a	Health Care	Medical/Dental Instruments	http://www.nasdaq.com/symbol/bvx
68	BWL/A	Bowl America, Inc.	14.75	n/a	n/a	Consumer Services	Services-Misc. Amusement & Recreation	http://www.nasdaq.com/symbol/bwl/a
69	BZC	Breeze-Eastern Corporation	10.05	$98.65M	n/a	Capital Goods	Military/Government/Technical	http://www.nasdaq.com/symbol/bzc
70	BTI	British American Tobacco p.l.c.	106.53	$99.29B	n/a	Consumer Non-Durables	Farming/Seeds/Milling	http://www.nasdaq.com/symbol/bti
71	CAK	CAMAC Energy Inc.	0.2816	$357.01M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/cak
72	CANF	Can-Fite Biopharma Ltd	3.85	$34.11M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/canf
73	ROX	Castle Brands, Inc.	1.54	$239.93M	2006	Consumer Non-Durables	Beverages (Production/Distribution)	http://www.nasdaq.com/symbol/rox
74	CAW	CCA Industries, Inc.	3.3999	$23.82M	n/a	Consumer Non-Durables	Package Goods/Cosmetics	http://www.nasdaq.com/symbol/caw
75	CVM	Cel-Sci Corporation	0.69	$63.03M	n/a	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/cvm
76	CVM/WS	Cel-Sci Corporation	0.1492	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/cvm/ws
77	CEF	Central Fund of Canada Limited	12.22	$3.11B	n/a	n/a	n/a	http://www.nasdaq.com/symbol/cef
78	GTU	Central Gold Trust	42.42	$818.66M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/gtu
79	CET	Central Securities Corporation	21.42	$516.59M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/cet
80	CCF	Chase Corporation	34.22	$312.41M	n/a	Consumer Durables	Building Products	http://www.nasdaq.com/symbol/ccf
81	CQH	Cheniere Energy Partners LP Holdings, LLC	21.92	$5.08B	2013	Public Utilities	Oil/Gas Transmission	http://www.nasdaq.com/symbol/cqh
82	CQP	Cheniere Energy Partners, LP	29.81	$10.07B	2007	Public Utilities	Oil/Gas Transmission	http://www.nasdaq.com/symbol/cqp
83	LNG	Cheniere Energy, Inc.	68.48	$16.22B	n/a	Public Utilities	Oil/Gas Transmission	http://www.nasdaq.com/symbol/lng
84	CVR	Chicago Rivet & Machine Co.	30.73	$29.69M	n/a	Technology	Industrial Machinery/Components	http://www.nasdaq.com/symbol/cvr
85	CNR	China Metro-Rural Holdings Limited	0.868	$63.84M	n/a	Finance	Real Estate	http://www.nasdaq.com/symbol/cnr
86	CPHI	China Pharma Holdings, Inc.	0.321	$13.99M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/cphi
87	CKX	CKX Lands, Inc.	15.585	$30.27M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/ckx
88	GLV	Clough Global Allocation Fund	14.676	$153.14M	2004	n/a	n/a	http://www.nasdaq.com/symbol/glv
89	GLQ	Clough Global Equity Fund	14.4687	$258.13M	2005	n/a	n/a	http://www.nasdaq.com/symbol/glq
90	GLO	Clough Global Opportunities Fund	12.46	$644.64M	2006	n/a	n/a	http://www.nasdaq.com/symbol/glo
91	CRV	Coast Distribution System, Inc. (The)	3.3654	$17.89M	n/a	Capital Goods	Automotive Aftermarket	http://www.nasdaq.com/symbol/crv
92	MOC	Command Security Corporation	2.87	$27.89M	n/a	Consumer Services	Diversified Commercial Services	http://www.nasdaq.com/symbol/moc
93	CIX	CompX International Inc.	11.6501	$144.51M	1998	Capital Goods	Industrial Machinery/Components	http://www.nasdaq.com/symbol/cix
94	LODE	Comstock Mining, Inc.	0.9156	$78.3M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/lode
95	CTO	Consolidated-Tomoka Land Co.	54.8	$321.52M	n/a	Finance	Real Estate	http://www.nasdaq.com/symbol/cto
96	MCF	Contango Oil & Gas Company	25.28	$489.8M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/mcf
97	CUO	Continental Materials Corporation	16.17	$26.68M	n/a	Capital Goods	Building Materials	http://www.nasdaq.com/symbol/cuo
98	CMT	Core Molding Technologies Inc	14.56	$111.68M	n/a	Consumer Non-Durables	Plastic Products	http://www.nasdaq.com/symbol/cmt
99	CRMD	CorMedix Inc	1.9	$42.53M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/crmd
100	CRMD/WS	CorMedix Inc	0.0911	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/crmd/ws
101	CFP	Cornerstone Progressive Return Fund	15.4	$252.04M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/cfp
102	CRF	Cornerstone Strategic Return Fund, Inc. (The)	20.37	$90.04M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/crf
103	CLM	Cornerstone Strategic Value Fund, Inc.	20.83	$42.22M	1987	n/a	n/a	http://www.nasdaq.com/symbol/clm
104	COVR	Cover-All Technologies Inc.	1.24	$33.03M	n/a	Technology	Computer Software: Prepackaged Software	http://www.nasdaq.com/symbol/covr
105	CVU	CPI Aerostructures, Inc.	11.73	$99.51M	n/a	Capital Goods	Military/Government/Technical	http://www.nasdaq.com/symbol/cvu
106	CIK	Credit Suisse Asset Management Income Fund, Inc.	3.23	$168.74M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/cik
107	DHY	Credit Suisse High Yield Bond Fund	2.8	$278.65M	1998	n/a	n/a	http://www.nasdaq.com/symbol/dhy
108	CRVP	Crystal Rock Holdings, Inc.	0.77	$16.45M	n/a	Consumer Non-Durables	Food Distributors	http://www.nasdaq.com/symbol/crvp
109	CTP	CTPartners Executive Search Inc.	12.3	$89.3M	2010	Technology	Diversified Commercial Services	http://www.nasdaq.com/symbol/ctp
110	CVSL	CVSL Inc.	8.58	$209.34M	2014	n/a	n/a	http://www.nasdaq.com/symbol/cvsl
111	DAKP	Dakota Plains Holdings, Inc.	1.57	$86.22M	n/a	Energy	Oil Refining/Marketing	http://www.nasdaq.com/symbol/dakp
112	DXR	Daxor Corporation	6.85	$27.63M	n/a	Health Care	Medical/Dental Instruments	http://www.nasdaq.com/symbol/dxr
113	DEJ	Dejour Energy Inc.	0.153	$27.91M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/dej
114	VCF	Delaware Investments Colorado Municipal Income Fund, Inc	14.24	$68.88M	1993	n/a	n/a	http://www.nasdaq.com/symbol/vcf
115	VFL	Delaware Investments Florida Insured Municipal Income Fund	13.34	$60.41M	1993	n/a	n/a	http://www.nasdaq.com/symbol/vfl
116	VMM	Delaware Investments Minnesota Municipal Income Fund II, Inc.	13.71	$157.73M	1993	n/a	n/a	http://www.nasdaq.com/symbol/vmm
117	DLA	Delta Apparel, Inc.	9.69	$76.34M	n/a	Consumer Non-Durables	Apparel	http://www.nasdaq.com/symbol/dla
118	DNN	Denison Mine Corp	0.95	$480.58M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/dnn
119	DGSE	DGSE Companies, Inc.	1.4	$17.11M	n/a	Consumer Services	Consumer Specialties	http://www.nasdaq.com/symbol/dgse
120	DPW	Digital Power Corporation	1.1563	$7.83M	n/a	Capital Goods	Industrial Machinery/Components	http://www.nasdaq.com/symbol/dpw
121	DSS	Document Security Systems, Inc.	0.32	$13.51M	n/a	Technology	EDP Services	http://www.nasdaq.com/symbol/dss
122	DMF	Dreyfus Municipal Income, Inc.	9.66	$200.1M	1988	n/a	n/a	http://www.nasdaq.com/symbol/dmf
123	GRF	Eagle Capital Growth Fund, Inc.	8.56	$26.75M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/grf
124	ESTE	Earthstone Energy, Inc.	21.85	$301.95M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/este
125	EVM	Eaton Vance California Municipal Bond Fund	11.94	$255.5M	2002	n/a	n/a	http://www.nasdaq.com/symbol/evm
126	EIA	Eaton Vance California Municipal Bond Fund II	12.94	$50.27M	2002	n/a	n/a	http://www.nasdaq.com/symbol/eia
127	CEV	Eaton Vance California Municipal Income Trust	13.09	$95.05M	1999	n/a	n/a	http://www.nasdaq.com/symbol/cev
128	EVV	Eaton Vance Limited Duration Income Fund	14.07	$1.65B	2003	n/a	n/a	http://www.nasdaq.com/symbol/evv
129	MAB	Eaton Vance Massachusetts Municipal Bond Fund	14.38	$25.43M	2002	n/a	n/a	http://www.nasdaq.com/symbol/mab
130	MMV	Eaton Vance Massachusetts Municipal Income Trust	13.83	$38.04M	1999	n/a	n/a	http://www.nasdaq.com/symbol/mmv
131	MIW	Eaton Vance Michigan Municipal Bond Fund	13.66	$20.68M	2002	n/a	n/a	http://www.nasdaq.com/symbol/miw
132	EMI	Eaton Vance Michigan Municipal Income Trust	12.87	$27.24M	1999	n/a	n/a	http://www.nasdaq.com/symbol/emi
133	EIM	Eaton Vance Municipal Bond Fund	12.98	$884.82M	2002	n/a	n/a	http://www.nasdaq.com/symbol/eim
134	EIV	Eaton Vance Municipal Bond Fund II	13.06	$130.77M	2002	n/a	n/a	http://www.nasdaq.com/symbol/eiv
135	EMJ	Eaton Vance New Jersey Municipal Bond Fund	13.13	$34.25M	2002	n/a	n/a	http://www.nasdaq.com/symbol/emj
136	EVJ	Eaton Vance New Jersey Municipal Income Trust	12.44	$58.21M	1999	n/a	n/a	http://www.nasdaq.com/symbol/evj
137	ENX	Eaton Vance New York Municipal Bond Fund	12.7	$199.16M	2002	n/a	n/a	http://www.nasdaq.com/symbol/enx
138	NYH	Eaton Vance New York Municipal Bond Fund II	12.25	$31.45M	2002	n/a	n/a	http://www.nasdaq.com/symbol/nyh
139	EVY	Eaton Vance New York Municipal Income Trust	13.8	$75.55M	1999	n/a	n/a	http://www.nasdaq.com/symbol/evy
140	EIO	Eaton Vance Ohio Municipal Bond Fund	12.74	$32.32M	2002	n/a	n/a	http://www.nasdaq.com/symbol/eio
141	EVO	Eaton Vance Ohio Municipal Income Trust	13.9301	$39.8M	1999	n/a	n/a	http://www.nasdaq.com/symbol/evo
142	EIP	Eaton Vance Pennsylvania Municipal Bond Fund	13.07	$38.69M	2002	n/a	n/a	http://www.nasdaq.com/symbol/eip
143	EVP	Eaton Vance Pennsylvania Municipal Income Trust	12.36	$33.62M	1999	n/a	n/a	http://www.nasdaq.com/symbol/evp
144	ELMD	Electromed, Inc.	2.35	$19.07M	2011	Health Care	Biotechnology: Electromedical & Electrotherapeutic Apparatus	http://www.nasdaq.com/symbol/elmd
145	ETAK	Elephant Talk Communications Corp.	0.7825	$118.74M	n/a	Public Utilities	Telecommunications Equipment	http://www.nasdaq.com/symbol/etak
146	ELLO	Ellomay Capital Ltd.	9.2	$98.44M	n/a	Public Utilities	Electric Utilities: Central	http://www.nasdaq.com/symbol/ello
147	ECF	Ellsworth Convertible Growth and Income Fund, Inc.	8.5	$111.7M	1986	n/a	n/a	http://www.nasdaq.com/symbol/ecf
148	EMAN	eMagin Corporation	2.55	$63.81M	n/a	Technology	Semiconductors	http://www.nasdaq.com/symbol/eman
149	EOX	Emerald Oil, Inc.	0.88	$58.63M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/eox
150	MSN	Emerson Radio Corporation	1.03	$27.94M	n/a	Consumer Non-Durables	Consumer Electronics/Appliances	http://www.nasdaq.com/symbol/msn
151	UUUU	Energy Fuels Inc	5.21	$102.55M	n/a	Basic Industries	Mining & Quarrying of Nonmetallic Minerals (No Fuels)	http://www.nasdaq.com/symbol/uuuu
152	ENRJ	EnerJex Resources, Inc.	1.3	$9.94M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/enrj
153	ENRJ^	EnerJex Resources, Inc.	18.0001	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/enrj^
154	ENSV	ENSERVCO Corporation	1.75	$64.85M	n/a	Energy	Oilfield Services/Equipment	http://www.nasdaq.com/symbol/ensv
155	EGI	Entree Gold Inc	0.1942	$28.54M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/egi
156	EVI	EnviroStarm, Inc.	2.5881	$18.2M	n/a	Consumer Services	Other Consumer Services	http://www.nasdaq.com/symbol/evi
157	ERB	ERBA Diagnostics, Inc.	3.2709	$143.95M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/erb
158	ESP	Espey Mfg. & Electronics Corp.	23.8	$56.14M	n/a	Capital Goods	Industrial Machinery/Components	http://www.nasdaq.com/symbol/esp
159	EMXX	Eurasian Minerals Inc.	0.757	$55.54M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/emxx
160	EVBN	Evans Bancorp, Inc.	24.3	$101.81M	n/a	Finance	Major Banks	http://www.nasdaq.com/symbol/evbn
161	EPM	Evolution Petroleum Corporation, Inc.	7.23	$237.13M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/epm
162	EPM^A	Evolution Petroleum Corporation, Inc.	25.75	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/epm^a
163	XRA	Exeter Resource Corporation	0.6425	$56.8M	n/a	Basic Industries	Mining & Quarrying of Nonmetallic Minerals (No Fuels)	http://www.nasdaq.com/symbol/xra
164	FPI	Farmland Partners Inc.	10.85	$83.89M	2014	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/fpi
165	FPP	FieldPoint Petroleum Corporation	1.68	$13.65M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/fpp
166	FPP/WS	FieldPoint Petroleum Corporation	0.25	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/fpp/ws
167	FEN	First Trust Energy Income and Growth Fund	34.47	$667.17M	2004	n/a	n/a	http://www.nasdaq.com/symbol/fen
168	FWV	First West Virginia Bancorp, Inc.	20.1401	$34.82M	n/a	Finance	Major Banks	http://www.nasdaq.com/symbol/fwv
169	BDL	Flanigan&#39;s Enterprises, Inc.	30.45	$56.6M	n/a	Consumer Services	Restaurants	http://www.nasdaq.com/symbol/bdl
170	FSI	Flexible Solutions International Inc.	1.075	$14.16M	n/a	Basic Industries	Major Chemicals	http://www.nasdaq.com/symbol/fsi
171	FTF	Franklin Limited Duration Income Trust	12.19	$327.13M	2003	n/a	n/a	http://www.nasdaq.com/symbol/ftf
172	FSP	Franklin Street Properties Corp.	12.72	$1.27B	n/a	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/fsp
173	FRD	Friedman Industries Inc.	6.75	$45.9M	n/a	Capital Goods	Steel/Iron Ore	http://www.nasdaq.com/symbol/frd
174	FRS	Frisch&#39;s Restaurants, Inc.	27.59	$141.2M	n/a	Consumer Services	Restaurants	http://www.nasdaq.com/symbol/frs
175	GGN	GAMCO Global Gold, Natural Reources & Income Trust 	7.27	$782.92M	2005	n/a	n/a	http://www.nasdaq.com/symbol/ggn
176	GGN^B	GAMCO Global Gold, Natural Reources & Income Trust 	21.7	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/ggn^b
177	EGAS	Gas Natural Inc.	10.71	$112.32M	n/a	Public Utilities	Oil/Gas Transmission	http://www.nasdaq.com/symbol/egas
178	GST	Gastar Exploration Inc.	2.21	$174.28M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/gst
179	GST^A	Gastar Exploration Inc.	19.5	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/gst^a
180	GST^B	Gastar Exploration Inc.	22.41	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/gst^b
181	JOB	General Employment Enterprises, Inc.	1.11	$28.75M	n/a	Technology	Diversified Commercial Services	http://www.nasdaq.com/symbol/job
182	GMO	General Moly, Inc	0.53	$48.2M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/gmo
183	GIG	GigOptix, Inc.	1.19	$39.1M	n/a	Technology	Semiconductors	http://www.nasdaq.com/symbol/gig
184	GSB	GlobalSCAPE, Inc.	2.4116	$50.57M	n/a	Technology	Computer Software: Prepackaged Software	http://www.nasdaq.com/symbol/gsb
185	GSAT	Globalstar, Inc.	2.72	$2.69B	2006	Consumer Services	Telecommunications Equipment	http://www.nasdaq.com/symbol/gsat
186	GLOW	Glowpoint, Inc.	1.03	$36.88M	n/a	Public Utilities	Telecommunications Equipment	http://www.nasdaq.com/symbol/glow
187	GORO	Gold Resource Corporation	3.64	$197.21M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/goro
188	GSV	Gold Standard Ventures Corporation	0.525	$64.96M	n/a	Basic Industries	Mining & Quarrying of Nonmetallic Minerals (No Fuels)	http://www.nasdaq.com/symbol/gsv
189	AUMN	Golden Minerals Company	0.5308	$28.14M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/aumn
190	GSS	Golden Star Resources, Ltd	0.272	$70.58M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/gss
191	GV	Goldfield Corporation (The)	2.36	$60.07M	n/a	Basic Industries	Water Supply	http://www.nasdaq.com/symbol/gv
192	GRC	Gorman-Rupp Company (The)	29.68	$779.41M	n/a	Capital Goods	Fluid Controls	http://www.nasdaq.com/symbol/grc
193	GTE	Gran Tierra Energy Inc.	3.17	$872.65M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/gte
194	GPL	Great Panther Silver Limited	0.68	$94.9M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/gpl
195	GRH	GreenHunter Resources, Inc.	0.62	$22M	n/a	Basic Industries	Major Chemicals	http://www.nasdaq.com/symbol/grh
196	GRH^C	GreenHunter Resources, Inc.	15.75	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/grh^c
197	SIM	Grupo Simec, S.A. de C.V.	9.326	$1.55B	1993	Basic Industries	Steel/Iron Ore	http://www.nasdaq.com/symbol/sim
198	GVP	GSE Systems, Inc.	1.75	$31.3M	n/a	Technology	Computer Software: Prepackaged Software	http://www.nasdaq.com/symbol/gvp
199	HCHC	HC2 Holdings, Inc.	8.19	$195.04M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/hchc
200	HEB	Hemispherx BioPharma, Inc.	0.24	$48.01M	n/a	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/heb
201	HLM^	Hillman Group Capital Trust	31.95	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/hlm^
202	HMG	HMG/Courtland Properties, Inc.	12.2	$12.8M	n/a	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/hmg
203	HH	Hooper Holmes, Inc.	0.5	$35.43M	n/a	Health Care	Hospital/Nursing Management	http://www.nasdaq.com/symbol/hh
204	HUSA	Houston American Energy Corporation	0.18	$9.39M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/husa
205	IBIO	iBio, Inc.	0.4532	$32.59M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/ibio
206	IBO	IBO (Listing Market - NYSE Amex Network B F)	153.82	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/ibo
207	IEC	IEC Electronics Corp.	4.6582	$47.24M	n/a	Technology	Electrical Products	http://www.nasdaq.com/symbol/iec
208	IG	IGI Laboratories, Inc.	10.01	$528.41M	n/a	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/ig
209	IMUC	ImmunoCellular Therapeutics, Ltd.	0.75	$47.04M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/imuc
210	IMH	Impac Mortgage Holdings, Inc.	8.3986	$80.53M	1997	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/imh
211	IMO	Imperial Oil Limited	37.92	$32.14B	n/a	Energy	Integrated oil Companies	http://www.nasdaq.com/symbol/imo
212	IOT	Income Opportunity Realty Investors, Inc.	6.189	$25.8M	n/a	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/iot
213	IRT	Independence Realty Trust, Inc.	9.37	$241.76M	2013	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/irt
214	IGC	India Globalization Capital Inc.	0.7	$9.66M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/igc
215	IGC/WS	India Globalization Capital Inc.	0.015	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/igc/ws
216	INFU	InfuSystems Holdings, Inc.	2.74	$61.06M	n/a	Health Care	Medical/Dental Instruments	http://www.nasdaq.com/symbol/infu
217	IHT	InnSuites Hospitality Trust	2.29	$18.96M	n/a	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/iht
218	NSPR	InspireMD, Inc.	0.86	$36.47M	n/a	Health Care	Medical/Dental Instruments	http://www.nasdaq.com/symbol/nspr
219	IFMI	Institutional Financial Markets, Inc.	1.61	$24.4M	n/a	Finance	Investment Bankers/Brokers/Service	http://www.nasdaq.com/symbol/ifmi
220	IDN	Intellicheck Mobilisa, Inc.	1.7	$8.39M	1999	Technology	Computer Software: Prepackaged Software	http://www.nasdaq.com/symbol/idn
221	INS	Intelligent Systems Corporation	1.78	$15.95M	1992	Capital Goods	Metal Fabrications	http://www.nasdaq.com/symbol/ins
222	THM	International Tower Hill Mines Ltd	0.4679	$54.42M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/thm
223	INTT	inTest Corporation	4.12	$43.52M	1997	Capital Goods	Electrical Products	http://www.nasdaq.com/symbol/intt
224	INUV	Inuvo, Inc	1.18	$27.81M	n/a	Consumer Services	Advertising	http://www.nasdaq.com/symbol/inuv
225	VKI	Invesco Advantage Municipal Income Trust II	11.76	$521.92M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/vki
226	ISR	IsoRay, Inc.	1.52	$83.42M	n/a	Health Care	Medical/Dental Instruments	http://www.nasdaq.com/symbol/isr
227	ISDR	Issuer Direct Corporation	9.55	$19.93M	n/a	Miscellaneous	Publishing	http://www.nasdaq.com/symbol/isdr
228	ITI	Iteris, Inc.	1.7	$55.35M	n/a	Consumer Durables	Telecommunications Equipment	http://www.nasdaq.com/symbol/iti
229	KIQ	Kelso Technologies Inc	5.25	$237.28M	n/a	Capital Goods	Railroads	http://www.nasdaq.com/symbol/kiq
230	LTS	Ladenburg Thalmann Financial Services Inc	3.96	$733.92M	n/a	Finance	Investment Bankers/Brokers/Service	http://www.nasdaq.com/symbol/lts
231	LTS^A	Ladenburg Thalmann Financial Services Inc	24.0995	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/lts^a
232	LSG	Lake Shore Gold Corp	0.8	$348.34M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/lsg
233	LAQ	Latin America Equity Fund, Inc. (The)	22.24	$165.66M	1991	n/a	n/a	http://www.nasdaq.com/symbol/laq
234	LGL	LGL Group, Inc. (The)	4.11	$10.66M	n/a	Capital Goods	Industrial Machinery/Components	http://www.nasdaq.com/symbol/lgl
235	LGL/WS	LGL Group, Inc. (The)	0.0159	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/lgl/ws
236	LBY	Libbey, Inc.	32.41	$702.18M	1993	Capital Goods	Industrial Machinery/Components	http://www.nasdaq.com/symbol/lby
237	LBMH	Liberator Medical Holdings, Inc.	3.13	$166.41M	n/a	Health Care	Medical/Nursing Services	http://www.nasdaq.com/symbol/lbmh
238	LIQT	LiqTech International, Inc.	0.75	$29.48M	n/a	Technology	Industrial Machinery/Components	http://www.nasdaq.com/symbol/liqt
239	LEI	Lucas Energy, Inc.	0.11	$3.85M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/lei
240	MCZ	Mad Catz Interactive Inc	0.457	$29.29M	n/a	Consumer Non-Durables	Recreational Products/Toys	http://www.nasdaq.com/symbol/mcz
241	MVG	Mag Silver Corporation	8.92	$613.85M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/mvg
242	MHR^C	Magnum Hunter Resources Corporation	20.48	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/mhr^c
243	MHR^D	Magnum Hunter Resources Corporation	33.8	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/mhr^d
244	MHR^E	Magnum Hunter Resources Corporation	19.98	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/mhr^e
245	MSTX	Mast Therapeutics, Inc.	0.538	$69.14M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/mstx
246	MHH	Mastech Holdings, Inc	10.37	$44.66M	n/a	Consumer Services	Professional Services	http://www.nasdaq.com/symbol/mhh
247	MDGN	Medgenics, Inc.	6.36	$157.75M	2011	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/mdgn
248	MDGN/WS	Medgenics, Inc.	2.3999	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/mdgn/ws
249	IPB	Merrill Lynch & Co., Inc.	27.9595	n/a	n/a	Finance	Finance: Consumer Services	http://www.nasdaq.com/symbol/ipb
250	MEA	Metalico Inc	0.2611	$15.23M	n/a	Basic Industries	Metal Fabrications	http://www.nasdaq.com/symbol/mea
251	MXC	Mexco Energy Corporation	4.9	$9.99M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/mxc
252	CCA	MFS California Insured Municipal Trust	11.29	$31.46M	1999	n/a	n/a	http://www.nasdaq.com/symbol/cca
253	MGT	MGT Capital Investments Inc	0.635	$6.69M	n/a	Miscellaneous	Multi-Sector Companies	http://www.nasdaq.com/symbol/mgt
254	MDW	Midway Gold Corporation	0.756	$133.47M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/mdw
255	MGH	Minco Gold Corporation	0.31	$15.67M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/mgh
256	MGN	Mines Management, Inc.	0.5445	$16.23M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/mgn
257	MVF	MuniVest Fund, Inc.	10.15	$649.42M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/mvf
258	MZA	MuniYield Arizona Fund, Inc.	16.57	$76.01M	1993	n/a	n/a	http://www.nasdaq.com/symbol/mza
259	NNVC	NanoViricides, Inc.	2.53	$147.55M	n/a	Health Care	Biotechnology: Commercial Physical & Biological Resarch	http://www.nasdaq.com/symbol/nnvc
260	NHC	National HealthCare Corporation	63.24	$891.58M	n/a	Health Care	Hospital/Nursing Management	http://www.nasdaq.com/symbol/nhc
261	NHC^A	National HealthCare Corporation	15.54	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/nhc^a
262	NAVB	Navidea Biopharmaceuticals, Inc.	1.89	$284.9M	n/a	Health Care	Biotechnology: In Vitro & In Vivo Diagnostic Substances	http://www.nasdaq.com/symbol/navb
263	NTIP	Network-1 Technologies, Inc.	2.35	$57.51M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/ntip
264	NBW	Neuberger Berman California Intermediate Municipal Fund Inc.	15.83	$87.66M	2002	n/a	n/a	http://www.nasdaq.com/symbol/nbw
265	NHS	Neuberger Berman High Yield Strategies Fund	12.35	$241.33M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/nhs
266	NBH	Neuberger Berman Intermediate Municipal Fund Inc.	15.669	$294.04M	2002	n/a	n/a	http://www.nasdaq.com/symbol/nbh
267	NML	Neuberger Berman MLP Income Fund Inc.	17.64	$997.08M	2013	n/a	n/a	http://www.nasdaq.com/symbol/nml
268	NBO	Neuberger Berman New York Intermediate Municipal Fund Inc.	14.57	$73.96M	2002	n/a	n/a	http://www.nasdaq.com/symbol/nbo
269	NRO	Neuberger Berman Real Estate Securities Income Fund, Inc.	5.43	$302.93M	2003	n/a	n/a	http://www.nasdaq.com/symbol/nro
270	CUR	Neuralstem, Inc.	3.22	$281.3M	n/a	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/cur
271	UWN	Nevada Gold & Casinos, Inc.	1.34	$21.74M	n/a	Consumer Services	Services-Misc. Amusement & Recreation	http://www.nasdaq.com/symbol/uwn
272	NSU	Nevsun Resources Ltd	3.83	$764.65M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/nsu
273	GBR	New Concept Energy, Inc	1.309	$2.55M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/gbr
274	NEN	New England Realty Associates Limited Partnership	47.56	$182.96M	n/a	Consumer Services	Building operators	http://www.nasdaq.com/symbol/nen
275	NGD	NEW GOLD INC.	4.73	$2.4B	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/ngd
276	PAL	North American Palladium, Ltd.	0.19	$73.44M	2000	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/pal
277	NAK	Northern Dynasty Minerals, Ltd.	0.42	$39.9M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/nak
278	NOG	Northern Oil and Gas, Inc.	5.7	$347.55M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/nog
279	NBY	NovaBay Pharmaceuticals, Inc.	0.61	$31.49M	2007	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/nby
280	NCQ	NovaCopper Inc.	0.61	$36.78M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/ncq
281	NG	Novagold Resources Inc.	3.69	$1.17B	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/ng
282	NTN	NTN Buzztime, Inc.	0.468	$43.2M	n/a	Consumer Services	Broadcasting	http://www.nasdaq.com/symbol/ntn
283	NVX	Nuveen California Dividend Advantage Municipal Fund 2	14.4651	$213.49M	2001	n/a	n/a	http://www.nasdaq.com/symbol/nvx
284	NZH	Nuveen California Dividend Advantage Municipal Fund 3	13.8	$333.3M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/nzh
285	NCB	Nuveen California Municipal Value Fund 2	16.81	$55.27M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/ncb
286	CFD	Nuveen Diversified Commodity Fund	12.38	$113.36M	2010	n/a	n/a	http://www.nasdaq.com/symbol/cfd
287	NXZ	Nuveen Dividend Advantage Municipal Fund 2	14.39	$424.19M	2001	n/a	n/a	http://www.nasdaq.com/symbol/nxz
288	NZF	Nuveen Dividend Advantage Municipal Fund 3	14.15	$571.66M	2007	n/a	n/a	http://www.nasdaq.com/symbol/nzf
289	NVG	Nuveen Dividend Advantage Municipal Income Fund	14.41	$427.14M	2002	n/a	n/a	http://www.nasdaq.com/symbol/nvg
290	CTF	Nuveen Long/Short Commodity Total Return Fund	16.6	$277.9M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/ctf
291	NOM	Nuveen Missouri Premium Income Municipal Fund	15.3	$35.65M	1993	n/a	n/a	http://www.nasdaq.com/symbol/nom
292	NMZ	Nuveen Municipal High Income Opportunity Fund	13.58	$679.98M	2003	n/a	n/a	http://www.nasdaq.com/symbol/nmz
293	NJV	Nuveen New Jersey Municipal Value Fund	14.95	$23.17M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/njv
294	NXK	Nuveen New York Dividend Advantage Municipal Fund 2	13.43	$87.07M	2001	n/a	n/a	http://www.nasdaq.com/symbol/nxk
295	NYV	Nuveen New York Municipal Value Fund 2	15.17	$35.64M	2009	n/a	n/a	http://www.nasdaq.com/symbol/nyv
296	NPN	Nuveen Pennsylvania Municipal Value Fund	15.374	$18.75M	2009	n/a	n/a	http://www.nasdaq.com/symbol/npn
297	JRS	Nuveen Real Estate Fund	11.98	$346.13M	2001	n/a	n/a	http://www.nasdaq.com/symbol/jrs
298	OGEN	Oragenics, Inc.	0.8002	$28.95M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/ogen
299	TIS	Orchids Paper Products Company	27.85	$243.91M	2005	Basic Industries	Paper	http://www.nasdaq.com/symbol/tis
300	ONVO	Organovo Holdings, Inc.	6.69	$538.58M	n/a	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/onvo
301	ONP	Orient Paper, Inc.	1	$20.32M	n/a	Consumer Durables	Containers/Packaging	http://www.nasdaq.com/symbol/onp
302	OESX	Orion Energy Systems, Inc.	4.84	$105.86M	2007	Consumer Durables	Building Products	http://www.nasdaq.com/symbol/oesx
303	OSGB	Overseas Shipholding Group, Inc.	5.16	$157.62M	n/a	Transportation	Marine Transportation	http://www.nasdaq.com/symbol/osgb
304	ORM	Owens Realty Mortgage, Inc.	14.41	$155.17M	2013	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/orm
305	PHF	Pacholder High Yield Fund, Inc.	7.42	$96.43M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/phf
306	PBM	Pacific Booker Minerals Inc	3.8	$46.98M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/pbm
307	PCG^A	Pacific Gas & Electric Co.	28.65	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/pcg^a
308	PCG^B	Pacific Gas & Electric Co.	26.5	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/pcg^b
309	PCG^C	Pacific Gas & Electric Co.	25.07	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/pcg^c
310	PCG^D	Pacific Gas & Electric Co.	24.08	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/pcg^d
311	PCG^E	Pacific Gas & Electric Co.	24.27	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/pcg^e
312	PCG^G	Pacific Gas & Electric Co.	23.1	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/pcg^g
313	PCG^H	Pacific Gas & Electric Co.	22.3	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/pcg^h
314	PCG^I	Pacific Gas & Electric Co.	21.5	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/pcg^i
315	PTN	Palatin Technologies, Inc.	0.72	$28.43M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/ptn
316	PARR	Par Petroleum Corporation	17	$625.61M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/parr
317	PZG	Paramount Gold and Silver Corp.	1.21	$196.05M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/pzg
318	PRK	Park National Corporation	82.5	$1.27B	n/a	Finance	Major Banks	http://www.nasdaq.com/symbol/prk
319	PED	Pedevco Corp.	0.3367	$11.15M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/ped
320	PFNX	Pfenex Inc.	7.443	$151.67M	2014	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/pfnx
321	PIP	PharmAthene, Inc	1.66	$105.2M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/pip
322	HNW	Pioneer Diversified High Income Trust	17.45	$145.41M	2007	n/a	n/a	http://www.nasdaq.com/symbol/hnw
323	PLG	Platinum Group Metals Ltd.	0.5	$383.06M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/plg
324	PLM	Polymet Mining Corp.	1.15	$317.18M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/plm
325	PW	Power REIT	9.39	$16.26M	n/a	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/pw
326	PW^A	Power REIT	25.25	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/pw^a
327	APTS	Preferred Apartment Communities, Inc.	9.48	$188.32M	2011	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/apts
328	PLX	Protalix BioTherapeutics, Inc.	2.34	$219.19M	n/a	Health Care	Biotechnology: Biological Products (No Diagnostic Substances)	http://www.nasdaq.com/symbol/plx
329	PVCT	Provectus Biopharmaceuticals, Inc.	0.8	$144.24M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/pvct
330	QRM	Quest Rare Minerals Ltd	0.1	$7.88M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/qrm
331	RLGT	Radiant Logistics, Inc.	4.35	$150.76M	n/a	Transportation	Oil Refining/Marketing	http://www.nasdaq.com/symbol/rlgt
332	RLGT^A	Radiant Logistics, Inc.	27.28	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/rlgt^a
333	REE	Rare Element Resources Ltd.	0.355	$16.94M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/ree
334	UTG	Reaves Utility Income Fund	32.5	$942.96M	2004	n/a	n/a	http://www.nasdaq.com/symbol/utg
335	REED	Reeds, Inc.	5.7	$74.45M	n/a	Consumer Non-Durables	Beverages (Production/Distribution)	http://www.nasdaq.com/symbol/reed
336	RWC	RELM Wireless Corporation	4.47	$61.08M	n/a	Technology	Radio And Television Broadcasting And Communications Equipment	http://www.nasdaq.com/symbol/rwc
337	RCG	RENN Fund, Inc.	1.3	$5.8M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/rcg
338	RVP	Retractable Technologies, Inc.	4.25	$116.79M	n/a	Health Care	Medical/Dental Instruments	http://www.nasdaq.com/symbol/rvp
339	RVM	Revett Mining Company, Inc.	0.7299	$28.54M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/rvm
340	RNN	Rexahn Pharmaceuticals, Inc.	0.89	$158.65M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/rnn
341	RIC	Richmont Mines, Inc.	3.58	$172.83M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/ric
342	REI	Ring Energy, Inc.	8.55	$219.98M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/rei
343	RIF	RMR Real Estate Income Fund.	21.72	$166.21M	n/a	n/a	n/a	http://www.nasdaq.com/symbol/rif
344	RPI	Roberts Realty Investors, Inc.	1.45	$16.82M	n/a	Consumer Services	Real Estate Investment Trusts	http://www.nasdaq.com/symbol/rpi
345	RBY	Rubicon Minerals Corp	1.12	$415M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/rby
346	SGA	Saga Communications, Inc.	42.78	$248.51M	n/a	Consumer Services	Broadcasting	http://www.nasdaq.com/symbol/sga
347	SSN	Samson Oil & Gas Limited	0.1801	$25.55M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/ssn
348	SPP	Sanchez Production Partners LLC	1.38	$39.74M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/spp
349	SAND          	Sandstorm Gold Ltd	3.98	$468.28M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/sand          
350	SARA	Saratoga Resources Inc	0.2299	$7.12M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/sara
351	SEB	Seaboard Corporation	4036	$4.72B	1996	n/a	n/a	http://www.nasdaq.com/symbol/seb
352	SVT	Servotronics, Inc.	6.21	$15.26M	n/a	Capital Goods	Industrial Machinery/Components	http://www.nasdaq.com/symbol/svt
353	SIF	SIFCO Industries, Inc.	30	$162.39M	n/a	Capital Goods	Aerospace	http://www.nasdaq.com/symbol/sif
354	SVBL	Silver Bull Resources, Inc.	0.14	$22.27M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/svbl
355	SVLC	SilverCrest Mines, Inc.	1.39	$165.07M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/svlc
356	SKY	Skyline Corporation	3.57	$29.96M	n/a	Basic Industries	Homebuilding	http://www.nasdaq.com/symbol/sky
357	SLI	SL Industries, Inc.	42.89	$177.74M	n/a	Capital Goods	Industrial Machinery/Components	http://www.nasdaq.com/symbol/sli
358	XPL	Solitario Exploration & Royalty Corp	0.95	$37.29M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/xpl
359	SCE^B	Southern California Edison Company	22.36	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/sce^b
360	SCE^C	Southern California Edison Company	22.28	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/sce^c
361	SCE^D	Southern California Edison Company	22.9	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/sce^d
362	SCE^E	Southern California Edison Company	23.885	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/sce^e
363	SGB	Southwest Georgia Financial Corporation	13.4	$34.14M	n/a	Finance	Major Banks	http://www.nasdaq.com/symbol/sgb
364	LOV	Spark Networks, Inc.	3.42	$83.22M	n/a	Miscellaneous	Business Services	http://www.nasdaq.com/symbol/lov
365	STRP	Straight Path Communications Inc.	16.88	$201.72M	2013	Public Utilities	Telecommunications Equipment	http://www.nasdaq.com/symbol/strp
366	SSY	SunLink Health Systems, Inc.	1.384	$13.07M	n/a	Health Care	Hospital/Nursing Management	http://www.nasdaq.com/symbol/ssy
367	SDPI	Superior Drilling Products, Inc.	3.65	$63.11M	2014	Energy	Metal Fabrications	http://www.nasdaq.com/symbol/sdpi
368	STS	Supreme Industries, Inc.	7.75	$128.18M	n/a	Capital Goods	Construction/Ag Equipment/Trucks	http://www.nasdaq.com/symbol/sts
369	SYRG	Synergy Resources Corporation	12.36	$1.05B	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/syrg
370	SYN	Synthetic Biologics, Inc	1.56	$113.12M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/syn
371	TRX	Tanzanian Royalty Exploration Corporation	0.54	$54.93M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/trx
372	TGB	Taseko Mines Limited	1.01	$224.03M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/tgb
373	TAS	Tasman Metals Ltd	0.4	$26.46M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/tas
374	TRC/WS	Tejon Ranch Co	1.751	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/trc/ws
375	TIK	Tel-Instrument Electronics Corp.	5.44	$17.69M	n/a	Capital Goods	Electrical Products	http://www.nasdaq.com/symbol/tik
376	TGC	Tengasco, Inc.	0.2785	$16.94M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/tgc
377	GLU	The Gabelli Global Utility and Income Trust	18.76	$77.13M	2004	n/a	n/a	http://www.nasdaq.com/symbol/glu
378	GLU^A	The Gabelli Global Utility and Income Trust	50.45	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/glu^a
379	TXMD	TherapeuticsMD, Inc.	4.11	$641.29M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/txmd
380	TPI	Tianyin Pharmaceutical Co., Inc.	0.65	$19.13M	n/a	Health Care	Major Pharmaceuticals	http://www.nasdaq.com/symbol/tpi
381	IDI	Tiger Media, Inc.	1.34	$48.78M	n/a	Consumer Services	Advertising	http://www.nasdaq.com/symbol/idi
382	TLR	Timberline Resources Corporation	0.758	$7.5M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/tlr
383	TGD	Timmons Gold Corp	1.16	$208.59M	2011	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/tgd
384	TOF	Tofutti Brands Inc.	5.4	$27.83M	n/a	Consumer Non-Durables	Specialty Foods	http://www.nasdaq.com/symbol/tof
385	TMP	Tompkins Financial Corporation	50.95	$752.18M	n/a	Finance	Major Banks	http://www.nasdaq.com/symbol/tmp
386	TAT	Transatlantic Petroleum Ltd	4.75	$193.34M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/tat
387	TRXC	TransEnterix, Inc.	3.25	$205.28M	n/a	Health Care	Medical/Dental Instruments	http://www.nasdaq.com/symbol/trxc
388	TPLM	Triangle Petroleum Corporation	4.53	$347.05M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/tplm
389	TRT	Trio-Tech International	3.15	$11.07M	n/a	Technology	Industrial Machinery/Components	http://www.nasdaq.com/symbol/trt
390	HTM	U.S. Geothermal Inc.	0.4605	$49.28M	n/a	Public Utilities	Electric Utilities: Central	http://www.nasdaq.com/symbol/htm
391	UAMY	United States Antimony Corporation	0.7	$46.22M	n/a	Capital Goods	Metal Fabrications	http://www.nasdaq.com/symbol/uamy
392	UUU	Universal Security Instruments, Inc.	6.02	$13.92M	n/a	Consumer Non-Durables	Electronic Components	http://www.nasdaq.com/symbol/uuu
393	UQM	UQM TECHNOLOGIES INC	0.789	$31.95M	n/a	Capital Goods	Industrial Machinery/Components	http://www.nasdaq.com/symbol/uqm
394	URG	Ur Energy Inc	0.85	$109.89M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/urg
395	URZ	Uranerz Energy Corporation	1.21	$116.03M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/urz
396	UEC	Uranium Energy Corp.	1.49	$136.7M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/uec
397	VTG	Vantage Drilling Company	0.35	$107.73M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/vtg
398	VSR	Versar, Inc.	3.05	$29.76M	1986	Consumer Services	Military/Government/Technical	http://www.nasdaq.com/symbol/vsr
399	VII	Vicon Industries, Inc.	1.7	$7.69M	n/a	Consumer Durables	Telecommunications Equipment	http://www.nasdaq.com/symbol/vii
400	VHC	VirnetX Holding Corp	5.11	$265.7M	n/a	Miscellaneous	Multi-Sector Companies	http://www.nasdaq.com/symbol/vhc
401	VGZ	Vista Gold Corporation	0.38	$31.31M	n/a	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/vgz
402	VISI	Volt Information Sciences, Inc.	10.45	$218.12M	n/a	Technology	Professional Services	http://www.nasdaq.com/symbol/visi
403	EAD	Wells Fargo Advantage Income Opportunities Fund	8.75	$621.1M	2003	n/a	n/a	http://www.nasdaq.com/symbol/ead
404	ERC	Wells Fargo Advantage Multi-Sector Income Fund	13.72	$576.99M	2003	n/a	n/a	http://www.nasdaq.com/symbol/erc
405	ERH	Wells Fargo Advantage Utilities and High Income Fund	13.29	$122.68M	2004	n/a	n/a	http://www.nasdaq.com/symbol/erh
406	SBI	Western Asset Intermediate Muni Fund Inc	9.86	$138.8M	1992	n/a	n/a	http://www.nasdaq.com/symbol/sbi
407	WRN	Western Copper and Gold Corporation	0.5535	$52.14M	2011	Basic Industries	Precious Metals	http://www.nasdaq.com/symbol/wrn
408	WYY	WidePoint Corporation	1.52	$123.91M	n/a	Technology	EDP Services	http://www.nasdaq.com/symbol/wyy
409	WTT	Wireless Telecom Group,  Inc.	2.74	$53.27M	n/a	Capital Goods	Electrical Products	http://www.nasdaq.com/symbol/wtt
410	YUMA	Yuma Energy, Inc.	1.55	$106.74M	n/a	Energy	Oil & Gas Production	http://www.nasdaq.com/symbol/yuma
411	YUMA^A	Yuma Energy, Inc.	17.525	n/a	n/a	n/a	n/a	http://www.nasdaq.com/symbol/yuma^a
412	ZBB	ZBB Energy Corporation	0.7	$27.33M	2007	Public Utilities	Electric Utilities: Central	http://www.nasdaq.com/symbol/zbb
\.


--
-- TOC entry 2256 (class 0 OID 0)
-- Dependencies: 249
-- Name: feed_amex_security_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('feed_amex_security_id_seq', 412, true);


--
-- TOC entry 2129 (class 2606 OID 27200)
-- Name: feed_amex_security_pk; Type: CONSTRAINT; Schema: public; Owner: admin; Tablespace: 
--

ALTER TABLE ONLY feed_amex_security
    ADD CONSTRAINT feed_amex_security_pk PRIMARY KEY (id);


-- Completed on 2015-01-19 23:55:44

--
-- PostgreSQL database dump complete
--

