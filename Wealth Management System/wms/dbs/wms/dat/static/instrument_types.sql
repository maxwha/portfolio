-- DELETE FROM instrument_types where  name in ( 'EQUITY','CASH', 'INDEX', 'CURRENCIES' );

DELETE FROM instrument_types where  name in ( 'EXCHANGE' );

INSERT INTO instrument_types ( name, title, nickname )
VALUES ( 'EQUITY', 'Equity', 'EQUITY' )
;

INSERT INTO instrument_types ( name, title, nickname )
VALUES ( 'CASH', 'Cash', 'CASH' )
;

INSERT INTO instrument_types ( name, title, nickname )
VALUES ( 'INDEX', 'Index', 'INDEX' )
;

INSERT INTO instrument_types ( name, title, nickname )
VALUES ( 'CURRENCIES', 'Currencies', 'CURRENCIES' )
;

