CREATE OR REPLACE FUNCTION get_products_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_product_id	int;

BEGIN

	select	id
	into	v_product_id
	from	products
	where	name	=	p_name
	;

	return	v_product_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_products_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_products_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_products_id" >>>'
	END
AS Result ;



