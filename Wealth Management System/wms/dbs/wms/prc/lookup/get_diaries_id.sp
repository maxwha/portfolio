CREATE OR REPLACE FUNCTION get_diaries_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_diaries_id	int;

BEGIN

	select	id
	into	v_diaries_id
	from	diaries
	where	name	=	p_name
	;

	return	v_diaries_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_diaries_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_diaries_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_diaries_id" >>>'
	END
AS Result ;



