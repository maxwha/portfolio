CREATE OR REPLACE FUNCTION get_users_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_user_id	int;

BEGIN

	select	id
	into	v_user_id
	from	users
	where	name	=	p_name
	;

	return	v_user_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_users_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_users_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_users_id" >>>'
	END
AS Result ;



