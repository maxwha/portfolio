CREATE OR REPLACE FUNCTION get_instrument_categories_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_instrument_category_id	int;

BEGIN

	select	id
	into	v_instrument_category_id
	from	instrument_categories
	where	name	=	p_name
	;

	return	v_instrument_category_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_instrument_categories_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_instrument_categories_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_instrument_categories_id" >>>'
	END
AS Result ;



