CREATE OR REPLACE FUNCTION get_clients_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_client_id	int;

BEGIN

	select	id
	into	v_client_id
	from	clients
	where	name	=	p_name
	;

	return	v_client_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_clients_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_clients_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_clients_id" >>>'
	END
AS Result ;



