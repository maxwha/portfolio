CREATE OR REPLACE FUNCTION get_ric_from_ticker
(	p_ticker	VARCHAR
)
RETURNS	varchar
AS
$$
DECLARE

v_ric	varchar;

BEGIN

	SELECT	case
		when	( strpos(p_ticker,'.') = 0 )
		then	p_ticker
		else	left( p_ticker, strpos(p_ticker,'.') -1 )
		end
	into	v_ric
	;

	return	v_ric;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_ric_from_ticker', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_ric_from_ticker" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_ric_from_ticker" >>>'
	END
AS Result ;



