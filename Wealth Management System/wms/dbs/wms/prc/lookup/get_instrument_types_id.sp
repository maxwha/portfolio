CREATE OR REPLACE FUNCTION get_instrument_types_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_instrument_type_id	int;

BEGIN

	select	id
	into	v_instrument_type_id
	from	instrument_types
	where	name	=	p_name
	;

	return	v_instrument_type_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_instrument_types_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_instrument_types_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_instrument_types_id" >>>'
	END
AS Result ;



