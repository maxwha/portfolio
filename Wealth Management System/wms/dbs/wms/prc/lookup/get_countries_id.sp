CREATE OR REPLACE FUNCTION get_countries_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_country_id	int;

BEGIN

	select	id
	into	v_country_id
	from	countries
	where	name	=	p_name
	;

	return	v_country_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_countries_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_countries_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_countries_id" >>>'
	END
AS Result ;



