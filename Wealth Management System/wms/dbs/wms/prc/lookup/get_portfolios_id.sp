CREATE OR REPLACE FUNCTION get_portfolios_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_portfolios_id	int;

BEGIN

	select	id
	into	v_portfolios_id
	from	portfolios
	where	name	=	p_name
	;

	return	v_portfolios_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_portfolios_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_portfolios_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_portfolios_id" >>>'
	END
AS Result ;



