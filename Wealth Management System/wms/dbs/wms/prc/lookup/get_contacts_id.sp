CREATE OR REPLACE FUNCTION get_contacts_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_contacts_id	int;

BEGIN

	select	id
	into	v_contacts_id
	from	contacts
	where	name	=	p_name
	;

	return	v_contacts_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_contacts_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_contacts_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_contacts_id" >>>'
	END
AS Result ;


