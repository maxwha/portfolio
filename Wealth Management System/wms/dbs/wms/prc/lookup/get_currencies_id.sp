CREATE OR REPLACE FUNCTION get_currencies_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_country_id	int;

BEGIN

	select	id
	into	v_country_id
	from	currencies
	where	name	=	p_name
	;

	return	v_country_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_currencies_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_currencies_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_currencies_id" >>>'
	END
AS Result ;



