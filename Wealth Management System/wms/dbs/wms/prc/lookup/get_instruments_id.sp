CREATE OR REPLACE FUNCTION get_instruments_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_instrument_id	int;

BEGIN

	select	id
	into	v_instrument_id
	from	instruments
	where	name	=	p_name
	;

	return	v_instrument_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_instruments_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_instruments_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_instruments_id" >>>'
	END
AS Result ;



