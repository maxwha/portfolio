CREATE OR REPLACE FUNCTION get_advisers_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_adviser_id	int;

BEGIN

	select	id
	into	v_adviser_id
	from	advisers
	where	name	=	p_name
	;

	return	v_adviser_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_advisers_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_advisers_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_advisers_id" >>>'
	END
AS Result ;



