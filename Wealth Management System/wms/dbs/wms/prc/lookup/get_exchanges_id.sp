CREATE OR REPLACE FUNCTION get_exchanges_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_exchange_id	int;

BEGIN

	select	id
	into	v_exchange_id
	from	exchanges
	where	name	=	p_name
	;

	return	v_exchange_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_exchanges_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_exchanges_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_exchanges_id" >>>'
	END
AS Result ;



