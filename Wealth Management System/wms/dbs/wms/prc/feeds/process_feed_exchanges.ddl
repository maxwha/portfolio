CREATE OR REPLACE FUNCTION	process_feed_exchanges
(
)
RETURNS void
AS $$

DECLARE


BEGIN

	UPDATE	exchanges	e
	SET	title		=	fe.exchange
	,	nickname	=	fe.symbol
	,	symbol		=	fe.symbol
	,	ticker_suffix	=	fe.ticker_suffix
	,	country_id	=	get_countries_id(UPPER(fe.country))
	FROM	feed_exchanges	fe
	WHERE	e.NAME		=	UPPER(fe.symbol)
	;

	INSERT	INTO exchanges
	(	name
	,	title
	,	nickname
	,	symbol
	,	ticker_suffix
	,	country_id
	)
	SELECT	UPPER(fe.symbol)
	,	fe.exchange
	,	fe.symbol
	,	fe.symbol
	,	fe.ticker_suffix
	,	get_countries_id(UPPER(fe.country))
	FROM	feed_exchanges	fe
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	exchanges	e
		where	e.id	=	get_exchanges_id(UPPER(fe.symbol))
	)
	;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_exchanges', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_exchanges" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_exchanges" >>>'
	END
AS Result ;

