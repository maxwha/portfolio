CREATE OR REPLACE FUNCTION	process_feed_index_components
(
)
RETURNS void
AS $$

DECLARE

exchange_id	int;

BEGIN

	INSERT	INTO indices
	(	name
	,	title
	,	nickname
	)
	SELECT	fic.index_name
	,	fic.index_name
	,	fic.index_name
	FROM	feed_index_components	fic
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	indices		i
		WHERE	i.id		=	get_xref_id( 'products', fic.symbol_type , fic.symbol )
		AND	product_id	=	get_indicies_id( fic.index_name )
	)
	;

/*
 *
 *	If the index components are treated as complete full downloads we remove all the existing 
 *	index components for thos indexes where are replacing
 *	
 *

		
	DELETE	FROM
		index_components	ic
	USING	feed_index_components	fic
	WHERE	ic.name			=	fic.index_name 
	;

or
	DELETE	FROM
		index_components
	WHERE	ic.name			IN
	(
		SELECT	DISTINCT
			fic.index_name
		FROM	feed_index_components	fic
	)
	;

 *
 */

--
--	Remove existing index_components
--

	DELETE	FROM
		index_components
	WHERE	ic.name			IN
	(
		SELECT	DISTINCT
			fic.index_name
		FROM	feed_index_components	fic
	)
	;


--
--	insert new index_components
--

	INSERT	INTO	index_components
	(	index_id
	,	product_id
	)
	SELECT	get_xref_id( 'products', fic.symbol_type , fic.symbol )
	,	get_indicies_id( fic.index_name )
	FROM	feed_index_components	fic
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	index_components
		WHERE	index_id	=	get_xref_id( 'products', fic.symbol_type , fic.symbol )
		AND	product_id	=	get_indicies_id( fic.index_name )
	)
	;
	

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_index_components', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_index_components" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_index_components" >>>'
	END
AS Result ;

