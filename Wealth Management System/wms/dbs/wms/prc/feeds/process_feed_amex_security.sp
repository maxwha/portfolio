CREATE OR REPLACE FUNCTION	process_feed_amex_security
(
--	P_TABLE  VARCHAR( 255 )
--,	P_COLUMN  VARCHAR( 255 )
)
RETURNS void
AS $$

DECLARE

exchange_id	int;

BEGIN

	SELECT	exchange_id = get_exchanges_id('AMEX');

	IF	( exchange_id IS NULL )
	THEN
		INSERT	INTO EXHANGES
		(	name
		,	title
		,	nickname
		,	symbol
		,	currency
		)
		VALUES
		(	'AMEX'
		,	'AMEX'
		,	'AMEX'
		,	'AMEX'
		,	'USD'
		)
		;

		select	exchange_id	= get_exchanges_id('AMEX');
	END IF
	;

	INSERT	INTO	INSTRUMENTS
	(	name
	,	title
	,	symbol
	,	nickname
	,	currency
	,	instrument_type
	)
	SELECT	fas.symbol
	,	fas.name
	,	fas.symbol
	,	fas.symbol
	,	'USD'
	,	get_instrument_types_id('EQUITY')
	FROM	feed_amex_security	fas
	WHERE	get_instruments_id(fas.symbol) IS NOT NULL
	;
	
	INSERT	INTO	PRODUCTS
	(	exchange_id
	,	instrument_id
	,	name
	,	title
	,	symbol
	,	nickname
	,	currency
	)
	SELECT	exchange_id
	,	get_instruments_id(fas.symbol)
	,	fas.symbol
	,	fas.name
	,	fas.symbol
	,	fas.symbol
	,	'USD'
	FROM	feed_amex_security	fas
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	products	p
		where	p.exchange_id	=	exchange_id
		and	p.instrument_id	=	get_instruments_id(fas.symbol)
		)
	;

--
--	Exchange Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	'exchanges'
	,	exchange_id
	,	'TICKER'
	,	'AMEX'
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'exchange'
		AND	cr.object_id		=	exchange_id
		AND	cr.xref_type		=	'TICKER'
	)
	;

--
--	Instruments Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'instruments'
	,	get_instruments_id(fas.symbol)
	,	'TICKER'
	,	fas.symbol
	FROM	feed_amex_security	fas
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'instruments'
		AND	cr.object_id		=	get_instruments_id(fas.symbol)
		AND	cr.xref_type		=	'TICKER'
	)
	;


--
--	Product Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'products'
	,	p.id
	,	'TICKER'
	,	fas.symbol
	FROM	feed_amex_security	fas
	JOIN	products		p
	ON	p.exchange_id		=	exchange_id
	AND	p.instrument_id		=	get_instruments_id(fas.symbol)
	
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'instruments'
		AND	cr.object_id		=	get_instruments_id(fas.symbol)
		AND	cr.xref_type		=	'TICKER'
	)
	;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_amex_security', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_amex_security" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_amex_security" >>>'
	END
AS Result ;

