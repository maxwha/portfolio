CREATE OR REPLACE FUNCTION	process_feed_indices
(
)
RETURNS void
AS $$

DECLARE


BEGIN

	UPDATE	indices		i
	SET	title		=	fi.index_title
	,	nickname	=	UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	FROM	feed_indices	fi
	WHERE	name		=	UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	;


	INSERT	INTO indices
	(	name
	,	title
	,	nickname
	)
	SELECT	DISTINCT
		UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	,	fi.index_title
	,	UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	FROM	feed_indices		fi
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	indices		i
		WHERE	i.name		=	UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	)
	;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_indices', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_indices" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_indices" >>>'
	END
AS Result ;

