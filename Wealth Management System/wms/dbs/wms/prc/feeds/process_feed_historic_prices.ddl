CREATE OR REPLACE FUNCTION	process_feed_historic_prices
(
)
RETURNS void
AS $$

DECLARE


BEGIN

	UPDATE	historic_prices		hp
	SET	open			=	fhp.open::numeric(36,8)
	,	high			=	fhp.high::numeric(36,8)
	,	low			=	fhp.low::numeric(36,8)
	,	close			=	fhp.close::numeric(36,8)
	,	volume			=	fhp.volume::numeric(36,8)
	,	adj_close		=	fhp.adj_close::numeric(36,8)
	FROM	feed_historic_prices	fhp
	WHERE	hp.product_id		=	fhp.product_id::int
	AND	hp.price_date		=	to_timestamp(fhp.price_date, 'YYYY-MM-DD' )
	;


	INSERT	INTO historic_prices
	(	product_id
	,	price_date
	,	open
	,	high
	,	low
	,	close
	,	volume
	,	adj_close
	)
	SELECT	fhp.product_id::int
	,	to_timestamp(fhp.price_date, 'YYYY-MM-DD' )
	,	fhp.open::numeric(36,8)
	,	fhp.high::numeric(36,8)
	,	fhp.low::numeric(36,8)
	,	fhp.close::numeric(36,8)
	,	fhp.volume::numeric(36,8)
	,	fhp.adj_close::numeric(36,8)
	FROM	feed_historic_prices		fhp
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	historic_prices		hp
		WHERE	hp.product_id		=	fhp.product_id::int
		AND	hp.price_date		=	to_timestamp(fhp.price_date, 'YYYY-MM-DD' )
	)
	;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_historic_prices', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_historic_prices" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_historic_prices" >>>'
	END
AS Result ;

