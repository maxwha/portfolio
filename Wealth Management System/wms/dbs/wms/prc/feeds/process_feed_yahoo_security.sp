CREATE OR REPLACE FUNCTION	process_feed_yahoo_security
(
--	P_TABLE  VARCHAR( 255 )
--,	P_COLUMN  VARCHAR( 255 )
)
RETURNS void
AS $$

DECLARE

exchange_id	int;

BEGIN

/*
 *
 *	indices

drop index if exists feed_yahoo_security_i1 cascade;
drop index if exists feed_yahoo_security_i2 cascade;
drop index if exists feed_yahoo_security_i3 cascade;

create index feed_yahoo_security_i1 on feed_yahoo_security ( exchange, ticker );
create index feed_yahoo_security_i2 on feed_yahoo_security ( category_code );
create index feed_yahoo_security_i3 on feed_yahoo_security ( ticker );

 *
 *
 */

	INSERT	INTO	exchanges
	(	name
	,	title
	,	nickname
	,	symbol
	)
	SELECT	DISTINCT
		fys.exchange
	,	MAX(fys.exchange)
	,	MAX(fys.exchange)
	,	MAX(fys.exchange)
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	exchanges	e
		WHERE	e.id		=	get_exchanges_id(fys.exchange)
		)
	GROUP BY
		fys.exchange
	;


	INSERT	INTO	instrument_categories
	(	name
	,	title
	)
	SELECT
		fys.category_code
	,	MAX(fys.category_name)
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	instrument_categories	ic
		WHERE	ic.name		=	fys.category_code
		)
	GROUP BY
		fys.category_code
	;


	INSERT	INTO	INSTRUMENTS
	(	name
	,	title
	,	symbol
	,	nickname
	,	instrument_type_id
	,	instrument_category_id
	)
	SELECT
		get_ric_from_ticker(fys.ticker)
	,	MAX(fys.name)
	,	MAX(get_ric_from_ticker(fys.ticker))
	,	MAX(get_ric_from_ticker(fys.ticker))
	,	MAX(get_instrument_types_id('EQUITY'))
	,	MAX(get_instrument_categories_id(fys.category_code))
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	instruments	i
		where	i.id		=	get_instruments_id(get_ric_from_ticker(fys.ticker))
		)
	GROUP BY
		get_ric_from_ticker(fys.ticker)
	;


	INSERT	INTO	PRODUCTS
	(	exchange_id
	,	instrument_id
	,	name
	,	title
	,	symbol
	)
	SELECT	get_exchanges_id(fys.exchange)
	,	get_instruments_id(get_ric_from_ticker(fys.ticker))
	,	max(fys.ticker)
	,	max(fys.name)
	,	max(fys.ticker)
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	products	p
		WHERE	p.exchange_id	=	get_exchanges_id(fys.exchange)
		AND	p.instrument_id	=	get_instruments_id(get_ric_from_ticker(fys.ticker))
		)
	GROUP BY
		get_exchanges_id(fys.exchange)
	,	get_instruments_id(get_ric_from_ticker(fys.ticker))
	;


--
--	Exchange Cross References
--
--
--	Instruments Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT
		'instruments'
	,	i.id
	,	'TICKER'
	,	i.symbol
	FROM	instruments		i
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'instruments'
		AND	cr.object_id		=	i.id
		AND	cr.xref_type		=	'TICKER'
	)
	;


--
--	Product Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT
		'products'
	,	p.id
	,	'TICKER'
	,	p.symbol
	FROM	products		p
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'products'
		AND	cr.object_id		=	p.id
		AND	cr.xref_type		=	'TICKER'
	)
	;



	INSERT	INTO	cross_references
	(	xref_value
	,	object_id
	,	object_type
	,	xref_type
	)
	SELECT	fys.ticker
	,	MAX(cr.object_id)
	,	'products'
	,	'YAHOO'
	FROM	feed_yahoo_security	fys
	JOIN	cross_references	cr
	ON	cr.object_type		=	'products'
	AND	cr.xref_type		=	'TICKER'
	AND	cr.xref_value		=	fys.ticker
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr2
		WHERE	cr2.object_type		=	'products'
		AND	cr2.object_id		=	cr.id
		AND	cr2.xref_type		=	'YAHOO'
	)
	GROUP BY
		fys.ticker
	;



END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_yahoo_security', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_yahoo_security" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_yahoo_security" >>>'
	END
AS Result ;

