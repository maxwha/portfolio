ALTER TABLE portfolios
DROP	CONSTRAINT IF EXISTS portfolios_uk
;

ALTER TABLE portfolios
ADD	CONSTRAINT portfolios_uk UNIQUE (client_id,title)
;

