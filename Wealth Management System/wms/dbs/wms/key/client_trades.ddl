ALTER TABLE client_trades
DROP	CONSTRAINT IF EXISTS client_trades_uk
;

ALTER TABLE client_trades
ADD	CONSTRAINT client_trades_uk UNIQUE (portfolio_id, product_id, id)
;

