ALTER TABLE instruments
DROP	CONSTRAINT IF EXISTS instruments_uk
;

ALTER TABLE instruments
ADD	CONSTRAINT instruments_uk UNIQUE (name, instrument_type_id)
;

