ALTER TABLE products
DROP	CONSTRAINT IF EXISTS products_uk
;

ALTER TABLE products
DROP	CONSTRAINT IF EXISTS products_uk1
;

ALTER TABLE products
ADD	CONSTRAINT products_uk UNIQUE (name, exchange_id)
;

ALTER TABLE products
ADD	CONSTRAINT products_uk1 UNIQUE (instrument_id, exchange_id)
;

