--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.4.0
-- Started on 2015-04-02 20:26:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 3316 (class 1262 OID 16394)
-- Name: wms; Type: DATABASE; Schema: -; Owner: fuxiadmin
--

CREATE DATABASE wms WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE wms OWNER TO fuxiadmin;

\connect wms

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 259 (class 3079 OID 12617)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3319 (class 0 OID 0)
-- Dependencies: 259
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 641 (class 1247 OID 27279)
-- Name: type; Type: TYPE; Schema: public; Owner: fuxiadmin
--

CREATE TYPE type AS ENUM (
    'Call',
    'In-person'
);


ALTER TYPE type OWNER TO fuxiadmin;

--
-- TOC entry 296 (class 1255 OID 16397)
-- Name: column_exists(character varying, character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION column_exists(p_table character varying, p_column character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

DECLARE

V_RETURN_VALUE	boolean		:=	FALSE;

V_TABLE		VARCHAR(255);
V_COLUMN	VARCHAR(255);

V_DOT		integer;
V_DOT2		integer;

V_PART_NAME	VARCHAR(255);

V_DATABASE_NAME	VARCHAR(255);
V_SCHEMA_NAME	VARCHAR(255);
V_TABLE_NAME	VARCHAR(255);


BEGIN
	V_TABLE		:= LOWER(P_TABLE);
	V_COLUMN	:= LOWER(P_COLUMN);
--	V_DOT		:= INSTR(P_TABLE,'.');
	V_DOT		:= position('.' in P_TABLE);

	IF	( V_DOT > 1 )
	THEN
		V_PART_NAME	:= substring(V_TABLE from (V_DOT+1) );
		V_DOT2		:= position('.' in V_PART_NAME );
		IF	( V_DOT2 > 0 )
		THEN
			V_DATABASE_NAME		= SUBSTRING(V_TABLE from 1 for (V_DOT-1) );
			IF	( V_DOT2 > 1 )
			THEN
				V_SCHEMA_NAME	= SUBSTRING(V_PART_NAME from 1 for (V_DOT2-1) );
			ELSE
				V_SCHEMA_NAME	= current_schema();
			END IF;
			V_TABLE_NAME		= SUBSTRING(V_PART_NAME from (V_DOT2+1) );
		ELSE
			V_DATABASE_NAME		= current_database();
			V_SCHEMA_NAME		= SUBSTRING(V_TABLE from 1 for (V_DOT-1) );
			V_TABLE_NAME		= SUBSTRING(V_TABLE from (V_DOT+1) );
		END IF;
	ELSE
		V_DATABASE_NAME	= current_database();
		V_SCHEMA_NAME	= current_schema();
		V_TABLE_NAME	= SUBSTRING(V_TABLE from (V_DOT+1) );
	END IF;

-- raise NOTICE ' P_TABLE=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_TABLE_NAME=%', P_TABLE, V_DATABASE_NAME, V_SCHEMA_NAME, V_TABLE_NAME;

	IF	EXISTS
	(
		SELECT	NULL
		FROM	information_schema.columns
		WHERE	table_catalog		= V_DATABASE_NAME
		AND	table_schema		= V_SCHEMA_NAME
		AND	table_name		= V_TABLE_NAME
		AND	column_name		= V_COLUMN
	)
	THEN
		V_RETURN_VALUE		=	TRUE;
	ELSE
		V_RETURN_VALUE		=	FALSE;
	END IF;

	RETURN 	COALESCE( V_RETURN_VALUE, FALSE );

END
$$;


ALTER FUNCTION public.column_exists(p_table character varying, p_column character varying) OWNER TO fuxiadmin;

--
-- TOC entry 293 (class 1255 OID 26726)
-- Name: generate_flask_model(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION generate_flask_model(p_table_name character varying) RETURNS TABLE(script_line character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE

r		RECORD;
t		RECORD;
columns		varchar;
list		varchar;
prefix		varchar;
py_tab		varchar(4);
py_quote	varchar(1);

v_database_name	varchar;
v_schema_name	varchar;
v_object_name	varchar;
v_table_name	varchar;
v_table_title	varchar;

num_rows	int;
column_count	int;

v_date_format	varchar;

BEGIN
	py_tab		=	'    ';
	py_quote	=	'''';
	v_date_format	=	'\%Y-\%m-\%d';

	select	database_name
	,	schema_name
	,	object_name
	into	v_database_name
	,	v_schema_name
	,	v_object_name
	from	parse_object_name( p_table_name )
	;

-- raise NOTICE ' P_TABLE_NAME=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_OBJECT_NAME=%', P_TABLE_NAME, V_DATABASE_NAME, V_SCHEMA_NAME, V_object_NAME;

	drop  table if exists table_list
	;
		
	create	temporary table table_list
	(	id			serial
	,	table_catalog		varchar
	,	table_schema		varchar
	,	table_name		varchar
	)
	;

	insert	into
		table_list
	(	table_catalog
	,	table_schema
	,	table_name
	)
	select	distinct
		st.table_catalog
	,	st.table_schema
	,	st.table_name
	FROM	information_schema.tables	st
	WHERE	st.table_name			not like 'ab_%'
	AND	st.table_name			not like 'feed_%'
	AND	st.table_name			not like 'message%'
	AND	st.table_name			not like 'tree%'
	AND	st.table_name			not like '%tag%'
	AND	st.table_catalog		=	V_DATABASE_NAME
	AND	st.table_schema			=	V_SCHEMA_NAME
--	AND	st.table_type			in	( 'BASE TABLE', 'VIEW' )
	AND	st.table_type			in	( 'BASE TABLE' )
	AND	st.table_name			like	V_OBJECT_NAME
	;


	drop  table if exists relations
	;

	create	temporary table relations
	(	id			serial
	,	table_name		varchar
	,	related_table_name	varchar
	,	relationship_name	varchar
	)
	;

	insert	into
		relations
	(	table_name
	,	related_table_name
	,	relationship_name
	)
	select	DISTINCT
		st.table_name				AS	"table_name"
	,	rel.related_table_name			AS	"related_table_name"
	,	rel.relationship_name			AS	"relationship_name"
	FROM	table_list				st
	JOIN	relationships				rel
	ON	rel.table_catalog			=	st.table_catalog
	AND	rel.table_schema			=	st.table_schema
	AND	rel.table_name				=	st.table_name
	AND	EXISTS
	(
		SELECT	NULL
		FROM	table_list			rtl
		WHERE	rtl.table_catalog		=	rel.related_table_catalog
		AND	rtl.table_schema		=	rel.related_table_schema
		AND	rtl.table_name			=	rel.related_table_name
	)
	;


	drop table if exists table_order
	;
		
	create	temporary table table_order
		(	id			serial
		,	table_name		varchar
		)
	;

-- Add the tables with no child relationships first

	insert	into
		table_order
	(	table_name )
	select	distinct
		st.table_name
	FROM	table_list			st
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	relations		rel
		WHERE	rel.table_name		=	st.table_name
	)
	;

-- then add the others chunk at a time
--
-- TODO: Had a problem with two tables with a 2-way relationship ( not M-M ) , have to consider this.
--	Got rid of one of the relationships for now.
--

	LOOP
		insert	into
		table_order
		(	table_name )
		select	distinct
			r1.table_name
			-- select *
		from	relations		r1
		WHERE	not exists
		(	select	null
			from	table_order	t1
			where	t1.table_name	= r1.table_name
		)
		and	not exists
		(
			select	null
			from	relations 		r2
			where	r2.table_name		= r1.table_name
			AND	r2.table_name		!= r2.related_table_name
			and	not exists
			(
				select	null
				from	table_order	t2
				where	t2.table_name	= r2.related_table_name
			)
		)
		;

		GET DIAGNOSTICS num_rows = ROW_COUNT;

		exit when num_rows < 1;

	END LOOP;

-- Start the script

	if	object_exists ( 'script', 'TABLE' )
	then
		drop table if exists script;
	end if;

	create	local temp table script
	(	id	serial
	,	line	varchar
	)
	;

	insert into script (line) values ( format( 'import datetime' ));
	insert into script (line) values ( format( '#from sqlalchemy import Table, Column, Integer, String, ForeignKey, Date, Text' ));
	insert into script (line) values ( format( 'from sqlalchemy import BigInteger, Boolean, Column, Date, DateTime, ForeignKey, Index, Integer, Numeric, String, Table, Text, text' ));
	insert into script (line) values ( format( 'from sqlalchemy.orm import relationship' ));
	insert into script (line) values ( format( 'from flask.ext.appbuilder import Model' ));
	insert into script (line) values ( format( 'from flask.ext.appbuilder.models.mixins import AuditMixin, FileColumn, ImageColumn' ));
	insert into script (line) values ( format( '' ));
	insert into script (line) values ( format( '# this seems to be used for views support' ));
	insert into script (line) values ( format( 'from sqlalchemy.ext.declarative import declarative_base' ));
	insert into script (line) values ( format( 'Base = declarative_base()' ));
	insert into script (line) values ( format( 'metadata = Base.metadata' ));
	insert into script (line) values ( format( '' ));

	insert into script (line) values ( format( 'def today():' ));
	insert into script (line) values ( format( '    return datetime.datetime.today().strftime(''%s'')', v_date_format ));
	insert into script (line) values ( format( '' ));


	FOR	t in
		SELECT	o.table_name			AS	"table_name"
		,	FORMAT_TITLE_CASE(o.table_name)	AS	"table_title"
		FROM	table_order			o
		order by
			id
	LOOP

	v_table_name	=	t.table_name;
	v_table_title	=	t.table_title;


	insert into script (line) values ( format( 'class %s(Model):', v_table_title ));
	insert into script (line) values ( format( '    __tablename__ = ''%s''', v_table_name ));
	insert into script (line) values ( format( '' ));

	FOR 	r	IN 
		SELECT	FORMAT_FLASK_COLUMN(c.table_catalog,c.table_schema,c.table_name,c.column_name)	AS	"flask_column"
		FROM	information_schema.columns	c
		WHERE	c.TABLE_CATALOG			= v_database_name
		AND	c.TABLE_SCHEMA			= v_schema_name
		AND	c.TABLE_NAME			= v_table_name
		AND	c.column_name not in ( 'owner_id' , 'version', 'create_user', 'create_date', 'update_user', 'update_date', 'pid' )
		ORDER BY c.ORDINAL_POSITION
	LOOP

	insert into script (line) values ( format( '    %s ', r.flask_column ));

	END LOOP;

	insert into script (line) values ( format( '' ));
--
--	TODO: Change this to handle multiple column FKs
--		CLUE: use the FK SQL in FORMAT_FLASK_COLUMN()
--

	FOR	r	IN 
		SELECT	rel.relationship_name				AS	"relationship_name"
		,	FORMAT_TITLE_CASE(rel.related_table_name)	AS	"related_table_title"
		FROM	relations					rel
		WHERE	rel.table_name					=	v_table_name
		AND	COALESCE(rel.relationship_name,'')		!=	''
	LOOP

	insert into script (line) values ( format( '    %s = relationship("%s") ', r.relationship_name, r.related_table_title ));

	END LOOP;

	insert into script (line) values ( format( '' ));

	if	exists
	(
		SELECT	NULL
		FROM	information_schema.columns	c
		WHERE	c.TABLE_CATALOG			= v_database_name
		AND	c.TABLE_SCHEMA			= v_schema_name
		AND	c.TABLE_NAME			= v_table_name
		AND	c.COLUMN_NAME			= 'name'
	)
	THEN

	insert into script (line) values ( format( '    def __repr__(self):' ));
	insert into script (line) values ( format( '        return self.name' ));
	insert into script (line) values ( format( '' ));

	END IF;

	END LOOP;

	insert into script (line) values ( format( '' ));

	drop table if exists table_list;
	drop table if exists relations;
	drop table if exists table_order;
		
	return	query
	select	line
	from	script
	order by
		id
	;

END
$$;


ALTER FUNCTION public.generate_flask_model(p_table_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 297 (class 1255 OID 26728)
-- Name: generate_flask_view(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION generate_flask_view(p_table_name character varying) RETURNS TABLE(script_line character varying)
    LANGUAGE plpgsql
    AS $$

DECLARE

r		RECORD;
t		RECORD;
columns		varchar;
list		varchar;
prefix		varchar;
py_tab		varchar(4);
py_quote	varchar(1);

v_database_name	varchar;
v_schema_name	varchar;
v_object_name	varchar;
v_table_name	varchar;
v_table_title	varchar;

num_rows	int;
column_count	int;

BEGIN
	py_tab		=	'    ';
	py_quote	=	'''';

	select	database_name
	,	schema_name
	,	object_name
	into	v_database_name
	,	v_schema_name
	,	v_object_name
	from	parse_object_name( p_table_name )
	;

-- raise NOTICE ' P_TABLE_NAME=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_OBJECT_NAME=%', P_TABLE_NAME, V_DATABASE_NAME, V_SCHEMA_NAME, V_object_NAME;

	drop  table if exists table_list
	;
		
	create	temporary table table_list
	(	id			serial
	,	table_catalog		varchar
	,	table_schema		varchar
	,	table_name		varchar
	)
	;

	insert	into
		table_list
	(	table_catalog
	,	table_schema
	,	table_name
	)
	select	distinct
		st.table_catalog
	,	st.table_schema
	,	st.table_name
	FROM	information_schema.tables	st
	WHERE	st.table_name			not like 'ab_%'
	AND	st.table_name			not like 'feed_%'
	AND	st.table_name			not like 'message%'
	AND	st.table_name			not like 'tree%'
	AND	st.table_name			not like '%tag%'
	AND	st.table_catalog		=	V_DATABASE_NAME
	AND	st.table_schema			=	V_SCHEMA_NAME
--	AND	st.table_type			in	( 'BASE TABLE', 'VIEW' )
	AND	st.table_type			in	( 'BASE TABLE' )
	AND	st.table_name			like	V_OBJECT_NAME
	;


	drop  table if exists relations
	;
		
	create	temporary table relations
	(	id			serial
	,	table_name		varchar
	,	related_table_name	varchar
	,	relationship_name	varchar
	)
	;

	insert	into
		relations
	(	table_name
	,	related_table_name
	,	relationship_name
	)
	select	DISTINCT
		st.table_name				AS	"table_name"
	,	rel.related_table_name			AS	"related_table_name"
	,	rel.relationship_name			AS	"relationship_name"
	FROM	table_list				st
	JOIN	relationships				rel
	ON	rel.table_catalog			=	st.table_catalog
	AND	rel.table_schema			=	st.table_schema
	AND	rel.table_name				=	st.table_name
	AND	EXISTS
	(
		SELECT	NULL
		FROM	table_list			rtl
		WHERE	rtl.table_catalog		=	rel.related_table_catalog
		AND	rtl.table_schema		=	rel.related_table_schema
		AND	rtl.table_name			=	rel.related_table_name
	)
	;

	drop table if exists table_order
	;
		
	create	temporary table table_order
		(	id			serial
		,	table_name		varchar
		)
	;

-- Add the tables with no child relationships first

	insert	into
		table_order
	(	table_name )
	select	distinct
		st.table_name
	FROM	table_list			st
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	relations		rel
		WHERE	rel.table_name		=	st.table_name
	)
	;

-- then add the others chunk at a time
--
-- TODO: Had a problem with two tables with a 2-way relationship ( not M-M ) , have to consider this.
--	Got rid of one of the relationships for now.
--

	LOOP
		insert	into
		table_order
		(	table_name )
		select	distinct
			r1.table_name
			-- select *
		from	relations		r1
		WHERE	r1.related_table_name	IS NOT NULL
		AND	not exists
		(	select	null
			from	table_order	t1
			where	t1.table_name	= r1.table_name
		)
		and	not exists
		(
			select	null
			from	relations 		r2
			where	r2.table_name		= r1.table_name
			AND	r2.table_name		!= r2.related_table_name
			and	not exists
			(
				select	null
				from	table_order	t2
				where	t2.table_name	= r2.related_table_name
			)
		)
		;

		GET DIAGNOSTICS num_rows = ROW_COUNT;

		exit when num_rows < 1;

	END LOOP;

-- Start the script

	if	object_exists ( 'script', 'TABLE' )
	then
		drop table if exists script;
	end if;

	create	local temp table script
	(	id	serial
	,	line	varchar
	)
	;

	insert into script (line) values ( format( 'from flask_appbuilder import ModelView' ));
	insert into script (line) values ( format( 'from flask_appbuilder.fieldwidgets import Select2Widget' ));
	insert into script (line) values ( format( 'from flask.ext.appbuilder.models.sqla.interface import SQLAInterface' ));
	insert into script (line) values ( format( '#from flask.ext.appbuilder.models.datamodel import SQLAModel' ));
	insert into script (line) values ( format( 'from wtforms.ext.sqlalchemy.fields import QuerySelectField' ));
	insert into script (line) values ( format( 'from app import appbuilder, db' ));
	insert into script (line) values ( format( '' ));
	insert into script (line) values ( format( 'from flask.ext.appbuilder.views import CompactCRUDMixin' ));
	insert into script (line) values ( format( '' ));
	insert into script (line) values ( format( 'from .models import *' ));
	insert into script (line) values ( format( '' ));

	FOR	t in
		SELECT	o.table_name			AS	"table_name"
		,	FORMAT_TITLE_CASE(o.table_name)	AS	"table_title"
		FROM	table_order			o
		order by
			id
	LOOP

	v_table_name	=	t.table_name;
	v_table_title	=	t.table_title;

	select	count(*)
	INTO	column_count
	from	information_schema.columns	c
	where	c.table_catalog		= V_DATABASE_NAME
	AND	c.table_schema		= V_SCHEMA_NAME
	AND	c.table_name		= v_table_name
	;

--	IF	( exists ( select null from relations where related_table_name = v_table_name ) )
--	THEN
--		insert into script (line) values ( format( 'class %sView(CompactCRUDMixin, ModelView):', v_table_title ));
--	ELSE
--		insert into script (line) values ( format( 'class %sView(ModelView):', v_table_title ));
--	END IF;

	insert into script (line) values ( format( 'class %sView(ModelView):', v_table_title ));
	insert into script (line) values ( format( '    datamodel = SQLAInterface(%s)', v_table_title ));
	insert into script (line) values ( format( '' ));


	list =	'';
	prefix	=	'[ ';

-- put the foreign keys at the top

	FOR	r	IN 
		SELECT	rel.relationship_name		AS	"column_name"
		FROM	information_schema.columns	c
		JOIN
			relationships			rel
		ON	rel.table_catalog		=	c.table_catalog
		AND	rel.table_schema		=	c.table_schema
		AND	rel.table_name			=	c.table_name
		AND	rel.column_name			=	c.column_name
		WHERE	c.table_catalog			=	v_database_name
		AND	c.table_schema			=	v_schema_name
		AND	c.table_name			=	v_table_name
		AND	c.column_name			not in ( 'id', 'owner_id', 'version', 'create_user', 'create_date', 'update_user', 'update_date', 'pid' )
		ORDER BY ORDINAL_POSITION
	LOOP
		list	=	list || prefix || py_quote || r.column_name || py_quote;
		prefix  =	', ';
	END LOOP;

-- now the rest of the columns

	FOR	r	IN 
		SELECT	c.column_name			AS	"column_name"
		FROM	information_schema.columns	c
		WHERE	c.table_catalog			= v_database_name
		AND	c.table_schema			= v_schema_name
		AND	c.table_name			= v_table_name
		AND	c.column_name			not in ( 'id', 'owner_id', 'version', 'create_user', 'create_date', 'update_user', 'update_date', 'pid' )
		AND	NOT EXISTS
		(
			SELECT	NULL
			FROM	relationships		rel
			WHERE	rel.table_catalog	=	c.table_catalog
			AND	rel.table_schema	=	c.table_schema
			AND	rel.table_name		=	c.table_name
			AND	rel.column_name		=	c.column_name
		)
		ORDER BY ORDINAL_POSITION
	LOOP
		list	=	list || prefix || py_quote || r.column_name || py_quote;
		prefix  =	', ';
	END LOOP;

	IF	( list != '' )
	THEN
		list	=	list || ' ];';
	insert into script (line) values ( format( '    list_columns = %s ', list ));
	insert into script (line) values ( format( '    show_columns = %s ', list ));
	insert into script (line) values ( format( '    edit_columns = %s ', list ));
	insert into script (line) values ( format( '    add_columns = %s ', list ));
	insert into script (line) values ( format( '' ));
	END IF;


	list =	'';
	prefix	=	'[ ';

	FOR	r	IN 
		SELECT	FORMAT_TITLE_CASE( rel.related_table_name )
							AS	"related_table_name"
		FROM	relations			rel
		WHERE	rel.table_name			=	v_table_name
		AND	rel.related_table_name		!=	v_table_name
	LOOP
		list	=	list || prefix || r.related_table_name || 'View ';
		prefix  =	', ';
	END LOOP;


	IF	( list != '' )
	THEN
		list	=	list || ' ];';
	insert into script (line) values ( format( '    related_views = %s ', list ));
	insert into script (line) values ( format( '' ));

	END IF;



	insert into script (line) values ( format( '#    show_template = ''appbuilder/general/model/show_cascade.html'' '));
	insert into script (line) values ( format( '#    edit_template = ''appbuilder/general/model/edit_cascade.html'' '));
	insert into script (line) values ( format( '' ));


	insert into script (line) values ( format( '' ));

	END LOOP;

/*
	FOR	t in
		SELECT	o.table_name			AS	"table_name"
		,	FORMAT_TITLE_CASE(o.table_name)	AS	"table_title"
		FROM	table_order			o
		order by
			id
	LOOP
	insert into script (line) values ( format( 'appbuilder.add_view_no_menu(%sView)', t.table_title ));
	END LOOP;
*/

	insert into script (line) values ( format( '' ));

	drop table if exists table_list;
	drop table if exists relations;
	drop table if exists table_order;

	return	query
	select	line
	from	script
	order by
		id
	;

END
$$;


ALTER FUNCTION public.generate_flask_view(p_table_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 273 (class 1255 OID 17617)
-- Name: get_advisers_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_advisers_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_adviser_id	int;

BEGIN

	select	id
	into	v_adviser_id
	from	advisers
	where	name	=	p_name
	;

	return	v_adviser_id;
END
$$;


ALTER FUNCTION public.get_advisers_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 276 (class 1255 OID 17618)
-- Name: get_clients_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_clients_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_client_id	int;

BEGIN

	select	id
	into	v_client_id
	from	clients
	where	name	=	p_name
	;

	return	v_client_id;
END
$$;


ALTER FUNCTION public.get_clients_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 287 (class 1255 OID 21875)
-- Name: get_contacts_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_contacts_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_contacts_id	int;

BEGIN

	select	id
	into	v_contacts_id
	from	contacts
	where	name	=	p_name
	;

	return	v_contacts_id;
END
$$;


ALTER FUNCTION public.get_contacts_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 266 (class 1255 OID 26872)
-- Name: get_countries_by_nickname(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_countries_by_nickname(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_country_id	int;

BEGIN

	select	id
	into	v_country_id
	from	countries
	where	nickname	=	p_name
	;

	return	v_country_id;
END
$$;


ALTER FUNCTION public.get_countries_by_nickname(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 277 (class 1255 OID 17619)
-- Name: get_countries_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_countries_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_country_id	int;

BEGIN

	select	id
	into	v_country_id
	from	countries
	where	name	=	p_name
	;

	return	v_country_id;
END
$$;


ALTER FUNCTION public.get_countries_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 288 (class 1255 OID 26873)
-- Name: get_currencies_by_nickname(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_currencies_by_nickname(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_country_id	int;

BEGIN

	select	id
	into	v_country_id
	from	currencies
	where	nickname	=	p_name
	;

	return	v_country_id;
END
$$;


ALTER FUNCTION public.get_currencies_by_nickname(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 278 (class 1255 OID 17620)
-- Name: get_currencies_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_currencies_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_country_id	int;

BEGIN

	select	id
	into	v_country_id
	from	currencies
	where	name	=	p_name
	;

	return	v_country_id;
END
$$;


ALTER FUNCTION public.get_currencies_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 294 (class 1255 OID 21876)
-- Name: get_diaries_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_diaries_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_diaries_id	int;

BEGIN

	select	id
	into	v_diaries_id
	from	diaries
	where	name	=	p_name
	;

	return	v_diaries_id;
END
$$;


ALTER FUNCTION public.get_diaries_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 279 (class 1255 OID 17621)
-- Name: get_exchanges_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_exchanges_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_exchange_id	int;

BEGIN

	select	id
	into	v_exchange_id
	from	exchanges
	where	name	=	p_name
	;

	return	v_exchange_id;
END
$$;


ALTER FUNCTION public.get_exchanges_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 275 (class 1255 OID 17623)
-- Name: get_instrument_categories_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_instrument_categories_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_instrument_category_id	int;

BEGIN

	select	id
	into	v_instrument_category_id
	from	instrument_categories
	where	name	=	p_name
	;

	return	v_instrument_category_id;
END
$$;


ALTER FUNCTION public.get_instrument_categories_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 281 (class 1255 OID 17624)
-- Name: get_instrument_types_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_instrument_types_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_instrument_type_id	int;

BEGIN

	select	id
	into	v_instrument_type_id
	from	instrument_types
	where	name	=	p_name
	;

	return	v_instrument_type_id;
END
$$;


ALTER FUNCTION public.get_instrument_types_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 280 (class 1255 OID 17622)
-- Name: get_instruments_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_instruments_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_instrument_id	int;

BEGIN

	select	id
	into	v_instrument_id
	from	instruments
	where	name	=	p_name
	;

	return	v_instrument_id;
END
$$;


ALTER FUNCTION public.get_instruments_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 289 (class 1255 OID 21877)
-- Name: get_portfolios_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_portfolios_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_portfolio_id	int;

BEGIN

	select	id
	into	v_portfolio_id
	from	portfolios
	where	name	=	p_name
	;

	return	v_portfolio_id;
END
$$;


ALTER FUNCTION public.get_portfolios_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 282 (class 1255 OID 17625)
-- Name: get_products_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_products_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_product_id	int;

BEGIN

	select	id
	into	v_product_id
	from	products
	where	name	=	p_name
	;

	return	v_product_id;
END
$$;


ALTER FUNCTION public.get_products_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 283 (class 1255 OID 17626)
-- Name: get_ric_from_ticker(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_ric_from_ticker(p_ticker character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE

v_ric	varchar;

BEGIN

	SELECT	case
		when	( strpos(p_ticker,'.') = 0 )
		then	p_ticker
		else	left( p_ticker, strpos(p_ticker,'.') -1 )
		end
	into	v_ric
	;

	return	v_ric;
END
$$;


ALTER FUNCTION public.get_ric_from_ticker(p_ticker character varying) OWNER TO fuxiadmin;

--
-- TOC entry 284 (class 1255 OID 17627)
-- Name: get_users_id(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION get_users_id(p_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

v_user_id	int;

BEGIN

	select	id
	into	v_user_id
	from	users
	where	name	=	p_name
	;

	return	v_user_id;
END
$$;


ALTER FUNCTION public.get_users_id(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 290 (class 1255 OID 21464)
-- Name: log(character varying, character varying, character varying, integer, timestamp without time zone, character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION log(p_message character varying, p_message_severity character varying DEFAULT 'INFO'::character varying, p_message_source character varying DEFAULT NULL::character varying, p_message_depth integer DEFAULT NULL::integer, p_message_date timestamp without time zone DEFAULT NULL::timestamp without time zone, p_message_user character varying DEFAULT NULL::character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$

DECLARE

RS			INT	:=	0;

V_MESSAGE_SEVERITY	INT;
V_DEBUG_MASK		INT;
V_DEBUG_STDOUT		VARCHAR(1);

V_DT			VARCHAR(30);
V_PREFIX		VARCHAR(255);

MESSAGE			VARCHAR(8000);

V_STD_LEN		int;
V_MAX_LEN		int;
V_PFX_LEN		int;

BEGIN

SELECT	V_MESSAGE_SEVERITY	=	0
,	V_DEBUG_MASK		=	0
,	V_DEBUG_STDOUT		=	'Y'
,	V_STD_LEN		=	50;

select	

select	V_MESSAGE_SEVERITY = to_number( COALESCE(value,'0') , '99999999999999' )
from	GLOBALS
where	code = 'log_' + TO_LOWER(P_MESSAGE_SEVERITY)
;

select	V_DEBUG_MASK = to_number( COALESCE(value,'0'), '999999999999' )
from	GLOBALS
where	code = 'log_mask'
;


select	V_DEBUG_STDOUT = TO_UPPER(SUBSTRING( LTRIM(RTRIM(isnull(value,''))), 1, 1 ))
from	GLOBALS
where	code = 'log_stdout'
;

--	SET	G_RS	=	SET_GLOBALS();

	IF	( ( V_DEBUG_MASK & V_MESSAGE_SEVERITY ) > 0 )
	BEGIN
		INSERT
		INTO	LOGS
		(	MESSAGE
		,	MESSAGE_SOURCE
		,	MESSAGE_DEPTH
		,	MESSAGE_SEVERITY
		,	MESSAGE_DATE
		,	MESSAGE_USER
		,	LOG_DATE
		,	USER_ID
		,	USER_SPID
		)
		VALUES
		(	P_MESSAGE
		,	P_MESSAGE_SOURCE
		,	COALESCE(P_MESSAGE_DEPTH, pg_trigger_depth() - 1 )
		,	COALESCE(P_MESSAGE_SEVERITY, 'INFO' )
		,	COALESCE(P_MESSAGE_DATE, current_timestamp )
		,	COALESCE(P_MESSAGE_USER, user )
		,	GETDATE()
		,	session_user
		,	pg_backend_pid()
		)
		;
--		SELECT	RS	=	@ERROR;

/*
 * Process ETL Wrapper process can't handle writing to system out, so remove until this can happen
 *
		IF	(( RS = 0 )
		AND	 ( V_DEBUG_STDOUT = 'Y' )
			)
		BEGIN
			SELECT	V_PFX_LEN	=	4 + COALESCE(P_MESSAGE_DEPTH,0) + LEN(COALESCE( LTRIM(RTRIM(P_MESSAGE_SOURCE)), '' ));
			SELECT	V_MAX_LEN	=	CASE
								WHEN	( V_PFX_LEN > V_STD_LEN )
								THEN	V_PFX_LEN
								ELSE	V_STD_LEN
							END;

			SELECT	V_DT	=	convert( varchar, COALESCE( P_MESSAGE_DATE, getdate()), 9 );
			SELECT	V_PREFIX	=	CONVERT( VARCHAR(3), COALESCE(P_MESSAGE_DEPTH,0) )
					+	SPACE( COALESCE(P_MESSAGE_DEPTH,0) + 1 )
					+	COALESCE( P_MESSAGE_SOURCE, '' )
					+	SPACE( V_MAX_LEN - ( COALESCE(P_MESSAGE_DEPTH,0) + LEN( COALESCE(P_MESSAGE_SOURCE,'') ) ));
			
			SELECT	MESSAGE =	'' + V_DT + ' : ' + V_PREFIX + ' : ' + COALESCE(P_MESSAGE,'') ; print MESSAGE;
			PRINT	MESSAGE;
		END;
 */

	END

	RETURN	RS;


END
$$;


ALTER FUNCTION public.log(p_message character varying, p_message_severity character varying, p_message_source character varying, p_message_depth integer, p_message_date timestamp without time zone, p_message_user character varying) OWNER TO fuxiadmin;

--
-- TOC entry 298 (class 1255 OID 16396)
-- Name: object_exists(character varying, character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION object_exists(p_name character varying, p_type character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$


DECLARE

V_RETURN_VALUE	Boolean		:= FALSE;

V_NAME		VARCHAR(255);
V_TYPE		VARCHAR(255);

V_DOT		integer;
V_DOT2		integer;

V_PART_NAME	VARCHAR(255);
V_DATABASE_NAME	VARCHAR(255);
V_SCHEMA_NAME	VARCHAR(255);
V_OBJECT_NAME	VARCHAR(255);

BEGIN

	SELECT	database_name
	,	schema_name
	,	object_name
	INTO	V_DATABASE_NAME
	,	V_SCHEMA_NAME
	,	V_OBJECT_NAME
	FROM	PARSE_OBJECT_NAME( LOWER( P_NAME ))
	;

-- raise NOTICE ' P_NAME=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_OBJECT_NAME=%', P_NAME, V_DATABASE_NAME, V_SCHEMA_NAME, V_OBJECT_NAME;

	V_TYPE		= COALESCE( LOWER(P_TYPE), 'routine' );

	IF	( V_TYPE = 'table' )
	THEN
		SELECT	TRUE
		INTO	V_RETURN_VALUE
		FROM	information_schema.tables
		WHERE	( table_catalog		= V_DATABASE_NAME
		AND	table_schema		= V_SCHEMA_NAME
		AND	table_name		= V_OBJECT_NAME
		AND	table_type		= 'BASE TABLE'
			)
		OR	( table_catalog		= V_DATABASE_NAME
		AND	table_name		= V_OBJECT_NAME
		AND	table_type		= 'LOCAL TEMPORARY'
			)
		;
	ELSEIF	( V_TYPE = 'view' )
	THEN
		SELECT	TRUE
		INTO	V_RETURN_VALUE
		FROM	information_schema.views
		WHERE	table_catalog		= V_DATABASE_NAME
		AND	table_schema		= V_SCHEMA_NAME
		AND	table_name		= V_OBJECT_NAME
		;
	ELSEIF	( V_TYPE = 'routine' )
	THEN
		SELECT	TRUE
		INTO	V_RETURN_VALUE
		FROM	information_schema.routines
		WHERE	routine_catalog		= V_DATABASE_NAME
		AND	routine_schema		= V_SCHEMA_NAME
		AND	routine_name		= V_OBJECT_NAME
		AND	routine_type		= 'FUNCTION'
		;
	ELSE 
		SELECT	FALSE
		INTO	V_RETURN_VALUE
		;
	END IF;

	RETURN 	COALESCE( V_RETURN_VALUE, FALSE);

END
$$;


ALTER FUNCTION public.object_exists(p_name character varying, p_type character varying) OWNER TO fuxiadmin;

--
-- TOC entry 292 (class 1255 OID 23246)
-- Name: parse_constraint_name(character varying, character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION parse_constraint_name(p_table_name character varying, p_constraint_name character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$


DECLARE

V_STRING	VARCHAR;

BEGIN
	V_STRING	=	replace(P_CONSTRAINT_NAME, P_TABLE_NAME||'_', '');
	V_STRING	=	replace(V_STRING, '_fk', '');
	V_STRING	=	replace(V_STRING, '_id', '');

	RETURN	V_STRING
	;

END
$$;


ALTER FUNCTION public.parse_constraint_name(p_table_name character varying, p_constraint_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 295 (class 1255 OID 25455)
-- Name: parse_object_name(character varying); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION parse_object_name(p_name character varying) RETURNS TABLE(database_name character varying, schema_name character varying, object_name character varying)
    LANGUAGE plpgsql
    AS $$


DECLARE

V_RETURN	RECORD;

V_NAME		VARCHAR(255);
V_TYPE		VARCHAR(255);

V_DOT		integer;
V_DOT2		integer;

V_PART_NAME	VARCHAR(255);
V_DATABASE_NAME	VARCHAR(255);
V_SCHEMA_NAME	VARCHAR(255);
V_OBJECT_NAME	VARCHAR(255);

BEGIN
	V_NAME		:= LOWER(P_NAME);
--	V_DOT		:= INSTR(P_NAME,'.');
	V_DOT		:= position('.' in P_NAME);

	IF	( V_DOT > 1 )
	THEN
		V_PART_NAME	:= substring(V_NAME from (V_DOT+1) );
		V_DOT2		:= position('.' in V_PART_NAME );
		IF	( V_DOT2 > 0 )
		THEN
			V_DATABASE_NAME		= SUBSTRING(V_NAME from 1 for (V_DOT-1) );
			IF	( V_DOT2 > 1 )
			THEN
				V_SCHEMA_NAME	= SUBSTRING(V_PART_NAME from 1 for (V_DOT2-1) );
			ELSE
				V_SCHEMA_NAME	= current_schema();
			END IF;
			V_OBJECT_NAME		= SUBSTRING(V_PART_NAME from (V_DOT2+1) );
		ELSE
			V_DATABASE_NAME		= current_database();
			V_SCHEMA_NAME		= SUBSTRING(V_NAME from 1 for (V_DOT-1) );
			V_OBJECT_NAME		= SUBSTRING(V_NAME from (V_DOT+1) );
		END IF;
	ELSE
		V_DATABASE_NAME	= current_database();
		V_SCHEMA_NAME	= current_schema();
		V_OBJECT_NAME	= SUBSTRING(V_NAME from (V_DOT+1) );
	END IF;

-- raise NOTICE ' P_NAME=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_OBJECT_NAME=%', P_NAME, V_DATABASE_NAME, V_SCHEMA_NAME, V_OBJECT_NAME;


	RETURN	QUERY
	SELECT	V_DATABASE_NAME
	,	V_SCHEMA_NAME
	,	V_OBJECT_NAME
	;

END
$$;


ALTER FUNCTION public.parse_object_name(p_name character varying) OWNER TO fuxiadmin;

--
-- TOC entry 285 (class 1255 OID 17612)
-- Name: process_feed_amex_security(); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION process_feed_amex_security() RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE

exchange_id	int;

BEGIN

	SELECT	exchange_id = get_exchanges_id('AMEX');

	IF	( exchange_id IS NULL )
	THEN
		INSERT	INTO EXHANGES
		(	name
		,	title
		,	nickname
		,	symbol
		,	currency
		)
		VALUES
		(	'AMEX'
		,	'AMEX'
		,	'AMEX'
		,	'AMEX'
		,	'USD'
		)
		;

		select	exchange_id	= get_exchanges_id('AMEX');
	END IF
	;

	INSERT	INTO	INSTRUMENTS
	(	name
	,	title
	,	symbol
	,	nickname
	,	currency
	,	instrument_type
	)
	SELECT	fas.symbol
	,	fas.name
	,	fas.symbol
	,	fas.symbol
	,	'USD'
	,	get_instrument_types_id('EQUITY')
	FROM	feed_amex_security	fas
	WHERE	get_instruments_id(fas.symbol) IS NOT NULL
	;
	
	INSERT	INTO	PRODUCTS
	(	exchange_id
	,	instrument_id
	,	name
	,	title
	,	symbol
	,	nickname
	,	currency
	)
	SELECT	exchange_id
	,	get_instruments_id(fas.symbol)
	,	fas.symbol
	,	fas.name
	,	fas.symbol
	,	fas.symbol
	,	'USD'
	FROM	feed_amex_security	fas
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	products	p
		where	p.exchange_id	=	exchange_id
		and	p.instrument_id	=	get_instruments_id(fas.symbol)
		)
	;

--
--	Exchange Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	'exchanges'
	,	exchange_id
	,	'TICKER'
	,	'AMEX'
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'exchange'
		AND	cr.object_id		=	exchange_id
		AND	cr.xref_type		=	'TICKER'
	)
	;

--
--	Instruments Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'instruments'
	,	get_instruments_id(fas.symbol)
	,	'TICKER'
	,	fas.symbol
	FROM	feed_amex_security	fas
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'instruments'
		AND	cr.object_id		=	get_instruments_id(fas.symbol)
		AND	cr.xref_type		=	'TICKER'
	)
	;


--
--	Product Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'products'
	,	p.id
	,	'TICKER'
	,	fas.symbol
	FROM	feed_amex_security	fas
	JOIN	products		p
	ON	p.exchange_id		=	exchange_id
	AND	p.instrument_id		=	get_instruments_id(fas.symbol)
	
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'instruments'
		AND	cr.object_id		=	get_instruments_id(fas.symbol)
		AND	cr.xref_type		=	'TICKER'
	)
	;

END
$$;


ALTER FUNCTION public.process_feed_amex_security() OWNER TO fuxiadmin;

--
-- TOC entry 300 (class 1255 OID 26839)
-- Name: process_feed_countries(); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION process_feed_countries() RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE


BEGIN

	UPDATE	countries	c
	SET	title		=	fc.country
	,	nickname	=	fc.iso_code
	FROM	feed_countries	fc
	WHERE	c.NAME		=	UPPER(fc.country)
	;

	INSERT	INTO countries
	(	name
	,	title
	,	nickname
	)
	SELECT	DISTINCT
		UPPER( fc.country ) 
	,	fc.country
	,	fc.iso_code
	FROM	feed_countries	fc
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	countries	c
		where	c.id	=	get_countries_id(UPPER(fc.country))
	)
	;


--
--	Country Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'countries'
	,	c.id
	,	'ISO'
	,	fc.iso_code
	FROM	feed_countries	fc
	JOIN	countries	c
	ON	c.id				=	get_countries_id(UPPER(fc.country))
	WHERE	COALESCE(fc.iso_code,'')	!=	''
	AND	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'countries'
		AND	cr.object_id		=	c.id
		AND	cr.xref_type		=	'ISO'
	)
	;



END
$$;


ALTER FUNCTION public.process_feed_countries() OWNER TO fuxiadmin;

--
-- TOC entry 299 (class 1255 OID 17613)
-- Name: process_feed_currencies(); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION process_feed_currencies() RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE

exchange_id	int;

BEGIN


	UPDATE	currencies	c
	SET	title		=	fc.currency_title
	,	nickname	=	fc.alpha_code
	FROM	feed_currencies	fc
	WHERE	c.NAME		=	UPPER(fc.currency_title)
	;


	INSERT	INTO currencies
	(	name
	,	title
	,	nickname
--	,	minor_unit
	)
	SELECT	DISTINCT
		UPPER(fc.currency_title)
	,	fc.currency_title
	,	fc.alpha_code
--	,	cast( COALESCE(nullif(fc.minor_unit,''),'0') as int )
--	,	fc.minor_unit::int
	FROM	feed_currencies	fc
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	currencies	c
		where	c.id	=	get_currencies_id(UPPER(fc.currency_title))
	)
	;


--
--	Currency Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'currencies'
	,	c.id
	,	'ISO ALPHA 2'
	,	fc.alpha_code
	FROM	feed_currencies	fc
	JOIN	currencies	c
	ON	c.id				=	get_currencies_id(UPPER(fc.currency_title))
	WHERE	COALESCE(fc.alpha_code,'')	!=	''
	AND	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'currencies'
		AND	cr.object_id		=	c.id
		AND	cr.xref_type		=	'ISO ALPHA 2'
	)
	;

	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'currencies'
	,	c.id
	,	'ISO NUMERIC 3'
	,	fc.num_code
	FROM	feed_currencies	fc
	JOIN	currencies	c
	ON	c.id				=	get_currencies_id(UPPER(fc.currency_title))
	WHERE	COALESCE(fc.num_code,'')	!=	''
	AND	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'currencies'
		AND	cr.object_id		=	c.id
		AND	cr.xref_type		=	'ISO NUMERIC 3'
	)
	;



END
$$;


ALTER FUNCTION public.process_feed_currencies() OWNER TO fuxiadmin;

--
-- TOC entry 291 (class 1255 OID 17614)
-- Name: process_feed_exchanges(); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION process_feed_exchanges() RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE


BEGIN

	UPDATE	exchanges	e
	SET	title		=	fe.exchange
	,	nickname	=	fe.symbol
	,	symbol		=	fe.symbol
	,	ticker_suffix	=	fe.ticker_suffix
	,	country_id	=	get_countries_id(UPPER(fe.country))
	FROM	feed_exchanges	fe
	WHERE	e.NAME		=	UPPER(fe.symbol)
	;

	INSERT	INTO exchanges
	(	name
	,	title
	,	nickname
	,	symbol
	,	ticker_suffix
	,	country_id
	)
	SELECT	UPPER(fe.symbol)
	,	fe.exchange
	,	fe.symbol
	,	fe.symbol
	,	fe.ticker_suffix
	,	get_countries_id(UPPER(fe.country))
	FROM	feed_exchanges	fe
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	exchanges	e
		where	e.id	=	get_exchanges_id(UPPER(fe.symbol))
	)
	;

END
$$;


ALTER FUNCTION public.process_feed_exchanges() OWNER TO fuxiadmin;

--
-- TOC entry 301 (class 1255 OID 26896)
-- Name: process_feed_historic_prices(); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION process_feed_historic_prices() RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE


BEGIN

	UPDATE	historic_prices		hp
	SET	open			=	fhp.open::numeric(36,8)
	,	high			=	fhp.high::numeric(36,8)
	,	low			=	fhp.low::numeric(36,8)
	,	close			=	fhp.close::numeric(36,8)
	,	volume			=	fhp.volume::numeric(36,8)
	,	adj_close		=	fhp.adj_close::numeric(36,8)
	FROM	feed_historic_prices	fhp
	WHERE	hp.product_id		=	fhp.product_id::int
	AND	hp.price_date		=	to_timestamp(fhp.price_date, 'YYYY-MM-DD' )
	;


	INSERT	INTO historic_prices
	(	product_id
	,	price_date
	,	open
	,	high
	,	low
	,	close
	,	volume
	,	adj_close
	)
	SELECT	fhp.product_id::int
	,	to_timestamp(fhp.price_date, 'YYYY-MM-DD' )
	,	fhp.open::numeric(36,8)
	,	fhp.high::numeric(36,8)
	,	fhp.low::numeric(36,8)
	,	fhp.close::numeric(36,8)
	,	fhp.volume::numeric(36,8)
	,	fhp.adj_close::numeric(36,8)
	FROM	feed_historic_prices		fhp
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	historic_prices		hp
		WHERE	hp.product_id		=	fhp.product_id::int
		AND	hp.price_date		=	to_timestamp(fhp.price_date, 'YYYY-MM-DD' )
	)
	;

END
$$;


ALTER FUNCTION public.process_feed_historic_prices() OWNER TO fuxiadmin;

--
-- TOC entry 286 (class 1255 OID 17615)
-- Name: process_feed_index_components(); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION process_feed_index_components() RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE

exchange_id	int;

BEGIN

	INSERT	INTO indices
	(	name
	,	title
	,	nickname
	)
	SELECT	fic.index_name
	,	fic.index_name
	,	fic.index_name
	FROM	feed_index_components	fic
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	indices		i
		WHERE	i.id		=	get_xref_id( 'products', fic.symbol_type , fic.symbol )
		AND	product_id	=	get_indicies_id( fic.index_name )
	)
	;

/*
 *
 *	If the index components are treated as complete full downloads we remove all the existing 
 *	index components for thos indexes where are replacing
 *	
 *

		
	DELETE	FROM
		index_components	ic
	USING	feed_index_components	fic
	WHERE	ic.name			=	fic.index_name 
	;

or
	DELETE	FROM
		index_components
	WHERE	ic.name			IN
	(
		SELECT	DISTINCT
			fic.index_name
		FROM	feed_index_components	fic
	)
	;

 *
 */

--
--	Remove existing index_components
--

	DELETE	FROM
		index_components
	WHERE	ic.name			IN
	(
		SELECT	DISTINCT
			fic.index_name
		FROM	feed_index_components	fic
	)
	;


--
--	insert new index_components
--

	INSERT	INTO	index_components
	(	index_id
	,	product_id
	)
	SELECT	get_xref_id( 'products', fic.symbol_type , fic.symbol )
	,	get_indicies_id( fic.index_name )
	FROM	feed_index_components	fic
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	index_components
		WHERE	index_id	=	get_xref_id( 'products', fic.symbol_type , fic.symbol )
		AND	product_id	=	get_indicies_id( fic.index_name )
	)
	;
	

END
$$;


ALTER FUNCTION public.process_feed_index_components() OWNER TO fuxiadmin;

--
-- TOC entry 302 (class 1255 OID 26927)
-- Name: process_feed_indices(); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION process_feed_indices() RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE


BEGIN

	UPDATE	indices		i
	SET	title		=	fi.index_title
	,	nickname	=	UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	FROM	feed_indices	fi
	WHERE	name		=	UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	;


	INSERT	INTO indices
	(	name
	,	title
	,	nickname
	)
	SELECT	DISTINCT
		UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	,	fi.index_title
	,	UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	FROM	feed_indices		fi
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	indices		i
		WHERE	i.name		=	UPPER(SUBSTRING(fi.index_name, POSITION( '/' in fi.index_name ) + 1 ))
	)
	;

END
$$;


ALTER FUNCTION public.process_feed_indices() OWNER TO fuxiadmin;

--
-- TOC entry 274 (class 1255 OID 17616)
-- Name: process_feed_yahoo_security(); Type: FUNCTION; Schema: public; Owner: fuxiadmin
--

CREATE FUNCTION process_feed_yahoo_security() RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE

exchange_id	int;

BEGIN

/*
 *
 *	indices

drop index if exists feed_yahoo_security_i1 cascade;
drop index if exists feed_yahoo_security_i2 cascade;
drop index if exists feed_yahoo_security_i3 cascade;

create index feed_yahoo_security_i1 on feed_yahoo_security ( exchange, ticker );
create index feed_yahoo_security_i2 on feed_yahoo_security ( category_code );
create index feed_yahoo_security_i3 on feed_yahoo_security ( ticker );

 *
 *
 */

	INSERT	INTO	exchanges
	(	name
	,	title
	,	nickname
	,	symbol
	)
	SELECT	DISTINCT
		fys.exchange
	,	MAX(fys.exchange)
	,	MAX(fys.exchange)
	,	MAX(fys.exchange)
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	exchanges	e
		WHERE	e.id		=	get_exchanges_id(fys.exchange)
		)
	GROUP BY
		fys.exchange
	;


	INSERT	INTO	instrument_categories
	(	name
	,	title
	)
	SELECT
		fys.category_code
	,	MAX(fys.category_name)
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	instrument_categories	ic
		WHERE	ic.name		=	fys.category_code
		)
	GROUP BY
		fys.category_code
	;


	INSERT	INTO	INSTRUMENTS
	(	name
	,	title
	,	symbol
	,	nickname
	,	instrument_type_id
	,	instrument_category_id
	)
	SELECT
		get_ric_from_ticker(fys.ticker)
	,	MAX(fys.name)
	,	MAX(get_ric_from_ticker(fys.ticker))
	,	MAX(get_ric_from_ticker(fys.ticker))
	,	MAX(get_instrument_types_id('EQUITY'))
	,	MAX(get_instrument_categories_id(fys.category_code))
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	instruments	i
		where	i.id		=	get_instruments_id(get_ric_from_ticker(fys.ticker))
		)
	GROUP BY
		get_ric_from_ticker(fys.ticker)
	;


	INSERT	INTO	PRODUCTS
	(	exchange_id
	,	instrument_id
	,	name
	,	title
	,	symbol
	)
	SELECT	get_exchanges_id(fys.exchange)
	,	get_instruments_id(get_ric_from_ticker(fys.ticker))
	,	max(fys.ticker)
	,	max(fys.name)
	,	max(fys.ticker)
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	products	p
		WHERE	p.exchange_id	=	get_exchanges_id(fys.exchange)
		AND	p.instrument_id	=	get_instruments_id(get_ric_from_ticker(fys.ticker))
		)
	GROUP BY
		get_exchanges_id(fys.exchange)
	,	get_instruments_id(get_ric_from_ticker(fys.ticker))
	;


--
--	Exchange Cross References
--
--
--	Instruments Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT
		'instruments'
	,	i.id
	,	'TICKER'
	,	i.symbol
	FROM	instruments		i
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'instruments'
		AND	cr.object_id		=	i.id
		AND	cr.xref_type		=	'TICKER'
	)
	;


--
--	Product Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT
		'products'
	,	p.id
	,	'TICKER'
	,	p.symbol
	FROM	products		p
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'products'
		AND	cr.object_id		=	p.id
		AND	cr.xref_type		=	'TICKER'
	)
	;



	INSERT	INTO	cross_references
	(	xref_value
	,	object_id
	,	object_type
	,	xref_type
	)
	SELECT	fys.ticker
	,	MAX(cr.object_id)
	,	'products'
	,	'YAHOO'
	FROM	feed_yahoo_security	fys
	JOIN	cross_references	cr
	ON	cr.object_type		=	'products'
	AND	cr.xref_type		=	'TICKER'
	AND	cr.xref_value		=	fys.ticker
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr2
		WHERE	cr2.object_type		=	'products'
		AND	cr2.object_id		=	cr.id
		AND	cr2.xref_type		=	'YAHOO'
	)
	GROUP BY
		fys.ticker
	;



END
$$;


ALTER FUNCTION public.process_feed_yahoo_security() OWNER TO fuxiadmin;

--
-- TOC entry 174 (class 1259 OID 21786)
-- Name: ab_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE ab_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ab_permission_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 177 (class 1259 OID 21840)
-- Name: ab_permission_view_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE ab_permission_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ab_permission_view_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 178 (class 1259 OID 21857)
-- Name: ab_permission_view_role_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE ab_permission_view_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ab_permission_view_role_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 175 (class 1259 OID 21795)
-- Name: ab_register_user_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE ab_register_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ab_register_user_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 208 (class 1259 OID 27525)
-- Name: ab_role_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE ab_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ab_role_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 176 (class 1259 OID 21814)
-- Name: ab_user_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE ab_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ab_user_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 207 (class 1259 OID 26811)
-- Name: ab_user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE ab_user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ab_user_role_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 173 (class 1259 OID 21777)
-- Name: ab_view_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE ab_view_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ab_view_menu_id_seq OWNER TO fuxiadmin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 246 (class 1259 OID 28362)
-- Name: addresses; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE addresses (
    id integer NOT NULL,
    client_id integer,
    line_1 text,
    line_2 text,
    city text,
    postcode text
);


ALTER TABLE addresses OWNER TO fuxiadmin;

--
-- TOC entry 245 (class 1259 OID 28360)
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE addresses_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3320 (class 0 OID 0)
-- Dependencies: 245
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE addresses_id_seq OWNED BY addresses.id;


--
-- TOC entry 210 (class 1259 OID 27926)
-- Name: cash_wallet; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE cash_wallet (
    id integer NOT NULL,
    portfolio_id integer,
    currency_symbol character varying(3),
    quantity numeric(36,8)
);


ALTER TABLE cash_wallet OWNER TO fuxiadmin;

--
-- TOC entry 209 (class 1259 OID 27924)
-- Name: cash_wallet_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE cash_wallet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cash_wallet_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3321 (class 0 OID 0)
-- Dependencies: 209
-- Name: cash_wallet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE cash_wallet_id_seq OWNED BY cash_wallet.id;


--
-- TOC entry 248 (class 1259 OID 28373)
-- Name: client_watchlists; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE client_watchlists (
    id integer NOT NULL,
    client_id integer,
    portfolio_id integer,
    symbol text,
    deleted_at timestamp without time zone
);


ALTER TABLE client_watchlists OWNER TO fuxiadmin;

--
-- TOC entry 247 (class 1259 OID 28371)
-- Name: client_watchlists_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE client_watchlists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_watchlists_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3322 (class 0 OID 0)
-- Dependencies: 247
-- Name: client_watchlists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE client_watchlists_id_seq OWNED BY client_watchlists.id;


--
-- TOC entry 212 (class 1259 OID 27958)
-- Name: clients; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE clients (
    id integer NOT NULL,
    name character varying(255),
    date_of_birth date,
    ni_number character varying(10),
    telephone text,
    email text,
    image_id text,
    first_name text,
    last_name text,
    national_insurance_number text,
    deleted_at date,
    user_id integer,
    custom_fields text
);


ALTER TABLE clients OWNER TO fuxiadmin;

--
-- TOC entry 211 (class 1259 OID 27956)
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE clients_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3323 (class 0 OID 0)
-- Dependencies: 211
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE clients_id_seq OWNED BY clients.id;


--
-- TOC entry 200 (class 1259 OID 26347)
-- Name: countries; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE countries (
    id integer NOT NULL,
    name character varying(255),
    title character varying(255),
    nickname character varying(50),
    currency_id integer,
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE countries OWNER TO fuxiadmin;

--
-- TOC entry 199 (class 1259 OID 26345)
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE countries_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3324 (class 0 OID 0)
-- Dependencies: 199
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE countries_id_seq OWNED BY countries.id;


--
-- TOC entry 180 (class 1259 OID 25625)
-- Name: cross_references; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE cross_references (
    id integer NOT NULL,
    object_type character varying(255),
    object_id integer,
    xref_type character varying(255),
    xref_value character varying(255),
    owner_id integer,
    effective_from timestamp without time zone,
    effective_to timestamp without time zone,
    version integer DEFAULT 1,
    create_user character varying(255) DEFAULT "current_user"(),
    create_date timestamp without time zone DEFAULT now(),
    update_user character varying(255) DEFAULT "current_user"(),
    update_date timestamp without time zone DEFAULT now(),
    pid integer
);


ALTER TABLE cross_references OWNER TO fuxiadmin;

--
-- TOC entry 205 (class 1259 OID 26792)
-- Name: cross_reference_mappings; Type: VIEW; Schema: public; Owner: fuxiadmin
--

CREATE VIEW cross_reference_mappings AS
 SELECT DISTINCT cr1.object_type AS user_id,
    cr1.object_id AS user_name,
    cr1.xref_type AS from_type,
    cr1.xref_value AS from_value,
    cr2.xref_type AS to_type,
    cr2.xref_value AS to_value
   FROM (cross_references cr1
     JOIN cross_references cr2 ON (((((cr2.object_type)::text = (cr1.object_type)::text) AND (cr2.object_id = cr1.object_id)) AND ((cr2.xref_type)::text <> (cr1.xref_type)::text))));


ALTER TABLE cross_reference_mappings OWNER TO fuxiadmin;

--
-- TOC entry 179 (class 1259 OID 25623)
-- Name: cross_references_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE cross_references_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cross_references_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3325 (class 0 OID 0)
-- Dependencies: 179
-- Name: cross_references_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE cross_references_id_seq OWNED BY cross_references.id;


--
-- TOC entry 202 (class 1259 OID 26365)
-- Name: currencies; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE currencies (
    id integer NOT NULL,
    name character varying(255),
    title character varying(255),
    nickname character varying(50),
    minor_unit integer,
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE currencies OWNER TO fuxiadmin;

--
-- TOC entry 201 (class 1259 OID 26363)
-- Name: currencies_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE currencies_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3326 (class 0 OID 0)
-- Dependencies: 201
-- Name: currencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE currencies_id_seq OWNED BY currencies.id;


--
-- TOC entry 190 (class 1259 OID 26234)
-- Name: exchanges; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE exchanges (
    id integer NOT NULL,
    name character varying(255),
    title character varying(255),
    nickname character varying(50),
    symbol character varying(50),
    currency_id integer,
    country_id integer,
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE exchanges OWNER TO fuxiadmin;

--
-- TOC entry 189 (class 1259 OID 26232)
-- Name: exchanges_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE exchanges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE exchanges_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 189
-- Name: exchanges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE exchanges_id_seq OWNED BY exchanges.id;


--
-- TOC entry 222 (class 1259 OID 28088)
-- Name: feed_amex_security; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_amex_security (
    id integer NOT NULL,
    symbol character varying(255),
    name character varying(255),
    lastsale character varying(255),
    marketcap character varying(255),
    ipoyear character varying(255),
    sector character varying(255),
    industry character varying(255),
    summaryquote character varying(255)
);


ALTER TABLE feed_amex_security OWNER TO fuxiadmin;

--
-- TOC entry 221 (class 1259 OID 28086)
-- Name: feed_amex_security_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_amex_security_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_amex_security_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 221
-- Name: feed_amex_security_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_amex_security_id_seq OWNED BY feed_amex_security.id;


--
-- TOC entry 224 (class 1259 OID 28099)
-- Name: feed_countries; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_countries (
    id integer NOT NULL,
    country character varying(255),
    iso_code character varying(255)
);


ALTER TABLE feed_countries OWNER TO fuxiadmin;

--
-- TOC entry 223 (class 1259 OID 28097)
-- Name: feed_countries_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_countries_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 223
-- Name: feed_countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_countries_id_seq OWNED BY feed_countries.id;


--
-- TOC entry 226 (class 1259 OID 28110)
-- Name: feed_currencies; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_currencies (
    id integer NOT NULL,
    country_name character varying(255),
    country_title character varying(255),
    entity character varying(255),
    country_code character varying(255),
    currency_title character varying(255),
    alpha_code character varying(255),
    num_code character varying(255),
    minor_unit character varying(10),
    minor_unit_int integer
);


ALTER TABLE feed_currencies OWNER TO fuxiadmin;

--
-- TOC entry 225 (class 1259 OID 28108)
-- Name: feed_currencies_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_currencies_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 225
-- Name: feed_currencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_currencies_id_seq OWNED BY feed_currencies.id;


--
-- TOC entry 228 (class 1259 OID 28121)
-- Name: feed_exchanges; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_exchanges (
    id integer NOT NULL,
    country character varying(255),
    exchange character varying(255),
    symbol character varying(255),
    ticker_suffix character varying(255),
    ticker character varying(255)
);


ALTER TABLE feed_exchanges OWNER TO fuxiadmin;

--
-- TOC entry 227 (class 1259 OID 28119)
-- Name: feed_exchanges_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_exchanges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_exchanges_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 227
-- Name: feed_exchanges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_exchanges_id_seq OWNED BY feed_exchanges.id;


--
-- TOC entry 230 (class 1259 OID 28134)
-- Name: feed_historic_prices; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_historic_prices (
    id integer NOT NULL,
    symbol character varying(255),
    product_id character varying(255),
    price_date character varying(255),
    open character varying(255),
    high character varying(255),
    low character varying(255),
    close character varying(255),
    volume character varying(255),
    adj_close character varying(255)
);


ALTER TABLE feed_historic_prices OWNER TO fuxiadmin;

--
-- TOC entry 229 (class 1259 OID 28132)
-- Name: feed_historic_prices_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_historic_prices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_historic_prices_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 229
-- Name: feed_historic_prices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_historic_prices_id_seq OWNED BY feed_historic_prices.id;


--
-- TOC entry 232 (class 1259 OID 28145)
-- Name: feed_index_components; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_index_components (
    id integer NOT NULL,
    index_name character varying(255),
    product_symbol_type character varying(255),
    product_symbol character varying(255),
    product_name character varying(255)
);


ALTER TABLE feed_index_components OWNER TO fuxiadmin;

--
-- TOC entry 231 (class 1259 OID 28143)
-- Name: feed_index_components_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_index_components_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_index_components_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 231
-- Name: feed_index_components_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_index_components_id_seq OWNED BY feed_index_components.id;


--
-- TOC entry 234 (class 1259 OID 28156)
-- Name: feed_indices; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_indices (
    id integer NOT NULL,
    index_name character varying(255),
    index_title character varying(255),
    start_date character varying(255),
    end_date character varying(255),
    frequency character varying(255),
    latest_update character varying(255)
);


ALTER TABLE feed_indices OWNER TO fuxiadmin;

--
-- TOC entry 233 (class 1259 OID 28154)
-- Name: feed_indices_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_indices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_indices_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 233
-- Name: feed_indices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_indices_id_seq OWNED BY feed_indices.id;


--
-- TOC entry 236 (class 1259 OID 28167)
-- Name: feed_lse_security; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_lse_security (
    id integer NOT NULL,
    symbol character varying(255),
    name character varying(255)
);


ALTER TABLE feed_lse_security OWNER TO fuxiadmin;

--
-- TOC entry 235 (class 1259 OID 28165)
-- Name: feed_lse_security_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_lse_security_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_lse_security_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3335 (class 0 OID 0)
-- Dependencies: 235
-- Name: feed_lse_security_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_lse_security_id_seq OWNED BY feed_lse_security.id;


--
-- TOC entry 182 (class 1259 OID 25641)
-- Name: feed_mappings; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_mappings (
    id integer NOT NULL,
    feed_type character varying(8000),
    tag character varying(8000),
    table_name character varying(8000),
    column_name character varying(8000),
    column_type character varying(8000),
    default_value character varying(8000),
    owner_id integer,
    effective_from timestamp without time zone,
    effective_to timestamp without time zone,
    version integer DEFAULT 1,
    create_user character varying(255) DEFAULT "current_user"(),
    create_date timestamp without time zone DEFAULT now(),
    update_user character varying(255) DEFAULT "current_user"(),
    update_date timestamp without time zone DEFAULT now(),
    pid integer
);


ALTER TABLE feed_mappings OWNER TO fuxiadmin;

--
-- TOC entry 181 (class 1259 OID 25639)
-- Name: feed_mappings_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_mappings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_mappings_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3336 (class 0 OID 0)
-- Dependencies: 181
-- Name: feed_mappings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_mappings_id_seq OWNED BY feed_mappings.id;


--
-- TOC entry 238 (class 1259 OID 28178)
-- Name: feed_nasdaq_security; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_nasdaq_security (
    id integer NOT NULL,
    symbol character varying(255),
    name character varying(255),
    lastsale character varying(255),
    marketcap character varying(255),
    ipoyear character varying(255),
    sector character varying(255),
    industry character varying(255),
    summaryquote character varying(255)
);


ALTER TABLE feed_nasdaq_security OWNER TO fuxiadmin;

--
-- TOC entry 237 (class 1259 OID 28176)
-- Name: feed_nasdaq_security_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_nasdaq_security_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_nasdaq_security_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3337 (class 0 OID 0)
-- Dependencies: 237
-- Name: feed_nasdaq_security_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_nasdaq_security_id_seq OWNED BY feed_nasdaq_security.id;


--
-- TOC entry 240 (class 1259 OID 28189)
-- Name: feed_nyse_security; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_nyse_security (
    id integer NOT NULL,
    symbol character varying(255),
    name character varying(255),
    lastsale character varying(255),
    marketcap character varying(255),
    ipoyear character varying(255),
    sector character varying(255),
    industry character varying(255),
    summaryquote character varying(255)
);


ALTER TABLE feed_nyse_security OWNER TO fuxiadmin;

--
-- TOC entry 239 (class 1259 OID 28187)
-- Name: feed_nyse_security_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_nyse_security_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_nyse_security_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3338 (class 0 OID 0)
-- Dependencies: 239
-- Name: feed_nyse_security_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_nyse_security_id_seq OWNED BY feed_nyse_security.id;


--
-- TOC entry 242 (class 1259 OID 28200)
-- Name: feed_yahoo_security; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE feed_yahoo_security (
    id integer NOT NULL,
    ticker character varying(255),
    name character varying(255),
    exchange character varying(255),
    category_name character varying(255),
    category_code character varying(255)
);


ALTER TABLE feed_yahoo_security OWNER TO fuxiadmin;

--
-- TOC entry 241 (class 1259 OID 28198)
-- Name: feed_yahoo_security_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE feed_yahoo_security_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE feed_yahoo_security_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 241
-- Name: feed_yahoo_security_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE feed_yahoo_security_id_seq OWNED BY feed_yahoo_security.id;


--
-- TOC entry 184 (class 1259 OID 25657)
-- Name: globals; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE globals (
    id integer NOT NULL,
    code character varying(8000),
    value character varying(8000),
    datatype character varying(8000) DEFAULT 'VARCHAR'::character varying,
    owner_id integer,
    effective_from timestamp without time zone,
    effective_to timestamp without time zone,
    version integer DEFAULT 1,
    create_user character varying(255) DEFAULT "current_user"(),
    create_date timestamp without time zone DEFAULT now(),
    update_user character varying(255) DEFAULT "current_user"(),
    update_date timestamp without time zone DEFAULT now(),
    pid integer
);


ALTER TABLE globals OWNER TO fuxiadmin;

--
-- TOC entry 183 (class 1259 OID 25655)
-- Name: globals_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE globals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE globals_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3340 (class 0 OID 0)
-- Dependencies: 183
-- Name: globals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE globals_id_seq OWNED BY globals.id;


--
-- TOC entry 186 (class 1259 OID 26051)
-- Name: historic_prices; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE historic_prices (
    id integer NOT NULL,
    product_id integer,
    price_date timestamp without time zone DEFAULT now(),
    open numeric(36,8),
    high numeric(36,8),
    low numeric(36,8),
    close numeric(36,8),
    volume numeric(36,8),
    adj_close numeric(36,8)
);


ALTER TABLE historic_prices OWNER TO fuxiadmin;

--
-- TOC entry 185 (class 1259 OID 26049)
-- Name: historic_prices_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE historic_prices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historic_prices_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 185
-- Name: historic_prices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE historic_prices_id_seq OWNED BY historic_prices.id;


--
-- TOC entry 258 (class 1259 OID 28515)
-- Name: historic_quotes; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE historic_quotes (
    id integer NOT NULL,
    product_id integer,
    latest_value numeric(36,8),
    open numeric(36,8),
    high numeric(36,8),
    low numeric(36,8),
    close numeric(36,8),
    quote_date timestamp without time zone DEFAULT now()
);


ALTER TABLE historic_quotes OWNER TO fuxiadmin;

--
-- TOC entry 257 (class 1259 OID 28513)
-- Name: historic_quotes_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE historic_quotes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historic_quotes_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 257
-- Name: historic_quotes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE historic_quotes_id_seq OWNED BY historic_quotes.id;


--
-- TOC entry 192 (class 1259 OID 26252)
-- Name: index_components; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE index_components (
    id integer NOT NULL,
    index_id integer,
    product_id integer,
    sequence integer DEFAULT 0,
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE index_components OWNER TO fuxiadmin;

--
-- TOC entry 191 (class 1259 OID 26250)
-- Name: index_components_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE index_components_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE index_components_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 191
-- Name: index_components_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE index_components_id_seq OWNED BY index_components.id;


--
-- TOC entry 171 (class 1259 OID 17331)
-- Name: indices; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE indices (
    id integer NOT NULL,
    name character varying(255),
    title character varying(255),
    nickname character varying(50),
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE indices OWNER TO fuxiadmin;

--
-- TOC entry 170 (class 1259 OID 17329)
-- Name: indices_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE indices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE indices_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 170
-- Name: indices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE indices_id_seq OWNED BY indices.id;


--
-- TOC entry 204 (class 1259 OID 26383)
-- Name: instrument_categories; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE instrument_categories (
    id integer NOT NULL,
    name character varying(255),
    title character varying(255),
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE instrument_categories OWNER TO fuxiadmin;

--
-- TOC entry 203 (class 1259 OID 26381)
-- Name: instrument_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE instrument_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE instrument_categories_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 203
-- Name: instrument_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE instrument_categories_id_seq OWNED BY instrument_categories.id;


--
-- TOC entry 196 (class 1259 OID 26291)
-- Name: instrument_types; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE instrument_types (
    id integer NOT NULL,
    name character varying(255),
    title character varying(255),
    nickname character varying(50),
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE instrument_types OWNER TO fuxiadmin;

--
-- TOC entry 195 (class 1259 OID 26289)
-- Name: instrument_types_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE instrument_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE instrument_types_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 195
-- Name: instrument_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE instrument_types_id_seq OWNED BY instrument_types.id;


--
-- TOC entry 194 (class 1259 OID 26273)
-- Name: instruments; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE instruments (
    id integer NOT NULL,
    name character varying(255),
    title character varying(255),
    nickname character varying(50),
    symbol character varying(50),
    instrument_type_id integer,
    instrument_category_id integer,
    effective_from timestamp without time zone,
    effective_to timestamp without time zone,
    currency_id integer
);


ALTER TABLE instruments OWNER TO fuxiadmin;

--
-- TOC entry 193 (class 1259 OID 26271)
-- Name: instruments_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE instruments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE instruments_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 193
-- Name: instruments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE instruments_id_seq OWNED BY instruments.id;


--
-- TOC entry 250 (class 1259 OID 28396)
-- Name: madness; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE madness (
    id integer NOT NULL,
    data text,
    updated_at timestamp without time zone DEFAULT now()
);


ALTER TABLE madness OWNER TO fuxiadmin;

--
-- TOC entry 249 (class 1259 OID 28394)
-- Name: madness_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE madness_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE madness_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 249
-- Name: madness_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE madness_id_seq OWNED BY madness.id;


--
-- TOC entry 252 (class 1259 OID 28419)
-- Name: meetings; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE meetings (
    id integer NOT NULL,
    client_id integer,
    type character varying(255),
    subject text,
    notes text,
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE meetings OWNER TO fuxiadmin;

--
-- TOC entry 251 (class 1259 OID 28417)
-- Name: meetings_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE meetings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE meetings_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 251
-- Name: meetings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE meetings_id_seq OWNED BY meetings.id;


--
-- TOC entry 244 (class 1259 OID 28330)
-- Name: notes; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE notes (
    id integer NOT NULL,
    client_id integer,
    title text NOT NULL,
    note text NOT NULL,
    deleted_at timestamp without time zone
);


ALTER TABLE notes OWNER TO fuxiadmin;

--
-- TOC entry 243 (class 1259 OID 28328)
-- Name: notes_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notes_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3350 (class 0 OID 0)
-- Dependencies: 243
-- Name: notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE notes_id_seq OWNED BY notes.id;


--
-- TOC entry 214 (class 1259 OID 28025)
-- Name: portfolio_positions; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE portfolio_positions (
    id integer NOT NULL,
    portfolio_id integer,
    product_id integer,
    currency_symbol character varying(3),
    quantity numeric(38,8),
    price numeric(36,8),
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE portfolio_positions OWNER TO fuxiadmin;

--
-- TOC entry 213 (class 1259 OID 28023)
-- Name: portfolio_positions_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE portfolio_positions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE portfolio_positions_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3351 (class 0 OID 0)
-- Dependencies: 213
-- Name: portfolio_positions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE portfolio_positions_id_seq OWNED BY portfolio_positions.id;


--
-- TOC entry 216 (class 1259 OID 28043)
-- Name: portfolios; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE portfolios (
    id integer NOT NULL,
    name character varying(50),
    title character varying(50),
    client_id integer,
    nickname character varying(50),
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE portfolios OWNER TO fuxiadmin;

--
-- TOC entry 215 (class 1259 OID 28041)
-- Name: portfolios_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE portfolios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE portfolios_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 215
-- Name: portfolios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE portfolios_id_seq OWNED BY portfolios.id;


--
-- TOC entry 198 (class 1259 OID 26309)
-- Name: products; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    exchange_id integer,
    instrument_id integer,
    name character varying(255),
    title character varying(255),
    symbol character varying(50),
    currency_id integer,
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE products OWNER TO fuxiadmin;

--
-- TOC entry 197 (class 1259 OID 26307)
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3353 (class 0 OID 0)
-- Dependencies: 197
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- TOC entry 188 (class 1259 OID 26121)
-- Name: quotes; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE quotes (
    id integer NOT NULL,
    product_id integer,
    latest_value numeric(36,8),
    open numeric(36,8),
    high numeric(36,8),
    low numeric(36,8),
    mid numeric(36,8),
    close numeric(36,8),
    bid numeric(36,8),
    offer numeric(36,8),
    quote_date timestamp without time zone DEFAULT now(),
    effective_from timestamp without time zone,
    effective_to timestamp without time zone
);


ALTER TABLE quotes OWNER TO fuxiadmin;

--
-- TOC entry 187 (class 1259 OID 26119)
-- Name: quotes_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE quotes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE quotes_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3354 (class 0 OID 0)
-- Dependencies: 187
-- Name: quotes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE quotes_id_seq OWNED BY quotes.id;


--
-- TOC entry 206 (class 1259 OID 26800)
-- Name: relationships; Type: VIEW; Schema: public; Owner: fuxiadmin
--

CREATE VIEW relationships AS
 SELECT DISTINCT st.table_catalog,
    st.table_schema,
    st.table_name,
    st.table_type,
    c.column_name,
    kcu.constraint_name AS key_name,
    ccu.table_catalog AS related_table_catalog,
    ccu.table_schema AS related_table_schema,
    ccu.table_name AS related_table_name,
    ccu.column_name AS related_column_name,
    ccu.constraint_name AS related_key_name,
    parse_constraint_name((st.table_name)::character varying, (kcu.constraint_name)::character varying) AS relationship_name
   FROM (((((information_schema.tables st
     JOIN information_schema.columns c ON (((((c.table_catalog)::text = (st.table_catalog)::text) AND ((c.table_schema)::text = (st.table_schema)::text)) AND ((c.table_name)::text = (st.table_name)::text))))
     JOIN information_schema.key_column_usage kcu ON ((((((kcu.table_catalog)::text = (c.table_catalog)::text) AND ((kcu.table_schema)::text = (c.table_schema)::text)) AND ((kcu.table_name)::text = (c.table_name)::text)) AND ((kcu.column_name)::text = (c.column_name)::text))))
     JOIN information_schema.table_constraints tc ON ((((((tc.constraint_catalog)::text = (kcu.constraint_catalog)::text) AND ((tc.constraint_schema)::text = (kcu.constraint_schema)::text)) AND ((tc.constraint_name)::text = (kcu.constraint_name)::text)) AND ((tc.constraint_type)::text = 'FOREIGN KEY'::text))))
     JOIN information_schema.referential_constraints rc ON (((((rc.constraint_catalog)::text = (tc.constraint_catalog)::text) AND ((rc.constraint_schema)::text = (tc.constraint_schema)::text)) AND ((rc.constraint_name)::text = (tc.constraint_name)::text))))
     LEFT JOIN information_schema.constraint_column_usage ccu ON (((((ccu.constraint_catalog)::text = (rc.unique_constraint_catalog)::text) AND ((ccu.constraint_schema)::text = (rc.unique_constraint_schema)::text)) AND ((ccu.constraint_name)::text = (rc.unique_constraint_name)::text))))
  WHERE ((ccu.table_name IS NOT NULL) AND ((st.table_type)::text = 'BASE TABLE'::text));


ALTER TABLE relationships OWNER TO fuxiadmin;

--
-- TOC entry 172 (class 1259 OID 21546)
-- Name: seq_ab_role_pk; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE seq_ab_role_pk
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_ab_role_pk OWNER TO fuxiadmin;

--
-- TOC entry 254 (class 1259 OID 28439)
-- Name: tickers; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE tickers (
    id integer NOT NULL,
    symbol text,
    mid numeric(36,8)
);


ALTER TABLE tickers OWNER TO fuxiadmin;

--
-- TOC entry 253 (class 1259 OID 28437)
-- Name: tickers_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE tickers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tickers_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3355 (class 0 OID 0)
-- Dependencies: 253
-- Name: tickers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE tickers_id_seq OWNED BY tickers.id;


--
-- TOC entry 218 (class 1259 OID 28067)
-- Name: user_sessions; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE user_sessions (
    id integer NOT NULL,
    user_id integer,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone DEFAULT now(),
    expired_at timestamp without time zone
);


ALTER TABLE user_sessions OWNER TO fuxiadmin;

--
-- TOC entry 217 (class 1259 OID 28065)
-- Name: user_sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE user_sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_sessions_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE user_sessions_id_seq OWNED BY user_sessions.id;


--
-- TOC entry 256 (class 1259 OID 28450)
-- Name: user_watchlists; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE user_watchlists (
    id integer NOT NULL,
    symbol text,
    user_id integer
);


ALTER TABLE user_watchlists OWNER TO fuxiadmin;

--
-- TOC entry 255 (class 1259 OID 28448)
-- Name: user_watchlists_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE user_watchlists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_watchlists_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 255
-- Name: user_watchlists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE user_watchlists_id_seq OWNED BY user_watchlists.id;


--
-- TOC entry 220 (class 1259 OID 28077)
-- Name: users; Type: TABLE; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email text,
    first_name text,
    last_name text,
    password text,
    is_super_user boolean,
    settings text,
    deleted_at timestamp without time zone,
    image text
);


ALTER TABLE users OWNER TO fuxiadmin;

--
-- TOC entry 219 (class 1259 OID 28075)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: fuxiadmin
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO fuxiadmin;

--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 219
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fuxiadmin
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 3066 (class 2604 OID 28365)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY addresses ALTER COLUMN id SET DEFAULT nextval('addresses_id_seq'::regclass);


--
-- TOC entry 3046 (class 2604 OID 27929)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY cash_wallet ALTER COLUMN id SET DEFAULT nextval('cash_wallet_id_seq'::regclass);


--
-- TOC entry 3067 (class 2604 OID 28376)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY client_watchlists ALTER COLUMN id SET DEFAULT nextval('client_watchlists_id_seq'::regclass);


--
-- TOC entry 3047 (class 2604 OID 27961)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY clients ALTER COLUMN id SET DEFAULT nextval('clients_id_seq'::regclass);


--
-- TOC entry 3043 (class 2604 OID 26350)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY countries ALTER COLUMN id SET DEFAULT nextval('countries_id_seq'::regclass);


--
-- TOC entry 3014 (class 2604 OID 25628)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY cross_references ALTER COLUMN id SET DEFAULT nextval('cross_references_id_seq'::regclass);


--
-- TOC entry 3044 (class 2604 OID 26368)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY currencies ALTER COLUMN id SET DEFAULT nextval('currencies_id_seq'::regclass);


--
-- TOC entry 3037 (class 2604 OID 26237)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY exchanges ALTER COLUMN id SET DEFAULT nextval('exchanges_id_seq'::regclass);


--
-- TOC entry 3054 (class 2604 OID 28091)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_amex_security ALTER COLUMN id SET DEFAULT nextval('feed_amex_security_id_seq'::regclass);


--
-- TOC entry 3055 (class 2604 OID 28102)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_countries ALTER COLUMN id SET DEFAULT nextval('feed_countries_id_seq'::regclass);


--
-- TOC entry 3056 (class 2604 OID 28113)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_currencies ALTER COLUMN id SET DEFAULT nextval('feed_currencies_id_seq'::regclass);


--
-- TOC entry 3057 (class 2604 OID 28124)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_exchanges ALTER COLUMN id SET DEFAULT nextval('feed_exchanges_id_seq'::regclass);


--
-- TOC entry 3058 (class 2604 OID 28137)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_historic_prices ALTER COLUMN id SET DEFAULT nextval('feed_historic_prices_id_seq'::regclass);


--
-- TOC entry 3059 (class 2604 OID 28148)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_index_components ALTER COLUMN id SET DEFAULT nextval('feed_index_components_id_seq'::regclass);


--
-- TOC entry 3060 (class 2604 OID 28159)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_indices ALTER COLUMN id SET DEFAULT nextval('feed_indices_id_seq'::regclass);


--
-- TOC entry 3061 (class 2604 OID 28170)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_lse_security ALTER COLUMN id SET DEFAULT nextval('feed_lse_security_id_seq'::regclass);


--
-- TOC entry 3020 (class 2604 OID 25644)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_mappings ALTER COLUMN id SET DEFAULT nextval('feed_mappings_id_seq'::regclass);


--
-- TOC entry 3062 (class 2604 OID 28181)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_nasdaq_security ALTER COLUMN id SET DEFAULT nextval('feed_nasdaq_security_id_seq'::regclass);


--
-- TOC entry 3063 (class 2604 OID 28192)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_nyse_security ALTER COLUMN id SET DEFAULT nextval('feed_nyse_security_id_seq'::regclass);


--
-- TOC entry 3064 (class 2604 OID 28203)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY feed_yahoo_security ALTER COLUMN id SET DEFAULT nextval('feed_yahoo_security_id_seq'::regclass);


--
-- TOC entry 3026 (class 2604 OID 25660)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY globals ALTER COLUMN id SET DEFAULT nextval('globals_id_seq'::regclass);


--
-- TOC entry 3033 (class 2604 OID 26054)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY historic_prices ALTER COLUMN id SET DEFAULT nextval('historic_prices_id_seq'::regclass);


--
-- TOC entry 3073 (class 2604 OID 28518)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY historic_quotes ALTER COLUMN id SET DEFAULT nextval('historic_quotes_id_seq'::regclass);


--
-- TOC entry 3038 (class 2604 OID 26255)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY index_components ALTER COLUMN id SET DEFAULT nextval('index_components_id_seq'::regclass);


--
-- TOC entry 3013 (class 2604 OID 17334)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY indices ALTER COLUMN id SET DEFAULT nextval('indices_id_seq'::regclass);


--
-- TOC entry 3045 (class 2604 OID 26386)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY instrument_categories ALTER COLUMN id SET DEFAULT nextval('instrument_categories_id_seq'::regclass);


--
-- TOC entry 3041 (class 2604 OID 26294)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY instrument_types ALTER COLUMN id SET DEFAULT nextval('instrument_types_id_seq'::regclass);


--
-- TOC entry 3040 (class 2604 OID 26276)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY instruments ALTER COLUMN id SET DEFAULT nextval('instruments_id_seq'::regclass);


--
-- TOC entry 3068 (class 2604 OID 28399)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY madness ALTER COLUMN id SET DEFAULT nextval('madness_id_seq'::regclass);


--
-- TOC entry 3070 (class 2604 OID 28422)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY meetings ALTER COLUMN id SET DEFAULT nextval('meetings_id_seq'::regclass);


--
-- TOC entry 3065 (class 2604 OID 28333)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY notes ALTER COLUMN id SET DEFAULT nextval('notes_id_seq'::regclass);


--
-- TOC entry 3048 (class 2604 OID 28028)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY portfolio_positions ALTER COLUMN id SET DEFAULT nextval('portfolio_positions_id_seq'::regclass);


--
-- TOC entry 3049 (class 2604 OID 28046)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY portfolios ALTER COLUMN id SET DEFAULT nextval('portfolios_id_seq'::regclass);


--
-- TOC entry 3042 (class 2604 OID 26312)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- TOC entry 3035 (class 2604 OID 26124)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY quotes ALTER COLUMN id SET DEFAULT nextval('quotes_id_seq'::regclass);


--
-- TOC entry 3071 (class 2604 OID 28442)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY tickers ALTER COLUMN id SET DEFAULT nextval('tickers_id_seq'::regclass);


--
-- TOC entry 3050 (class 2604 OID 28070)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY user_sessions ALTER COLUMN id SET DEFAULT nextval('user_sessions_id_seq'::regclass);


--
-- TOC entry 3072 (class 2604 OID 28453)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY user_watchlists ALTER COLUMN id SET DEFAULT nextval('user_watchlists_id_seq'::regclass);


--
-- TOC entry 3053 (class 2604 OID 28080)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 3168 (class 2606 OID 28370)
-- Name: addresses_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT addresses_pk PRIMARY KEY (id);


--
-- TOC entry 3126 (class 2606 OID 27931)
-- Name: cash_wallet_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY cash_wallet
    ADD CONSTRAINT cash_wallet_pk PRIMARY KEY (id);


--
-- TOC entry 3170 (class 2606 OID 28381)
-- Name: client_watchlists_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY client_watchlists
    ADD CONSTRAINT client_watchlists_pk PRIMARY KEY (id);


--
-- TOC entry 3128 (class 2606 OID 27971)
-- Name: clients_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_pk PRIMARY KEY (id);


--
-- TOC entry 3130 (class 2606 OID 27973)
-- Name: clients_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_uk UNIQUE (name);


--
-- TOC entry 3114 (class 2606 OID 26360)
-- Name: countries_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT countries_pk PRIMARY KEY (id);


--
-- TOC entry 3116 (class 2606 OID 26362)
-- Name: countries_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT countries_uk UNIQUE (name);


--
-- TOC entry 3080 (class 2606 OID 25638)
-- Name: cross_references_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY cross_references
    ADD CONSTRAINT cross_references_pk PRIMARY KEY (id);


--
-- TOC entry 3118 (class 2606 OID 26378)
-- Name: currencies_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_pk PRIMARY KEY (id);


--
-- TOC entry 3120 (class 2606 OID 28429)
-- Name: currencies_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_uk UNIQUE (name);


--
-- TOC entry 3091 (class 2606 OID 26247)
-- Name: exchanges_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY exchanges
    ADD CONSTRAINT exchanges_pk PRIMARY KEY (id);


--
-- TOC entry 3093 (class 2606 OID 26249)
-- Name: exchanges_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY exchanges
    ADD CONSTRAINT exchanges_uk UNIQUE (name);


--
-- TOC entry 3144 (class 2606 OID 28096)
-- Name: feed_amex_security_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_amex_security
    ADD CONSTRAINT feed_amex_security_pk PRIMARY KEY (id);


--
-- TOC entry 3146 (class 2606 OID 28107)
-- Name: feed_countries_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_countries
    ADD CONSTRAINT feed_countries_pk PRIMARY KEY (id);


--
-- TOC entry 3148 (class 2606 OID 28118)
-- Name: feed_currencies_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_currencies
    ADD CONSTRAINT feed_currencies_pk PRIMARY KEY (id);


--
-- TOC entry 3150 (class 2606 OID 28129)
-- Name: feed_exchanges_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_exchanges
    ADD CONSTRAINT feed_exchanges_pk PRIMARY KEY (id);


--
-- TOC entry 3152 (class 2606 OID 28142)
-- Name: feed_historic_prices_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_historic_prices
    ADD CONSTRAINT feed_historic_prices_pk PRIMARY KEY (id);


--
-- TOC entry 3154 (class 2606 OID 28153)
-- Name: feed_index_components_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_index_components
    ADD CONSTRAINT feed_index_components_pk PRIMARY KEY (id);


--
-- TOC entry 3156 (class 2606 OID 28164)
-- Name: feed_indices_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_indices
    ADD CONSTRAINT feed_indices_pk PRIMARY KEY (id);


--
-- TOC entry 3158 (class 2606 OID 28175)
-- Name: feed_lse_security_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_lse_security
    ADD CONSTRAINT feed_lse_security_pk PRIMARY KEY (id);


--
-- TOC entry 3082 (class 2606 OID 25654)
-- Name: feed_mappings_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_mappings
    ADD CONSTRAINT feed_mappings_pk PRIMARY KEY (id);


--
-- TOC entry 3160 (class 2606 OID 28186)
-- Name: feed_nasdaq_security_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_nasdaq_security
    ADD CONSTRAINT feed_nasdaq_security_pk PRIMARY KEY (id);


--
-- TOC entry 3162 (class 2606 OID 28197)
-- Name: feed_nyse_security_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_nyse_security
    ADD CONSTRAINT feed_nyse_security_pk PRIMARY KEY (id);


--
-- TOC entry 3164 (class 2606 OID 28208)
-- Name: feed_yahoo_security_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY feed_yahoo_security
    ADD CONSTRAINT feed_yahoo_security_pk PRIMARY KEY (id);


--
-- TOC entry 3084 (class 2606 OID 25671)
-- Name: globals_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY globals
    ADD CONSTRAINT globals_pk PRIMARY KEY (id);


--
-- TOC entry 3087 (class 2606 OID 26065)
-- Name: historic_prices_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY historic_prices
    ADD CONSTRAINT historic_prices_pk PRIMARY KEY (id);


--
-- TOC entry 3180 (class 2606 OID 28521)
-- Name: historic_quotes_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY historic_quotes
    ADD CONSTRAINT historic_quotes_pk PRIMARY KEY (id);


--
-- TOC entry 3095 (class 2606 OID 26266)
-- Name: index_components_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY index_components
    ADD CONSTRAINT index_components_pk PRIMARY KEY (id);


--
-- TOC entry 3097 (class 2606 OID 26268)
-- Name: index_components_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY index_components
    ADD CONSTRAINT index_components_uk UNIQUE (product_id, index_id);


--
-- TOC entry 3076 (class 2606 OID 17344)
-- Name: indices_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY indices
    ADD CONSTRAINT indices_pk PRIMARY KEY (id);


--
-- TOC entry 3078 (class 2606 OID 17346)
-- Name: indices_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY indices
    ADD CONSTRAINT indices_uk UNIQUE (name);


--
-- TOC entry 3122 (class 2606 OID 26396)
-- Name: instrument_categories_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY instrument_categories
    ADD CONSTRAINT instrument_categories_pk PRIMARY KEY (id);


--
-- TOC entry 3124 (class 2606 OID 26398)
-- Name: instrument_categories_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY instrument_categories
    ADD CONSTRAINT instrument_categories_uk UNIQUE (name);


--
-- TOC entry 3104 (class 2606 OID 26304)
-- Name: instrument_types_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY instrument_types
    ADD CONSTRAINT instrument_types_pk PRIMARY KEY (id);


--
-- TOC entry 3106 (class 2606 OID 26306)
-- Name: instrument_types_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY instrument_types
    ADD CONSTRAINT instrument_types_uk UNIQUE (name);


--
-- TOC entry 3100 (class 2606 OID 26286)
-- Name: instruments_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY instruments
    ADD CONSTRAINT instruments_pk PRIMARY KEY (id);


--
-- TOC entry 3102 (class 2606 OID 28506)
-- Name: instruments_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY instruments
    ADD CONSTRAINT instruments_uk UNIQUE (name, instrument_type_id);


--
-- TOC entry 3172 (class 2606 OID 28405)
-- Name: madness_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY madness
    ADD CONSTRAINT madness_pk PRIMARY KEY (id);


--
-- TOC entry 3174 (class 2606 OID 28427)
-- Name: meetings_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY meetings
    ADD CONSTRAINT meetings_pk PRIMARY KEY (id);


--
-- TOC entry 3166 (class 2606 OID 28338)
-- Name: notes_pkey; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY notes
    ADD CONSTRAINT notes_pkey PRIMARY KEY (id);


--
-- TOC entry 3132 (class 2606 OID 28038)
-- Name: portfolio_positions_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY portfolio_positions
    ADD CONSTRAINT portfolio_positions_pk PRIMARY KEY (id);


--
-- TOC entry 3134 (class 2606 OID 28040)
-- Name: portfolio_positions_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY portfolio_positions
    ADD CONSTRAINT portfolio_positions_uk UNIQUE (portfolio_id, product_id);


--
-- TOC entry 3136 (class 2606 OID 28056)
-- Name: portfolios_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY portfolios
    ADD CONSTRAINT portfolios_pk PRIMARY KEY (id);


--
-- TOC entry 3138 (class 2606 OID 28508)
-- Name: portfolios_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY portfolios
    ADD CONSTRAINT portfolios_uk UNIQUE (client_id, title);


--
-- TOC entry 3108 (class 2606 OID 26322)
-- Name: products_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pk PRIMARY KEY (id);


--
-- TOC entry 3110 (class 2606 OID 28510)
-- Name: products_uk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_uk UNIQUE (name, exchange_id);


--
-- TOC entry 3112 (class 2606 OID 28512)
-- Name: products_uk1; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_uk1 UNIQUE (instrument_id, exchange_id);


--
-- TOC entry 3089 (class 2606 OID 26135)
-- Name: quotes_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY quotes
    ADD CONSTRAINT quotes_pk PRIMARY KEY (id);


--
-- TOC entry 3176 (class 2606 OID 28447)
-- Name: tickers_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY tickers
    ADD CONSTRAINT tickers_pk PRIMARY KEY (id);


--
-- TOC entry 3140 (class 2606 OID 28074)
-- Name: user_sessions_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY user_sessions
    ADD CONSTRAINT user_sessions_pk PRIMARY KEY (id);


--
-- TOC entry 3178 (class 2606 OID 28458)
-- Name: user_watchlists_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY user_watchlists
    ADD CONSTRAINT user_watchlists_pk PRIMARY KEY (id);


--
-- TOC entry 3142 (class 2606 OID 28085)
-- Name: users_pk; Type: CONSTRAINT; Schema: public; Owner: fuxiadmin; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);


--
-- TOC entry 3085 (class 1259 OID 26883)
-- Name: historic_prices_i1; Type: INDEX; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE INDEX historic_prices_i1 ON historic_prices USING btree (product_id, price_date);


--
-- TOC entry 3098 (class 1259 OID 26876)
-- Name: instruments_i1; Type: INDEX; Schema: public; Owner: fuxiadmin; Tablespace: 
--

CREATE UNIQUE INDEX instruments_i1 ON instruments USING btree (name, instrument_type_id);


--
-- TOC entry 3192 (class 2606 OID 28470)
-- Name: cash_wallet_currency_symbol_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY cash_wallet
    ADD CONSTRAINT cash_wallet_currency_symbol_fk FOREIGN KEY (currency_symbol) REFERENCES currencies(name);


--
-- TOC entry 3191 (class 2606 OID 28243)
-- Name: cash_wallet_portfolio_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY cash_wallet
    ADD CONSTRAINT cash_wallet_portfolio_id_fk FOREIGN KEY (portfolio_id) REFERENCES portfolios(id);


--
-- TOC entry 3200 (class 2606 OID 28383)
-- Name: client_watchlists_client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY client_watchlists
    ADD CONSTRAINT client_watchlists_client_id_fk FOREIGN KEY (client_id) REFERENCES clients(id);


--
-- TOC entry 3201 (class 2606 OID 28388)
-- Name: client_watchlists_portfolio_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY client_watchlists
    ADD CONSTRAINT client_watchlists_portfolio_id_fk FOREIGN KEY (portfolio_id) REFERENCES portfolios(id);


--
-- TOC entry 3193 (class 2606 OID 28258)
-- Name: clients_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 3184 (class 2606 OID 26576)
-- Name: exchanges_country_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY exchanges
    ADD CONSTRAINT exchanges_country_id_fk FOREIGN KEY (country_id) REFERENCES countries(id);


--
-- TOC entry 3183 (class 2606 OID 26571)
-- Name: exchanges_currency_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY exchanges
    ADD CONSTRAINT exchanges_currency_id_fk FOREIGN KEY (currency_id) REFERENCES currencies(id);


--
-- TOC entry 3181 (class 2606 OID 26581)
-- Name: historic_prices_product_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY historic_prices
    ADD CONSTRAINT historic_prices_product_id_fk FOREIGN KEY (product_id) REFERENCES products(id);


--
-- TOC entry 3186 (class 2606 OID 26596)
-- Name: index_components_index_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY index_components
    ADD CONSTRAINT index_components_index_id_fk FOREIGN KEY (index_id) REFERENCES indices(id);


--
-- TOC entry 3185 (class 2606 OID 26591)
-- Name: index_components_product_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY index_components
    ADD CONSTRAINT index_components_product_id_fk FOREIGN KEY (product_id) REFERENCES products(id);


--
-- TOC entry 3188 (class 2606 OID 26606)
-- Name: instruments_instrument_category_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY instruments
    ADD CONSTRAINT instruments_instrument_category_id_fk FOREIGN KEY (instrument_category_id) REFERENCES instrument_categories(id);


--
-- TOC entry 3187 (class 2606 OID 26601)
-- Name: instruments_instrument_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY instruments
    ADD CONSTRAINT instruments_instrument_type_id_fk FOREIGN KEY (instrument_type_id) REFERENCES instrument_types(id);


--
-- TOC entry 3199 (class 2606 OID 28339)
-- Name: notes_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY notes
    ADD CONSTRAINT notes_client_id_fkey FOREIGN KEY (client_id) REFERENCES clients(id);


--
-- TOC entry 3194 (class 2606 OID 28432)
-- Name: portfolio_positions_currency_symbol_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY portfolio_positions
    ADD CONSTRAINT portfolio_positions_currency_symbol_fk FOREIGN KEY (currency_symbol) REFERENCES currencies(name);


--
-- TOC entry 3195 (class 2606 OID 28288)
-- Name: portfolio_positions_portfolio_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY portfolio_positions
    ADD CONSTRAINT portfolio_positions_portfolio_id_fk FOREIGN KEY (portfolio_id) REFERENCES portfolios(id);


--
-- TOC entry 3196 (class 2606 OID 28293)
-- Name: portfolio_positions_product_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY portfolio_positions
    ADD CONSTRAINT portfolio_positions_product_id_fk FOREIGN KEY (product_id) REFERENCES products(id);


--
-- TOC entry 3197 (class 2606 OID 28298)
-- Name: portfolios_client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY portfolios
    ADD CONSTRAINT portfolios_client_id_fk FOREIGN KEY (client_id) REFERENCES clients(id);


--
-- TOC entry 3190 (class 2606 OID 28495)
-- Name: products_currency_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_currency_id_fk FOREIGN KEY (currency_id) REFERENCES currencies(id);


--
-- TOC entry 3189 (class 2606 OID 28485)
-- Name: products_instrument_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_instrument_id_fk FOREIGN KEY (instrument_id) REFERENCES instruments(id);


--
-- TOC entry 3182 (class 2606 OID 26641)
-- Name: quotes_product_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY quotes
    ADD CONSTRAINT quotes_product_id_fk FOREIGN KEY (product_id) REFERENCES products(id);


--
-- TOC entry 3198 (class 2606 OID 28318)
-- Name: user_sessions_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY user_sessions
    ADD CONSTRAINT user_sessions_user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 3202 (class 2606 OID 28500)
-- Name: user_watchlist_users_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: fuxiadmin
--

ALTER TABLE ONLY user_watchlists
    ADD CONSTRAINT user_watchlist_users_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 3318 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: fuxiadmin
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM fuxiadmin;
GRANT ALL ON SCHEMA public TO fuxiadmin;
GRANT ALL ON SCHEMA public TO PUBLIC;
GRANT ALL ON SCHEMA public TO rds_superuser WITH GRANT OPTION;


-- Completed on 2015-04-02 20:28:48

--
-- PostgreSQL database dump complete
--

