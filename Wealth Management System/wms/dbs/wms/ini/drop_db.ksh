#!/bin/ksh
#***************************************************************************
#* 
#*  Module Name      :   drop_db
#*  Author           :   S Maxwell
#*  Creation Date    :   15/1/15
#*  Synopsis         :   drops a postgress db and initial user
#* 
#*  SCCS Keywords : 
#*                  Module name create_db 
#* 
#***************************************************************************
#*             M O D I F I C A T I O N    H I S T O R Y                    *
#***************************************************************************
#*  DATE  | Author  | RDate  | Reviewer| Ver   | Reason                    *
#***************************************************************************
#* ddmmyy | nnnnnnn | ddmmyy | nnnnnnn | xx.yy | rrrrrrrrrrrrrrrrrrrrrrrrr *
#***************************************************************************


_sparam=${1}

SCRIPT=`basename $0`

_tmpfile=${TMP}/$$.tmp

cat << !! 							> ${_tmpfile}
DROP DATABASE IF EXISTS $DB_DB;

!!

lddbdba ${_sparam} -D "" ${_tmpfile}

rm -f ${_tmpfile}

