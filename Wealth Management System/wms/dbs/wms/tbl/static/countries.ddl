
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'countries', 'TABLE' )
THEN

CREATE TABLE  countries
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	currency_id		INTEGER
				NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT countries_pk PRIMARY KEY (id)
,	CONSTRAINT countries_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('countries','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE countries >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE countries >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE countries ALREADY EXISTS >>';
END IF;

END

$$;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'countries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN countries >>';
	IF COLUMN_EXISTS( 'countries', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM countries >>';
		ALTER TABLE IF EXISTS countries
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'countries', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM countries >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM countries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN countries >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'countries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN countries >>';
	IF COLUMN_EXISTS( 'countries', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM countries >>';
		ALTER TABLE IF EXISTS countries
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'countries', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM countries >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM countries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN countries >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'countries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN countries >>';
	IF COLUMN_EXISTS( 'countries', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM countries >>';
		ALTER TABLE IF EXISTS countries
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'countries', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM countries >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM countries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN countries >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'countries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN countries >>';
	IF COLUMN_EXISTS( 'countries', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM countries >>';
		ALTER TABLE IF EXISTS countries
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'countries', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM countries >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM countries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN countries >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'countries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN countries >>';
	IF COLUMN_EXISTS( 'countries', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM countries >>';
		ALTER TABLE IF EXISTS countries
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'countries', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM countries >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM countries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN countries >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'countries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN countries >>';
	IF COLUMN_EXISTS( 'countries', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM countries >>';
		ALTER TABLE IF EXISTS countries
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'countries', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM countries >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM countries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN countries >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'countries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN countries >>';
	IF COLUMN_EXISTS( 'countries', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM countries >>';
		ALTER TABLE IF EXISTS countries
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'countries', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM countries >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM countries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN countries >>';
	END IF;
END IF;

END

$$;




select	CASE
	WHEN OBJECT_EXISTS( 'countries', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "countries" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "countries" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



