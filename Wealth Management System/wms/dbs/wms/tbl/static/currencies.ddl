
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'currencies', 'TABLE' )
THEN

CREATE TABLE  currencies
(	id			SERIAL
,	name			VARCHAR(255)	NULL
,	title			VARCHAR(255)	NULL
,	nickname		VARCHAR(50)	NULL
,	minor_unit		int		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT currencies_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('currencies','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE currencies >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE currencies >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE currencies ALREADY EXISTS >>';
END IF;

END

$$;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'currencies', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN currencies >>';
	IF COLUMN_EXISTS( 'currencies', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM currencies >>';
		ALTER TABLE IF EXISTS currencies
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'currencies', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM currencies >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM currencies >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN currencies >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'currencies', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN currencies >>';
	IF COLUMN_EXISTS( 'currencies', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM currencies >>';
		ALTER TABLE IF EXISTS currencies
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'currencies', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM currencies >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM currencies >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN currencies >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'currencies', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN currencies >>';
	IF COLUMN_EXISTS( 'currencies', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM currencies >>';
		ALTER TABLE IF EXISTS currencies
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'currencies', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM currencies >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM currencies >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN currencies >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'currencies', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN currencies >>';
	IF COLUMN_EXISTS( 'currencies', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM currencies >>';
		ALTER TABLE IF EXISTS currencies
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'currencies', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM currencies >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM currencies >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN currencies >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'currencies', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN currencies >>';
	IF COLUMN_EXISTS( 'currencies', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM currencies >>';
		ALTER TABLE IF EXISTS currencies
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'currencies', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM currencies >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM currencies >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN currencies >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'currencies', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN currencies >>';
	IF COLUMN_EXISTS( 'currencies', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM currencies >>';
		ALTER TABLE IF EXISTS currencies
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'currencies', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM currencies >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM currencies >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN currencies >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'currencies', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN currencies >>';
	IF COLUMN_EXISTS( 'currencies', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM currencies >>';
		ALTER TABLE IF EXISTS currencies
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'currencies', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM currencies >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM currencies >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN currencies >>';
	END IF;
END IF;

END

$$;





select	CASE
	WHEN OBJECT_EXISTS( 'currencies', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "currencies" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "currencies" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



