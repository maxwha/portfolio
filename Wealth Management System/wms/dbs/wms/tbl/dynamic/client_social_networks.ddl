
DO $$
	
BEGIN

IF OBJECT_EXISTS( 'client_social_networks', 'TABLE' )
THEN
	RAISE NOTICE '<<< DROPPING TABLE "client_social_networks" >>';
	DROP TABLE IF EXISTS client_social_networks CASCADE;
	IF	OBJECT_EXISTS('client_social_networks','TABLE')
	THEN
		RAISE NOTICE '<<< FAILED TO DROP TABLE "client_social_networks" >>';
	ELSE
		RAISE NOTICE '<<< DROPPED TABLE "client_social_networks" >>';
	END IF;
ELSE
	RAISE NOTICE '<<< NO TABLE TO DROP "client_social_networks" >>';
END IF;

END

$$;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

