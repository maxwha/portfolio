
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'client_watchlists', 'TABLE' )
THEN

CREATE TABLE  client_watchlists
(	id				SERIAL
,	client_id			int
					NULL
,	portfolio_id			int
					NULL
,	symbol				TEXT
					NULL
,	deleted_at			timestamp	NULL
/*
-- Standard Columns
,	owner_id			integer		NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
*/
,	CONSTRAINT client_watchlists_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('client_watchlists','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE client_watchlists >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE client_watchlists >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE client_watchlists ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'client_watchlists', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "client_watchlists" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "client_watchlists" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



