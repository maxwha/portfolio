
DO $$
	
BEGIN

IF OBJECT_EXISTS( 'adviser_diaries', 'TABLE' )
THEN
	RAISE NOTICE '<<< DROPPING TABLE "adviser_diaries" >>';
	DROP TABLE IF EXISTS adviser_diaries CASCADE;
	IF	OBJECT_EXISTS('adviser_diaries','TABLE')
	THEN
		RAISE NOTICE '<<< FAILED TO DROP TABLE "adviser_diaries" >>';
	ELSE
		RAISE NOTICE '<<< DROPPED TABLE "adviser_diaries" >>';
	END IF;
ELSE
	RAISE NOTICE '<<< NO TABLE TO DROP "adviser_diaries" >>';
END IF;

END

$$;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

