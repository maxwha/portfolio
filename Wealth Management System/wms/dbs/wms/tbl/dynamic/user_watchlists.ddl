
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'user_watchlists', 'TABLE' )
THEN

CREATE TABLE  user_watchlists
(	id				SERIAL
,	symbol				TEXT
					NULL
,	user_id				int
					NULL
/*
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
*/
,	CONSTRAINT user_watchlists_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('user_watchlists','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE user_watchlists >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE user_watchlists >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE user_watchlists ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'user_watchlists', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "user_watchlists" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "user_watchlists" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



