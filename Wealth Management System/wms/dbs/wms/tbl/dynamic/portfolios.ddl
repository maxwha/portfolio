
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'portfolios', 'TABLE' )
THEN

CREATE TABLE  portfolios
(	id			SERIAL
,	name			VARCHAR(50)
				NULL
,	title			VARCHAR(50)
				NULL
,	client_id		integer
				NULL
,	nickname		VARCHAR(50)
				NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT portfolios_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('portfolios','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE portfolios >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE portfolios >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE portfolios ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolios', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN portfolios >>';
	IF COLUMN_EXISTS( 'portfolios', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM portfolios >>';
		ALTER TABLE IF EXISTS portfolios
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'portfolios', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM portfolios >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM portfolios >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN portfolios >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolios', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN portfolios >>';
	IF COLUMN_EXISTS( 'portfolios', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM portfolios >>';
		ALTER TABLE IF EXISTS portfolios
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'portfolios', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM portfolios >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM portfolios >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN portfolios >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolios', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN portfolios >>';
	IF COLUMN_EXISTS( 'portfolios', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM portfolios >>';
		ALTER TABLE IF EXISTS portfolios
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'portfolios', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM portfolios >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM portfolios >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN portfolios >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolios', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN portfolios >>';
	IF COLUMN_EXISTS( 'portfolios', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM portfolios >>';
		ALTER TABLE IF EXISTS portfolios
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'portfolios', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM portfolios >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM portfolios >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN portfolios >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolios', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN portfolios >>';
	IF COLUMN_EXISTS( 'portfolios', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM portfolios >>';
		ALTER TABLE IF EXISTS portfolios
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'portfolios', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM portfolios >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM portfolios >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN portfolios >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolios', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN portfolios >>';
	IF COLUMN_EXISTS( 'portfolios', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM portfolios >>';
		ALTER TABLE IF EXISTS portfolios
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'portfolios', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM portfolios >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM portfolios >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN portfolios >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolios', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN portfolios >>';
	IF COLUMN_EXISTS( 'portfolios', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM portfolios >>';
		ALTER TABLE IF EXISTS portfolios
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'portfolios', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM portfolios >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM portfolios >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN portfolios >>';
	END IF;
END IF;

END

$$;



select	CASE
	WHEN OBJECT_EXISTS( 'portfolios', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "portfolios" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "portfolios" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



