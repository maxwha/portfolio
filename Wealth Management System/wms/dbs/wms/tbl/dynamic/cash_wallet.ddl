
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'cash_wallet', 'TABLE' )
THEN

CREATE TABLE  cash_wallet
(	id				SERIAL
,	portfolio_id			int
					NULL
,	currency_symbol			VARCHAR(3)
					NULL
,	quantity			NUMERIC(36,8)
					NULL
/*
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
*/
,	CONSTRAINT cash_wallet_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('cash_wallet','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE cash_wallet >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE cash_wallet >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE cash_wallet ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'cash_wallet', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "cash_wallet" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "cash_wallet" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



