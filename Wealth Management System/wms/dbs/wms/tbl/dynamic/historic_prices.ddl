
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN

CREATE TABLE  historic_prices
(	id			SERIAL
,	product_id		integer
				NULL
,	price_date		timestamp	DEFAULT current_timestamp
				NULL
,	open			NUMERIC(36,8)
				NULL
,	high			NUMERIC(36,8)
				NULL
,	low			NUMERIC(36,8)
				NULL
,	close			NUMERIC(36,8)
				NULL
,	volume			NUMERIC(36,8)
				NULL
,	adj_close		NUMERIC(36,8)
				NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT historic_prices_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('historic_prices','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE historic_prices >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE historic_prices >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE historic_prices ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN historic_prices >>';
	IF COLUMN_EXISTS( 'historic_prices', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM historic_prices >>';
		ALTER TABLE IF EXISTS historic_prices
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'historic_prices', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM historic_prices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM historic_prices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN historic_prices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_from" EXISTS IN historic_prices >>';
	IF COLUMN_EXISTS( 'historic_prices', 'effective_from' )
	THEN
		RAISE NOTICE	'<<< REMOVING "effective_from" FROM historic_prices >>';
		ALTER TABLE IF EXISTS historic_prices
		DROP	effective_from
		;
		IF NOT COLUMN_EXISTS( 'historic_prices', 'effective_from' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "effective_from" FROM historic_prices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "effective_from" FROM historic_prices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "effective_from" DOES NOT EXIST IN historic_prices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_to" EXISTS IN historic_prices >>';
	IF COLUMN_EXISTS( 'historic_prices', 'effective_to' )
	THEN
		RAISE NOTICE	'<<< REMOVING "effective_to" FROM historic_prices >>';
		ALTER TABLE IF EXISTS historic_prices
		DROP	effective_to
		;
		IF NOT COLUMN_EXISTS( 'historic_prices', 'effective_to' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "effective_to" FROM historic_prices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "effective_to" FROM historic_prices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "effective_to" DOES NOT EXIST IN historic_prices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN historic_prices >>';
	IF COLUMN_EXISTS( 'historic_prices', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM historic_prices >>';
		ALTER TABLE IF EXISTS historic_prices
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'historic_prices', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM historic_prices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM historic_prices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN historic_prices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN historic_prices >>';
	IF COLUMN_EXISTS( 'historic_prices', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM historic_prices >>';
		ALTER TABLE IF EXISTS historic_prices
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'historic_prices', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM historic_prices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM historic_prices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN historic_prices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN historic_prices >>';
	IF COLUMN_EXISTS( 'historic_prices', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM historic_prices >>';
		ALTER TABLE IF EXISTS historic_prices
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'historic_prices', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM historic_prices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM historic_prices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN historic_prices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN historic_prices >>';
	IF COLUMN_EXISTS( 'historic_prices', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM historic_prices >>';
		ALTER TABLE IF EXISTS historic_prices
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'historic_prices', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM historic_prices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM historic_prices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN historic_prices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN historic_prices >>';
	IF COLUMN_EXISTS( 'historic_prices', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM historic_prices >>';
		ALTER TABLE IF EXISTS historic_prices
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'historic_prices', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM historic_prices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM historic_prices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN historic_prices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN historic_prices >>';
	IF COLUMN_EXISTS( 'historic_prices', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM historic_prices >>';
		ALTER TABLE IF EXISTS historic_prices
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'historic_prices', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM historic_prices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM historic_prices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN historic_prices >>';
	END IF;
END IF;

END

$$;


select	CASE
	WHEN OBJECT_EXISTS( 'historic_prices', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "historic_prices" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "historic_prices" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


