
DO $$
	
BEGIN

IF OBJECT_EXISTS( 'contact_diaries', 'TABLE' )
THEN
	RAISE NOTICE '<<< DROPPING TABLE "contact_diaries" >>';
	DROP TABLE IF EXISTS contact_diaries CASCADE;
	IF	OBJECT_EXISTS('contact_diaries','TABLE')
	THEN
		RAISE NOTICE '<<< FAILED TO DROP TABLE "contact_diaries" >>';
	ELSE
		RAISE NOTICE '<<< DROPPED TABLE "contact_diaries" >>';
	END IF;
ELSE
	RAISE NOTICE '<<< NO TABLE TO DROP "contact_diaries" >>';
END IF;

END

$$;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

