
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'contacts', 'TABLE' )
THEN

CREATE TABLE  contacts
(	id				SERIAL
,	name				VARCHAR(255)
					NULL
,	title				VARCHAR(255)
					NULL
,	position			VARCHAR(255)
					NULL
,	forename			VARCHAR(255)
					NULL
,	surname				VARCHAR(255)
					NULL
,	honours				VARCHAR(255)
					NULL
,	phone				VARCHAR(255)
					NULL
,	email				VARCHAR(255)
					NULL
,	address_name			VARCHAR(255)
					NULL
,	address_no			VARCHAR(255)
					NULL
,	address_street			VARCHAR(255)
					NULL
,	address_district		vARCHAR(255)
					NULL
,	address_town			VARCHAR(255)
					NULL
,	address_city			VARCHAR(255)
					NULL
,	address_county			VARCHAR(255)
					NULL
,	address_country			VARCHAR(255)
					NULL
,	address_postcode		VARCHAR(255)
					NULL
,	client_id			INT
					NULL
,	sequence			int		default 0
					null
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
,	CONSTRAINT contacts_pk PRIMARY KEY (id)
,	CONSTRAINT contacts_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('contacts','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE contacts >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE contacts >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE contacts ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'contacts', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "contacts" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "contacts" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



