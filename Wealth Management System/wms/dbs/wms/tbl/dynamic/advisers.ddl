
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'advisers', 'TABLE' )
THEN

CREATE TABLE  ADVISERS
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	forenames		VARCHAR(255)
				NULL
,	surname			VARCHAR(255)
				NULL
,	phone			VARCHAR(255)
				NULL
,	email			VARCHAR(255)
				NULL
,	managed_by_id		INT
				NULL
,	user_id			INT
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT advisers_pk PRIMARY KEY (id)
,	CONSTRAINT advisers_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('advisers','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE advisers >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE advisers >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE advisers ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'advisers', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "advisers" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "advisers" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



