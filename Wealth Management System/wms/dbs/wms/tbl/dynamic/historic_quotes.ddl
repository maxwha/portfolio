
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN

CREATE TABLE  historic_quotes
(	id			SERIAL
,	product_id		INT
				NULL
,	latest_value		NUMERIC(36,8)
				NULL
,	open			NUMERIC(36,8)
				NULL
,	high			NUMERIC(36,8)
				NULL
,	low			NUMERIC(36,8)
				NULL
,	close			NUMERIC(36,8)
				NULL
,	quote_date		timestamp	DEFAULT current_timestamp
				NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT historic_quotes_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('historic_quotes','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE historic_quotes >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE historic_quotes >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE historic_quotes ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN historic_quotes >>';
	IF COLUMN_EXISTS( 'historic_quotes', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM historic_quotes >>';
		ALTER TABLE IF EXISTS historic_quotes
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'historic_quotes', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM historic_quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM historic_quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN historic_quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_from" EXISTS IN historic_quotes >>';
	IF COLUMN_EXISTS( 'historic_quotes', 'effective_from' )
	THEN
		RAISE NOTICE	'<<< REMOVING "effective_from" FROM historic_quotes >>';
		ALTER TABLE IF EXISTS historic_quotes
		DROP	effective_from
		;
		IF NOT COLUMN_EXISTS( 'historic_quotes', 'effective_from' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "effective_from" FROM historic_quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "effective_from" FROM historic_quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "effective_from" DOES NOT EXIST IN historic_quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_to" EXISTS IN historic_quotes >>';
	IF COLUMN_EXISTS( 'historic_quotes', 'effective_to' )
	THEN
		RAISE NOTICE	'<<< REMOVING "effective_to" FROM historic_quotes >>';
		ALTER TABLE IF EXISTS historic_quotes
		DROP	effective_to
		;
		IF NOT COLUMN_EXISTS( 'historic_quotes', 'effective_to' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "effective_to" FROM historic_quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "effective_to" FROM historic_quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "effective_to" DOES NOT EXIST IN historic_quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN historic_quotes >>';
	IF COLUMN_EXISTS( 'historic_quotes', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM historic_quotes >>';
		ALTER TABLE IF EXISTS historic_quotes
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'historic_quotes', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM historic_quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM historic_quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN historic_quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN historic_quotes >>';
	IF COLUMN_EXISTS( 'historic_quotes', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM historic_quotes >>';
		ALTER TABLE IF EXISTS historic_quotes
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'historic_quotes', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM historic_quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM historic_quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN historic_quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN historic_quotes >>';
	IF COLUMN_EXISTS( 'historic_quotes', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM historic_quotes >>';
		ALTER TABLE IF EXISTS historic_quotes
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'historic_quotes', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM historic_quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM historic_quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN historic_quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN historic_quotes >>';
	IF COLUMN_EXISTS( 'historic_quotes', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM historic_quotes >>';
		ALTER TABLE IF EXISTS historic_quotes
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'historic_quotes', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM historic_quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM historic_quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN historic_quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN historic_quotes >>';
	IF COLUMN_EXISTS( 'historic_quotes', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM historic_quotes >>';
		ALTER TABLE IF EXISTS historic_quotes
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'historic_quotes', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM historic_quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM historic_quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN historic_quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN historic_quotes >>';
	IF COLUMN_EXISTS( 'historic_quotes', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM historic_quotes >>';
		ALTER TABLE IF EXISTS historic_quotes
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'historic_quotes', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM historic_quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM historic_quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN historic_quotes >>';
	END IF;
END IF;

END

$$;




select	CASE
	WHEN OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "historic_quotes" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "historic_quotes" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



