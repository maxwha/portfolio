
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'madness', 'TABLE' )
THEN

CREATE TABLE  madness
(	id				SERIAL
,	data				TEXT
					NULL
,	updated_at			timestamp	DEFAULT current_timestamp
					NULL
/*
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
*/
,	CONSTRAINT madness_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('madness','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE madness >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE madness >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE madness ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'madness', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "madness" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "madness" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



