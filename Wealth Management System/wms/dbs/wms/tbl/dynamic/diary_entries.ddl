
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'diary_entries', 'TABLE' )
THEN

CREATE TABLE  diary_entries
(	id			SERIAL
,	diary_id		INT
				NULL
,	subject			VARCHAR(255)
				NULL
,	location		VARCHAR(255)
				NULL
,	start_time		TIMESTAMP
				NULL
,	end_time		TIMESTAMP
				NULL
,	details			TEXT
				NULL
,	notes			TEXT
				NULL
,	user_id			INT
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT diary_entries_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('diary_entries','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE diary_entries >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE diary_entries >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE diary_entries ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'diary_entries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "client_id" IN diary_entries >>';
	IF NOT COLUMN_EXISTS( 'diary_entries', 'client_id' )
	THEN
		RAISE NOTICE	'<<< ADDING "client_id" TO diary_entries >>';
		ALTER TABLE IF EXISTS diary_entries
		ADD	client_id		INTEGER	NULL
		;
		IF COLUMN_EXISTS( 'diary_entries', 'client_id' )
		THEN
			RAISE NOTICE	'<<< ADDED "client_id" TO diary_entries >>';
		ELSE
			RAISE NOTICE	'<<< FAILED TO ADD "client_id" TO diary_entries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "client_id" EXISTS IN diary_entries >>';
	END IF;
END IF;

END

$$;


select	CASE
	WHEN OBJECT_EXISTS( 'diary_entries', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "diary_entries" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "diary_entries" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



