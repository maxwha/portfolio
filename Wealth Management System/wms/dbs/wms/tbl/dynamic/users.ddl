
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'users', 'TABLE' )
THEN

CREATE TABLE  users
(	id				SERIAL
,	email				TEXT
					NULL
,	first_name			TEXT
					NULL
,	last_name			TEXT
					NULL
,	password			TEXT
					NULL
,	is_super_user			boolean
					NULL
,	settings			TEXT
					NULL
,	suspended_at			timestamp
					NULL
,	image				TEXT
					NULL
/*
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
*/
,	CONSTRAINT users_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('users','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE users >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE users >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE users ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'users', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "suspended_at" EXISTS IN users >>';
	IF COLUMN_EXISTS( 'users', 'suspended_at' )
	THEN
		RAISE NOTICE	'<<< RENAMING "suspended_at" TO "deleted_at" IN users >>';
		ALTER TABLE users RENAME COLUMN suspended_at TO deleted_at ;
	ELSE
		RAISE NOTICE	'<<< COLUMN "suspended_at" DOES NOT EXIST IN users >>';
	END IF;
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'users', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "users" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "users" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



