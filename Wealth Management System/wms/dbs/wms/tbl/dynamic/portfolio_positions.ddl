
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN

CREATE TABLE  portfolio_positions
(	id			SERIAL
,	portfolio_id		INT
				NULL
,	product_id		INT
				NULL
,	currency_symbol		VARCHAR(3)
				NULL
,	quantity		NUMERIC( 36,8 )
				NULL
,	price			NUMERIC( 36,8 )
				NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT portfolio_positions_pk PRIMARY KEY (id)
,	CONSTRAINT portfolio_positions_uk UNIQUE (portfolio_id, product_id)
);
	IF	OBJECT_EXISTS('portfolio_positions','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE portfolio_positions >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE portfolio_positions >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE portfolio_positions ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "currency_id" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'currency_id' )
	AND COLUMN_EXISTS( 'portfolio_positions', 'currency' )
	THEN
		RAISE NOTICE	'<<< UPDATING "currency_id" IN portfolio_positions >>';
		UPDATE	portfolio_positions
		SET	currency_id		=	get_currencies_by_nickname(currency)
		;
	ELSE
		RAISE NOTICE	'<<< EITHER "currency_id" OR "currency" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;

DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "currency" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'currency' )
	THEN
		RAISE NOTICE	'<<< REMOVING "currency" FROM portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		DROP	currency
		;
		IF NOT COLUMN_EXISTS( 'portfolio_positions', 'currency' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "currency" FROM portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "currency" FROM portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "currency" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;

DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "currencies_id" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'currencies_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "currencies_id" FROM portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		DROP	currencies_id
		;
		IF NOT COLUMN_EXISTS( 'portfolio_positions', 'currencies_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "currencies_id" FROM portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "currencies_id" FROM portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "currencies_id" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "quantity" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'quantity' )
	THEN
		RAISE NOTICE	'<<< ALTERING "quantity" IN portfolio_positions >>';
		ALTER TABLE portfolio_positions ALTER COLUMN quantity TYPE NUMERIC(38,8) ;
	ELSE
		RAISE NOTICE	'<<< COLUMN "quantity" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "currency" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'currency' )
	THEN
		RAISE NOTICE	'<<< RENAMING "currency" TO "currency_symbol" IN portfolio_positions >>';
		ALTER TABLE portfolio_positions RENAME COLUMN currency TO currency_symbol ;
	ELSE
		RAISE NOTICE	'<<< COLUMN "currency" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;

DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "currency_symbol" EXISTS IN portfolio_positions >>';
	IF NOT COLUMN_EXISTS( 'portfolio_positions', 'currency_symbol' )
	THEN
		RAISE NOTICE	'<<< ADDING "currency_symbol" TO portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		ADD	currency_symbol		VARCHAR(255)	NULL
		;
		IF COLUMN_EXISTS( 'portfolio_positions', 'currency_symbol' )
		THEN
			RAISE NOTICE	'<<< SUCCESS ADDING "currency_symbol" TO portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE ADDING "currency_symbol" TO portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "currency_symbol" EXISTS IN portfolio_positions >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'portfolio_positions', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'portfolio_positions', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'portfolio_positions', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'portfolio_positions', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'portfolio_positions', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'portfolio_positions', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN portfolio_positions >>';
	IF COLUMN_EXISTS( 'portfolio_positions', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM portfolio_positions >>';
		ALTER TABLE IF EXISTS portfolio_positions
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'portfolio_positions', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM portfolio_positions >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM portfolio_positions >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN portfolio_positions >>';
	END IF;
END IF;

END

$$;




select	CASE
	WHEN OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "portfolio_positions" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "portfolio_positions" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



