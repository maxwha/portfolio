
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'meetings', 'TABLE' )
THEN

CREATE TABLE  meetings
(	id				SERIAL
,	client_id			int
					NULL
,	type				VARCHAR(255)
					NULL
,	subject				TEXT
					NULL
,	notes				TEXT
					NULL
,	start_date			timestamp
					NULL
,	end_date			timestamp
					NULL
,	deleted_at			timestamp
					NULL
/*
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
*/
,	CONSTRAINT meetings_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('meetings','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE meetings >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE meetings >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE meetings ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'meetings', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "meetings" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "meetings" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



