
DO $$
	
BEGIN

IF OBJECT_EXISTS( 'client_diaries', 'TABLE' )
THEN
	RAISE NOTICE '<<< DROPPING TABLE "client_diaries" >>';
	DROP TABLE IF EXISTS client_diaries CASCADE;
	IF	OBJECT_EXISTS('client_diaries','TABLE')
	THEN
		RAISE NOTICE '<<< FAILED TO DROP TABLE "client_diaries" >>';
	ELSE
		RAISE NOTICE '<<< DROPPED TABLE "client_diaries" >>';
	END IF;
ELSE
	RAISE NOTICE '<<< NO TABLE TO DROP "client_diaries" >>';
END IF;

END

$$;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

