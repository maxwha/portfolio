
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'user_sessions', 'TABLE' )
THEN

CREATE TABLE  user_sessions
(	id				SERIAL
,	user_id				int
					NULL
,	created_at			timestamp DEFAULT current_timestamp
					NULL
,	updated_at			timestamp DEFAULT current_timestamp
					NULL
,	expired_at			timestamp
					NULL
/*
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
*/
,	CONSTRAINT user_sessions_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('user_sessions','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE user_sessions >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE user_sessions >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE user_sessions ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'user_sessions', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "user_sessions" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "user_sessions" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



