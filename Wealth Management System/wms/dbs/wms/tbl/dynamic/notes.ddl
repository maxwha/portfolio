
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'notes', 'TABLE' )
THEN

CREATE TABLE  notes
(	id			SERIAL
,	client_id		INT
				NULL
,	title			TEXT
				NULL
,	note			TEXT
				NULL
,	deleted_at		TIMESTAMP
				NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT notes_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('notes','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE notes >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE notes >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE notes ALREADY EXISTS >>';
END IF;

END

$$;


select	CASE
	WHEN OBJECT_EXISTS( 'notes', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "notes" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "notes" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



