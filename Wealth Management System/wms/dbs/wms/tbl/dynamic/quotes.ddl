
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'quotes', 'TABLE' )
THEN

CREATE TABLE  quotes
(	id			SERIAL
,	product_id		INTEGER
				NULL
,	latest_value		NUMERIC(36,8)
				NULL
,	open			NUMERIC(36,8)
				NULL
,	high			NUMERIC(36,8)
				NULL
,	low			NUMERIC(36,8)
				NULL
,	mid			NUMERIC(36,8)
				NULL
,	close			NUMERIC(36,8)
				NULL
,	bid			NUMERIC(36,8)
				NULL
,	offer			NUMERIC(36,8)
				NULL
,	quote_date		timestamp	DEFAULT current_timestamp
				NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT quotes_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('quotes','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE quotes >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE quotes >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE quotes ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN quotes >>';
	IF COLUMN_EXISTS( 'quotes', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM quotes >>';
		ALTER TABLE IF EXISTS quotes
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'quotes', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN quotes >>';
	IF COLUMN_EXISTS( 'quotes', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM quotes >>';
		ALTER TABLE IF EXISTS quotes
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'quotes', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN quotes >>';
	IF COLUMN_EXISTS( 'quotes', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM quotes >>';
		ALTER TABLE IF EXISTS quotes
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'quotes', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN quotes >>';
	IF COLUMN_EXISTS( 'quotes', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM quotes >>';
		ALTER TABLE IF EXISTS quotes
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'quotes', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN quotes >>';
	IF COLUMN_EXISTS( 'quotes', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM quotes >>';
		ALTER TABLE IF EXISTS quotes
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'quotes', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN quotes >>';
	IF COLUMN_EXISTS( 'quotes', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM quotes >>';
		ALTER TABLE IF EXISTS quotes
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'quotes', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN quotes >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'quotes', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN quotes >>';
	IF COLUMN_EXISTS( 'quotes', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM quotes >>';
		ALTER TABLE IF EXISTS quotes
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'quotes', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM quotes >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM quotes >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN quotes >>';
	END IF;
END IF;

END

$$;




select	CASE
	WHEN OBJECT_EXISTS( 'quotes', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "quotes" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "quotes" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



