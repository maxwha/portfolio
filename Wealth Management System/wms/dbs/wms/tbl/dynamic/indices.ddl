
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'indices', 'TABLE' )
THEN

CREATE TABLE  indices
(	id			SERIAL
,	name			VARCHAR(255)	NULL
,	title			VARCHAR(255)	NULL
,	nickname		VARCHAR(50)	NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT indices_pk PRIMARY KEY (id)
,	CONSTRAINT indices_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('indices','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE indices >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE indices >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE indices ALREADY EXISTS >>';
END IF;

END

$$;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN indices >>';
	IF COLUMN_EXISTS( 'indices', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM indices >>';
		ALTER TABLE IF EXISTS indices
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'indices', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM indices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN indices >>';
	END IF;
END IF;

END

$$;


/*
DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_from" EXISTS IN indices >>';
	IF COLUMN_EXISTS( 'indices', 'effective_from' )
	THEN
		RAISE NOTICE	'<<< REMOVING "effective_from" FROM indices >>';
		ALTER TABLE IF EXISTS indices
		DROP	effective_from
		;
		IF NOT COLUMN_EXISTS( 'indices', 'effective_from' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "effective_from" FROM indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "effective_from" FROM indices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "effective_from" DOES NOT EXIST IN indices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_to" EXISTS IN indices >>';
	IF COLUMN_EXISTS( 'indices', 'effective_to' )
	THEN
		RAISE NOTICE	'<<< REMOVING "effective_to" FROM indices >>';
		ALTER TABLE IF EXISTS indices
		DROP	effective_to
		;
		IF NOT COLUMN_EXISTS( 'indices', 'effective_to' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "effective_to" FROM indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "effective_to" FROM indices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "effective_to" DOES NOT EXIST IN indices >>';
	END IF;
END IF;

END

$$;
*/


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN indices >>';
	IF COLUMN_EXISTS( 'indices', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM indices >>';
		ALTER TABLE IF EXISTS indices
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'indices', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM indices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN indices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN indices >>';
	IF COLUMN_EXISTS( 'indices', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM indices >>';
		ALTER TABLE IF EXISTS indices
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'indices', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM indices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN indices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN indices >>';
	IF COLUMN_EXISTS( 'indices', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM indices >>';
		ALTER TABLE IF EXISTS indices
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'indices', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM indices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN indices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN indices >>';
	IF COLUMN_EXISTS( 'indices', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM indices >>';
		ALTER TABLE IF EXISTS indices
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'indices', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM indices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN indices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN indices >>';
	IF COLUMN_EXISTS( 'indices', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM indices >>';
		ALTER TABLE IF EXISTS indices
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'indices', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM indices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN indices >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN indices >>';
	IF COLUMN_EXISTS( 'indices', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM indices >>';
		ALTER TABLE IF EXISTS indices
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'indices', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM indices >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN indices >>';
	END IF;
END IF;

END

$$;

DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_from" IN indices >>';
	IF NOT COLUMN_EXISTS( 'indices', 'effective_from' )
	THEN
		ALTER TABLE IF EXISTS indices
		ADD	effective_from		TIMESTAMP	NULL
		;
		IF COLUMN_EXISTS( 'indices', 'effective_from' )
		THEN
			RAISE NOTICE	'<<< ADDED effective_from TO indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING effective_from TO indices >>';
		END IF;
	END IF;
END IF;

END

$$;

DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'indices', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_to" IN indices >>';
	IF NOT COLUMN_EXISTS( 'indices', 'effective_to' )
	THEN
		ALTER TABLE IF EXISTS indices
		ADD	effective_to		TIMESTAMP	NULL
		;
		IF COLUMN_EXISTS( 'indices', 'effective_to' )
		THEN
			RAISE NOTICE	'<<< ADDED effective_to TO indices >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING effective_to TO indices >>';
		END IF;
	END IF;
END IF;

END

$$;



select	CASE
	WHEN OBJECT_EXISTS( 'indices', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "indices" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "indices" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



