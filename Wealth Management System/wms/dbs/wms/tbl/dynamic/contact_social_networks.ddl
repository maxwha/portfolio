
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'contact_social_networks', 'TABLE' )
THEN

CREATE TABLE  contact_social_networks
(	id				SERIAL
,	contact_id			INT
					NULL
,	social_network_id		INT
					NULL
,	identifier			VARCHAR(255)
					NULL
,	sequence			INT		DEFAULT 0
					NULL
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
,	CONSTRAINT contact_social_networks_pk PRIMARY KEY (id)
,	CONSTRAINT contact_social_networks_uk UNIQUE (contact_id,social_network_id,identifier)
);
	IF	OBJECT_EXISTS('contact_social_networks','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE contact_social_networks >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE contact_social_networks >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE contact_social_networks ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'contact_social_networks', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "contact_social_networks" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "contact_social_networks" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



