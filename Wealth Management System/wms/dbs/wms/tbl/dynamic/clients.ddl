
DO $$
	
BEGIN

IF	NOT OBJECT_EXISTS( 'clients', 'TABLE' )
THEN

CREATE TABLE  clients
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	forename		VARCHAR(255)
				NULL
,	surname			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	shortname		VARCHAR(50)
				NULL
,	adviser_id		INT
				NULL
,	date_of_birth		DATE
				NULL
,	ni_number		VARCHAR(10)
				NULL
,	telephone		TEXT
				NULL
,	email			TEXT
				NULL
,	image_id		TEXT
				NULL
--,	contact_id		int
--				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT clients_pk PRIMARY KEY (id)
,	CONSTRAINT clients_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('clients','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE clients >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE clients >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE clients ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "date_of_birth" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'date_of_birth' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	date_of_birth		DATE	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'date_of_birth' )
		THEN
			RAISE NOTICE	'<<< ADDED date_of_birth TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING date_of_birth TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "ni_number" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'ni_number' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	ni_number		VARCHAR(10)	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'ni_number' )
		THEN
			RAISE NOTICE	'<<< ADDED ni_number TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING ni_number TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "image_id" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'image_id' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	image_id		VARCHAR(10)	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'image_id' )
		THEN
			RAISE NOTICE	'<<< ADDED image_id TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING image_id TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "first_name" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'first_name' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	first_name		TEXT	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'first_name' )
		THEN
			RAISE NOTICE	'<<< ADDED first_name TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING first_name TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "last_name" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'last_name' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	last_name		TEXT	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'last_name' )
		THEN
			RAISE NOTICE	'<<< ADDED last_name TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING last_name TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "national_insurance_number" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'national_insurance_number' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	national_insurance_number		TEXT	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'national_insurance_number' )
		THEN
			RAISE NOTICE	'<<< ADDED national_insurance_number TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING national_insurance_number TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "telephone" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'telephone' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	telephone		TEXT	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'telephone' )
		THEN
			RAISE NOTICE	'<<< ADDED telephone TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING telephone TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "email" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'email' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	email		TEXT	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'email' )
		THEN
			RAISE NOTICE	'<<< ADDED email TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING email TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;

DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "deleted_at" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'deleted_at' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	deleted_at		DATE	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'deleted_at' )
		THEN
			RAISE NOTICE	'<<< ADDED deleted_at TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING deleted_at TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "user_id" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'user_id' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	user_id		INT	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'user_id' )
		THEN
			RAISE NOTICE	'<<< ADDED user_id TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING user_id TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "custom_fields" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'custom_fields' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	custom_fields		TEXT	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'custom_fields' )
		THEN
			RAISE NOTICE	'<<< ADDED custom_fields TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING custom_fields TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "image" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'image' )
	THEN
		RAISE NOTICE	'<<< ALTERING "image" IN clients >>';
		ALTER TABLE clients ALTER COLUMN image TYPE TEXT ;
	ELSE
		RAISE NOTICE	'<<< COLUMN "image" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;




DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'clients', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_from" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'effective_from' )
	THEN
		RAISE NOTICE	'<<< REMOVING "effective_from" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	effective_from
		;
		IF NOT COLUMN_EXISTS( 'clients', 'effective_from' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "effective_from" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "effective_from" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "effective_from" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "effective_to" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'effective_to' )
	THEN
		RAISE NOTICE	'<<< REMOVING "effective_to" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	effective_to
		;
		IF NOT COLUMN_EXISTS( 'clients', 'effective_to' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "effective_to" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "effective_to" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "effective_to" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'clients', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'clients', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'clients', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'clients', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'clients', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'clients', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "forename" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'forename' )
	THEN
		RAISE NOTICE	'<<< REMOVING "forename" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	forename
		;
		IF NOT COLUMN_EXISTS( 'clients', 'forename' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "forename" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "forename" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "forename" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "surname" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'surname' )
	THEN
		RAISE NOTICE	'<<< REMOVING "surname" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	surname
		;
		IF NOT COLUMN_EXISTS( 'clients', 'surname' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "surname" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "surname" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "surname" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "title" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'title' )
	THEN
		RAISE NOTICE	'<<< REMOVING "title" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	title
		;
		IF NOT COLUMN_EXISTS( 'clients', 'title' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "title" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "title" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "title" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "shortname" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'shortname' )
	THEN
		RAISE NOTICE	'<<< REMOVING "shortname" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	shortname
		;
		IF NOT COLUMN_EXISTS( 'clients', 'shortname' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "shortname" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "shortname" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "shortname" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "adviser_id" EXISTS IN clients >>';
	IF COLUMN_EXISTS( 'clients', 'adviser_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "adviser_id" FROM clients >>';
		ALTER TABLE IF EXISTS clients
		DROP	adviser_id
		;
		IF NOT COLUMN_EXISTS( 'clients', 'adviser_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "adviser_id" FROM clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "adviser_id" FROM clients >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "adviser_id" DOES NOT EXIST IN clients >>';
	END IF;
END IF;

END

$$;




select	CASE
	WHEN OBJECT_EXISTS( 'clients', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "clients" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "clients" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



/*
DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "contact_id" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'contact_id' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	contact_id		VARCHAR(10)	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'contact_id' )
		THEN
			RAISE NOTICE	'<<< ADDED contact_id TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING contact_id TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;

*/

