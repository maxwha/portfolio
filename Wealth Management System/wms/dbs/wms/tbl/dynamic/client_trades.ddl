
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'client_trades', 'TABLE' )
THEN

CREATE TABLE  client_trades
(	id			SERIAL
,	portfolio_id		INT
				NULL
,	product_id		INT
				NULL
,	buy_sell		VARCHAR(1)
				NULL
,	quantity		NUMERIC( 36,8 )
				NULL
,	currency_id		INT
				NULL
,	price			NUMERIC( 36,8 )
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT client_trades_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('client_trades','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE client_trades >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE client_trades >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE client_trades ALREADY EXISTS >>';
END IF;

END

$$;


select	CASE
	WHEN OBJECT_EXISTS( 'client_trades', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "client_trades" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "client_trades" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



