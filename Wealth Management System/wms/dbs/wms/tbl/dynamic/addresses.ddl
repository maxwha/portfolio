
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'addresses', 'TABLE' )
THEN

CREATE TABLE  addresses
(	id				SERIAL
,	client_id			int
					NULL
,	line_1				TEXT
					NULL
,	line_2				TEXT
					NULL
,	city				TEXT
					NULL
,	postcode			TEXT
					NULL
/*
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
*/
,	CONSTRAINT addresses_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('addresses','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE addresses >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE addresses >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE addresses ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'addresses', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "addresses" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "addresses" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



