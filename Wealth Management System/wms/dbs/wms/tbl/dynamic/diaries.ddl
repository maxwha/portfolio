
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'diaries', 'TABLE' )
THEN

CREATE TABLE  diaries
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	adviser_id		INT
				NULL
,	user_id			INT
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT diaries_pk PRIMARY KEY (id)
,	CONSTRAINT diaries_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('diaries','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE diaries >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE diaries >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE diaries ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'diaries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "adviser_id" IN diaries >>';
	IF NOT COLUMN_EXISTS( 'diaries', 'adviser_id' )
	THEN
		RAISE NOTICE	'<<< ADDING "adviser_id" TO diaries >>';
		ALTER TABLE IF EXISTS diaries
		ADD	adviser_id		INTEGER	NULL
		;
		IF COLUMN_EXISTS( 'diaries', 'adviser_id' )
		THEN
			RAISE NOTICE	'<<< ADDED "adviser_id" TO diaries >>';
		ELSE
			RAISE NOTICE	'<<< FAILED TO ADD "adviser_id" TO diaries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "adviser_id" EXISTS IN diaries >>';
	END IF;
END IF;

END

$$;




select	CASE
	WHEN OBJECT_EXISTS( 'diaries', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "diaries" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "diaries" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



