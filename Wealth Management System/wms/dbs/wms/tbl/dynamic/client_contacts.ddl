/*
 *	client_contacts
 *
 *	table no longer required , client_id added to contacts
 *
 */

DO $$
	
BEGIN

IF OBJECT_EXISTS( 'client_contacts', 'TABLE' )
THEN
	DROP	TABLE	client_contacts	CASCADE;

	IF	OBJECT_EXISTS('client_contacts','TABLE')
	THEN
		RAISE NOTICE	'<<< FAILURE DROPPING TABLE client_contacts >>';
	ELSE
		RAISE NOTICE	'<<< SUCCESS DROPPING TABLE client_contacts >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE client_contacts DOES NOT EXISTS >>';
END IF;

END

$$;


/*
 *	NO LONGER REQUIRED
 *
 

DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'client_contacts', 'TABLE' )
THEN

CREATE TABLE  client_contacts
(	id				SERIAL
,	client_id			INT
					NULL
,	contact_id			INT
					NULL
,	sequence			INT		DEFAULT 0
					NULL
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
,	CONSTRAINT client_contacts_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('client_contacts','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE client_contacts >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE client_contacts >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE client_contacts ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'client_contacts', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "client_contacts" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "client_contacts" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


*/

