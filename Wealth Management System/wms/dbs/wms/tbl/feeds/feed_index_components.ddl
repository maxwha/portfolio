DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_index_components', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_index_components CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_index_components', 'TABLE' )
	THEN
	CREATE TABLE feed_index_components
	(	id			SERIAL
	,	index_name		VARCHAR(255)	NULL
	,	product_symbol_type	VARCHAR(255)	NULL
	,	product_symbol		VARCHAR(255)	NULL
	,	product_name		VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_index_components_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_index_components','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_index_components >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_index_components >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_index_components ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_index_components', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_index_components" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_index_components" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

