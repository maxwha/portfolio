DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_historic_prices', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_historic_prices CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_historic_prices', 'TABLE' )
	THEN
	CREATE TABLE feed_historic_prices
	(	id			SERIAL
	,	symbol			VARCHAR(255)	NULL
	,	product_id		VARCHAR(255)	NULL
	,	price_date		VARCHAR(255)	NULL
	,	open			VARCHAR(255)	NULL
	,	high			VARCHAR(255)	NULL
	,	low			VARCHAR(255)	NULL
	,	close			VARCHAR(255)	NULL
	,	volume			VARCHAR(255)	NULL
	,	adj_close		VARCHAR(255)	NULL
	,	CONSTRAINT feed_historic_prices_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_historic_prices','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_historic_prices >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_historic_prices >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_historic_prices ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_historic_prices', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_historic_prices" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_historic_prices" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

