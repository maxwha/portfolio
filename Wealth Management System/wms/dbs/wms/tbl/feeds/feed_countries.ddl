DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_countries', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_countries CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_countries', 'TABLE' )
	THEN
	CREATE TABLE feed_countries
	(	id			SERIAL
	,	country			VARCHAR(255)	NULL
	,	iso_code		VARCHAR(255)	NULL
	,	CONSTRAINT feed_countries_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_countries','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_countries >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_countries >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_countries ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_countries', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_countries" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_countries" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

