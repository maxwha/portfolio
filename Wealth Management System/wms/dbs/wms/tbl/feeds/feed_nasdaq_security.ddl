DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_nasdaq_security', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_nasdaq_security CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_nasdaq_security', 'TABLE' )
	THEN
	CREATE TABLE feed_nasdaq_security
	(	id			SERIAL
	,	symbol			VARCHAR(255)	NULL
	,	name			VARCHAR(255)	NULL
	,	lastsale		VARCHAR(255)	NULL
	,	marketcap		VARCHAR(255)	NULL
	,	ipoyear			VARCHAR(255)	NULL
	,	sector			VARCHAR(255)	NULL
	,	industry		VARCHAR(255)	NULL
	,	summaryquote		VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_nasdaq_security_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_nasdaq_security','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_nasdaq_security >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_nasdaq_security >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_nasdaq_security ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_nasdaq_security', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_nasdaq_security" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_nasdaq_security" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

