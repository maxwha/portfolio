DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_indices', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_indices CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_indices', 'TABLE' )
	THEN
	CREATE TABLE feed_indices
	(	id			SERIAL
	,	index_name		VARCHAR(255)	NULL
	,	index_title		VARCHAR(255)	NULL
	,	start_date		VARCHAR(255)	NULL
	,	end_date		VARCHAR(255)	NULL
	,	frequency		VARCHAR(255)	NULL
	,	latest_update		VARCHAR(255)	NULL
	,	CONSTRAINT feed_indices_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_indices','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_indices >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_indices >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_indices ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_indices', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_indices" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_indices" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

