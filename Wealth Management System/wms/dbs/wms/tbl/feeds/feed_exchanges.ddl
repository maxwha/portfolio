DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_exchanges', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_exchanges CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_exchanges', 'TABLE' )
	THEN
	CREATE TABLE feed_exchanges
	(	id			SERIAL
	,	country			VARCHAR(255)	NULL
	,	exchange		VARCHAR(255)	NULL
	,	symbol			VARCHAR(255)	NULL
	,	ticker_suffix		VARCHAR(255)	NULL
	,	ticker			VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_exchanges_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_exchanges','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_exchanges >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_exchanges >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_exchanges ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_exchanges', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_exchanges" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_exchanges" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

