DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_lse_security', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_lse_security CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_lse_security', 'TABLE' )
	THEN
	CREATE TABLE feed_lse_security
	(	id			SERIAL
	,	symbol			VARCHAR(255)	NULL
	,	name			VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_lse_security_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_lse_security','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_lse_security >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_lse_security >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_lse_security ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_lse_security', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_lse_security" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_lse_security" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

