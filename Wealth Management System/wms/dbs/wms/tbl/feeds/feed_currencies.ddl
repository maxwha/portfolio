DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_currencies', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_currencies CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_currencies', 'TABLE' )
	THEN
	CREATE TABLE feed_currencies
	(	id			SERIAL
	,	country_name		VARCHAR(255)	NULL
	,	country_title		VARCHAR(255)	NULL
	,	entity			VARCHAR(255)	NULL
	,	country_code		VARCHAR(255)	NULL
	,	currency_title		VARCHAR(255)	NULL
	,	alpha_code		VARCHAR(255)	NULL
	,	num_code		VARCHAR(255)	NULL
	,	minor_unit		VARCHAR(10)	NULL
	,	minor_unit_int		int		NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_currencies_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_currencies','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_currencies >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_currencies >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_currencies ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_currencies', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_currencies" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_currencies" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

