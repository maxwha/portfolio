
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'social_networks', 'TABLE' )
THEN

CREATE TABLE  social_networks
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	website			VARCHAR(255)
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT social_networks_pk PRIMARY KEY (id)
,	CONSTRAINT social_networks_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('social_networks','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE social_networks >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE social_networks >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE social_networks ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'social_networks', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "social_networks" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "social_networks" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



