
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN

CREATE TABLE  exchanges
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	symbol			VARCHAR(50)
				NULL
,	currency_id		INTEGER
				NULL
,	country_id		INTEGER
				NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT exchanges_pk PRIMARY KEY (id)
,	CONSTRAINT exchanges_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('exchanges','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE exchanges >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE exchanges >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE exchanges ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "ticker_suffix" IN exchanges >>';
	IF NOT COLUMN_EXISTS( 'exchanges', 'ticker_suffix' )
	THEN
		ALTER TABLE IF EXISTS exchanges
		ADD	ticker_suffix		VARCHAR(10)	NULL
		;
		IF COLUMN_EXISTS( 'exchanges', 'ticker_suffix' )
		THEN
			RAISE NOTICE	'<<< ADDED ticker_suffix TO exchanges >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING ticker_suffix TO exchanges >>';
		END IF;
	END IF;
END IF;

END

$$;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN exchanges >>';
	IF COLUMN_EXISTS( 'exchanges', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM exchanges >>';
		ALTER TABLE IF EXISTS exchanges
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'exchanges', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM exchanges >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM exchanges >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN exchanges >>';
	END IF;
END IF;

END

$$;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN exchanges >>';
	IF COLUMN_EXISTS( 'exchanges', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM exchanges >>';
		ALTER TABLE IF EXISTS exchanges
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'exchanges', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM exchanges >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM exchanges >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN exchanges >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN exchanges >>';
	IF COLUMN_EXISTS( 'exchanges', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM exchanges >>';
		ALTER TABLE IF EXISTS exchanges
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'exchanges', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM exchanges >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM exchanges >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN exchanges >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN exchanges >>';
	IF COLUMN_EXISTS( 'exchanges', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM exchanges >>';
		ALTER TABLE IF EXISTS exchanges
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'exchanges', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM exchanges >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM exchanges >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN exchanges >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN exchanges >>';
	IF COLUMN_EXISTS( 'exchanges', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM exchanges >>';
		ALTER TABLE IF EXISTS exchanges
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'exchanges', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM exchanges >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM exchanges >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN exchanges >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN exchanges >>';
	IF COLUMN_EXISTS( 'exchanges', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM exchanges >>';
		ALTER TABLE IF EXISTS exchanges
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'exchanges', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM exchanges >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM exchanges >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN exchanges >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN exchanges >>';
	IF COLUMN_EXISTS( 'exchanges', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM exchanges >>';
		ALTER TABLE IF EXISTS exchanges
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'exchanges', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM exchanges >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM exchanges >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN exchanges >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "ticker_suffix" EXISTS IN exchanges >>';
	IF COLUMN_EXISTS( 'exchanges', 'ticker_suffix' )
	THEN
		RAISE NOTICE	'<<< REMOVING "ticker_suffix" FROM exchanges >>';
		ALTER TABLE IF EXISTS exchanges
		DROP	ticker_suffix
		;
		IF NOT COLUMN_EXISTS( 'exchanges', 'ticker_suffix' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "ticker_suffix" FROM exchanges >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "ticker_suffix" FROM exchanges >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "ticker_suffix" DOES NOT EXIST IN exchanges >>';
	END IF;
END IF;

END

$$;





select	CASE
	WHEN OBJECT_EXISTS( 'exchanges', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "exchanges" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "exchanges" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



