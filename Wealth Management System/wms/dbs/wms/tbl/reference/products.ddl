
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'products', 'TABLE' )
THEN

CREATE TABLE  products
(	id			SERIAL
,	exchange_id		INT
				NULL
,	instrument_id		INT
				NULL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	symbol			VARCHAR(50)
				NULL
,	currency_id		int
				NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT products_pk PRIMARY KEY (id)
--,	CONSTRAINT products_uk UNIQUE (name)
--,	CONSTRAINT products_uk1 UNIQUE (exchange_id,instrument_id)
);
	IF	OBJECT_EXISTS('products','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE products >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE products >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE products ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'products', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN products >>';
	IF COLUMN_EXISTS( 'products', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM products >>';
		ALTER TABLE IF EXISTS products
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'products', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM products >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM products >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN products >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'products', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN products >>';
	IF COLUMN_EXISTS( 'products', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM products >>';
		ALTER TABLE IF EXISTS products
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'products', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM products >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM products >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN products >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'products', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN products >>';
	IF COLUMN_EXISTS( 'products', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM products >>';
		ALTER TABLE IF EXISTS products
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'products', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM products >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM products >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN products >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'products', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN products >>';
	IF COLUMN_EXISTS( 'products', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM products >>';
		ALTER TABLE IF EXISTS products
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'products', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM products >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM products >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN products >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'products', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN products >>';
	IF COLUMN_EXISTS( 'products', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM products >>';
		ALTER TABLE IF EXISTS products
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'products', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM products >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM products >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN products >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'products', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN products >>';
	IF COLUMN_EXISTS( 'products', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM products >>';
		ALTER TABLE IF EXISTS products
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'products', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM products >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM products >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN products >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'products', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN products >>';
	IF COLUMN_EXISTS( 'products', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM products >>';
		ALTER TABLE IF EXISTS products
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'products', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM products >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM products >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN products >>';
	END IF;
END IF;

END

$$;



select	CASE
	WHEN OBJECT_EXISTS( 'products', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "products" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "products" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



