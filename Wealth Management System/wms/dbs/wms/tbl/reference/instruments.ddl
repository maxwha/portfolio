
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN

CREATE TABLE  instruments
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	symbol			VARCHAR(50)
				NULL
,	currency		VARCHAR(3)
				NULL
,	instrument_type_id	INT
				NULL
,	instrument_category_id	INT
				NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT instruments_pk PRIMARY KEY (id)
--,	CONSTRAINT instruments_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('instruments','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE instruments >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE instruments >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE instruments ALREADY EXISTS >>';
END IF;

END

$$;

DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "currency_id" EXISTS IN instruments >>';
	IF NOT COLUMN_EXISTS( 'instruments', 'currency_id' )
	THEN
		RAISE NOTICE	'<<< ADDING "currency_id" TO instruments >>';
		ALTER TABLE IF EXISTS instruments
		ADD	currency_id		INTEGER	NULL
		;
		IF COLUMN_EXISTS( 'instruments', 'currency_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS ADDING "currency_id" TO instruments >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE ADDING "currency_id" TO instruments >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "currency_id" EXISTS IN instruments >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "currency_id" EXISTS IN instruments >>';
	IF COLUMN_EXISTS( 'instruments', 'currency_id' )
	AND COLUMN_EXISTS( 'instruments', 'currency' )
	THEN
		RAISE NOTICE	'<<< UPDATING "currency_id" IN instruments >>';
		UPDATE	instruments
		SET	currency_id		=	get_currencies_by_nickname(currency)
		;
	ELSE
		RAISE NOTICE	'<<< EITHER "currency_id" OR "currency" DOES NOT EXIST IN instruments >>';
	END IF;
END IF;

END

$$;

DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "currency" EXISTS IN instruments >>';
	IF COLUMN_EXISTS( 'instruments', 'currency' )
	THEN
		RAISE NOTICE	'<<< REMOVING "currency" FROM instruments >>';
		ALTER TABLE IF EXISTS instruments
		DROP	currency
		;
		IF NOT COLUMN_EXISTS( 'instruments', 'currency' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "currency" FROM instruments >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "currency" FROM instruments >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "currency" DOES NOT EXIST IN instruments >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN instruments >>';
	IF COLUMN_EXISTS( 'instruments', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM instruments >>';
		ALTER TABLE IF EXISTS instruments
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'instruments', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM instruments >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM instruments >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN instruments >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN instruments >>';
	IF COLUMN_EXISTS( 'instruments', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM instruments >>';
		ALTER TABLE IF EXISTS instruments
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'instruments', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM instruments >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM instruments >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN instruments >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN instruments >>';
	IF COLUMN_EXISTS( 'instruments', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM instruments >>';
		ALTER TABLE IF EXISTS instruments
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'instruments', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM instruments >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM instruments >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN instruments >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN instruments >>';
	IF COLUMN_EXISTS( 'instruments', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM instruments >>';
		ALTER TABLE IF EXISTS instruments
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'instruments', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM instruments >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM instruments >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN instruments >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN instruments >>';
	IF COLUMN_EXISTS( 'instruments', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM instruments >>';
		ALTER TABLE IF EXISTS instruments
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'instruments', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM instruments >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM instruments >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN instruments >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN instruments >>';
	IF COLUMN_EXISTS( 'instruments', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM instruments >>';
		ALTER TABLE IF EXISTS instruments
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'instruments', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM instruments >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM instruments >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN instruments >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN instruments >>';
	IF COLUMN_EXISTS( 'instruments', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM instruments >>';
		ALTER TABLE IF EXISTS instruments
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'instruments', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM instruments >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM instruments >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN instruments >>';
	END IF;
END IF;

END

$$;




select	CASE
	WHEN OBJECT_EXISTS( 'instruments', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "instruments" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "instruments" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



