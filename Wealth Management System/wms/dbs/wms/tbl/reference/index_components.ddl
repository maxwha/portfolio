
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'index_components', 'TABLE' )
THEN

CREATE TABLE  index_components
(	id			SERIAL
,	index_id		INT
				NULL
,	product_id		INT
				NULL
,	sequence		INT		DEFAULT 0
				NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT index_components_pk PRIMARY KEY (id)
,	CONSTRAINT index_components_uk UNIQUE (product_id, index_id)
);
	IF	OBJECT_EXISTS('index_components','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE index_components >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE index_components >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE index_components ALREADY EXISTS >>';
END IF;

END

$$;




DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'index_components', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN index_components >>';
	IF COLUMN_EXISTS( 'index_components', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM index_components >>';
		ALTER TABLE IF EXISTS index_components
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'index_components', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM index_components >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM index_components >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN index_components >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'index_components', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN index_components >>';
	IF COLUMN_EXISTS( 'index_components', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM index_components >>';
		ALTER TABLE IF EXISTS index_components
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'index_components', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM index_components >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM index_components >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN index_components >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'index_components', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN index_components >>';
	IF COLUMN_EXISTS( 'index_components', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM index_components >>';
		ALTER TABLE IF EXISTS index_components
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'index_components', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM index_components >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM index_components >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN index_components >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'index_components', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN index_components >>';
	IF COLUMN_EXISTS( 'index_components', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM index_components >>';
		ALTER TABLE IF EXISTS index_components
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'index_components', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM index_components >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM index_components >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN index_components >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'index_components', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN index_components >>';
	IF COLUMN_EXISTS( 'index_components', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM index_components >>';
		ALTER TABLE IF EXISTS index_components
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'index_components', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM index_components >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM index_components >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN index_components >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'index_components', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN index_components >>';
	IF COLUMN_EXISTS( 'index_components', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM index_components >>';
		ALTER TABLE IF EXISTS index_components
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'index_components', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM index_components >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM index_components >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN index_components >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'index_components', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN index_components >>';
	IF COLUMN_EXISTS( 'index_components', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM index_components >>';
		ALTER TABLE IF EXISTS index_components
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'index_components', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM index_components >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM index_components >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN index_components >>';
	END IF;
END IF;

END

$$;



select	CASE
	WHEN OBJECT_EXISTS( 'index_components', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "index_components" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "index_components" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



