
DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'instrument_types', 'TABLE' )
THEN

CREATE TABLE  instrument_types
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT instrument_types_pk PRIMARY KEY (id)
,	CONSTRAINT instrument_types_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('instrument_types','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE instrument_types >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE instrument_types >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE instrument_types ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instrument_types', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "owner_id" EXISTS IN instrument_types >>';
	IF COLUMN_EXISTS( 'instrument_types', 'owner_id' )
	THEN
		RAISE NOTICE	'<<< REMOVING "owner_id" FROM instrument_types >>';
		ALTER TABLE IF EXISTS instrument_types
		DROP	owner_id
		;
		IF NOT COLUMN_EXISTS( 'instrument_types', 'owner_id' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "owner_id" FROM instrument_types >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "owner_id" FROM instrument_types >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "owner_id" DOES NOT EXIST IN instrument_types >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instrument_types', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "version" EXISTS IN instrument_types >>';
	IF COLUMN_EXISTS( 'instrument_types', 'version' )
	THEN
		RAISE NOTICE	'<<< REMOVING "version" FROM instrument_types >>';
		ALTER TABLE IF EXISTS instrument_types
		DROP	version
		;
		IF NOT COLUMN_EXISTS( 'instrument_types', 'version' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "version" FROM instrument_types >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "version" FROM instrument_types >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "version" DOES NOT EXIST IN instrument_types >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instrument_types', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_user" EXISTS IN instrument_types >>';
	IF COLUMN_EXISTS( 'instrument_types', 'create_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_user" FROM instrument_types >>';
		ALTER TABLE IF EXISTS instrument_types
		DROP	create_user
		;
		IF NOT COLUMN_EXISTS( 'instrument_types', 'create_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_user" FROM instrument_types >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_user" FROM instrument_types >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_user" DOES NOT EXIST IN instrument_types >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instrument_types', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "create_date" EXISTS IN instrument_types >>';
	IF COLUMN_EXISTS( 'instrument_types', 'create_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "create_date" FROM instrument_types >>';
		ALTER TABLE IF EXISTS instrument_types
		DROP	create_date
		;
		IF NOT COLUMN_EXISTS( 'instrument_types', 'create_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "create_date" FROM instrument_types >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "create_date" FROM instrument_types >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "create_date" DOES NOT EXIST IN instrument_types >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instrument_types', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_user" EXISTS IN instrument_types >>';
	IF COLUMN_EXISTS( 'instrument_types', 'update_user' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_user" FROM instrument_types >>';
		ALTER TABLE IF EXISTS instrument_types
		DROP	update_user
		;
		IF NOT COLUMN_EXISTS( 'instrument_types', 'update_user' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_user" FROM instrument_types >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_user" FROM instrument_types >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_user" DOES NOT EXIST IN instrument_types >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instrument_types', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "update_date" EXISTS IN instrument_types >>';
	IF COLUMN_EXISTS( 'instrument_types', 'update_date' )
	THEN
		RAISE NOTICE	'<<< REMOVING "update_date" FROM instrument_types >>';
		ALTER TABLE IF EXISTS instrument_types
		DROP	update_date
		;
		IF NOT COLUMN_EXISTS( 'instrument_types', 'update_date' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "update_date" FROM instrument_types >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "update_date" FROM instrument_types >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "update_date" DOES NOT EXIST IN instrument_types >>';
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'instrument_types', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "pid" EXISTS IN instrument_types >>';
	IF COLUMN_EXISTS( 'instrument_types', 'pid' )
	THEN
		RAISE NOTICE	'<<< REMOVING "pid" FROM instrument_types >>';
		ALTER TABLE IF EXISTS instrument_types
		DROP	pid
		;
		IF NOT COLUMN_EXISTS( 'instrument_types', 'pid' )
		THEN
			RAISE NOTICE	'<<< SUCCESS DROPPING "pid" FROM instrument_types >>';
		ELSE
			RAISE NOTICE	'<<< FAILURE DROPPING "pid" FROM instrument_types >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "pid" DOES NOT EXIST IN instrument_types >>';
	END IF;
END IF;

END

$$;




select	CASE
	WHEN OBJECT_EXISTS( 'instrument_types', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "instrument_types" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "instrument_types" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



