create or replace function get_t1() returns refcursor
as
$$
declare
ref	refcursor;

begin
	OPEN ref for select * from t1;
	return ref;
end;
$$
LANGUAGE PLPGSQL;


