create table t1
(	key1	serial
,	col1	varchar(20)	null
,	col2	integer		null
,	col3	numeric( 36,8)	null
,	col4	timestamp	null	default	current_timestamp
)
;


insert	into t1 ( col1, col2, col3 ) values ( 'a', 1, 1.1 );
insert	into t1 ( col1, col2, col3 ) values ( 'b', 2, 2.2 );
insert	into t1 ( col1, col2, col3 ) values ( 'c', 3, 3.3 );
insert	into t1 ( col1, col2, col3 ) values ( 'd', 4, 4.4 );
insert	into t1 ( col1, col2, col3 ) values ( 'e', 5, 5.5 );
insert	into t1 ( col1, col2, col3 ) values ( 'f', 6, 6.6 );
insert	into t1 ( col1, col2, col3 ) values ( 'g', 7, 7.7 );
insert	into t1 ( col1, col2, col3 ) values ( 'h', 8, 8.8 );
insert	into t1 ( col1, col2, col3 ) values ( 'i', 9, 9.9 );




