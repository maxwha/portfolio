#!/bin/ksh
#***************************************************************************
#*
#*  Module Name      :   get_indicies.sh
#*  Author           :   H Maxwell
#*  Creation Date    :   6/3/15
#*  Synopsis         :   
#*
#***************************************************************************
#*             M O D I F I C A T I O N    H I S T O R Y                    *
#***************************************************************************
#*  DATE  | Author  | RDate  | Reviewer| Ver   | Reason                    *
#***************************************************************************
#* ddmmyy | nnnnnnn | ddmmyy | nnnnnnn | xx.yy | rrrrrrrrrrrrrrrrrrrrrrrrr *
#***************************************************************************

here=`pwd`

typeset -i page=1
typeset -i keepOnRunning=1

_SCRIPT=`basename $0`

_FILE="${DAT}/indices.csv"
_CHUNK="${TMP}/indices.part"
_CFG="${CFG}/load_feed_indices.properties"

rm -rf "${_FILE}"
rm -rf "${_CHUNK}*"

while	[ ${keepOnRunning} -ne 0 ]
do
	url="http://www.quandl.com/api/v2/datasets.csv?query=*&source_code=YAHOO&per_page=10000&page=${page}&auth_token=Y9fxYH8Yvfjor2H27zs1"
	curl ${url} > ${_CHUNK}
	keepOnRunning=`cat ${_CHUNK} | wc -l `
	cat ${_CHUNK} >> ${_FILE}
	let page=${page}+1
done

_os=`uname -o`
if	[ "${_os}" = "Cygwin" ]
then
	_WCFG=`javapath -w ${_CFG}`
	_WFILE=`javapath -w ${_FILE}`
else
	_WCFG=${_CFG}
	_WFILE=${_FILE}
fi


JDBCLoad -f ${_WFILE} -u ${DB_USER} -p ${DB_PASS} -s ${DB_JDBC_URL} -P ${_WCFG}



