#!/bin/ksh

. /var/www/wms-api/wms-api_env/bin/activate

dt_from=`sql -t "select to_char(now()- interval '1' day, 'YYYY-MM-DD' )"`
dt_to=`sql -t "select to_char(now(), 'YYYY-MM-DD' )"`

python	/var/www/wms-api/wms-api/get_historic_prices.py "${dt_from}" "${dt_to}"


