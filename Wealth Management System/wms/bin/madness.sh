#!/bin/ksh
#***************************************************************************
#*
#*  Module Name      :   madness.sh
#*  Author           :   H Maxwell
#*  Creation Date    :   31/3/15
#*  Synopsis         :   script to call the Wealth API and initiate the madness query
#*
#***************************************************************************
#*             M O D I F I C A T I O N    H I S T O R Y                    *
#***************************************************************************
#*  DATE  | Author  | RDate  | Reviewer| Ver   | Reason                    *
#***************************************************************************
#* ddmmyy | nnnnnnn | ddmmyy | nnnnnnn | xx.yy | rrrrrrrrrrrrrrrrrrrrrrrrr *
#***************************************************************************

here=`pwd`

typeset -i page=1
typeset -i keepOnRunning=1

_SCRIPT=`basename $0`

_APIURL="http://ec2-54-149-134-31.us-west-2.compute.amazonaws.com:5010"
_APIURL="http://localhost:5010"
#_APIURL="http://178.62.18.202:1337"
_APIUSER="admin@fuxi.com"
_APIPASS="admin"


echo curl  --cookie-jar ${HOME}/.cookie_jar --data "\"username=${_APIUSER}&password=${_APIPASS}\"" ${_APIURL}/auth
eval curl  --cookie-jar ${HOME}/.cookie_jar --data "\"username=${_APIUSER}&password=${_APIPASS}\"" ${_APIURL}/auth
echo

echo curl  --cookie ${HOME}/.cookie_jar -X GET "${_APIURL}/api/regions"
eval curl  --cookie ${HOME}/.cookie_jar -X GET "${_APIURL}/api/regions"	> /dev/null
echo


cd $here


