#!/bin/bash

rm -rf  $DB_INI/initial_data.sql

PGPASSWORD=casper pg_dump --host localhost --port 5432 --username "postgres" --no-password --format plain --section data --inserts --column-inserts --verbose \
	--table "public.advisers" \
	--table "public.clients" \
	--table "public.contact_social_networks" \
	--table "public.contacts" \
	--table "public.countries" \
	--table "public.cross_references" \
	--table "public.currencies" \
	--table "public.diaries" \
	--table "public.diary_entries" \
	--table "public.exchanges" \
	--table "public.globals" \
	--table "public.historic_prices" \
	--table "public.historic_quotes" \
	--table "public.index_components" \
	--table "public.indices" \
	--table "public.instrument_categories" \
	--table "public.instrument_types" \
	--table "public.instruments" \
	--table "public.logins" \
	--table "public.lookup_codes" \
	--table "public.lookups" \
	--table "public.portfolio_positions" \
	--table "public.portfolios" \
	--table "public.products" \
	--table "public.quotes" \
	--table "public.roles" \
	--table "public.social_networks" \
	--table "public.user_roles" \
	--table "public.users" \
	--file "$DB_INI\initial_data.sql" \
	"wms"






