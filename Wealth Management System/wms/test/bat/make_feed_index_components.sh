#!/bin/bash
#
#	make_feed_index_components.sh
#
#	script to convert and index file downloaded from http://www.hl.co.uk/
#
#	http://www.hl.co.uk/shares/stock-market-summary/ftse-100
#
#	example invocation:
#
#	make_feed_index_components.sh FTSE100 EPIC ftse100_components.raw index_components.csv
#
_INDEX="${1}"
_SYMBOL="${2}"
_RAW_FILE="${3}"
_CSV_FILE="${4}"

if	[[ "${INDEX}" = ""
||	   "${INDEX}" = ""
||	   "${INDEX}" = ""
||	   "${INDEX}" = ""
	]]
then
	echo	"usage ${0} index_name symbol_type raw_file csv_file"
	exit	0
fi


echo "running... 's/^/${_INDEX},${_SYMBOL},/' ${_RAW_FILE} > ${_CSV_FILE}"
eval sed 's/^/${_INDEX},${_SYMBOL},/' ${_RAW_FILE} > ${_CSV_FILE}

exit	1

