delete from portfolio_positions
where portfolio_id in (
select id from portfolios WHERE name IN ('001', '002', '003', '004', '005', '006', '007', '008', '009', '010')
)
;

delete from portfolio_positions
where portfolio_id in (
select id FROM portfolios WHERE name IN ('P001', 'P002', 'P003', 'P004', 'P005', 'P006', 'P007', 'P008', 'P009', 'P010')
)
;

DELETE FROM portfolios WHERE name IN ('001', '002', '003', '004', '005', '006', '007', '008', '009', '010');

DELETE FROM portfolios WHERE name IN ('P001', 'P002', 'P003', 'P004', 'P005', 'P006', 'P007', 'P008', 'P009', 'P010');

INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P001', 'My First Portfolio', get_clients_id('Harry Hill'), 'First');
INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P002', 'Mega Bucks', get_clients_id('Andrew Murray'), 'Mega');
INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P003', 'Portfolio', get_clients_id('Jeremy Kyle'), 'Port');
INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P004', 'Deal With It', get_clients_id('Noel Edmonds'), 'Deal');
INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P005', 'Test Portfolio', get_clients_id('Annabelle Richards'), 'Test');
INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P006', 'Roberts Portfolio', get_clients_id('Laura Roberts'), 'Rob');
INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P007', 'Cash Money', get_clients_id('Adam Asif'), 'Cash');
INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P008', 'Miller Time', get_clients_id('Craig Miller'), 'Miller');
INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P009', 'Winter Is Coming', get_clients_id('Abigail Winters'), 'Winter');
INSERT INTO portfolios (name, title, client_id, nickname) VALUES ('P010', 'Sax Fund', get_clients_id('Lisa Smith'), 'Sax');

