DELETE FROM advisers WHERE name IN ('Harry Maxwell', 'Clive Owen', 'Duncan Smith', 'James Elliot');

INSERT INTO advisers (name, title, nickname, forenames, surname, phone, email, managed_by_id ) VALUES ('Harry Maxwell', 'Mr', 'Harry', 'Harry', 'Maxwell', '07029384777', 'harry.maxwell@hotmail.com', null );

INSERT INTO advisers (name, title, nickname, forenames, surname, phone, email, managed_by_id ) VALUES ('Clive Owen', 'Mr', 'Clive', 'Clive', 'Owen', '07567894302', 'clive.owen@yahoo.com', get_advisers_id('Harry Maxwell') );
INSERT INTO advisers (name, title, nickname, forenames, surname, phone, email, managed_by_id ) VALUES ('Duncan Smith', 'Mr', 'Duncan', 'Duncan', 'Smith', '07823459321', 'duncan.smith@live.com', get_advisers_id('Harry Maxwell') );
INSERT INTO advisers (name, title, nickname, forenames, surname, phone, email, managed_by_id ) VALUES ('James Elliot', 'Mr', 'James', 'James', 'Elliot', '07900576843', 'james.elliot@gmail.com', get_advisers_id('Harry Maxwell') );
