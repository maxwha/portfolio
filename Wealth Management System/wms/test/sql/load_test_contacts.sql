DELETE FROM contacts WHERE name IN ('Harry Hill', 'Andrew Murray', 'Jeremy Kyle', 'Noel Edmonds', 'Annabelle Richards', 'Abigail Winters', 'Laura Roberts', 'Adam Asif', 'Craig Miller', 'Lisa Smith');


INSERT INTO contacts (name, client_id, title, position, forename, surname, honours, phone, email, address_name, address_no, address_street, address_district, address_town, address_city, address_county, address_country, address_postcode)
VALUES ('Harry Hill', get_clients_id('Harry Hill'), 'Mr', 001, 'Harry', 'Hill', 'OBE', '07958463451', 'harry.hill@hotmail.com', 'Hill House', 13, 'Groove Street', 'District 9', 'Funky Town', 'Jazz City', 'Midlothian', 'Scotland', 'EH3 4JZ');

INSERT INTO contacts (name, client_id, title, position, forename, surname, honours, phone, email, address_name, address_no, address_street, address_district, address_town, address_city, address_county, address_country, address_postcode)
VALUES ('Andrew Murray', get_clients_id('Andrew Murray'), 'Mr', 002, 'Andrew', 'Murray', 'OBE', '07823457102', 'andy.murray@msn.com', 'Murray Manor', 189, 'Court 1', 'Wimbledon', 'Twickenham', 'London', 'London', 'England', 'SW19 5AE');

INSERT INTO contacts (name, client_id, title, position, forename, surname, honours, phone, email, address_name, address_no, address_street, address_district, address_town, address_city, address_county, address_country, address_postcode)
VALUES ('Jeremy Kyle', get_clients_id('Jeremy Kyle'), 'Mr', 003, 'Jeremy', 'Kyle', null, '07849506321', 'jezza.kyle@itv.co.uk', 'Studio 3', 34, 'ITV HQ', 'Westend', 'Kirkstall', 'Leeds', 'Yorkshire', 'England', 'LS3 1JS');

INSERT INTO contacts (name, client_id, title, position, forename, surname, honours, phone, email, address_name, address_no, address_street, address_district, address_town, address_city, address_county, address_country, address_postcode)
VALUES ('Noel Edmonds', get_clients_id('Noel Edmonds'), 'Mr', 004, 'Noel', 'Edmonds', null, '07785349321', 'noel.edmonds@c4.co.uk', 'Deal Or No Deal', 123, 'Channel 4', 'Eastend', 'Hamilton', 'Glasgow', 'Glasgow', 'Scotland', 'GL3 9KS');

INSERT INTO contacts (name, client_id, title, position, forename, surname, honours, phone, email, address_name, address_no, address_street, address_district, address_town, address_city, address_county, address_country, address_postcode)
VALUES ('Annabelle Richards', get_clients_id('Annabelle Richards'), 'Mrs', 005, 'Annabelle', 'Richards', null, '07785856743', 'anna.richards@hotmail.co.uk', null, 345, 'Griffin Street', 'Samual Heath', 'Bexley', 'Kent', 'Essex', 'England', 'BL4 5LG');

INSERT INTO contacts (name, client_id, title, position, forename, surname, honours, phone, email, address_name, address_no, address_street, address_district, address_town, address_city, address_county, address_country, address_postcode)
VALUES ('Abigail Winters', get_clients_id('Abigail Winters'), 'Miss', 006, 'Abigail', 'Winters', null, '07719827345', 'gail.winters@msn.com', null, 823, 'Simple Street', 'Hillend', 'Lyon', 'Lyon', 'South', 'France', 'LY01 4LH');

INSERT INTO contacts (name, client_id, title, position, forename, surname, honours, phone, email, address_name, address_no, address_street, address_district, address_town, address_city, address_county, address_country, address_postcode)
VALUES ('Laura Roberts', get_clients_id('Laura Roberts'), 'Miss', 007, 'Laura', 'Roberts', null, '07834457210', 'laura.roberts@gmail.co.uk', null, 94, 'Test Street', 'Morriston', 'Craigville', 'Swansea', 'Swansea', 'England', 'SA99 1AE');

INSERT INTO contacts (name, client_id, title, position, forename, surname, honours, phone, email, address_name, address_no, address_street, address_district, address_town, address_city, address_county, address_country, address_postcode)
VALUES ('Adam Asif', get_clients_id('Adam Asif'), 'Mr', 008, 'Adam', 'Asif', null, '07029345432', 'adam.asif@gmail.com', 'Heriot-Watt', null, 'Riccarton', 'Currie', 'Edinburgh', 'Edinburgh', 'Midlothian', 'Scotland', 'EH14 5AS');

INSERT INTO contacts (name, client_id, title, position, forename, surname, honours, phone, email, address_name, address_no, address_street, address_district, address_town, address_city, address_county, address_country, address_postcode)
VALUES ('Lisa Smith', get_clients_id('Lisa Smith'), 'Mrs', 010, 'Lisa', 'Smith', null, '07912232852', 'lisa.smith@hotmail.co.uk', 'High Banks', 2, 'West Terrace', 'Cranbrook', 'Warrington', 'Blackburn', 'Darwen', 'England', 'BB2 4JX');