DELETE FROM client_trades WHERE portfolio_id IN 
	(
		001,
		002,
		003,
		004,
		005,
		006,
		007,
		008,
		009,
		010
	);
	
DELETE FROM client_trades WHERE portfolio_id IN 
	(
		get_portfolios_id('P001'), 
		get_portfolios_id('P002'), 
		get_portfolios_id('P003'), 
		get_portfolios_id('P004'), 
		get_portfolios_id('P005'), 
		get_portfolios_id('P006'), 
		get_portfolios_id('P007'), 
		get_portfolios_id('P008'), 
		get_portfolios_id('P009'), 
		get_portfolios_id('P010')
	);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P001'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 10000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P001'), get_products_id('DC.L'), 'B',get_currencies_by_nickname('GBP'), 100, 2.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P001'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 200.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P001'), get_products_id('GOOG'), 'B', get_currencies_by_nickname('GBP'), 500, 1.50);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P001'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 750.00, 1.00);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P002'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 10000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P002'), get_products_id('DC.L'), 'B', get_currencies_by_nickname('GBP'), 1000, 2.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P002'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 2000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P002'), get_products_id('WEIR.L'), 'B', get_currencies_by_nickname('GBP'), 1000, 0.50);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P002'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 500.00, 1.00);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P003'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 10000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P003'), get_products_id('DC.L'), 'B', get_currencies_by_nickname('GBP'), 5000, 2.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P003'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 10000.00, 1.00);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P004'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 10000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P004'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 1000.00, 1.00);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P005'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 75000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P005'), get_products_id('DC.L'), 'B', get_currencies_by_nickname('GBP'), 10000, 2.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P005'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 20000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P005'), get_products_id('WEIR.L'), 'B', get_currencies_by_nickname('GBP'), 50000, 0.50);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P005'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 25000.00, 1.00);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P006'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 10000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P006'), get_products_id('DC.L'), 'B', get_currencies_by_nickname('GBP'), 2000, 2.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P006'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 4000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P006'), get_products_id('WEIR.L'), 'B', get_currencies_by_nickname('GBP'), 2000, 0.50);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P006'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 1000.00, 1.00);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P007'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 1000000.00, 1.00);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P008'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 100000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P008'), get_products_id('DC.L'), 'B', get_currencies_by_nickname('GBP'), 25000, 2.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P008'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 50000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P008'), get_products_id('WEIR.L'), 'B', get_currencies_by_nickname('GBP'), 100, 0.50);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P008'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 50.00, 1.00);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P009'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 10000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P009'), get_products_id('DC.L'), 'B', get_currencies_by_nickname('GBP'), 5000, 2.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P009'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 10000.00, 1.00);

INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P010'), get_products_id('GBP'), 'B', get_currencies_by_nickname('GBP'), 600000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P010'), get_products_id('DC.L'), 'B', get_currencies_by_nickname('GBP'), 110000, 2.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P010'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 220000.00, 1.00);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P010'), get_products_id('WEIR.L'), 'B', get_currencies_by_nickname('GBP'), 300000, 0.50);
INSERT INTO client_trades (portfolio_id, product_id, buy_sell, currency_id, quantity, price) VALUES (get_portfolios_id('P010'), get_products_id('GBP'), 'S', get_currencies_by_nickname('GBP'), 150000.00, 1.00);
