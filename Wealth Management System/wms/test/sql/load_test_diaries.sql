DELETE FROM diaries WHERE name IN ('Harry Maxwell', 'Clive Owen', 'Duncan Smith', 'James Elliot');
INSERT INTO diaries (name, title, nickname, user_id, adviser_id) VALUES ('Harry Maxwell', 'My First Diary', NULL, NULL, get_advisers_id('Harry Maxwell') );
INSERT INTO diaries (name, title, nickname, user_id, adviser_id) VALUES ('Clive Owen', 'Clives Diary', NULL, NULL, get_advisers_id('Clive Owen') );
INSERT INTO diaries (name, title, nickname, user_id, adviser_id) VALUES ('Duncan Smith', 'Duncs Diary', NULL, NULL, get_advisers_id('Duncan Smith') );
INSERT INTO diaries (name, title, nickname, user_id, adviser_id) VALUES ('James Elliot', 'James Diary', NULL, NULL, get_advisers_id('James Elliot') );
