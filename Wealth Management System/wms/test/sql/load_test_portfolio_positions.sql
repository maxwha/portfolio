DELETE FROM portfolio_positions WHERE portfolio_id IN 
	( get_portfolios_id('P001')
	, get_portfolios_id('P002')
	, get_portfolios_id('P003')
	, get_portfolios_id('P004')
	, get_portfolios_id('P005')
	, get_portfolios_id('P006')
	, get_portfolios_id('P007')
	, get_portfolios_id('P008')
	, get_portfolios_id('P009')
	, get_portfolios_id('P010')
	);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P001'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 9050.00, 1.00);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P001'), get_products_id('DC.L'), get_currencies_by_nickname('GBP'), 100, 2.50);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P001'), get_products_id('GOOG'), get_currencies_by_nickname('GBP'), 500, 1.00);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P002'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 7500.00, 1.00);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P002'), get_products_id('DC.L'), get_currencies_by_nickname('GBP'), 1000, 2.50);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P002'), get_products_id('WEIR.L'), get_currencies_by_nickname('GBP'), 1000, 1.00);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P003'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 0.00, 1.00);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P003'), get_products_id('DC.L'), get_currencies_by_nickname('GBP'), 5000, 2.50);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P004'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 9000.00, 1.00);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P005'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 30000.00, 1.00);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P005'), get_products_id('DC.L'), get_currencies_by_nickname('GBP'), 10000, 2.50);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P005'), get_products_id('WEIR.L'), get_currencies_by_nickname('GBP'), 50000, 1.00);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P006'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 5000.00, 1.00);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P006'), get_products_id('DC.L'), get_currencies_by_nickname('GBP'), 2000, 2.50);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P006'), get_products_id('WEIR.L'), get_currencies_by_nickname('GBP'), 2000, 1.00);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P007'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 1000000.00, 1.00);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P008'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 74950.00, 1.00);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P008'), get_products_id('DC.L'), get_currencies_by_nickname('GBP'), 25000, 2.50);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P008'), get_products_id('WEIR.L'), get_currencies_by_nickname('GBP'), 100, 1.00);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P009'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 0.00, 1.00);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P009'), get_products_id('DC.L'), get_currencies_by_nickname('GBP'), 5000, 2.50);

INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P010'), get_products_id('GBP'), get_currencies_by_nickname('GBP'), 230000.00, 1.00);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P010'), get_products_id('DC.L'), get_currencies_by_nickname('GBP'), 110000, 2.50);
INSERT INTO portfolio_positions (portfolio_id, product_id, currency_id, quantity, price) VALUES (get_portfolios_id('P010'), get_products_id('WEIR.L'), get_currencies_by_nickname('GBP'), 300000, 1.00);

