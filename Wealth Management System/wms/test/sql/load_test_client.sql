DELETE 
FROM clients
CASCADE
WHERE name IN ('Harry Hill', 'Andrew Murray', 'Jeremy Kyle', 'Noel Edmonds', 'Annabelle Richards', 'Abigail Winters', 'Laura Roberts', 'Adam Asif', 'Craig Miller', 'Lisa Smith');

INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Harry Hill', 'Harry', 'Hill', 'Mr', 'Harry', get_advisers_id('Clive Owen'), to_timestamp( '24 Dec 1988', 'DD Mon YYYY'), 'JZ927346A', null);
INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Andrew Murray', 'Andrew', 'Murray', 'Mr', 'Andy', get_advisers_id('Clive Owen'),to_timestamp('01 Jan 1990', 'DD Mon YYY') , 'JW827362B', null);
INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Jeremy Kyle', 'Jeremy', 'Kyle', 'Mr', 'Jezza', get_advisers_id('Clive Owen'),to_timestamp('01 Jan 1980', 'DD Mon YYY'), 'JH816253A', null);
INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Noel Edmonds', 'Noel', 'Edmonds', 'Mr', 'Noel', get_advisers_id('Duncan Smith'),to_timestamp('01 Jan 1980', 'DD Mon YYY') , 'JZ527364A', null);
INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Annabelle Richards', 'Annabelle', 'Richards', 'Mrs', 'Anna', get_advisers_id('Clive Owen'),to_timestamp('10 May 1962', 'DD Mon YYY') , 'JU895687H', null);
INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Abigail Winters', 'Abigail', 'Winters', 'Miss', 'Gail',get_advisers_id('Duncan Smith'),to_timestamp('24 Jun 1958', 'DD Mon YYY') , 'JZ124527Y', null);
INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Laura Roberts', 'Laura', 'Roberts', 'Miss', 'Laura',get_advisers_id('James Elliot'),to_timestamp('06 Feb 1981', 'DD Mon YYY') , 'JS129874G', null);
INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Adam Asif', 'Adam', 'Asif', 'Mr', 'Adam',get_advisers_id('James Elliot'),to_timestamp('10 Mar 1991', 'DD Mon YYY') , 'JK274432F', null);
INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Craig Miller', 'Craig', 'Miller', 'Mr', 'Craig',get_advisers_id('James Elliot'),to_timestamp('15 Sep 1968', 'DD Mon YYY') , 'JB427645G', null);
INSERT INTO clients (name, forename, surname, title, shortname, adviser_id, date_of_birth, ni_number, image_id ) VALUES ('Lisa Smith', 'Lisa', 'Smith', 'Mrs', 'Lisa',get_advisers_id('Harry Maxwell'),to_timestamp('11 Nov 1977', 'DD Mon YYY') , 'JS736152T', null);
