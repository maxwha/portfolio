--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-04-01 05:03:06 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 3084 (class 0 OID 28077)
-- Dependencies: 220
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: fuxiadmin
--

INSERT INTO users (id, email, first_name, last_name, password, is_super_user, settings, deleted_at, image) VALUES (1, 'admin@fuxi.com', 'Mohamed', 'Sharif', '$2a$12$tTe.N07nrujGSHaldXmXZeaEzVBCxWyFqFzzXPZh8vho004GXFzoy', NULL, NULL, NULL, NULL);


--
-- TOC entry 3090 (class 0 OID 0)
-- Dependencies: 219
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fuxiadmin
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


-- Completed on 2015-04-01 05:03:06 UTC

--
-- PostgreSQL database dump complete
--

