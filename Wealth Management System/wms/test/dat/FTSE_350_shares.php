
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FTSE 350 share prices, stock market and UK share price information.</title>
<meta name="description" content="FTSE 350 share prices, stock market index constituents UK share price information." />
<meta name="keywords" content="share prices, live, share dealing, share price charts, share dealing information,constituents, market news,stock market news, share price" />

 <script SRC="http://www.livecharts.co.uk/html/membersbanners.js"></script>
 
<link href="/styles/pricestyle.php" rel="stylesheet" type="text/css" />
<!--[if IE 7]><link type="text/css" rel="stylesheet" href="/test_styles/ie7_screen.css" /><![endif]-->
<!--[if (IE 6)|(IE 5)|(IE 5.5)|(IE 4.0)]><link type="text/css" rel="stylesheet" href="/test_styles/ie_6_style_fix_shares.css" /><![endif]-->
<script type="text/javascript">
	var adnxsId = 712748;
	</script>
	<script type="text/javascript" src="http://cdn.adnxs.com/megatag.js"></script>
	
	<script src="http://www.dianomioffers.co.uk/smartads_js.epl?id=1580"></script>
	<meta name="robots" content="NOODP">
</head>
<body id="home">
 <div class="bill"></div><div class="outercont">
<div class="container">
 <div class="top">
        <h1><a href="http://www.livecharts.co.uk/"><img src="/images/live_charts_logo.jpg" alt="livecharts.co.uk" /></a></h1>
        <!--Banner BIG -->
        <div class="banner">

     <!-- <script type="text/javascript">adnxs.megatag.load({ size: '728x90', promoSizes: ['1x1'] });</script> -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Global Header 728 LC -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-7932798795997603"
     data-ad-slot="2410397437"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
     </div>
      <script>
<!--
//wbds_firstdone=true;
//-->
</script>

          <div class="date"><a style="color: #8a8a8a;text-decoration:none;" href="http://www.livecharts.co.uk/cookies.php" rel="nofollow">Cookies</a>&nbsp;|&nbsp;<a style="color: #8a8a8a;text-decoration:none;" href="http://www.livecharts.co.uk/share_map.php" title="price search">Search For A Share Price</a>&nbsp;|&nbsp;
            <script language="JavaScript"><!-- Begin
var months=new Array(13);
months[1]="January";
months[2]="February";
months[3]="March";
months[4]="April";
months[5]="May";
months[6]="June";
months[7]="July";
months[8]="August";
months[9]="September";
months[10]="October";
months[11]="November";
months[12]="December";
var time=new Date();
var lmonth=months[time.getMonth() + 1];
var date=time.getDate();
var year=time.getYear();
if (year < 2000)
year = year + 1900;
document.write(lmonth + " ");
document.write(date + ", " + year);
// End -->
</script>
        </div>
        <div class="clearfix"></div>
        <div class="nav">

                                   <ul>
                 <li><a href="http://www.livecharts.co.uk/">Home</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price.php" title="share prices">Share prices</a><img src="/images/NewSplashSmall.gif" width="12px" height="10px" border="0" /></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_watch_list.php" title="share watch list">Monitor</a><img src="/images/NewSplashSmall.gif" width="12px" height="10px" border="0" /></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/market_news.php" title="stock market news">Stock market news</a><img src="/images/NewSplashSmall.gif" width="12px" height="10px" border="0" /></li>
                <li><a href="http://www.livecharts.co.uk/share_members/threads.php" title="share chat">Share chat</a><img src="/images/NewSplashSmall.gif" width="12px" height="10px"  border="0" /></li>
                <li><a href="http://www.livecharts.co.uk/chart_menu.php" title="trading charts">Live charts</a></li>
                <li><a href="http://www.livecharts.co.uk/daily_charts/daily_charts.php" title="historical graphs">Daily charts</a></li>
                <li><a href="http://www.livecharts.co.uk/trend_signals/trend_forecast_chart_selector.php" title="market trends">Market trends</a></li>
                <li><a href="http://www.livecharts.co.uk/futures_commodities/futures_commodities.php" title="futures trading">Futures</a></li>
                <li><a href="http://www.livecharts.co.uk/ForexCharts/forexpage.php" title="forex">Forex</a></li>
                <li><a href="http://www.livecharts.co.uk/pivot_points/pivot_points.php" title="pivot points">Pivot points</a></li>
                <li><a href="http://www.livecharts.co.uk/membersarea/tradingtools/tradingtools.php" title="daytrading tools">Tools & data</a></li>
                <li><a href="http://www.livecharts.co.uk/historicaldata.php" title="historical data">Historical data</a></li>
                <li><a href="http://www.livecharts.co.uk/livewire/">Live wire</a></li>
                <li><a href="http://www.livecharts.co.uk/mobile_trading.php" title="mobile trading charts">Mobile</a></li>
                <li><a href="http://www.livecharts.co.uk/membersarea/threads.php" title="daytrading message board">Day trading chat</a></li>
<li><a href="http://www.livecharts.co.uk/economiccalendar.php" title="Economic data calendar">Economic data</a></li>
                

            </ul>
        </div>
         <!-- Language selection - copy your form here -->
        <div class="language">
                   <!-- Flag -->
           <div class="flags"> 
	

<div class="edu" style="margin-top:7px;"><a href="/trading/" title="trading markets"><img src="/images/NewSplashSmall.gif" width="12px" height="10px" border="0" alt="highlight" />
Trading Info Centre</a><br/>

</div>
		</div>
		</div>
         <div class="clearfix"></div>                                  
        <!-- Sub Navigation -->
        <div class="subnav">
            <ul> <li><a href="http://www.livecharts.co.uk/share_prices/members_features.php" title="members features">Members area</a></li>
		
							<li><a href="http://www.livecharts.co.uk/trading/share-dealing.php">Share Trading Guides</a></li>
                <li><a href="http://www.livecharts.co.uk/share_dealing_guide/share_dealing.php" title="share dealing">Share dealing</a></li>
                <li><a href="http://www.livecharts.co.uk/share_dealing_guide/investing_online.php" title="investing online">Investing online</a></li>
                <li><a href="http://www.livecharts.co.uk/share_dealing_guide/trading_shares.php" title="share trading">Share trading</a></li>

              <li><a href="http://www.livecharts.co.uk/advertise.php">Advertise!</a></li>
                <li><a rel="nofollow" href="http://www.capitalspreads.com/agentdirect.php?url=/demoregistration&agentid=247">Spread betting demo</a></li>
            </ul>
                    </div>
            <!-- Login message board link -->
            <div class="message_board"> <a href="http://www.livecharts.co.uk/share_members/threads.php">Login to share chat</a> </div>
            <!-- Add to RSS link-->
            <!--<div class="rss"> <a href="http://www.livecharts.co.uk/livewire/wp-rss2.php">Add to RSS</a> <img src="/images/rss.png" alt="RSS" /> </div> -->
    </div>
    <div class="content">
        
		<!-- Content column middle -->
        <div class="middle_col">
        <div class="mid_container">
       
			 

    <div class="fl_left">
	<div class="title_box">
    <h2 style="font-size:17px;"><strong>FTSE 350 Share Prices</strong></h2>
	
	</div>
	</div>
	<div class="clearfix"></div>
	<div class="fl_left">
			
		<a style="color: #2e47c5;" href="http://www.livecharts.co.uk/share_prices/share_price.php" title="share prices">
			<img style="padding: 1px 3px 2px 0;vertical-align: top;" src="/images/share_arrow.gif" alt="prices" />Prices
		</a>
		
		<a style="color: #2e47c5;" href="http://www.livecharts.co.uk/share_prices/resultscalendar.php" title="company results">
			<img style="padding: 1px 3px 2px 0;vertical-align: top;margin-left:5px;" src="/images/share_arrow.gif" alt="results" />Results Calendar
		</a>
		
		<a style="color: #2e47c5;" href="http://www.livecharts.co.uk/share_prices/share_chart.php" title="charts">
			<img style="padding: 1px 3px 2px 0;vertical-align: top;margin-left:5px;" src="/images/share_arrow.gif" alt="charts"/>Charts
		</a>
		
		<a style="color: #2e47c5;" href="http://www.livecharts.co.uk/share_prices/share_watch_list.php" title="personalised watch list">
			<img style="padding: 1px 3px 2px 0;vertical-align: top;margin-left:5px;" src="/images/share_arrow.gif" alt="watchlist" />Watchlist
		</a>
		
		<a style="color: #2e47c5;" href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=7" title="brokers share tips">
			<img style="padding: 1px 3px 2px 0;vertical-align: top;margin-left:5px;" src="/images/share_arrow.gif" alt="brokers tips" />Share Tips
		</a>
	</div>
	 <div class="clearfix"></div>

	<div class="news_width">
		<div class="title_news">
    <h2><strong>FTSE 350</strong></h2>
	</div>
	  <div class="clearfix"></div>
    	<div class="news_links">
	          	<table cellpadding="3" cellspacing="0" border="0" width="440px">
	<tr style="font-weight:bold">
		<td>Name</td>
		<td align="right">Price</td>
		<td align="right" nowrap="nowrap">% Chng</td>
		<td align="right">High</td>
		<td align="right">Low</td>
	</tr>
			<tr>
			<td><a title="3i Group share price III share price" href="./share_price/symbol-III">3i Group</a></td>
			<td title="III share price"align="right" style="color:blue">435.00</td>
			<td align="right" style="color:blue">1.21%</td>
				<td align="right">436.90</td>
			<td align="right">428.90</td>
		</tr>
				<tr>
			<td><a title="3i Infrastructure share price 3IN share price" href="./share_price/symbol-3IN">3i Infrastructure</a></td>
			<td title="3IN share price"align="right" style="color:blue">152.80</td>
			<td align="right" style="color:blue">0.20%</td>
				<td align="right">152.90</td>
			<td align="right">151.50</td>
		</tr>
				<tr>
			<td><a title="Aberdeen Asset Management share price ADN share price" href="./share_price/symbol-ADN">Aberdeen Asset Management</a></td>
			<td title="ADN share price"align="right" style="color:red">416.90</td>
			<td align="right" style="color:red">-0.14%</td>
				<td align="right">418.10</td>
			<td align="right">412.40</td>
		</tr>
				<tr>
			<td><a title="Aberforth Smaller Companies Trust share price ASL share price" href="./share_price/symbol-ASL">Aberforth Smaller Companies Trust</a></td>
			<td title="ASL share price"align="right" style="color:">1,068.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">1,071.25</td>
			<td align="right">1,058.79</td>
		</tr>
				<tr>
			<td><a title="Acacia Mining share price ACA share price" href="./share_price/symbol-ACA">Acacia Mining</a></td>
			<td title="ACA share price"align="right" style="color:red">293.40</td>
			<td align="right" style="color:red">-0.20%</td>
				<td align="right">304.90</td>
			<td align="right">290.10</td>
		</tr>
				<tr>
			<td><a title="Admiral Group share price ADM share price" href="./share_price/symbol-ADM">Admiral Group</a></td>
			<td title="ADM share price"align="right" style="color:blue">1,415.00</td>
			<td align="right" style="color:blue">3.44%</td>
				<td align="right">1,417.00</td>
			<td align="right">1,366.00</td>
		</tr>
				<tr>
			<td><a title="Aegis Group share price AGS share price" href="./share_price/symbol-AGS">Aegis Group</a></td>
			<td title="AGS share price"align="right" style="color:">239.80</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">240.00</td>
			<td align="right">239.70</td>
		</tr>
				<tr>
			<td><a title="Afren share price AFR share price" href="./share_price/symbol-AFR">Afren</a></td>
			<td title="AFR share price"align="right" style="color:red">26.34</td>
			<td align="right" style="color:red">-3.55%</td>
				<td align="right">28.95</td>
			<td align="right">25.19</td>
		</tr>
				<tr>
			<td><a title="African Barrick Gold share price ABG share price" href="./share_price/symbol-ABG">African Barrick Gold</a></td>
			<td title="ABG share price"align="right" style="color:red">235.20</td>
			<td align="right" style="color:red">-0.47%</td>
				<td align="right">239.10</td>
			<td align="right">232.20</td>
		</tr>
				<tr>
			<td><a title="Aggreko share price AGK share price" href="./share_price/symbol-AGK">Aggreko</a></td>
			<td title="AGK share price"align="right" style="color:blue">1,550.00</td>
			<td align="right" style="color:blue">2.45%</td>
				<td align="right">1,556.00</td>
			<td align="right">1,509.00</td>
		</tr>
				<tr>
			<td><a title="AL Noor Hospitals Group share price ANH share price" href="./share_price/symbol-ANH">AL Noor Hospitals Group</a></td>
			<td title="ANH share price"align="right" style="color:blue">964.50</td>
			<td align="right" style="color:blue">1.26%</td>
				<td align="right">975.00</td>
			<td align="right">954.50</td>
		</tr>
				<tr>
			<td><a title="Alent share price ALNT share price" href="./share_price/symbol-ALNT">Alent</a></td>
			<td title="ALNT share price"align="right" style="color:blue">374.40</td>
			<td align="right" style="color:blue">1.19%</td>
				<td align="right">377.90</td>
			<td align="right">360.00</td>
		</tr>
				<tr>
			<td><a title="Alliance Trust share price ATST share price" href="./share_price/symbol-ATST">Alliance Trust</a></td>
			<td title="ATST share price"align="right" style="color:blue">483.80</td>
			<td align="right" style="color:blue">1.28%</td>
				<td align="right">483.80</td>
			<td align="right">479.30</td>
		</tr>
				<tr>
			<td><a title="Amec share price AMEC share price" href="./share_price/symbol-AMEC">Amec</a></td>
			<td title="AMEC share price"align="right" style="color:red">1,058.00</td>
			<td align="right" style="color:red">-2.13%</td>
				<td align="right">1,093.00</td>
			<td align="right">1,048.00</td>
		</tr>
				<tr>
			<td><a title="Amec Foster Wheeler share price AMFW share price" href="./share_price/symbol-AMFW">Amec Foster Wheeler</a></td>
			<td title="AMFW share price"align="right" style="color:red">789.50</td>
			<td align="right" style="color:red">-0.75%</td>
				<td align="right">796.50</td>
			<td align="right">775.50</td>
		</tr>
				<tr>
			<td><a title="Amlin share price AML share price" href="./share_price/symbol-AML">Amlin</a></td>
			<td title="AML share price"align="right" style="color:blue">480.00</td>
			<td align="right" style="color:blue">0.21%</td>
				<td align="right">481.00</td>
			<td align="right">477.90</td>
		</tr>
				<tr>
			<td><a title="Anglo American share price AAL share price" href="./share_price/symbol-AAL">Anglo American</a></td>
			<td title="AAL share price"align="right" style="color:blue">1,141.00</td>
			<td align="right" style="color:blue">0.09%</td>
				<td align="right">1,154.50</td>
			<td align="right">1,131.00</td>
		</tr>
				<tr>
			<td><a title="Antofagasta share price ANTO share price" href="./share_price/symbol-ANTO">Antofagasta</a></td>
			<td title="ANTO share price"align="right" style="color:red">702.50</td>
			<td align="right" style="color:red">-3.44%</td>
				<td align="right">729.00</td>
			<td align="right">701.50</td>
		</tr>
				<tr>
			<td><a title="AO World share price AO. share price" href="./share_price/symbol-AO.">AO World</a></td>
			<td title="AO. share price"align="right" style="color:red">260.70</td>
			<td align="right" style="color:red">-0.69%</td>
				<td align="right">266.91</td>
			<td align="right">260.00</td>
		</tr>
				<tr>
			<td><a title="ARM Holdings share price ARM share price" href="./share_price/symbol-ARM">ARM Holdings</a></td>
			<td title="ARM share price"align="right" style="color:red">988.00</td>
			<td align="right" style="color:red">-1.00%</td>
				<td align="right">992.50</td>
			<td align="right">974.72</td>
		</tr>
				<tr>
			<td><a title="Ashmore Group share price ASHM share price" href="./share_price/symbol-ASHM">Ashmore Group</a></td>
			<td title="ASHM share price"align="right" style="color:blue">266.80</td>
			<td align="right" style="color:blue">1.06%</td>
				<td align="right">273.10</td>
			<td align="right">257.00</td>
		</tr>
				<tr>
			<td><a title="Ashtead Group share price AHT share price" href="./share_price/symbol-AHT">Ashtead Group</a></td>
			<td title="AHT share price"align="right" style="color:red">1,077.00</td>
			<td align="right" style="color:red">-3.58%</td>
				<td align="right">1,116.00</td>
			<td align="right">1,071.00</td>
		</tr>
				<tr>
			<td><a title="Associated British Foods share price ABF share price" href="./share_price/symbol-ABF">Associated British Foods</a></td>
			<td title="ABF share price"align="right" style="color:blue">3,134.00</td>
			<td align="right" style="color:blue">0.87%</td>
				<td align="right">3,144.00</td>
			<td align="right">3,089.00</td>
		</tr>
				<tr>
			<td><a title="AstraZeneca share price AZN share price" href="./share_price/symbol-AZN">AstraZeneca</a></td>
			<td title="AZN share price"align="right" style="color:blue">4,716.50</td>
			<td align="right" style="color:blue">0.94%</td>
				<td align="right">4,754.00</td>
			<td align="right">4,651.00</td>
		</tr>
				<tr>
			<td><a title="Atkins (WS) share price ATK share price" href="./share_price/symbol-ATK">Atkins (WS)</a></td>
			<td title="ATK share price"align="right" style="color:blue">1,347.00</td>
			<td align="right" style="color:blue">0.52%</td>
				<td align="right">1,355.75</td>
			<td align="right">1,335.00</td>
		</tr>
				<tr>
			<td><a title="Aveva Group share price AVV share price" href="./share_price/symbol-AVV">Aveva Group</a></td>
			<td title="AVV share price"align="right" style="color:red">1,282.00</td>
			<td align="right" style="color:red">-1.23%</td>
				<td align="right">1,306.00</td>
			<td align="right">1,278.00</td>
		</tr>
				<tr>
			<td><a title="Aviva share price AV. share price" href="./share_price/symbol-AV.">Aviva</a></td>
			<td title="AV. share price"align="right" style="color:blue">491.00</td>
			<td align="right" style="color:blue">1.43%</td>
				<td align="right">491.20</td>
			<td align="right">479.84</td>
		</tr>
				<tr>
			<td><a title="Babcock International Group share price BAB share price" href="./share_price/symbol-BAB">Babcock International Group</a></td>
			<td title="BAB share price"align="right" style="color:red">1,018.00</td>
			<td align="right" style="color:red">-0.20%</td>
				<td align="right">1,044.00</td>
			<td align="right">1,014.00</td>
		</tr>
				<tr>
			<td><a title="BAE Systems share price BA. share price" href="./share_price/symbol-BA.">BAE Systems</a></td>
			<td title="BA. share price"align="right" style="color:blue">476.60</td>
			<td align="right" style="color:blue">1.36%</td>
				<td align="right">476.90</td>
			<td align="right">468.20</td>
		</tr>
				<tr>
			<td><a title="Balfour Beatty share price BBY share price" href="./share_price/symbol-BBY">Balfour Beatty</a></td>
			<td title="BBY share price"align="right" style="color:blue">206.10</td>
			<td align="right" style="color:blue">0.44%</td>
				<td align="right">209.40</td>
			<td align="right">204.50</td>
		</tr>
				<tr>
			<td><a title="Bank of Georgia Holdings share price BGEO share price" href="./share_price/symbol-BGEO">Bank of Georgia Holdings</a></td>
			<td title="BGEO share price"align="right" style="color:red">2,026.00</td>
			<td align="right" style="color:red">-2.92%</td>
				<td align="right">2,110.00</td>
			<td align="right">2,005.00</td>
		</tr>
				<tr>
			<td><a title="Bankers Inv Trust share price BNKR share price" href="./share_price/symbol-BNKR">Bankers Inv Trust</a></td>
			<td title="BNKR share price"align="right" style="color:blue">589.50</td>
			<td align="right" style="color:blue">1.38%</td>
				<td align="right">589.50</td>
			<td align="right">583.03</td>
		</tr>
				<tr>
			<td><a title="Barclays share price BARC share price" href="./share_price/symbol-BARC">Barclays</a></td>
			<td title="BARC share price"align="right" style="color:blue">233.60</td>
			<td align="right" style="color:blue">0.95%</td>
				<td align="right">234.90</td>
			<td align="right">229.65</td>
		</tr>
				<tr>
			<td><a title="Barr (A.G.) share price BAG share price" href="./share_price/symbol-BAG">Barr (A.G.)</a></td>
			<td title="BAG share price"align="right" style="color:blue">586.50</td>
			<td align="right" style="color:blue">0.60%</td>
				<td align="right">595.00</td>
			<td align="right">581.00</td>
		</tr>
				<tr>
			<td><a title="Barratt Developments share price BDEV share price" href="./share_price/symbol-BDEV">Barratt Developments</a></td>
			<td title="BDEV share price"align="right" style="color:blue">430.00</td>
			<td align="right" style="color:blue">1.51%</td>
				<td align="right">437.00</td>
			<td align="right">420.40</td>
		</tr>
				<tr>
			<td><a title="BBA Aviation share price BBA share price" href="./share_price/symbol-BBA">BBA Aviation</a></td>
			<td title="BBA share price"align="right" style="color:blue">355.80</td>
			<td align="right" style="color:blue">0.23%</td>
				<td align="right">356.50</td>
			<td align="right">349.00</td>
		</tr>
				<tr>
			<td><a title="Beazley share price BEZ share price" href="./share_price/symbol-BEZ">Beazley</a></td>
			<td title="BEZ share price"align="right" style="color:red">278.20</td>
			<td align="right" style="color:red">-0.43%</td>
				<td align="right">281.36</td>
			<td align="right">277.30</td>
		</tr>
				<tr>
			<td><a title="Bellway share price BWY share price" href="./share_price/symbol-BWY">Bellway</a></td>
			<td title="BWY share price"align="right" style="color:blue">1,775.00</td>
			<td align="right" style="color:blue">0.85%</td>
				<td align="right">1,795.00</td>
			<td align="right">1,747.00</td>
		</tr>
				<tr>
			<td><a title="Berendsen share price BRSN share price" href="./share_price/symbol-BRSN">Berendsen</a></td>
			<td title="BRSN share price"align="right" style="color:blue">1,107.00</td>
			<td align="right" style="color:blue">0.91%</td>
				<td align="right">1,109.00</td>
			<td align="right">1,090.00</td>
		</tr>
				<tr>
			<td><a title="Berkeley Group Holdings (The) share price BKG share price" href="./share_price/symbol-BKG">Berkeley Group Holdings (The)</a></td>
			<td title="BKG share price"align="right" style="color:blue">2,359.00</td>
			<td align="right" style="color:blue">1.94%</td>
				<td align="right">2,366.00</td>
			<td align="right">2,302.92</td>
		</tr>
				<tr>
			<td><a title="Betfair Group share price BET share price" href="./share_price/symbol-BET">Betfair Group</a></td>
			<td title="BET share price"align="right" style="color:red">1,481.00</td>
			<td align="right" style="color:red">-3.71%</td>
				<td align="right">1,543.00</td>
			<td align="right">1,471.00</td>
		</tr>
				<tr>
			<td><a title="BG Group share price BG. share price" href="./share_price/symbol-BG.">BG Group</a></td>
			<td title="BG. share price"align="right" style="color:blue">820.70</td>
			<td align="right" style="color:blue">2.59%</td>
				<td align="right">832.10</td>
			<td align="right">785.20</td>
		</tr>
				<tr>
			<td><a title="BH Macro Ltd. GBP Shares share price BHMG share price" href="./share_price/symbol-BHMG">BH Macro Ltd. GBP Shares</a></td>
			<td title="BHMG share price"align="right" style="color:">2,033.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">2,034.99</td>
			<td align="right">2,026.00</td>
		</tr>
				<tr>
			<td><a title="BHP Billiton share price BLT share price" href="./share_price/symbol-BLT">BHP Billiton</a></td>
			<td title="BLT share price"align="right" style="color:blue">1,354.50</td>
			<td align="right" style="color:blue">0.26%</td>
				<td align="right">1,377.50</td>
			<td align="right">1,332.84</td>
		</tr>
				<tr>
			<td><a title="Big Yellow Group share price BYG share price" href="./share_price/symbol-BYG">Big Yellow Group</a></td>
			<td title="BYG share price"align="right" style="color:red">591.50</td>
			<td align="right" style="color:red">-1.00%</td>
				<td align="right">597.98</td>
			<td align="right">578.50</td>
		</tr>
				<tr>
			<td><a title="BlackRock World Mining Trust share price BRWM share price" href="./share_price/symbol-BRWM">BlackRock World Mining Trust</a></td>
			<td title="BRWM share price"align="right" style="color:blue">315.10</td>
			<td align="right" style="color:blue">0.67%</td>
				<td align="right">317.60</td>
			<td align="right">312.00</td>
		</tr>
				<tr>
			<td><a title="BlueBay Asset Management share price BBAY share price" href="./share_price/symbol-BBAY">BlueBay Asset Management</a></td>
			<td title="BBAY share price"align="right" style="color:">483.80</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">483.80</td>
			<td align="right">483.80</td>
		</tr>
				<tr>
			<td><a title="BlueCrest AllBlue Fund Ltd. GBP Shares share price BABS share price" href="./share_price/symbol-BABS">BlueCrest AllBlue Fund Ltd. GBP Shares</a></td>
			<td title="BABS share price"align="right" style="color:red">185.60</td>
			<td align="right" style="color:red">-0.16%</td>
				<td align="right">186.30</td>
			<td align="right">185.00</td>
		</tr>
				<tr>
			<td><a title="Bodycote share price BOY share price" href="./share_price/symbol-BOY">Bodycote</a></td>
			<td title="BOY share price"align="right" style="color:blue">648.00</td>
			<td align="right" style="color:blue">2.86%</td>
				<td align="right">656.00</td>
			<td align="right">644.00</td>
		</tr>
				<tr>
			<td><a title="Booker Group share price BOK share price" href="./share_price/symbol-BOK">Booker Group</a></td>
			<td title="BOK share price"align="right" style="color:blue">163.30</td>
			<td align="right" style="color:blue">2.00%</td>
				<td align="right">164.20</td>
			<td align="right">159.01</td>
		</tr>
				<tr>
			<td><a title="Bovis Homes Group share price BVS share price" href="./share_price/symbol-BVS">Bovis Homes Group</a></td>
			<td title="BVS share price"align="right" style="color:blue">834.50</td>
			<td align="right" style="color:blue">2.08%</td>
				<td align="right">836.50</td>
			<td align="right">811.00</td>
		</tr>
				<tr>
			<td><a title="BP share price BP. share price" href="./share_price/symbol-BP.">BP</a></td>
			<td title="BP. share price"align="right" style="color:red">395.70</td>
			<td align="right" style="color:red">-0.21%</td>
				<td align="right">398.18</td>
			<td align="right">389.00</td>
		</tr>
				<tr>
			<td><a title="Brewin Dolphin Holdings share price BRW share price" href="./share_price/symbol-BRW">Brewin Dolphin Holdings</a></td>
			<td title="BRW share price"align="right" style="color:blue">290.20</td>
			<td align="right" style="color:blue">0.10%</td>
				<td align="right">294.80</td>
			<td align="right">286.30</td>
		</tr>
				<tr>
			<td><a title="Brit Insurance Holdings NV (DI) share price BRE share price" href="./share_price/symbol-BRE">Brit Insurance Holdings NV (DI)</a></td>
			<td title="BRE share price"align="right" style="color:">1,075.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">1,080.00</td>
			<td align="right">1,071.00</td>
		</tr>
				<tr>
			<td><a title="British Airways share price BAY share price" href="./share_price/symbol-BAY">British Airways</a></td>
			<td title="BAY share price"align="right" style="color:">275.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">328.08</td>
			<td align="right">275.00</td>
		</tr>
				<tr>
			<td><a title="British American Tobacco share price BATS share price" href="./share_price/symbol-BATS">British American Tobacco</a></td>
			<td title="BATS share price"align="right" style="color:blue">3,583.00</td>
			<td align="right" style="color:blue">1.73%</td>
				<td align="right">3,586.00</td>
			<td align="right">3,508.00</td>
		</tr>
				<tr>
			<td><a title="British Empire Securities & General Trust share price BTEM share price" href="./share_price/symbol-BTEM">British Empire Securities & General Trust</a></td>
			<td title="BTEM share price"align="right" style="color:blue">518.00</td>
			<td align="right" style="color:blue">0.39%</td>
				<td align="right">522.50</td>
			<td align="right">516.00</td>
		</tr>
				<tr>
			<td><a title="British Land Co share price BLND share price" href="./share_price/symbol-BLND">British Land Co</a></td>
			<td title="BLND share price"align="right" style="color:red">783.50</td>
			<td align="right" style="color:red">-0.06%</td>
				<td align="right">791.00</td>
			<td align="right">780.00</td>
		</tr>
				<tr>
			<td><a title="British Sky Broadcasting Group share price BSY share price" href="./share_price/symbol-BSY">British Sky Broadcasting Group</a></td>
			<td title="BSY share price"align="right" style="color:red">850.50</td>
			<td align="right" style="color:red">-1.39%</td>
				<td align="right">858.00</td>
			<td align="right">840.50</td>
		</tr>
				<tr>
			<td><a title="Britvic share price BVIC share price" href="./share_price/symbol-BVIC">Britvic</a></td>
			<td title="BVIC share price"align="right" style="color:blue">681.00</td>
			<td align="right" style="color:blue">1.41%</td>
				<td align="right">681.00</td>
			<td align="right">669.00</td>
		</tr>
				<tr>
			<td><a title="Brown (N.) Group share price BWNG share price" href="./share_price/symbol-BWNG">Brown (N.) Group</a></td>
			<td title="BWNG share price"align="right" style="color:blue">402.50</td>
			<td align="right" style="color:blue">3.90%</td>
				<td align="right">403.80</td>
			<td align="right">388.00</td>
		</tr>
				<tr>
			<td><a title="BSS Group share price BTSM share price" href="./share_price/symbol-BTSM">BSS Group</a></td>
			<td title="BTSM share price"align="right" style="color:">477.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">477.00</td>
			<td align="right">477.00</td>
		</tr>
				<tr>
			<td><a title="BT Group share price BT.A share price" href="./share_price/symbol-BT.A">BT Group</a></td>
			<td title="BT.A share price"align="right" style="color:blue">399.50</td>
			<td align="right" style="color:blue">0.40%</td>
				<td align="right">400.90</td>
			<td align="right">394.99</td>
		</tr>
				<tr>
			<td><a title="BTG share price BTG share price" href="./share_price/symbol-BTG">BTG</a></td>
			<td title="BTG share price"align="right" style="color:blue">824.00</td>
			<td align="right" style="color:blue">0.80%</td>
				<td align="right">825.50</td>
			<td align="right">816.50</td>
		</tr>
				<tr>
			<td><a title="BTG share price BGC share price" href="./share_price/symbol-BGC">BTG</a></td>
			<td title="BGC share price"align="right" style="color:">387.50</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">389.80</td>
			<td align="right">378.10</td>
		</tr>
				<tr>
			<td><a title="Bunzl share price BNZL share price" href="./share_price/symbol-BNZL">Bunzl</a></td>
			<td title="BNZL share price"align="right" style="color:red">1,827.00</td>
			<td align="right" style="color:red">-0.11%</td>
				<td align="right">1,841.00</td>
			<td align="right">1,822.00</td>
		</tr>
				<tr>
			<td><a title="Burberry Group share price BRBY share price" href="./share_price/symbol-BRBY">Burberry Group</a></td>
			<td title="BRBY share price"align="right" style="color:blue">1,672.00</td>
			<td align="right" style="color:blue">0.54%</td>
				<td align="right">1,679.00</td>
			<td align="right">1,657.00</td>
		</tr>
				<tr>
			<td><a title="Bwin.party Digital Entertainment share price BPTY share price" href="./share_price/symbol-BPTY">Bwin.party Digital Entertainment</a></td>
			<td title="BPTY share price"align="right" style="color:red">107.50</td>
			<td align="right" style="color:red">-0.65%</td>
				<td align="right">110.22</td>
			<td align="right">107.00</td>
		</tr>
				<tr>
			<td><a title="Cable & Wireless Communications share price CWC share price" href="./share_price/symbol-CWC">Cable & Wireless Communications</a></td>
			<td title="CWC share price"align="right" style="color:blue">49.96</td>
			<td align="right" style="color:blue">2.00%</td>
				<td align="right">50.00</td>
			<td align="right">48.74</td>
		</tr>
				<tr>
			<td><a title="Cairn Energy share price CNE share price" href="./share_price/symbol-CNE">Cairn Energy</a></td>
			<td title="CNE share price"align="right" style="color:blue">164.00</td>
			<td align="right" style="color:blue">2.12%</td>
				<td align="right">166.80</td>
			<td align="right">157.60</td>
		</tr>
				<tr>
			<td><a title="Caledonia Investments share price CLDN share price" href="./share_price/symbol-CLDN">Caledonia Investments</a></td>
			<td title="CLDN share price"align="right" style="color:blue">2,318.00</td>
			<td align="right" style="color:blue">0.52%</td>
				<td align="right">2,320.00</td>
			<td align="right">2,302.00</td>
		</tr>
				<tr>
			<td><a title="Capita share price CPI share price" href="./share_price/symbol-CPI">Capita</a></td>
			<td title="CPI share price"align="right" style="color:blue">1,076.00</td>
			<td align="right" style="color:blue">0.37%</td>
				<td align="right">1,077.00</td>
			<td align="right">1,063.00</td>
		</tr>
				<tr>
			<td><a title="Capital & Counties Properties share price CAPC share price" href="./share_price/symbol-CAPC">Capital & Counties Properties</a></td>
			<td title="CAPC share price"align="right" style="color:blue">375.50</td>
			<td align="right" style="color:blue">1.10%</td>
				<td align="right">376.80</td>
			<td align="right">370.80</td>
		</tr>
				<tr>
			<td><a title="Capital Shopping Centres Group share price CSCG share price" href="./share_price/symbol-CSCG">Capital Shopping Centres Group</a></td>
			<td title="CSCG share price"align="right" style="color:blue">361.20</td>
			<td align="right" style="color:blue">0.03%</td>
				<td align="right">363.70</td>
			<td align="right">360.70</td>
		</tr>
				<tr>
			<td><a title="Caracal Energy Inc (DI) share price CRCL share price" href="./share_price/symbol-CRCL">Caracal Energy Inc (DI)</a></td>
			<td title="CRCL share price"align="right" style="color:">549.75</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">551.00</td>
			<td align="right">549.00</td>
		</tr>
				<tr>
			<td><a title="Carillion share price CLLN share price" href="./share_price/symbol-CLLN">Carillion</a></td>
			<td title="CLLN share price"align="right" style="color:blue">328.00</td>
			<td align="right" style="color:blue">0.55%</td>
				<td align="right">328.90</td>
			<td align="right">324.00</td>
		</tr>
				<tr>
			<td><a title="Carnival share price CCL share price" href="./share_price/symbol-CCL">Carnival</a></td>
			<td title="CCL share price"align="right" style="color:blue">3,066.00</td>
			<td align="right" style="color:blue">0.10%</td>
				<td align="right">3,094.00</td>
			<td align="right">3,040.00</td>
		</tr>
				<tr>
			<td><a title="Carphone Warehouse Group share price CPW share price" href="./share_price/symbol-CPW">Carphone Warehouse Group</a></td>
			<td title="CPW share price"align="right" style="color:blue">343.20</td>
			<td align="right" style="color:blue">0.44%</td>
				<td align="right">346.50</td>
			<td align="right">341.10</td>
		</tr>
				<tr>
			<td><a title="Catlin Group Ltd. share price CGL share price" href="./share_price/symbol-CGL">Catlin Group Ltd.</a></td>
			<td title="CGL share price"align="right" style="color:blue">700.00</td>
			<td align="right" style="color:blue">0.36%</td>
				<td align="right">701.50</td>
			<td align="right">695.00</td>
		</tr>
				<tr>
			<td><a title="Centamin (DI) share price CEY share price" href="./share_price/symbol-CEY">Centamin (DI)</a></td>
			<td title="CEY share price"align="right" style="color:blue">66.00</td>
			<td align="right" style="color:blue">0.61%</td>
				<td align="right">67.30</td>
			<td align="right">65.75</td>
		</tr>
				<tr>
			<td><a title="Centrica share price CNA share price" href="./share_price/symbol-CNA">Centrica</a></td>
			<td title="CNA share price"align="right" style="color:blue">273.20</td>
			<td align="right" style="color:blue">4.67%</td>
				<td align="right">274.30</td>
			<td align="right">266.00</td>
		</tr>
				<tr>
			<td><a title="Charter International share price CHTR share price" href="./share_price/symbol-CHTR">Charter International</a></td>
			<td title="CHTR share price"align="right" style="color:">968.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">969.00</td>
			<td align="right">963.52</td>
		</tr>
				<tr>
			<td><a title="Chloride Group share price CHLD share price" href="./share_price/symbol-CHLD">Chloride Group</a></td>
			<td title="CHLD share price"align="right" style="color:red">374.60</td>
			<td align="right" style="color:red">-0.11%</td>
				<td align="right">374.90</td>
			<td align="right">374.00</td>
		</tr>
				<tr>
			<td><a title="Cineworld Group share price CINE share price" href="./share_price/symbol-CINE">Cineworld Group</a></td>
			<td title="CINE share price"align="right" style="color:red">404.50</td>
			<td align="right" style="color:red">-2.76%</td>
				<td align="right">412.80</td>
			<td align="right">401.30</td>
		</tr>
				<tr>
			<td><a title="City of London Inv Trust share price CTY share price" href="./share_price/symbol-CTY">City of London Inv Trust</a></td>
			<td title="CTY share price"align="right" style="color:blue">383.50</td>
			<td align="right" style="color:blue">1.40%</td>
				<td align="right">383.50</td>
			<td align="right">377.21</td>
		</tr>
				<tr>
			<td><a title="Close Brothers Group share price CBG share price" href="./share_price/symbol-CBG">Close Brothers Group</a></td>
			<td title="CBG share price"align="right" style="color:blue">1,488.00</td>
			<td align="right" style="color:blue">0.20%</td>
				<td align="right">1,502.00</td>
			<td align="right">1,480.00</td>
		</tr>
				<tr>
			<td><a title="CLS Holdings share price CLI share price" href="./share_price/symbol-CLI">CLS Holdings</a></td>
			<td title="CLI share price"align="right" style="color:blue">1,500.00</td>
			<td align="right" style="color:blue">1.35%</td>
				<td align="right">1,521.00</td>
			<td align="right">1,482.00</td>
		</tr>
				<tr>
			<td><a title="Cobham share price COB share price" href="./share_price/symbol-COB">Cobham</a></td>
			<td title="COB share price"align="right" style="color:red">316.60</td>
			<td align="right" style="color:red">-4.35%</td>
				<td align="right">328.10</td>
			<td align="right">313.50</td>
		</tr>
				<tr>
			<td><a title="Coca-Cola HBC AG (CDI) share price CCH share price" href="./share_price/symbol-CCH">Coca-Cola HBC AG (CDI)</a></td>
			<td title="CCH share price"align="right" style="color:blue">1,141.00</td>
			<td align="right" style="color:blue">1.42%</td>
				<td align="right">1,163.00</td>
			<td align="right">1,120.00</td>
		</tr>
				<tr>
			<td><a title="COLT Group SA share price COLT share price" href="./share_price/symbol-COLT">COLT Group SA</a></td>
			<td title="COLT share price"align="right" style="color:red">128.70</td>
			<td align="right" style="color:red">-0.31%</td>
				<td align="right">130.50</td>
			<td align="right">126.10</td>
		</tr>
				<tr>
			<td><a title="Compass Group share price CPG share price" href="./share_price/symbol-CPG">Compass Group</a></td>
			<td title="CPG share price"align="right" style="color:blue">1,119.00</td>
			<td align="right" style="color:blue">0.90%</td>
				<td align="right">1,124.00</td>
			<td align="right">1,104.00</td>
		</tr>
				<tr>
			<td><a title="Computacenter share price CCC share price" href="./share_price/symbol-CCC">Computacenter</a></td>
			<td title="CCC share price"align="right" style="color:blue">605.00</td>
			<td align="right" style="color:blue">1.77%</td>
				<td align="right">624.00</td>
			<td align="right">589.84</td>
		</tr>
				<tr>
			<td><a title="Connaught share price CNT share price" href="./share_price/symbol-CNT">Connaught</a></td>
			<td title="CNT share price"align="right" style="color:">16.65</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">16.65</td>
			<td align="right">16.65</td>
		</tr>
				<tr>
			<td><a title="Countrywide share price CWD share price" href="./share_price/symbol-CWD">Countrywide</a></td>
			<td title="CWD share price"align="right" style="color:blue">452.00</td>
			<td align="right" style="color:blue">1.35%</td>
				<td align="right">460.40</td>
			<td align="right">446.20</td>
		</tr>
				<tr>
			<td><a title="Cranswick share price CWK share price" href="./share_price/symbol-CWK">Cranswick</a></td>
			<td title="CWK share price"align="right" style="color:blue">1,402.00</td>
			<td align="right" style="color:blue">2.04%</td>
				<td align="right">1,420.00</td>
			<td align="right">1,376.00</td>
		</tr>
				<tr>
			<td><a title="Crest Nicholson Holdings share price CRST share price" href="./share_price/symbol-CRST">Crest Nicholson Holdings</a></td>
			<td title="CRST share price"align="right" style="color:blue">369.70</td>
			<td align="right" style="color:blue">2.01%</td>
				<td align="right">371.60</td>
			<td align="right">361.52</td>
		</tr>
				<tr>
			<td><a title="CRH share price CRH share price" href="./share_price/symbol-CRH">CRH</a></td>
			<td title="CRH share price"align="right" style="color:blue">1,533.00</td>
			<td align="right" style="color:blue">0.39%</td>
				<td align="right">1,543.45</td>
			<td align="right">1,516.00</td>
		</tr>
				<tr>
			<td><a title="Croda International share price CRDA share price" href="./share_price/symbol-CRDA">Croda International</a></td>
			<td title="CRDA share price"align="right" style="color:blue">2,676.00</td>
			<td align="right" style="color:blue">0.91%</td>
				<td align="right">2,692.00</td>
			<td align="right">2,648.60</td>
		</tr>
				<tr>
			<td><a title="CSR share price CSR share price" href="./share_price/symbol-CSR">CSR</a></td>
			<td title="CSR share price"align="right" style="color:red">857.00</td>
			<td align="right" style="color:red">-0.29%</td>
				<td align="right">859.00</td>
			<td align="right">850.50</td>
		</tr>
				<tr>
			<td><a title="Daejan Holdings share price DJAN share price" href="./share_price/symbol-DJAN">Daejan Holdings</a></td>
			<td title="DJAN share price"align="right" style="color:blue">5,765.00</td>
			<td align="right" style="color:blue">0.26%</td>
				<td align="right">5,765.00</td>
			<td align="right">5,685.00</td>
		</tr>
				<tr>
			<td><a title="Dairy Crest Group share price DCG share price" href="./share_price/symbol-DCG">Dairy Crest Group</a></td>
			<td title="DCG share price"align="right" style="color:blue">475.50</td>
			<td align="right" style="color:blue">1.56%</td>
				<td align="right">476.20</td>
			<td align="right">461.44</td>
		</tr>
				<tr>
			<td><a title="Davis Service Group share price DVSG share price" href="./share_price/symbol-DVSG">Davis Service Group</a></td>
			<td title="DVSG share price"align="right" style="color:">439.80</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">443.10</td>
			<td align="right">436.70</td>
		</tr>
				<tr>
			<td><a title="DCC share price DCC share price" href="./share_price/symbol-DCC">DCC</a></td>
			<td title="DCC share price"align="right" style="color:blue">3,483.00</td>
			<td align="right" style="color:blue">0.37%</td>
				<td align="right">3,490.00</td>
			<td align="right">3,452.00</td>
		</tr>
				<tr>
			<td><a title="De La Rue share price DLAR share price" href="./share_price/symbol-DLAR">De La Rue</a></td>
			<td title="DLAR share price"align="right" style="color:blue">510.50</td>
			<td align="right" style="color:blue">0.99%</td>
				<td align="right">511.50</td>
			<td align="right">504.00</td>
		</tr>
				<tr>
			<td><a title="Debenhams share price DEB share price" href="./share_price/symbol-DEB">Debenhams</a></td>
			<td title="DEB share price"align="right" style="color:red">69.30</td>
			<td align="right" style="color:red">-7.66%</td>
				<td align="right">71.98</td>
			<td align="right">68.00</td>
		</tr>
				<tr>
			<td><a title="Dechra Pharmaceuticals share price DPH share price" href="./share_price/symbol-DPH">Dechra Pharmaceuticals</a></td>
			<td title="DPH share price"align="right" style="color:blue">865.00</td>
			<td align="right" style="color:blue">3.59%</td>
				<td align="right">872.00</td>
			<td align="right">840.42</td>
		</tr>
				<tr>
			<td><a title="Derwent London share price DLN share price" href="./share_price/symbol-DLN">Derwent London</a></td>
			<td title="DLN share price"align="right" style="color:blue">3,092.00</td>
			<td align="right" style="color:blue">1.28%</td>
				<td align="right">3,099.00</td>
			<td align="right">3,054.00</td>
		</tr>
				<tr>
			<td><a title="Diageo share price DGE share price" href="./share_price/symbol-DGE">Diageo</a></td>
			<td title="DGE share price"align="right" style="color:blue">1,871.00</td>
			<td align="right" style="color:blue">1.49%</td>
				<td align="right">1,880.00</td>
			<td align="right">1,844.50</td>
		</tr>
				<tr>
			<td><a title="Dignity share price DTY share price" href="./share_price/symbol-DTY">Dignity</a></td>
			<td title="DTY share price"align="right" style="color:blue">1,788.00</td>
			<td align="right" style="color:blue">0.45%</td>
				<td align="right">1,808.36</td>
			<td align="right">1,729.00</td>
		</tr>
				<tr>
			<td><a title="Dimension Data Holdings share price DDT share price" href="./share_price/symbol-DDT">Dimension Data Holdings</a></td>
			<td title="DDT share price"align="right" style="color:">121.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">122.00</td>
			<td align="right">120.00</td>
		</tr>
				<tr>
			<td><a title="Diploma share price DPLM share price" href="./share_price/symbol-DPLM">Diploma</a></td>
			<td title="DPLM share price"align="right" style="color:blue">728.50</td>
			<td align="right" style="color:blue">0.48%</td>
				<td align="right">744.00</td>
			<td align="right">714.77</td>
		</tr>
				<tr>
			<td><a title="Direct Line Insurance Group share price DLG share price" href="./share_price/symbol-DLG">Direct Line Insurance Group</a></td>
			<td title="DLG share price"align="right" style="color:blue">309.00</td>
			<td align="right" style="color:blue">2.28%</td>
				<td align="right">309.00</td>
			<td align="right">301.00</td>
		</tr>
				<tr>
			<td><a title="Dixons Carphone share price DC. share price" href="./share_price/symbol-DC.">Dixons Carphone</a></td>
			<td title="DC. share price"align="right" style="color:blue">458.40</td>
			<td align="right" style="color:blue">1.87%</td>
				<td align="right">459.12</td>
			<td align="right">446.66</td>
		</tr>
				<tr>
			<td><a title="Dixons Retail  share price DXNS share price" href="./share_price/symbol-DXNS">Dixons Retail </a></td>
			<td title="DXNS share price"align="right" style="color:">52.95</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">52.95</td>
			<td align="right">52.95</td>
		</tr>
				<tr>
			<td><a title="Domino Printing Sciences share price DNO share price" href="./share_price/symbol-DNO">Domino Printing Sciences</a></td>
			<td title="DNO share price"align="right" style="color:blue">661.00</td>
			<td align="right" style="color:blue">1.69%</td>
				<td align="right">662.50</td>
			<td align="right">649.50</td>
		</tr>
				<tr>
			<td><a title="Domino's Pizza Group share price DOM share price" href="./share_price/symbol-DOM">Domino's Pizza Group</a></td>
			<td title="DOM share price"align="right" style="color:blue">704.50</td>
			<td align="right" style="color:blue">0.14%</td>
				<td align="right">712.00</td>
			<td align="right">699.40</td>
		</tr>
				<tr>
			<td><a title="Drax Group share price DRX share price" href="./share_price/symbol-DRX">Drax Group</a></td>
			<td title="DRX share price"align="right" style="color:blue">376.20</td>
			<td align="right" style="color:blue">5.67%</td>
				<td align="right">379.20</td>
			<td align="right">365.10</td>
		</tr>
				<tr>
			<td><a title="DSG International share price DSGI share price" href="./share_price/symbol-DSGI">DSG International</a></td>
			<td title="DSGI share price"align="right" style="color:">23.65</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">23.65</td>
			<td align="right">23.65</td>
		</tr>
				<tr>
			<td><a title="Dunelm Group share price DNLM share price" href="./share_price/symbol-DNLM">Dunelm Group</a></td>
			<td title="DNLM share price"align="right" style="color:blue">846.50</td>
			<td align="right" style="color:blue">0.30%</td>
				<td align="right">859.00</td>
			<td align="right">838.15</td>
		</tr>
				<tr>
			<td><a title="easyJet share price EZJ share price" href="./share_price/symbol-EZJ">easyJet</a></td>
			<td title="EZJ share price"align="right" style="color:blue">1,641.00</td>
			<td align="right" style="color:blue">0.61%</td>
				<td align="right">1,651.00</td>
			<td align="right">1,624.00</td>
		</tr>
				<tr>
			<td><a title="Edinburgh Inv Trust share price EDIN share price" href="./share_price/symbol-EDIN">Edinburgh Inv Trust</a></td>
			<td title="EDIN share price"align="right" style="color:blue">662.00</td>
			<td align="right" style="color:blue">1.53%</td>
				<td align="right">662.00</td>
			<td align="right">653.59</td>
		</tr>
				<tr>
			<td><a title="Electra Private Equity share price ELTA share price" href="./share_price/symbol-ELTA">Electra Private Equity</a></td>
			<td title="ELTA share price"align="right" style="color:">3,006.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">3,016.80</td>
			<td align="right">3,000.00</td>
		</tr>
				<tr>
			<td><a title="Electrocomponents share price ECM share price" href="./share_price/symbol-ECM">Electrocomponents</a></td>
			<td title="ECM share price"align="right" style="color:blue">214.00</td>
			<td align="right" style="color:blue">2.39%</td>
				<td align="right">214.20</td>
			<td align="right">208.58</td>
		</tr>
				<tr>
			<td><a title="Elementis share price ELM share price" href="./share_price/symbol-ELM">Elementis</a></td>
			<td title="ELM share price"align="right" style="color:red">274.40</td>
			<td align="right" style="color:red">-0.22%</td>
				<td align="right">278.10</td>
			<td align="right">272.90</td>
		</tr>
				<tr>
			<td><a title="Enterprise Inns share price ETI share price" href="./share_price/symbol-ETI">Enterprise Inns</a></td>
			<td title="ETI share price"align="right" style="color:blue">111.90</td>
			<td align="right" style="color:blue">1.73%</td>
				<td align="right">112.30</td>
			<td align="right">109.20</td>
		</tr>
				<tr>
			<td><a title="Entertainment One Limited share price ETO share price" href="./share_price/symbol-ETO">Entertainment One Limited</a></td>
			<td title="ETO share price"align="right" style="color:red">297.80</td>
			<td align="right" style="color:red">-4.24%</td>
				<td align="right">310.00</td>
			<td align="right">296.00</td>
		</tr>
				<tr>
			<td><a title="Essentra share price ESNT share price" href="./share_price/symbol-ESNT">Essentra</a></td>
			<td title="ESNT share price"align="right" style="color:blue">771.50</td>
			<td align="right" style="color:blue">2.32%</td>
				<td align="right">774.00</td>
			<td align="right">753.27</td>
		</tr>
				<tr>
			<td><a title="esure Group share price ESUR share price" href="./share_price/symbol-ESUR">esure Group</a></td>
			<td title="ESUR share price"align="right" style="color:blue">221.10</td>
			<td align="right" style="color:blue">3.27%</td>
				<td align="right">221.65</td>
			<td align="right">215.80</td>
		</tr>
				<tr>
			<td><a title="Euromoney Institutional Investor share price ERM share price" href="./share_price/symbol-ERM">Euromoney Institutional Investor</a></td>
			<td title="ERM share price"align="right" style="color:blue">1,005.00</td>
			<td align="right" style="color:blue">0.10%</td>
				<td align="right">1,007.00</td>
			<td align="right">992.20</td>
		</tr>
				<tr>
			<td><a title="Evraz share price EVR share price" href="./share_price/symbol-EVR">Evraz</a></td>
			<td title="EVR share price"align="right" style="color:blue">146.90</td>
			<td align="right" style="color:blue">0.27%</td>
				<td align="right">150.20</td>
			<td align="right">144.70</td>
		</tr>
				<tr>
			<td><a title="Experian share price EXPN share price" href="./share_price/symbol-EXPN">Experian</a></td>
			<td title="EXPN share price"align="right" style="color:red">1,082.00</td>
			<td align="right" style="color:red">-0.73%</td>
				<td align="right">1,093.00</td>
			<td align="right">1,080.00</td>
		</tr>
				<tr>
			<td><a title="F&C Asset Management share price FCAM share price" href="./share_price/symbol-FCAM">F&C Asset Management</a></td>
			<td title="FCAM share price"align="right" style="color:">119.70</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">119.90</td>
			<td align="right">119.40</td>
		</tr>
				<tr>
			<td><a title="F&C Commercial Property Trust Ltd. share price FCPT share price" href="./share_price/symbol-FCPT">F&C Commercial Property Trust Ltd.</a></td>
			<td title="FCPT share price"align="right" style="color:blue">140.70</td>
			<td align="right" style="color:blue">0.72%</td>
				<td align="right">140.85</td>
			<td align="right">139.60</td>
		</tr>
				<tr>
			<td><a title="Fidelity China Special Situations share price FCSS share price" href="./share_price/symbol-FCSS">Fidelity China Special Situations</a></td>
			<td title="FCSS share price"align="right" style="color:blue">142.30</td>
			<td align="right" style="color:blue">1.21%</td>
				<td align="right">142.39</td>
			<td align="right">140.34</td>
		</tr>
				<tr>
			<td><a title="Fidelity European Values share price FEV share price" href="./share_price/symbol-FEV">Fidelity European Values</a></td>
			<td title="FEV share price"align="right" style="color:blue">161.80</td>
			<td align="right" style="color:blue">1.13%</td>
				<td align="right">162.10</td>
			<td align="right">160.40</td>
		</tr>
				<tr>
			<td><a title="Fidessa Group share price FDSA share price" href="./share_price/symbol-FDSA">Fidessa Group</a></td>
			<td title="FDSA share price"align="right" style="color:blue">2,512.00</td>
			<td align="right" style="color:blue">1.62%</td>
				<td align="right">2,512.00</td>
			<td align="right">2,450.00</td>
		</tr>
				<tr>
			<td><a title="Filtrona PLC share price FLTR share price" href="./share_price/symbol-FLTR">Filtrona PLC</a></td>
			<td title="FLTR share price"align="right" style="color:blue">683.00</td>
			<td align="right" style="color:blue">2.71%</td>
				<td align="right">683.00</td>
			<td align="right">665.00</td>
		</tr>
				<tr>
			<td><a title="FirstGroup share price FGP share price" href="./share_price/symbol-FGP">FirstGroup</a></td>
			<td title="FGP share price"align="right" style="color:blue">107.40</td>
			<td align="right" style="color:blue">1.13%</td>
				<td align="right">108.10</td>
			<td align="right">105.50</td>
		</tr>
				<tr>
			<td><a title="Fisher (James) & Sons share price FSJ share price" href="./share_price/symbol-FSJ">Fisher (James) & Sons</a></td>
			<td title="FSJ share price"align="right" style="color:red">1,075.00</td>
			<td align="right" style="color:red">-2.98%</td>
				<td align="right">1,104.84</td>
			<td align="right">1,063.00</td>
		</tr>
				<tr>
			<td><a title="Foreign and Colonial Inv Trust share price FRCL share price" href="./share_price/symbol-FRCL">Foreign and Colonial Inv Trust</a></td>
			<td title="FRCL share price"align="right" style="color:blue">438.50</td>
			<td align="right" style="color:blue">1.48%</td>
				<td align="right">438.50</td>
			<td align="right">432.04</td>
		</tr>
				<tr>
			<td><a title="Forth Ports share price FPT share price" href="./share_price/symbol-FPT">Forth Ports</a></td>
			<td title="FPT share price"align="right" style="color:">1,627.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">1,629.00</td>
			<td align="right">1,627.00</td>
		</tr>
				<tr>
			<td><a title="Fresnillo share price FRES share price" href="./share_price/symbol-FRES">Fresnillo</a></td>
			<td title="FRES share price"align="right" style="color:blue">831.00</td>
			<td align="right" style="color:blue">0.67%</td>
				<td align="right">843.00</td>
			<td align="right">826.00</td>
		</tr>
				<tr>
			<td><a title="Friends Life Group Limited share price FLG share price" href="./share_price/symbol-FLG">Friends Life Group Limited</a></td>
			<td title="FLG share price"align="right" style="color:blue">371.90</td>
			<td align="right" style="color:blue">1.89%</td>
				<td align="right">371.90</td>
			<td align="right">362.40</td>
		</tr>
				<tr>
			<td><a title="G4S share price GFS share price" href="./share_price/symbol-GFS">G4S</a></td>
			<td title="GFS share price"align="right" style="color:blue">278.50</td>
			<td align="right" style="color:blue">1.27%</td>
				<td align="right">279.90</td>
			<td align="right">273.20</td>
		</tr>
				<tr>
			<td><a title="Galiform share price GFRM share price" href="./share_price/symbol-GFRM">Galiform</a></td>
			<td title="GFRM share price"align="right" style="color:blue">71.40</td>
			<td align="right" style="color:blue">0.42%</td>
				<td align="right">71.95</td>
			<td align="right">70.33</td>
		</tr>
				<tr>
			<td><a title="Galliford Try share price GFRD share price" href="./share_price/symbol-GFRD">Galliford Try</a></td>
			<td title="GFRD share price"align="right" style="color:red">1,245.00</td>
			<td align="right" style="color:red">-0.40%</td>
				<td align="right">1,263.00</td>
			<td align="right">1,239.00</td>
		</tr>
				<tr>
			<td><a title="Game Digital share price GMD share price" href="./share_price/symbol-GMD">Game Digital</a></td>
			<td title="GMD share price"align="right" style="color:blue">349.50</td>
			<td align="right" style="color:blue">1.60%</td>
				<td align="right">354.80</td>
			<td align="right">345.00</td>
		</tr>
				<tr>
			<td><a title="Gartmore Group Ltd. share price GRT share price" href="./share_price/symbol-GRT">Gartmore Group Ltd.</a></td>
			<td title="GRT share price"align="right" style="color:">119.80</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">119.80</td>
			<td align="right">112.20</td>
		</tr>
				<tr>
			<td><a title="Genesis Emerging Markets Fund Ltd Ptg NPV share price GSS share price" href="./share_price/symbol-GSS">Genesis Emerging Markets Fund Ltd Ptg NPV</a></td>
			<td title="GSS share price"align="right" style="color:blue">535.00</td>
			<td align="right" style="color:blue">1.52%</td>
				<td align="right">537.00</td>
			<td align="right">532.50</td>
		</tr>
				<tr>
			<td><a title="Genus share price GNS share price" href="./share_price/symbol-GNS">Genus</a></td>
			<td title="GNS share price"align="right" style="color:blue">1,275.00</td>
			<td align="right" style="color:blue">0.08%</td>
				<td align="right">1,297.00</td>
			<td align="right">1,260.00</td>
		</tr>
				<tr>
			<td><a title="GKN share price GKN share price" href="./share_price/symbol-GKN">GKN</a></td>
			<td title="GKN share price"align="right" style="color:blue">356.50</td>
			<td align="right" style="color:blue">2.30%</td>
				<td align="right">357.00</td>
			<td align="right">345.90</td>
		</tr>
				<tr>
			<td><a title="GlaxoSmithKline share price GSK share price" href="./share_price/symbol-GSK">GlaxoSmithKline</a></td>
			<td title="GSK share price"align="right" style="color:blue">1,427.00</td>
			<td align="right" style="color:blue">1.03%</td>
				<td align="right">1,432.00</td>
			<td align="right">1,406.00</td>
		</tr>
				<tr>
			<td><a title="Glencore share price GLEN share price" href="./share_price/symbol-GLEN">Glencore</a></td>
			<td title="GLEN share price"align="right" style="color:red">269.25</td>
			<td align="right" style="color:red">-3.65%</td>
				<td align="right">279.00</td>
			<td align="right">268.80</td>
		</tr>
				<tr>
			<td><a title="Go-Ahead Group share price GOG share price" href="./share_price/symbol-GOG">Go-Ahead Group</a></td>
			<td title="GOG share price"align="right" style="color:blue">2,498.00</td>
			<td align="right" style="color:blue">0.28%</td>
				<td align="right">2,523.00</td>
			<td align="right">2,482.00</td>
		</tr>
				<tr>
			<td><a title="Grafton Group Units share price GFTU share price" href="./share_price/symbol-GFTU">Grafton Group Units</a></td>
			<td title="GFTU share price"align="right" style="color:blue">629.50</td>
			<td align="right" style="color:blue">1.12%</td>
				<td align="right">629.50</td>
			<td align="right">622.00</td>
		</tr>
				<tr>
			<td><a title="Grainger share price GRI share price" href="./share_price/symbol-GRI">Grainger</a></td>
			<td title="GRI share price"align="right" style="color:blue">193.60</td>
			<td align="right" style="color:blue">0.73%</td>
				<td align="right">193.70</td>
			<td align="right">191.00</td>
		</tr>
				<tr>
			<td><a title="Great Portland Estates share price GPOR share price" href="./share_price/symbol-GPOR">Great Portland Estates</a></td>
			<td title="GPOR share price"align="right" style="color:blue">742.50</td>
			<td align="right" style="color:blue">0.27%</td>
				<td align="right">749.50</td>
			<td align="right">740.00</td>
		</tr>
				<tr>
			<td><a title="Greencore Group share price GNC share price" href="./share_price/symbol-GNC">Greencore Group</a></td>
			<td title="GNC share price"align="right" style="color:red">289.40</td>
			<td align="right" style="color:red">-1.23%</td>
				<td align="right">294.40</td>
			<td align="right">287.30</td>
		</tr>
				<tr>
			<td><a title="Greene King share price GNK share price" href="./share_price/symbol-GNK">Greene King</a></td>
			<td title="GNK share price"align="right" style="color:blue">781.00</td>
			<td align="right" style="color:blue">1.69%</td>
				<td align="right">783.00</td>
			<td align="right">764.50</td>
		</tr>
				<tr>
			<td><a title="Greggs share price GRG share price" href="./share_price/symbol-GRG">Greggs</a></td>
			<td title="GRG share price"align="right" style="color:blue">819.00</td>
			<td align="right" style="color:blue">9.27%</td>
				<td align="right">821.00</td>
			<td align="right">780.00</td>
		</tr>
				<tr>
			<td><a title="Halfords Group share price HFD share price" href="./share_price/symbol-HFD">Halfords Group</a></td>
			<td title="HFD share price"align="right" style="color:red">425.40</td>
			<td align="right" style="color:red">-1.07%</td>
				<td align="right">431.75</td>
			<td align="right">424.10</td>
		</tr>
				<tr>
			<td><a title="Halma share price HLMA share price" href="./share_price/symbol-HLMA">Halma</a></td>
			<td title="HLMA share price"align="right" style="color:blue">690.00</td>
			<td align="right" style="color:blue">1.47%</td>
				<td align="right">692.00</td>
			<td align="right">676.00</td>
		</tr>
				<tr>
			<td><a title="Hammerson share price HMSO share price" href="./share_price/symbol-HMSO">Hammerson</a></td>
			<td title="HMSO share price"align="right" style="color:blue">625.50</td>
			<td align="right" style="color:blue">0.24%</td>
				<td align="right">627.50</td>
			<td align="right">621.00</td>
		</tr>
				<tr>
			<td><a title="Hansteen Holdings share price HSTN share price" href="./share_price/symbol-HSTN">Hansteen Holdings</a></td>
			<td title="HSTN share price"align="right" style="color:blue">110.40</td>
			<td align="right" style="color:blue">1.19%</td>
				<td align="right">110.60</td>
			<td align="right">108.93</td>
		</tr>
				<tr>
			<td><a title="Hargreaves Lansdown share price HL. share price" href="./share_price/symbol-HL.">Hargreaves Lansdown</a></td>
			<td title="HL. share price"align="right" style="color:blue">951.00</td>
			<td align="right" style="color:blue">1.55%</td>
				<td align="right">954.50</td>
			<td align="right">928.96</td>
		</tr>
				<tr>
			<td><a title="Hays share price HAS share price" href="./share_price/symbol-HAS">Hays</a></td>
			<td title="HAS share price"align="right" style="color:red">147.90</td>
			<td align="right" style="color:red">-0.60%</td>
				<td align="right">149.70</td>
			<td align="right">147.30</td>
		</tr>
				<tr>
			<td><a title="Hellermanntyton Group share price HTY share price" href="./share_price/symbol-HTY">Hellermanntyton Group</a></td>
			<td title="HTY share price"align="right" style="color:red">312.10</td>
			<td align="right" style="color:red">-0.41%</td>
				<td align="right">318.00</td>
			<td align="right">311.30</td>
		</tr>
				<tr>
			<td><a title="Henderson Group share price HGG share price" href="./share_price/symbol-HGG">Henderson Group</a></td>
			<td title="HGG share price"align="right" style="color:blue">223.40</td>
			<td align="right" style="color:blue">3.14%</td>
				<td align="right">223.70</td>
			<td align="right">216.30</td>
		</tr>
				<tr>
			<td><a title="Heritage Oil share price HOIL share price" href="./share_price/symbol-HOIL">Heritage Oil</a></td>
			<td title="HOIL share price"align="right" style="color:">319.30</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">319.90</td>
			<td align="right">319.20</td>
		</tr>
				<tr>
			<td><a title="HICL Infrastructure Company Ltd share price HICL share price" href="./share_price/symbol-HICL">HICL Infrastructure Company Ltd</a></td>
			<td title="HICL share price"align="right" style="color:red">155.00</td>
			<td align="right" style="color:red">-0.06%</td>
				<td align="right">155.70</td>
			<td align="right">154.68</td>
		</tr>
				<tr>
			<td><a title="Hikma Pharmaceuticals share price HIK share price" href="./share_price/symbol-HIK">Hikma Pharmaceuticals</a></td>
			<td title="HIK share price"align="right" style="color:blue">2,474.00</td>
			<td align="right" style="color:blue">3.95%</td>
				<td align="right">2,500.00</td>
			<td align="right">2,386.00</td>
		</tr>
				<tr>
			<td><a title="Hiscox Ltd (CDI) share price HSX share price" href="./share_price/symbol-HSX">Hiscox Ltd (CDI)</a></td>
			<td title="HSX share price"align="right" style="color:blue">738.50</td>
			<td align="right" style="color:blue">0.54%</td>
				<td align="right">741.00</td>
			<td align="right">729.00</td>
		</tr>
				<tr>
			<td><a title="Home Retail Group share price HOME share price" href="./share_price/symbol-HOME">Home Retail Group</a></td>
			<td title="HOME share price"align="right" style="color:blue">217.90</td>
			<td align="right" style="color:blue">3.03%</td>
				<td align="right">218.40</td>
			<td align="right">209.80</td>
		</tr>
				<tr>
			<td><a title="Homeserve share price HSV share price" href="./share_price/symbol-HSV">Homeserve</a></td>
			<td title="HSV share price"align="right" style="color:blue">339.50</td>
			<td align="right" style="color:blue">0.50%</td>
				<td align="right">341.41</td>
			<td align="right">335.00</td>
		</tr>
				<tr>
			<td><a title="Howden Joinery Group share price HWDN share price" href="./share_price/symbol-HWDN">Howden Joinery Group</a></td>
			<td title="HWDN share price"align="right" style="color:blue">413.30</td>
			<td align="right" style="color:blue">0.44%</td>
				<td align="right">414.60</td>
			<td align="right">408.20</td>
		</tr>
				<tr>
			<td><a title="HSBC Holdings share price HSBA share price" href="./share_price/symbol-HSBA">HSBC Holdings</a></td>
			<td title="HSBA share price"align="right" style="color:blue">605.50</td>
			<td align="right" style="color:blue">0.46%</td>
				<td align="right">608.13</td>
			<td align="right">602.90</td>
		</tr>
				<tr>
			<td><a title="Hunting share price HTG share price" href="./share_price/symbol-HTG">Hunting</a></td>
			<td title="HTG share price"align="right" style="color:red">432.40</td>
			<td align="right" style="color:red">-1.95%</td>
				<td align="right">442.50</td>
			<td align="right">431.70</td>
		</tr>
				<tr>
			<td><a title="ICAP share price IAP share price" href="./share_price/symbol-IAP">ICAP</a></td>
			<td title="IAP share price"align="right" style="color:blue">451.20</td>
			<td align="right" style="color:blue">2.66%</td>
				<td align="right">451.50</td>
			<td align="right">437.80</td>
		</tr>
				<tr>
			<td><a title="IG Group Holdings share price IGG share price" href="./share_price/symbol-IGG">IG Group Holdings</a></td>
			<td title="IGG share price"align="right" style="color:blue">749.50</td>
			<td align="right" style="color:blue">2.81%</td>
				<td align="right">752.00</td>
			<td align="right">729.00</td>
		</tr>
				<tr>
			<td><a title="IMI share price IMI share price" href="./share_price/symbol-IMI">IMI</a></td>
			<td title="IMI share price"align="right" style="color:blue">1,210.00</td>
			<td align="right" style="color:blue">0.50%</td>
				<td align="right">1,214.00</td>
			<td align="right">1,200.00</td>
		</tr>
				<tr>
			<td><a title="Imperial Tobacco Group share price IMT share price" href="./share_price/symbol-IMT">Imperial Tobacco Group</a></td>
			<td title="IMT share price"align="right" style="color:blue">2,973.00</td>
			<td align="right" style="color:blue">0.20%</td>
				<td align="right">2,984.00</td>
			<td align="right">2,952.00</td>
		</tr>
				<tr>
			<td><a title="Inchcape share price INCH share price" href="./share_price/symbol-INCH">Inchcape</a></td>
			<td title="INCH share price"align="right" style="color:blue">711.50</td>
			<td align="right" style="color:blue">0.64%</td>
				<td align="right">712.50</td>
			<td align="right">705.50</td>
		</tr>
				<tr>
			<td><a title="Infinis Energy share price INFI share price" href="./share_price/symbol-INFI">Infinis Energy</a></td>
			<td title="INFI share price"align="right" style="color:red">208.00</td>
			<td align="right" style="color:red">-0.72%</td>
				<td align="right">215.90</td>
			<td align="right">206.74</td>
		</tr>
				<tr>
			<td><a title="Informa share price INF share price" href="./share_price/symbol-INF">Informa</a></td>
			<td title="INF share price"align="right" style="color:blue">485.90</td>
			<td align="right" style="color:blue">2.29%</td>
				<td align="right">486.40</td>
			<td align="right">474.80</td>
		</tr>
				<tr>
			<td><a title="Inmarsat share price ISAT share price" href="./share_price/symbol-ISAT">Inmarsat</a></td>
			<td title="ISAT share price"align="right" style="color:blue">819.00</td>
			<td align="right" style="color:blue">0.24%</td>
				<td align="right">825.74</td>
			<td align="right">811.00</td>
		</tr>
				<tr>
			<td><a title="InterContinental Hotels Group share price IHG share price" href="./share_price/symbol-IHG">InterContinental Hotels Group</a></td>
			<td title="IHG share price"align="right" style="color:blue">2,654.00</td>
			<td align="right" style="color:blue">0.68%</td>
				<td align="right">2,701.00</td>
			<td align="right">2,622.00</td>
		</tr>
				<tr>
			<td><a title="Intermediate Capital Group share price ICP share price" href="./share_price/symbol-ICP">Intermediate Capital Group</a></td>
			<td title="ICP share price"align="right" style="color:blue">466.40</td>
			<td align="right" style="color:blue">0.09%</td>
				<td align="right">467.40</td>
			<td align="right">463.90</td>
		</tr>
				<tr>
			<td><a title="International Consolidated Airlines Group SA (CDI) share price IAG share price" href="./share_price/symbol-IAG">International Consolidated Airlines Group SA (CDI)</a></td>
			<td title="IAG share price"align="right" style="color:blue">487.10</td>
			<td align="right" style="color:blue">3.79%</td>
				<td align="right">488.40</td>
			<td align="right">469.79</td>
		</tr>
				<tr>
			<td><a title="International Personal Finance share price IPF share price" href="./share_price/symbol-IPF">International Personal Finance</a></td>
			<td title="IPF share price"align="right" style="color:blue">432.40</td>
			<td align="right" style="color:blue">1.84%</td>
				<td align="right">433.20</td>
			<td align="right">417.30</td>
		</tr>
				<tr>
			<td><a title="International Power share price IPR share price" href="./share_price/symbol-IPR">International Power</a></td>
			<td title="IPR share price"align="right" style="color:">417.50</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">417.70</td>
			<td align="right">417.40</td>
		</tr>
				<tr>
			<td><a title="International Public Partnerships Ltd. share price INPP share price" href="./share_price/symbol-INPP">International Public Partnerships Ltd.</a></td>
			<td title="INPP share price"align="right" style="color:blue">136.20</td>
			<td align="right" style="color:blue">0.07%</td>
				<td align="right">136.50</td>
			<td align="right">135.95</td>
		</tr>
				<tr>
			<td><a title="Interserve share price IRV share price" href="./share_price/symbol-IRV">Interserve</a></td>
			<td title="IRV share price"align="right" style="color:red">519.50</td>
			<td align="right" style="color:red">-0.10%</td>
				<td align="right">523.00</td>
			<td align="right">506.34</td>
		</tr>
				<tr>
			<td><a title="Intertek Group share price ITRK share price" href="./share_price/symbol-ITRK">Intertek Group</a></td>
			<td title="ITRK share price"align="right" style="color:red">2,362.00</td>
			<td align="right" style="color:red">-1.30%</td>
				<td align="right">2,412.00</td>
			<td align="right">2,353.00</td>
		</tr>
				<tr>
			<td><a title="Intu Properties share price INTU share price" href="./share_price/symbol-INTU">Intu Properties</a></td>
			<td title="INTU share price"align="right" style="color:blue">342.20</td>
			<td align="right" style="color:blue">1.91%</td>
				<td align="right">342.70</td>
			<td align="right">335.40</td>
		</tr>
				<tr>
			<td><a title="Investec share price INVP share price" href="./share_price/symbol-INVP">Investec</a></td>
			<td title="INVP share price"align="right" style="color:blue">544.50</td>
			<td align="right" style="color:blue">2.16%</td>
				<td align="right">545.50</td>
			<td align="right">533.00</td>
		</tr>
				<tr>
			<td><a title="IP Group share price IPO share price" href="./share_price/symbol-IPO">IP Group</a></td>
			<td title="IPO share price"align="right" style="color:blue">218.10</td>
			<td align="right" style="color:blue">2.11%</td>
				<td align="right">222.80</td>
			<td align="right">212.10</td>
		</tr>
				<tr>
			<td><a title="ITV share price ITV share price" href="./share_price/symbol-ITV">ITV</a></td>
			<td title="ITV share price"align="right" style="color:blue">220.10</td>
			<td align="right" style="color:blue">3.58%</td>
				<td align="right">220.30</td>
			<td align="right">216.00</td>
		</tr>
				<tr>
			<td><a title="Jardine Lloyd Thompson Group share price JLT share price" href="./share_price/symbol-JLT">Jardine Lloyd Thompson Group</a></td>
			<td title="JLT share price"align="right" style="color:blue">916.00</td>
			<td align="right" style="color:blue">1.33%</td>
				<td align="right">917.00</td>
			<td align="right">906.50</td>
		</tr>
				<tr>
			<td><a title="JD Sports Fashion share price JD. share price" href="./share_price/symbol-JD.">JD Sports Fashion</a></td>
			<td title="JD. share price"align="right" style="color:red">468.30</td>
			<td align="right" style="color:red">-0.68%</td>
				<td align="right">481.93</td>
			<td align="right">467.10</td>
		</tr>
				<tr>
			<td><a title="John Laing Infrastructure Fund Ltd share price JLIF share price" href="./share_price/symbol-JLIF">John Laing Infrastructure Fund Ltd</a></td>
			<td title="JLIF share price"align="right" style="color:blue">122.00</td>
			<td align="right" style="color:blue">0.16%</td>
				<td align="right">122.50</td>
			<td align="right">121.40</td>
		</tr>
				<tr>
			<td><a title="Johnson Matthey share price JMAT share price" href="./share_price/symbol-JMAT">Johnson Matthey</a></td>
			<td title="JMAT share price"align="right" style="color:blue">3,492.00</td>
			<td align="right" style="color:blue">0.03%</td>
				<td align="right">3,522.00</td>
			<td align="right">3,463.80</td>
		</tr>
				<tr>
			<td><a title="JPMorgan American Inv Trust share price JAM share price" href="./share_price/symbol-JAM">JPMorgan American Inv Trust</a></td>
			<td title="JAM share price"align="right" style="color:blue">293.90</td>
			<td align="right" style="color:blue">0.58%</td>
				<td align="right">293.90</td>
			<td align="right">290.50</td>
		</tr>
				<tr>
			<td><a title="JPMorgan Emerging Markets Inv Trust share price JMG share price" href="./share_price/symbol-JMG">JPMorgan Emerging Markets Inv Trust</a></td>
			<td title="JMG share price"align="right" style="color:blue">604.50</td>
			<td align="right" style="color:blue">0.92%</td>
				<td align="right">605.00</td>
			<td align="right">599.53</td>
		</tr>
				<tr>
			<td><a title="JPMorgan European Fledgling Inv Trust share price JFF share price" href="./share_price/symbol-JFF">JPMorgan European Fledgling Inv Trust</a></td>
			<td title="JFF share price"align="right" style="color:">684.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">684.00</td>
			<td align="right">684.00</td>
		</tr>
				<tr>
			<td><a title="Jupiter Fund Management share price JUP share price" href="./share_price/symbol-JUP">Jupiter Fund Management</a></td>
			<td title="JUP share price"align="right" style="color:">345.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">346.50</td>
			<td align="right">338.79</td>
		</tr>
				<tr>
			<td><a title="Just Retirement Group share price JRG share price" href="./share_price/symbol-JRG">Just Retirement Group</a></td>
			<td title="JRG share price"align="right" style="color:blue">137.30</td>
			<td align="right" style="color:blue">0.07%</td>
				<td align="right">141.10</td>
			<td align="right">136.38</td>
		</tr>
				<tr>
			<td><a title="Kaz Minerals share price KAZ share price" href="./share_price/symbol-KAZ">Kaz Minerals</a></td>
			<td title="KAZ share price"align="right" style="color:red">233.70</td>
			<td align="right" style="color:red">-7.48%</td>
				<td align="right">250.90</td>
			<td align="right">232.70</td>
		</tr>
				<tr>
			<td><a title="Keller Group share price KLR share price" href="./share_price/symbol-KLR">Keller Group</a></td>
			<td title="KLR share price"align="right" style="color:blue">905.00</td>
			<td align="right" style="color:blue">1.91%</td>
				<td align="right">906.06</td>
			<td align="right">888.00</td>
		</tr>
				<tr>
			<td><a title="Kentz Corporation Ltd. share price KENZ share price" href="./share_price/symbol-KENZ">Kentz Corporation Ltd.</a></td>
			<td title="KENZ share price"align="right" style="color:">934.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">934.00</td>
			<td align="right">934.00</td>
		</tr>
				<tr>
			<td><a title="Kier Group share price KIE share price" href="./share_price/symbol-KIE">Kier Group</a></td>
			<td title="KIE share price"align="right" style="color:red">1,501.00</td>
			<td align="right" style="color:red">-0.27%</td>
				<td align="right">1,510.00</td>
			<td align="right">1,477.00</td>
		</tr>
				<tr>
			<td><a title="Kingfisher share price KGF share price" href="./share_price/symbol-KGF">Kingfisher</a></td>
			<td title="KGF share price"align="right" style="color:blue">331.30</td>
			<td align="right" style="color:blue">2.13%</td>
				<td align="right">331.80</td>
			<td align="right">324.40</td>
		</tr>
				<tr>
			<td><a title="Ladbrokes share price LAD share price" href="./share_price/symbol-LAD">Ladbrokes</a></td>
			<td title="LAD share price"align="right" style="color:blue">108.10</td>
			<td align="right" style="color:blue">3.25%</td>
				<td align="right">108.60</td>
			<td align="right">104.70</td>
		</tr>
				<tr>
			<td><a title="Laird share price LRD share price" href="./share_price/symbol-LRD">Laird</a></td>
			<td title="LRD share price"align="right" style="color:red">310.20</td>
			<td align="right" style="color:red">-2.27%</td>
				<td align="right">318.06</td>
			<td align="right">306.50</td>
		</tr>
				<tr>
			<td><a title="Lancashire Holdings Limited share price LRE share price" href="./share_price/symbol-LRE">Lancashire Holdings Limited</a></td>
			<td title="LRE share price"align="right" style="color:blue">571.00</td>
			<td align="right" style="color:blue">1.69%</td>
				<td align="right">571.50</td>
			<td align="right">557.00</td>
		</tr>
				<tr>
			<td><a title="Land Securities Group share price LAND share price" href="./share_price/symbol-LAND">Land Securities Group</a></td>
			<td title="LAND share price"align="right" style="color:blue">1,204.00</td>
			<td align="right" style="color:blue">0.92%</td>
				<td align="right">1,211.00</td>
			<td align="right">1,190.00</td>
		</tr>
				<tr>
			<td><a title="Law Debenture Corp. share price LWDB share price" href="./share_price/symbol-LWDB">Law Debenture Corp.</a></td>
			<td title="LWDB share price"align="right" style="color:blue">524.50</td>
			<td align="right" style="color:blue">1.35%</td>
				<td align="right">524.59</td>
			<td align="right">517.50</td>
		</tr>
				<tr>
			<td><a title="Legal & General Group share price LGEN share price" href="./share_price/symbol-LGEN">Legal & General Group</a></td>
			<td title="LGEN share price"align="right" style="color:blue">247.70</td>
			<td align="right" style="color:blue">1.23%</td>
				<td align="right">247.80</td>
			<td align="right">243.50</td>
		</tr>
				<tr>
			<td><a title="Lloyds Banking Group share price LLOY share price" href="./share_price/symbol-LLOY">Lloyds Banking Group</a></td>
			<td title="LLOY share price"align="right" style="color:blue">74.00</td>
			<td align="right" style="color:blue">0.58%</td>
				<td align="right">74.26</td>
			<td align="right">73.02</td>
		</tr>
				<tr>
			<td><a title="London & Stamford Property  share price LSP share price" href="./share_price/symbol-LSP">London & Stamford Property </a></td>
			<td title="LSP share price"align="right" style="color:red">113.50</td>
			<td align="right" style="color:red">-3.07%</td>
				<td align="right">117.10</td>
			<td align="right">112.30</td>
		</tr>
				<tr>
			<td><a title="London Stock Exchange Group share price LSE share price" href="./share_price/symbol-LSE">London Stock Exchange Group</a></td>
			<td title="LSE share price"align="right" style="color:blue">2,258.00</td>
			<td align="right" style="color:blue">2.96%</td>
				<td align="right">2,259.00</td>
			<td align="right">2,188.00</td>
		</tr>
				<tr>
			<td><a title="LondonMetric Property share price LMP share price" href="./share_price/symbol-LMP">LondonMetric Property</a></td>
			<td title="LMP share price"align="right" style="color:">151.10</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">152.10</td>
			<td align="right">150.20</td>
		</tr>
				<tr>
			<td><a title="Lonmin share price LMI share price" href="./share_price/symbol-LMI">Lonmin</a></td>
			<td title="LMI share price"align="right" style="color:red">166.60</td>
			<td align="right" style="color:red">-2.00%</td>
				<td align="right">171.00</td>
			<td align="right">166.10</td>
		</tr>
				<tr>
			<td><a title="Man Group share price EMG share price" href="./share_price/symbol-EMG">Man Group</a></td>
			<td title="EMG share price"align="right" style="color:blue">162.00</td>
			<td align="right" style="color:blue">0.19%</td>
				<td align="right">163.90</td>
			<td align="right">160.60</td>
		</tr>
				<tr>
			<td><a title="Marks & Spencer Group share price MKS share price" href="./share_price/symbol-MKS">Marks & Spencer Group</a></td>
			<td title="MKS share price"align="right" style="color:blue">463.10</td>
			<td align="right" style="color:blue">3.30%</td>
				<td align="right">463.50</td>
			<td align="right">446.30</td>
		</tr>
				<tr>
			<td><a title="Marston's share price MARS share price" href="./share_price/symbol-MARS">Marston's</a></td>
			<td title="MARS share price"align="right" style="color:blue">146.00</td>
			<td align="right" style="color:blue">2.46%</td>
				<td align="right">147.30</td>
			<td align="right">142.90</td>
		</tr>
				<tr>
			<td><a title="Meggitt share price MGGT share price" href="./share_price/symbol-MGGT">Meggitt</a></td>
			<td title="MGGT share price"align="right" style="color:blue">520.50</td>
			<td align="right" style="color:blue">0.29%</td>
				<td align="right">524.00</td>
			<td align="right">516.00</td>
		</tr>
				<tr>
			<td><a title="Melrose Industries share price MRO share price" href="./share_price/symbol-MRO">Melrose Industries</a></td>
			<td title="MRO share price"align="right" style="color:blue">266.50</td>
			<td align="right" style="color:blue">0.08%</td>
				<td align="right">268.20</td>
			<td align="right">264.40</td>
		</tr>
				<tr>
			<td><a title="Mercantile Investment Trust (The) share price MRC share price" href="./share_price/symbol-MRC">Mercantile Investment Trust (The)</a></td>
			<td title="MRC share price"align="right" style="color:blue">1,469.00</td>
			<td align="right" style="color:blue">1.59%</td>
				<td align="right">1,469.00</td>
			<td align="right">1,446.50</td>
		</tr>
				<tr>
			<td><a title="Merlin Entertainments share price MERL share price" href="./share_price/symbol-MERL">Merlin Entertainments</a></td>
			<td title="MERL share price"align="right" style="color:blue">401.30</td>
			<td align="right" style="color:blue">2.92%</td>
				<td align="right">401.30</td>
			<td align="right">386.50</td>
		</tr>
				<tr>
			<td><a title="Michael Page International share price MPI share price" href="./share_price/symbol-MPI">Michael Page International</a></td>
			<td title="MPI share price"align="right" style="color:blue">454.30</td>
			<td align="right" style="color:blue">6.52%</td>
				<td align="right">454.60</td>
			<td align="right">427.00</td>
		</tr>
				<tr>
			<td><a title="Micro Focus International share price MCRO share price" href="./share_price/symbol-MCRO">Micro Focus International</a></td>
			<td title="MCRO share price"align="right" style="color:red">1,045.00</td>
			<td align="right" style="color:red">-0.29%</td>
				<td align="right">1,051.00</td>
			<td align="right">1,034.00</td>
		</tr>
				<tr>
			<td><a title="Millennium & Copthorne Hotels share price MLC share price" href="./share_price/symbol-MLC">Millennium & Copthorne Hotels</a></td>
			<td title="MLC share price"align="right" style="color:red">571.50</td>
			<td align="right" style="color:red">-0.44%</td>
				<td align="right">584.50</td>
			<td align="right">569.00</td>
		</tr>
				<tr>
			<td><a title="Misys share price MSY share price" href="./share_price/symbol-MSY">Misys</a></td>
			<td title="MSY share price"align="right" style="color:">349.70</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">350.40</td>
			<td align="right">349.50</td>
		</tr>
				<tr>
			<td><a title="Mitchells & Butlers share price MAB share price" href="./share_price/symbol-MAB">Mitchells & Butlers</a></td>
			<td title="MAB share price"align="right" style="color:blue">411.80</td>
			<td align="right" style="color:blue">0.39%</td>
				<td align="right">419.43</td>
			<td align="right">410.90</td>
		</tr>
				<tr>
			<td><a title="Mitie Group share price MTO share price" href="./share_price/symbol-MTO">Mitie Group</a></td>
			<td title="MTO share price"align="right" style="color:blue">273.20</td>
			<td align="right" style="color:blue">0.66%</td>
				<td align="right">275.30</td>
			<td align="right">269.70</td>
		</tr>
				<tr>
			<td><a title="Mondi share price MNDI share price" href="./share_price/symbol-MNDI">Mondi</a></td>
			<td title="MNDI share price"align="right" style="color:blue">1,084.00</td>
			<td align="right" style="color:blue">1.59%</td>
				<td align="right">1,089.00</td>
			<td align="right">1,065.00</td>
		</tr>
				<tr>
			<td><a title="Moneysupermarket.com Group share price MONY share price" href="./share_price/symbol-MONY">Moneysupermarket.com Group</a></td>
			<td title="MONY share price"align="right" style="color:blue">227.00</td>
			<td align="right" style="color:blue">0.49%</td>
				<td align="right">230.90</td>
			<td align="right">225.30</td>
		</tr>
				<tr>
			<td><a title="Monks Inv Trust share price MNKS share price" href="./share_price/symbol-MNKS">Monks Inv Trust</a></td>
			<td title="MNKS share price"align="right" style="color:blue">394.50</td>
			<td align="right" style="color:blue">0.77%</td>
				<td align="right">394.50</td>
			<td align="right">390.00</td>
		</tr>
				<tr>
			<td><a title="Morgan Advanced Materials share price MGAM share price" href="./share_price/symbol-MGAM">Morgan Advanced Materials</a></td>
			<td title="MGAM share price"align="right" style="color:blue">309.00</td>
			<td align="right" style="color:blue">1.54%</td>
				<td align="right">309.70</td>
			<td align="right">304.83</td>
		</tr>
				<tr>
			<td><a title="Morgan Crucible Co share price MGCR share price" href="./share_price/symbol-MGCR">Morgan Crucible Co</a></td>
			<td title="MGCR share price"align="right" style="color:red">282.40</td>
			<td align="right" style="color:red">-0.81%</td>
				<td align="right">290.70</td>
			<td align="right">281.20</td>
		</tr>
				<tr>
			<td><a title="Morrison (Wm) Supermarkets share price MRW share price" href="./share_price/symbol-MRW">Morrison (Wm) Supermarkets</a></td>
			<td title="MRW share price"align="right" style="color:blue">184.20</td>
			<td align="right" style="color:blue">4.13%</td>
				<td align="right">189.10</td>
			<td align="right">179.80</td>
		</tr>
				<tr>
			<td><a title="Murray International Trust share price MYI share price" href="./share_price/symbol-MYI">Murray International Trust</a></td>
			<td title="MYI share price"align="right" style="color:blue">1,016.00</td>
			<td align="right" style="color:blue">1.30%</td>
				<td align="right">1,016.00</td>
			<td align="right">1,002.00</td>
		</tr>
				<tr>
			<td><a title="National Express Group share price NEX share price" href="./share_price/symbol-NEX">National Express Group</a></td>
			<td title="NEX share price"align="right" style="color:blue">259.80</td>
			<td align="right" style="color:blue">1.05%</td>
				<td align="right">260.30</td>
			<td align="right">256.50</td>
		</tr>
				<tr>
			<td><a title="National Grid share price NG. share price" href="./share_price/symbol-NG.">National Grid</a></td>
			<td title="NG. share price"align="right" style="color:blue">931.60</td>
			<td align="right" style="color:blue">1.19%</td>
				<td align="right">937.90</td>
			<td align="right">918.90</td>
		</tr>
				<tr>
			<td><a title="NB Global Floating Rate Income Fund Ltd GBP share price NBLS share price" href="./share_price/symbol-NBLS">NB Global Floating Rate Income Fund Ltd GBP</a></td>
			<td title="NBLS share price"align="right" style="color:blue">97.15</td>
			<td align="right" style="color:blue">0.21%</td>
				<td align="right">97.30</td>
			<td align="right">96.90</td>
		</tr>
				<tr>
			<td><a title="Next share price NXT share price" href="./share_price/symbol-NXT">Next</a></td>
			<td title="NXT share price"align="right" style="color:blue">7,020.00</td>
			<td align="right" style="color:blue">1.08%</td>
				<td align="right">7,025.00</td>
			<td align="right">6,925.00</td>
		</tr>
				<tr>
			<td><a title="NMC Health share price NMC share price" href="./share_price/symbol-NMC">NMC Health</a></td>
			<td title="NMC share price"align="right" style="color:">500.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">510.52</td>
			<td align="right">496.00</td>
		</tr>
				<tr>
			<td><a title="Northgate share price NTG share price" href="./share_price/symbol-NTG">Northgate</a></td>
			<td title="NTG share price"align="right" style="color:blue">606.50</td>
			<td align="right" style="color:blue">0.25%</td>
				<td align="right">611.00</td>
			<td align="right">603.00</td>
		</tr>
				<tr>
			<td><a title="Northumbrian Water Group share price NWG share price" href="./share_price/symbol-NWG">Northumbrian Water Group</a></td>
			<td title="NWG share price"align="right" style="color:">464.20</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">464.60</td>
			<td align="right">464.00</td>
		</tr>
				<tr>
			<td><a title="Nostrum Oil & Gas share price NOG share price" href="./share_price/symbol-NOG">Nostrum Oil & Gas</a></td>
			<td title="NOG share price"align="right" style="color:blue">437.40</td>
			<td align="right" style="color:blue">1.16%</td>
				<td align="right">450.00</td>
			<td align="right">435.00</td>
		</tr>
				<tr>
			<td><a title="Ocado Group share price OCDO share price" href="./share_price/symbol-OCDO">Ocado Group</a></td>
			<td title="OCDO share price"align="right" style="color:blue">411.40</td>
			<td align="right" style="color:blue">2.88%</td>
				<td align="right">418.90</td>
			<td align="right">396.00</td>
		</tr>
				<tr>
			<td><a title="Old Mutual share price OML share price" href="./share_price/symbol-OML">Old Mutual</a></td>
			<td title="OML share price"align="right" style="color:blue">193.30</td>
			<td align="right" style="color:blue">1.31%</td>
				<td align="right">194.40</td>
			<td align="right">190.60</td>
		</tr>
				<tr>
			<td><a title="Ophir Energy share price OPHR share price" href="./share_price/symbol-OPHR">Ophir Energy</a></td>
			<td title="OPHR share price"align="right" style="color:red">127.90</td>
			<td align="right" style="color:red">-1.62%</td>
				<td align="right">130.00</td>
			<td align="right">126.70</td>
		</tr>
				<tr>
			<td><a title="Oxford Instruments share price OXIG share price" href="./share_price/symbol-OXIG">Oxford Instruments</a></td>
			<td title="OXIG share price"align="right" style="color:blue">1,137.00</td>
			<td align="right" style="color:blue">0.35%</td>
				<td align="right">1,145.50</td>
			<td align="right">1,116.50</td>
		</tr>
				<tr>
			<td><a title="Pace share price PIC share price" href="./share_price/symbol-PIC">Pace</a></td>
			<td title="PIC share price"align="right" style="color:blue">351.60</td>
			<td align="right" style="color:blue">2.78%</td>
				<td align="right">363.60</td>
			<td align="right">345.00</td>
		</tr>
				<tr>
			<td><a title="Paragon Group Of Companies share price PAG share price" href="./share_price/symbol-PAG">Paragon Group Of Companies</a></td>
			<td title="PAG share price"align="right" style="color:blue">405.30</td>
			<td align="right" style="color:blue">0.42%</td>
				<td align="right">414.50</td>
			<td align="right">403.90</td>
		</tr>
				<tr>
			<td><a title="PartyGaming share price PRTY share price" href="./share_price/symbol-PRTY">PartyGaming</a></td>
			<td title="PRTY share price"align="right" style="color:blue">204.00</td>
			<td align="right" style="color:blue">5.48%</td>
				<td align="right">207.90</td>
			<td align="right">191.30</td>
		</tr>
				<tr>
			<td><a title="PayPoint share price PAY share price" href="./share_price/symbol-PAY">PayPoint</a></td>
			<td title="PAY share price"align="right" style="color:red">871.00</td>
			<td align="right" style="color:red">-1.64%</td>
				<td align="right">899.50</td>
			<td align="right">863.00</td>
		</tr>
				<tr>
			<td><a title="Pearson share price PSON share price" href="./share_price/symbol-PSON">Pearson</a></td>
			<td title="PSON share price"align="right" style="color:blue">1,197.00</td>
			<td align="right" style="color:blue">1.01%</td>
				<td align="right">1,208.00</td>
			<td align="right">1,178.00</td>
		</tr>
				<tr>
			<td><a title="Pennon Group share price PNN share price" href="./share_price/symbol-PNN">Pennon Group</a></td>
			<td title="PNN share price"align="right" style="color:blue">915.00</td>
			<td align="right" style="color:blue">0.27%</td>
				<td align="right">919.50</td>
			<td align="right">909.00</td>
		</tr>
				<tr>
			<td><a title="Perpetual Income & Growth Inv Trust share price PLI share price" href="./share_price/symbol-PLI">Perpetual Income & Growth Inv Trust</a></td>
			<td title="PLI share price"align="right" style="color:blue">398.20</td>
			<td align="right" style="color:blue">0.56%</td>
				<td align="right">399.37</td>
			<td align="right">395.37</td>
		</tr>
				<tr>
			<td><a title="Persimmon share price PSN share price" href="./share_price/symbol-PSN">Persimmon</a></td>
			<td title="PSN share price"align="right" style="color:blue">1,494.00</td>
			<td align="right" style="color:blue">1.77%</td>
				<td align="right">1,503.64</td>
			<td align="right">1,460.00</td>
		</tr>
				<tr>
			<td><a title="Personal Assets Trust share price PNL share price" href="./share_price/symbol-PNL">Personal Assets Trust</a></td>
			<td title="PNL share price"align="right" style="color:blue">35,210.00</td>
			<td align="right" style="color:blue">0.57%</td>
				<td align="right">35,200.00</td>
			<td align="right">35,060.00</td>
		</tr>
				<tr>
			<td><a title="Petra Diamonds Ltd.(DI) share price PDL share price" href="./share_price/symbol-PDL">Petra Diamonds Ltd.(DI)</a></td>
			<td title="PDL share price"align="right" style="color:red">189.10</td>
			<td align="right" style="color:red">-0.68%</td>
				<td align="right">190.50</td>
			<td align="right">188.30</td>
		</tr>
				<tr>
			<td><a title="Petrofac Ltd. share price PFC share price" href="./share_price/symbol-PFC">Petrofac Ltd.</a></td>
			<td title="PFC share price"align="right" style="color:blue">613.50</td>
			<td align="right" style="color:blue">0.90%</td>
				<td align="right">622.00</td>
			<td align="right">602.00</td>
		</tr>
				<tr>
			<td><a title="Pets at Home Group share price PETS share price" href="./share_price/symbol-PETS">Pets at Home Group</a></td>
			<td title="PETS share price"align="right" style="color:red">199.70</td>
			<td align="right" style="color:red">-0.15%</td>
				<td align="right">202.70</td>
			<td align="right">197.10</td>
		</tr>
				<tr>
			<td><a title="Phoenix Group Holdings (DI) share price PHNX share price" href="./share_price/symbol-PHNX">Phoenix Group Holdings (DI)</a></td>
			<td title="PHNX share price"align="right" style="color:blue">826.00</td>
			<td align="right" style="color:blue">3.12%</td>
				<td align="right">826.00</td>
			<td align="right">797.00</td>
		</tr>
				<tr>
			<td><a title="Playtech share price PTEC share price" href="./share_price/symbol-PTEC">Playtech</a></td>
			<td title="PTEC share price"align="right" style="color:red">647.50</td>
			<td align="right" style="color:red">-0.69%</td>
				<td align="right">654.50</td>
			<td align="right">643.50</td>
		</tr>
				<tr>
			<td><a title="Polar Capital Technology Trust share price PCT share price" href="./share_price/symbol-PCT">Polar Capital Technology Trust</a></td>
			<td title="PCT share price"align="right" style="color:blue">580.00</td>
			<td align="right" style="color:blue">1.22%</td>
				<td align="right">580.00</td>
			<td align="right">573.88</td>
		</tr>
				<tr>
			<td><a title="Polymetal International share price POLY share price" href="./share_price/symbol-POLY">Polymetal International</a></td>
			<td title="POLY share price"align="right" style="color:red">567.50</td>
			<td align="right" style="color:red">-0.44%</td>
				<td align="right">574.50</td>
			<td align="right">563.00</td>
		</tr>
				<tr>
			<td><a title="Poundland Group share price PLND share price" href="./share_price/symbol-PLND">Poundland Group</a></td>
			<td title="PLND share price"align="right" style="color:blue">351.20</td>
			<td align="right" style="color:blue">1.01%</td>
				<td align="right">353.11</td>
			<td align="right">345.88</td>
		</tr>
				<tr>
			<td><a title="Premier Farnell share price PFL share price" href="./share_price/symbol-PFL">Premier Farnell</a></td>
			<td title="PFL share price"align="right" style="color:blue">165.00</td>
			<td align="right" style="color:blue">0.06%</td>
				<td align="right">166.70</td>
			<td align="right">163.70</td>
		</tr>
				<tr>
			<td><a title="Premier Oil share price PMO share price" href="./share_price/symbol-PMO">Premier Oil</a></td>
			<td title="PMO share price"align="right" style="color:blue">137.10</td>
			<td align="right" style="color:blue">1.26%</td>
				<td align="right">139.00</td>
			<td align="right">132.00</td>
		</tr>
				<tr>
			<td><a title="Provident Financial share price PFG share price" href="./share_price/symbol-PFG">Provident Financial</a></td>
			<td title="PFG share price"align="right" style="color:blue">2,533.00</td>
			<td align="right" style="color:blue">1.36%</td>
				<td align="right">2,536.00</td>
			<td align="right">2,494.00</td>
		</tr>
				<tr>
			<td><a title="Prudential share price PRU share price" href="./share_price/symbol-PRU">Prudential</a></td>
			<td title="PRU share price"align="right" style="color:blue">1,499.50</td>
			<td align="right" style="color:blue">1.42%</td>
				<td align="right">1,502.50</td>
			<td align="right">1,465.41</td>
		</tr>
				<tr>
			<td><a title="PZ Cussons share price PZC share price" href="./share_price/symbol-PZC">PZ Cussons</a></td>
			<td title="PZC share price"align="right" style="color:blue">305.90</td>
			<td align="right" style="color:blue">2.69%</td>
				<td align="right">306.00</td>
			<td align="right">296.50</td>
		</tr>
				<tr>
			<td><a title="QinetiQ Group share price QQ. share price" href="./share_price/symbol-QQ.">QinetiQ Group</a></td>
			<td title="QQ. share price"align="right" style="color:blue">187.50</td>
			<td align="right" style="color:blue">0.37%</td>
				<td align="right">188.90</td>
			<td align="right">186.60</td>
		</tr>
				<tr>
			<td><a title="Randgold Resources Ltd. share price RRS share price" href="./share_price/symbol-RRS">Randgold Resources Ltd.</a></td>
			<td title="RRS share price"align="right" style="color:red">4,974.00</td>
			<td align="right" style="color:red">-0.92%</td>
				<td align="right">5,135.63</td>
			<td align="right">4,960.56</td>
		</tr>
				<tr>
			<td><a title="Rank Group share price RNK share price" href="./share_price/symbol-RNK">Rank Group</a></td>
			<td title="RNK share price"align="right" style="color:blue">158.80</td>
			<td align="right" style="color:blue">0.51%</td>
				<td align="right">161.00</td>
			<td align="right">158.00</td>
		</tr>
				<tr>
			<td><a title="Rathbone Brothers share price RAT share price" href="./share_price/symbol-RAT">Rathbone Brothers</a></td>
			<td title="RAT share price"align="right" style="color:blue">2,079.00</td>
			<td align="right" style="color:blue">1.76%</td>
				<td align="right">2,081.00</td>
			<td align="right">2,044.00</td>
		</tr>
				<tr>
			<td><a title="Reckitt Benckiser Group share price RB. share price" href="./share_price/symbol-RB.">Reckitt Benckiser Group</a></td>
			<td title="RB. share price"align="right" style="color:blue">5,340.00</td>
			<td align="right" style="color:blue">0.19%</td>
				<td align="right">5,388.61</td>
			<td align="right">5,290.00</td>
		</tr>
				<tr>
			<td><a title="Redefine International share price RDI share price" href="./share_price/symbol-RDI">Redefine International</a></td>
			<td title="RDI share price"align="right" style="color:blue">55.75</td>
			<td align="right" style="color:blue">1.09%</td>
				<td align="right">56.45</td>
			<td align="right">55.30</td>
		</tr>
				<tr>
			<td><a title="Redrow share price RDW share price" href="./share_price/symbol-RDW">Redrow</a></td>
			<td title="RDW share price"align="right" style="color:blue">274.00</td>
			<td align="right" style="color:blue">3.36%</td>
				<td align="right">274.90</td>
			<td align="right">264.00</td>
		</tr>
				<tr>
			<td><a title="Reed Elsevier share price REL share price" href="./share_price/symbol-REL">Reed Elsevier</a></td>
			<td title="REL share price"align="right" style="color:blue">1,106.00</td>
			<td align="right" style="color:blue">0.55%</td>
				<td align="right">1,119.50</td>
			<td align="right">1,098.00</td>
		</tr>
				<tr>
			<td><a title="Regus share price RGU share price" href="./share_price/symbol-RGU">Regus</a></td>
			<td title="RGU share price"align="right" style="color:blue">205.50</td>
			<td align="right" style="color:blue">0.54%</td>
				<td align="right">206.80</td>
			<td align="right">205.00</td>
		</tr>
				<tr>
			<td><a title="Renishaw share price RSW share price" href="./share_price/symbol-RSW">Renishaw</a></td>
			<td title="RSW share price"align="right" style="color:blue">2,331.00</td>
			<td align="right" style="color:blue">0.47%</td>
				<td align="right">2,335.97</td>
			<td align="right">2,282.05</td>
		</tr>
				<tr>
			<td><a title="Rentokil Initial share price RTO share price" href="./share_price/symbol-RTO">Rentokil Initial</a></td>
			<td title="RTO share price"align="right" style="color:blue">121.00</td>
			<td align="right" style="color:blue">1.68%</td>
				<td align="right">121.90</td>
			<td align="right">119.20</td>
		</tr>
				<tr>
			<td><a title="Resolution Ltd. share price RSL share price" href="./share_price/symbol-RSL">Resolution Ltd.</a></td>
			<td title="RSL share price"align="right" style="color:blue">302.90</td>
			<td align="right" style="color:blue">0.43%</td>
				<td align="right">304.40</td>
			<td align="right">301.00</td>
		</tr>
				<tr>
			<td><a title="Restaurant Group share price RTN share price" href="./share_price/symbol-RTN">Restaurant Group</a></td>
			<td title="RTN share price"align="right" style="color:blue">687.50</td>
			<td align="right" style="color:blue">0.73%</td>
				<td align="right">687.50</td>
			<td align="right">673.00</td>
		</tr>
				<tr>
			<td><a title="Rexam share price REX share price" href="./share_price/symbol-REX">Rexam</a></td>
			<td title="REX share price"align="right" style="color:blue">445.50</td>
			<td align="right" style="color:blue">0.29%</td>
				<td align="right">446.40</td>
			<td align="right">443.10</td>
		</tr>
				<tr>
			<td><a title="Rightmove share price RMV share price" href="./share_price/symbol-RMV">Rightmove</a></td>
			<td title="RMV share price"align="right" style="color:blue">2,154.00</td>
			<td align="right" style="color:blue">1.36%</td>
				<td align="right">2,169.00</td>
			<td align="right">2,123.00</td>
		</tr>
				<tr>
			<td><a title="Rio Tinto share price RIO share price" href="./share_price/symbol-RIO">Rio Tinto</a></td>
			<td title="RIO share price"align="right" style="color:red">2,907.00</td>
			<td align="right" style="color:red">-0.60%</td>
				<td align="right">2,928.50</td>
			<td align="right">2,893.00</td>
		</tr>
				<tr>
			<td><a title="RIT Capital Partners share price RCP share price" href="./share_price/symbol-RCP">RIT Capital Partners</a></td>
			<td title="RCP share price"align="right" style="color:blue">1,424.00</td>
			<td align="right" style="color:blue">1.21%</td>
				<td align="right">1,425.00</td>
			<td align="right">1,408.80</td>
		</tr>
				<tr>
			<td><a title="Riverstone Energy Limited share price RSE share price" href="./share_price/symbol-RSE">Riverstone Energy Limited</a></td>
			<td title="RSE share price"align="right" style="color:blue">868.50</td>
			<td align="right" style="color:blue">0.81%</td>
				<td align="right">879.00</td>
			<td align="right">860.50</td>
		</tr>
				<tr>
			<td><a title="Rolls-Royce Holdings share price RR. share price" href="./share_price/symbol-RR.">Rolls-Royce Holdings</a></td>
			<td title="RR. share price"align="right" style="color:red">857.50</td>
			<td align="right" style="color:red">-1.27%</td>
				<td align="right">868.54</td>
			<td align="right">849.83</td>
		</tr>
				<tr>
			<td><a title="Rotork share price ROR share price" href="./share_price/symbol-ROR">Rotork</a></td>
			<td title="ROR share price"align="right" style="color:blue">2,329.00</td>
			<td align="right" style="color:blue">2.78%</td>
				<td align="right">2,333.00</td>
			<td align="right">2,261.00</td>
		</tr>
				<tr>
			<td><a title="Royal Bank of Scotland Group share price RBS share price" href="./share_price/symbol-RBS">Royal Bank of Scotland Group</a></td>
			<td title="RBS share price"align="right" style="color:blue">374.20</td>
			<td align="right" style="color:blue">1.55%</td>
				<td align="right">374.90</td>
			<td align="right">365.49</td>
		</tr>
				<tr>
			<td><a title="Royal Dutch Shell 'A' share price RDSA share price" href="./share_price/symbol-RDSA">Royal Dutch Shell 'A'</a></td>
			<td title="RDSA share price"align="right" style="color:red">2,074.00</td>
			<td align="right" style="color:red">-0.22%</td>
				<td align="right">2,089.50</td>
			<td align="right">2,038.50</td>
		</tr>
				<tr>
			<td><a title="Royal Dutch Shell 'B' share price RDSB share price" href="./share_price/symbol-RDSB">Royal Dutch Shell 'B'</a></td>
			<td title="RDSB share price"align="right" style="color:red">2,122.00</td>
			<td align="right" style="color:red">-0.21%</td>
				<td align="right">2,137.50</td>
			<td align="right">2,085.79</td>
		</tr>
				<tr>
			<td><a title="Royal Mail share price RMG share price" href="./share_price/symbol-RMG">Royal Mail</a></td>
			<td title="RMG share price"align="right" style="color:blue">426.70</td>
			<td align="right" style="color:blue">1.33%</td>
				<td align="right">428.80</td>
			<td align="right">421.00</td>
		</tr>
				<tr>
			<td><a title="RPC Group share price RPC share price" href="./share_price/symbol-RPC">RPC Group</a></td>
			<td title="RPC share price"align="right" style="color:blue">551.50</td>
			<td align="right" style="color:blue">2.51%</td>
				<td align="right">553.00</td>
			<td align="right">534.50</td>
		</tr>
				<tr>
			<td><a title="RPS Group share price RPS share price" href="./share_price/symbol-RPS">RPS Group</a></td>
			<td title="RPS share price"align="right" style="color:red">197.90</td>
			<td align="right" style="color:red">-0.50%</td>
				<td align="right">199.50</td>
			<td align="right">196.30</td>
		</tr>
				<tr>
			<td><a title="RSA Insurance Group share price RSA share price" href="./share_price/symbol-RSA">RSA Insurance Group</a></td>
			<td title="RSA share price"align="right" style="color:blue">445.20</td>
			<td align="right" style="color:blue">2.11%</td>
				<td align="right">445.20</td>
			<td align="right">432.60</td>
		</tr>
				<tr>
			<td><a title="SABMiller share price SAB share price" href="./share_price/symbol-SAB">SABMiller</a></td>
			<td title="SAB share price"align="right" style="color:blue">3,392.00</td>
			<td align="right" style="color:blue">1.18%</td>
				<td align="right">3,394.50</td>
			<td align="right">3,337.00</td>
		</tr>
				<tr>
			<td><a title="Sage Group share price SGE share price" href="./share_price/symbol-SGE">Sage Group</a></td>
			<td title="SGE share price"align="right" style="color:red">461.00</td>
			<td align="right" style="color:red">-0.86%</td>
				<td align="right">467.50</td>
			<td align="right">458.70</td>
		</tr>
				<tr>
			<td><a title="Sainsbury (J) share price SBRY share price" href="./share_price/symbol-SBRY">Sainsbury (J)</a></td>
			<td title="SBRY share price"align="right" style="color:blue">248.40</td>
			<td align="right" style="color:blue">3.37%</td>
				<td align="right">252.00</td>
			<td align="right">241.70</td>
		</tr>
				<tr>
			<td><a title="Savills share price SVS share price" href="./share_price/symbol-SVS">Savills</a></td>
			<td title="SVS share price"align="right" style="color:">648.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">660.00</td>
			<td align="right">646.50</td>
		</tr>
				<tr>
			<td><a title="Schroders share price SDR share price" href="./share_price/symbol-SDR">Schroders</a></td>
			<td title="SDR share price"align="right" style="color:blue">2,685.00</td>
			<td align="right" style="color:blue">2.44%</td>
				<td align="right">2,896.00</td>
			<td align="right">2,631.00</td>
		</tr>
				<tr>
			<td><a title="Scottish Inv Trust share price SCIN share price" href="./share_price/symbol-SCIN">Scottish Inv Trust</a></td>
			<td title="SCIN share price"align="right" style="color:blue">610.50</td>
			<td align="right" style="color:blue">0.58%</td>
				<td align="right">610.50</td>
			<td align="right">605.90</td>
		</tr>
				<tr>
			<td><a title="Scottish Mortgage Inv Trust share price SMT share price" href="./share_price/symbol-SMT">Scottish Mortgage Inv Trust</a></td>
			<td title="SMT share price"align="right" style="color:blue">249.50</td>
			<td align="right" style="color:blue">0.77%</td>
				<td align="right">249.70</td>
			<td align="right">247.00</td>
		</tr>
				<tr>
			<td><a title="SEGRO share price SGRO share price" href="./share_price/symbol-SGRO">SEGRO</a></td>
			<td title="SGRO share price"align="right" style="color:red">398.60</td>
			<td align="right" style="color:red">-0.28%</td>
				<td align="right">402.20</td>
			<td align="right">397.60</td>
		</tr>
				<tr>
			<td><a title="Senior share price SNR share price" href="./share_price/symbol-SNR">Senior</a></td>
			<td title="SNR share price"align="right" style="color:">308.60</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">309.30</td>
			<td align="right">304.30</td>
		</tr>
				<tr>
			<td><a title="Serco Group share price SRP share price" href="./share_price/symbol-SRP">Serco Group</a></td>
			<td title="SRP share price"align="right" style="color:blue">156.90</td>
			<td align="right" style="color:blue">2.28%</td>
				<td align="right">159.00</td>
			<td align="right">153.30</td>
		</tr>
				<tr>
			<td><a title="Severn Trent share price SVT share price" href="./share_price/symbol-SVT">Severn Trent</a></td>
			<td title="SVT share price"align="right" style="color:blue">2,101.00</td>
			<td align="right" style="color:blue">1.45%</td>
				<td align="right">2,128.00</td>
			<td align="right">2,063.00</td>
		</tr>
				<tr>
			<td><a title="Shaftesbury share price SHB share price" href="./share_price/symbol-SHB">Shaftesbury</a></td>
			<td title="SHB share price"align="right" style="color:blue">797.00</td>
			<td align="right" style="color:blue">0.38%</td>
				<td align="right">799.00</td>
			<td align="right">791.00</td>
		</tr>
				<tr>
			<td><a title="Shire Plc share price SHP share price" href="./share_price/symbol-SHP">Shire Plc</a></td>
			<td title="SHP share price"align="right" style="color:red">4,627.00</td>
			<td align="right" style="color:red">-1.57%</td>
				<td align="right">4,675.00</td>
			<td align="right">4,511.29</td>
		</tr>
				<tr>
			<td><a title="SIG share price SHI share price" href="./share_price/symbol-SHI">SIG</a></td>
			<td title="SHI share price"align="right" style="color:blue">167.00</td>
			<td align="right" style="color:blue">2.90%</td>
				<td align="right">167.60</td>
			<td align="right">161.00</td>
		</tr>
				<tr>
			<td><a title="Sky share price SKY share price" href="./share_price/symbol-SKY">Sky</a></td>
			<td title="SKY share price"align="right" style="color:blue">900.50</td>
			<td align="right" style="color:blue">1.18%</td>
				<td align="right">907.50</td>
			<td align="right">885.50</td>
		</tr>
				<tr>
			<td><a title="Smith & Nephew share price SN. share price" href="./share_price/symbol-SN.">Smith & Nephew</a></td>
			<td title="SN. share price"align="right" style="color:red">1,162.00</td>
			<td align="right" style="color:red">-1.53%</td>
				<td align="right">1,185.00</td>
			<td align="right">1,153.00</td>
		</tr>
				<tr>
			<td><a title="Smith (DS) share price SMDS share price" href="./share_price/symbol-SMDS">Smith (DS)</a></td>
			<td title="SMDS share price"align="right" style="color:red">333.80</td>
			<td align="right" style="color:red">-0.63%</td>
				<td align="right">338.10</td>
			<td align="right">331.90</td>
		</tr>
				<tr>
			<td><a title="Smiths Group share price SMIN share price" href="./share_price/symbol-SMIN">Smiths Group</a></td>
			<td title="SMIN share price"align="right" style="color:blue">1,081.00</td>
			<td align="right" style="color:blue">1.31%</td>
				<td align="right">1,087.00</td>
			<td align="right">1,068.00</td>
		</tr>
				<tr>
			<td><a title="Soco International share price SIA share price" href="./share_price/symbol-SIA">Soco International</a></td>
			<td title="SIA share price"align="right" style="color:red">254.60</td>
			<td align="right" style="color:red">-0.16%</td>
				<td align="right">255.90</td>
			<td align="right">249.10</td>
		</tr>
				<tr>
			<td><a title="Spectris share price SXS share price" href="./share_price/symbol-SXS">Spectris</a></td>
			<td title="SXS share price"align="right" style="color:blue">2,031.00</td>
			<td align="right" style="color:blue">0.54%</td>
				<td align="right">2,044.00</td>
			<td align="right">2,015.00</td>
		</tr>
				<tr>
			<td><a title="Spirax-Sarco Engineering share price SPX share price" href="./share_price/symbol-SPX">Spirax-Sarco Engineering</a></td>
			<td title="SPX share price"align="right" style="color:blue">2,919.00</td>
			<td align="right" style="color:blue">2.24%</td>
				<td align="right">2,924.00</td>
			<td align="right">2,851.00</td>
		</tr>
				<tr>
			<td><a title="Spirit Pub Company share price SPRT share price" href="./share_price/symbol-SPRT">Spirit Pub Company</a></td>
			<td title="SPRT share price"align="right" style="color:blue">108.50</td>
			<td align="right" style="color:blue">1.88%</td>
				<td align="right">109.00</td>
			<td align="right">106.50</td>
		</tr>
				<tr>
			<td><a title="Sports Direct International share price SPD share price" href="./share_price/symbol-SPD">Sports Direct International</a></td>
			<td title="SPD share price"align="right" style="color:blue">713.00</td>
			<td align="right" style="color:blue">2.74%</td>
				<td align="right">716.00</td>
			<td align="right">688.08</td>
		</tr>
				<tr>
			<td><a title="SSE share price SSE share price" href="./share_price/symbol-SSE">SSE</a></td>
			<td title="SSE share price"align="right" style="color:blue">1,523.00</td>
			<td align="right" style="color:blue">1.33%</td>
				<td align="right">1,537.00</td>
			<td align="right">1,498.26</td>
		</tr>
				<tr>
			<td><a title="SSL International share price SSL share price" href="./share_price/symbol-SSL">SSL International</a></td>
			<td title="SSL share price"align="right" style="color:">1,163.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">1,163.00</td>
			<td align="right">1,162.00</td>
		</tr>
				<tr>
			<td><a title="SSP Group share price SSPG share price" href="./share_price/symbol-SSPG">SSP Group</a></td>
			<td title="SSPG share price"align="right" style="color:red">255.00</td>
			<td align="right" style="color:red">-0.93%</td>
				<td align="right">261.00</td>
			<td align="right">249.43</td>
		</tr>
				<tr>
			<td><a title="St James's Place share price STJ share price" href="./share_price/symbol-STJ">St James's Place</a></td>
			<td title="STJ share price"align="right" style="color:blue">804.50</td>
			<td align="right" style="color:blue">2.09%</td>
				<td align="right">806.00</td>
			<td align="right">783.00</td>
		</tr>
				<tr>
			<td><a title="St. Modwen Properties share price SMP share price" href="./share_price/symbol-SMP">St. Modwen Properties</a></td>
			<td title="SMP share price"align="right" style="color:red">411.30</td>
			<td align="right" style="color:red">-0.17%</td>
				<td align="right">415.60</td>
			<td align="right">408.97</td>
		</tr>
				<tr>
			<td><a title="Stagecoach Group share price SGC share price" href="./share_price/symbol-SGC">Stagecoach Group</a></td>
			<td title="SGC share price"align="right" style="color:blue">361.90</td>
			<td align="right" style="color:blue">0.81%</td>
				<td align="right">363.70</td>
			<td align="right">360.30</td>
		</tr>
				<tr>
			<td><a title="Standard Chartered share price STAN share price" href="./share_price/symbol-STAN">Standard Chartered</a></td>
			<td title="STAN share price"align="right" style="color:blue">932.20</td>
			<td align="right" style="color:blue">1.19%</td>
				<td align="right">937.80</td>
			<td align="right">912.60</td>
		</tr>
				<tr>
			<td><a title="Standard Life share price SL. share price" href="./share_price/symbol-SL.">Standard Life</a></td>
			<td title="SL. share price"align="right" style="color:blue">394.70</td>
			<td align="right" style="color:blue">1.36%</td>
				<td align="right">395.30</td>
			<td align="right">387.50</td>
		</tr>
				<tr>
			<td><a title="Supergroup share price SGP share price" href="./share_price/symbol-SGP">Supergroup</a></td>
			<td title="SGP share price"align="right" style="color:blue">811.50</td>
			<td align="right" style="color:blue">5.53%</td>
				<td align="right">820.21</td>
			<td align="right">757.50</td>
		</tr>
				<tr>
			<td><a title="SVG Capital share price SVI share price" href="./share_price/symbol-SVI">SVG Capital</a></td>
			<td title="SVI share price"align="right" style="color:blue">434.10</td>
			<td align="right" style="color:blue">0.21%</td>
				<td align="right">436.20</td>
			<td align="right">431.20</td>
		</tr>
				<tr>
			<td><a title="Synergy Health share price SYR share price" href="./share_price/symbol-SYR">Synergy Health</a></td>
			<td title="SYR share price"align="right" style="color:red">2,111.50</td>
			<td align="right" style="color:red">-0.40%</td>
				<td align="right">2,137.00</td>
			<td align="right">2,076.00</td>
		</tr>
				<tr>
			<td><a title="Synthomer share price SYNT share price" href="./share_price/symbol-SYNT">Synthomer</a></td>
			<td title="SYNT share price"align="right" style="color:blue">239.10</td>
			<td align="right" style="color:blue">0.89%</td>
				<td align="right">241.70</td>
			<td align="right">231.60</td>
		</tr>
				<tr>
			<td><a title="TalkTalk Telecom Group share price TALK share price" href="./share_price/symbol-TALK">TalkTalk Telecom Group</a></td>
			<td title="TALK share price"align="right" style="color:blue">314.60</td>
			<td align="right" style="color:blue">0.93%</td>
				<td align="right">315.20</td>
			<td align="right">311.08</td>
		</tr>
				<tr>
			<td><a title="Tate & Lyle share price TATE share price" href="./share_price/symbol-TATE">Tate & Lyle</a></td>
			<td title="TATE share price"align="right" style="color:blue">653.00</td>
			<td align="right" style="color:blue">1.56%</td>
				<td align="right">658.00</td>
			<td align="right">643.50</td>
		</tr>
				<tr>
			<td><a title="Taylor Wimpey share price TW. share price" href="./share_price/symbol-TW.">Taylor Wimpey</a></td>
			<td title="TW. share price"align="right" style="color:blue">128.00</td>
			<td align="right" style="color:blue">3.06%</td>
				<td align="right">128.30</td>
			<td align="right">123.60</td>
		</tr>
				<tr>
			<td><a title="Ted Baker share price TED share price" href="./share_price/symbol-TED">Ted Baker</a></td>
			<td title="TED share price"align="right" style="color:blue">2,378.00</td>
			<td align="right" style="color:blue">0.55%</td>
				<td align="right">2,388.72</td>
			<td align="right">2,300.00</td>
		</tr>
				<tr>
			<td><a title="Telecity Group share price TCY share price" href="./share_price/symbol-TCY">Telecity Group</a></td>
			<td title="TCY share price"align="right" style="color:blue">822.00</td>
			<td align="right" style="color:blue">3.72%</td>
				<td align="right">825.00</td>
			<td align="right">792.50</td>
		</tr>
				<tr>
			<td><a title="Telecom Plus share price TEP share price" href="./share_price/symbol-TEP">Telecom Plus</a></td>
			<td title="TEP share price"align="right" style="color:red">1,138.00</td>
			<td align="right" style="color:red">-2.40%</td>
				<td align="right">1,176.00</td>
			<td align="right">1,133.45</td>
		</tr>
				<tr>
			<td><a title="Temple Bar Inv Trust share price TMPL share price" href="./share_price/symbol-TMPL">Temple Bar Inv Trust</a></td>
			<td title="TMPL share price"align="right" style="color:blue">1,190.00</td>
			<td align="right" style="color:blue">1.45%</td>
				<td align="right">1,190.00</td>
			<td align="right">1,166.44</td>
		</tr>
				<tr>
			<td><a title="Templeton Emerging Markets Inv Trust share price TEM share price" href="./share_price/symbol-TEM">Templeton Emerging Markets Inv Trust</a></td>
			<td title="TEM share price"align="right" style="color:blue">572.50</td>
			<td align="right" style="color:blue">0.88%</td>
				<td align="right">573.64</td>
			<td align="right">567.53</td>
		</tr>
				<tr>
			<td><a title="Tesco share price TSCO share price" href="./share_price/symbol-TSCO">Tesco</a></td>
			<td title="TSCO share price"align="right" style="color:blue">211.10</td>
			<td align="right" style="color:blue">3.20%</td>
				<td align="right">214.70</td>
			<td align="right">203.50</td>
		</tr>
				<tr>
			<td><a title="Thomas Cook Group share price TCG share price" href="./share_price/symbol-TCG">Thomas Cook Group</a></td>
			<td title="TCG share price"align="right" style="color:blue">130.60</td>
			<td align="right" style="color:blue">2.92%</td>
				<td align="right">132.10</td>
			<td align="right">126.40</td>
		</tr>
				<tr>
			<td><a title="Tomkins share price TOMK share price" href="./share_price/symbol-TOMK">Tomkins</a></td>
			<td title="TOMK share price"align="right" style="color:">324.40</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">324.40</td>
			<td align="right">324.40</td>
		</tr>
				<tr>
			<td><a title="TR Property Inv Trust share price TRY share price" href="./share_price/symbol-TRY">TR Property Inv Trust</a></td>
			<td title="TRY share price"align="right" style="color:blue">286.70</td>
			<td align="right" style="color:blue">0.92%</td>
				<td align="right">287.00</td>
			<td align="right">284.50</td>
		</tr>
				<tr>
			<td><a title="Travis Perkins share price TPK share price" href="./share_price/symbol-TPK">Travis Perkins</a></td>
			<td title="TPK share price"align="right" style="color:blue">1,819.00</td>
			<td align="right" style="color:blue">1.22%</td>
				<td align="right">1,831.00</td>
			<td align="right">1,793.00</td>
		</tr>
				<tr>
			<td><a title="TUI Travel share price TT. share price" href="./share_price/symbol-TT.">TUI Travel</a></td>
			<td title="TT. share price"align="right" style="color:">437.60</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">437.60</td>
			<td align="right">437.60</td>
		</tr>
				<tr>
			<td><a title="Tullett Prebon share price TLPR share price" href="./share_price/symbol-TLPR">Tullett Prebon</a></td>
			<td title="TLPR share price"align="right" style="color:blue">304.80</td>
			<td align="right" style="color:blue">5.83%</td>
				<td align="right">305.60</td>
			<td align="right">288.60</td>
		</tr>
				<tr>
			<td><a title="Tullow Oil share price TLW share price" href="./share_price/symbol-TLW">Tullow Oil</a></td>
			<td title="TLW share price"align="right" style="color:red">366.70</td>
			<td align="right" style="color:red">-5.22%</td>
				<td align="right">384.39</td>
			<td align="right">365.70</td>
		</tr>
				<tr>
			<td><a title="UBM share price UBM share price" href="./share_price/symbol-UBM">UBM</a></td>
			<td title="UBM share price"align="right" style="color:blue">495.90</td>
			<td align="right" style="color:blue">2.56%</td>
				<td align="right">496.70</td>
			<td align="right">483.70</td>
		</tr>
				<tr>
			<td><a title="UDG Healthcare Public Limited Company share price UDG share price" href="./share_price/symbol-UDG">UDG Healthcare Public Limited Company</a></td>
			<td title="UDG share price"align="right" style="color:blue">405.10</td>
			<td align="right" style="color:blue">0.97%</td>
				<td align="right">406.00</td>
			<td align="right">398.20</td>
		</tr>
				<tr>
			<td><a title="UK Commercial Property Trust share price UKCM share price" href="./share_price/symbol-UKCM">UK Commercial Property Trust</a></td>
			<td title="UKCM share price"align="right" style="color:blue">90.70</td>
			<td align="right" style="color:blue">0.22%</td>
				<td align="right">90.70</td>
			<td align="right">90.05</td>
		</tr>
				<tr>
			<td><a title="Ultra Electronics Holdings share price ULE share price" href="./share_price/symbol-ULE">Ultra Electronics Holdings</a></td>
			<td title="ULE share price"align="right" style="color:red">1,821.00</td>
			<td align="right" style="color:red">-1.09%</td>
				<td align="right">1,849.00</td>
			<td align="right">1,813.00</td>
		</tr>
				<tr>
			<td><a title="Unilever share price ULVR share price" href="./share_price/symbol-ULVR">Unilever</a></td>
			<td title="ULVR share price"align="right" style="color:blue">2,707.00</td>
			<td align="right" style="color:blue">1.08%</td>
				<td align="right">2,717.00</td>
			<td align="right">2,664.00</td>
		</tr>
				<tr>
			<td><a title="Unite Group share price UTG share price" href="./share_price/symbol-UTG">Unite Group</a></td>
			<td title="UTG share price"align="right" style="color:blue">484.00</td>
			<td align="right" style="color:blue">1.64%</td>
				<td align="right">484.00</td>
			<td align="right">477.00</td>
		</tr>
				<tr>
			<td><a title="United Utilities Group share price UU. share price" href="./share_price/symbol-UU.">United Utilities Group</a></td>
			<td title="UU. share price"align="right" style="color:red">940.50</td>
			<td align="right" style="color:red">-1.00%</td>
				<td align="right">963.00</td>
			<td align="right">938.50</td>
		</tr>
				<tr>
			<td><a title="Vedanta Resources share price VED share price" href="./share_price/symbol-VED">Vedanta Resources</a></td>
			<td title="VED share price"align="right" style="color:red">489.20</td>
			<td align="right" style="color:red">-1.87%</td>
				<td align="right">501.55</td>
			<td align="right">486.20</td>
		</tr>
				<tr>
			<td><a title="Vesuvius share price CKSN share price" href="./share_price/symbol-CKSN">Vesuvius</a></td>
			<td title="CKSN share price"align="right" style="color:blue">645.00</td>
			<td align="right" style="color:blue">0.62%</td>
				<td align="right">645.50</td>
			<td align="right">633.00</td>
		</tr>
				<tr>
			<td><a title="Vesuvius share price VSVS share price" href="./share_price/symbol-VSVS">Vesuvius</a></td>
			<td title="VSVS share price"align="right" style="color:blue">450.00</td>
			<td align="right" style="color:blue">3.33%</td>
				<td align="right">452.00</td>
			<td align="right">440.00</td>
		</tr>
				<tr>
			<td><a title="Victrex plc share price VCT share price" href="./share_price/symbol-VCT">Victrex plc</a></td>
			<td title="VCT share price"align="right" style="color:blue">2,086.00</td>
			<td align="right" style="color:blue">0.10%</td>
				<td align="right">2,100.00</td>
			<td align="right">2,073.00</td>
		</tr>
				<tr>
			<td><a title="Vodafone Group share price VOD share price" href="./share_price/symbol-VOD">Vodafone Group</a></td>
			<td title="VOD share price"align="right" style="color:blue">229.70</td>
			<td align="right" style="color:blue">1.28%</td>
				<td align="right">230.45</td>
			<td align="right">226.08</td>
		</tr>
				<tr>
			<td><a title="VT Group share price VTG share price" href="./share_price/symbol-VTG">VT Group</a></td>
			<td title="VTG share price"align="right" style="color:">795.00</td>
			<td align="right" style="color:">0.00%</td>
				<td align="right">795.00</td>
			<td align="right">795.00</td>
		</tr>
				<tr>
			<td><a title="Weir Group share price WEIR share price" href="./share_price/symbol-WEIR">Weir Group</a></td>
			<td title="WEIR share price"align="right" style="color:red">1,684.00</td>
			<td align="right" style="color:red">-2.26%</td>
				<td align="right">1,692.77</td>
			<td align="right">1,654.00</td>
		</tr>
				<tr>
			<td><a title="Wetherspoon (J.D.) share price JDW share price" href="./share_price/symbol-JDW">Wetherspoon (J.D.)</a></td>
			<td title="JDW share price"align="right" style="color:blue">836.50</td>
			<td align="right" style="color:blue">4.17%</td>
				<td align="right">838.00</td>
			<td align="right">795.50</td>
		</tr>
				<tr>
			<td><a title="WH Smith share price SMWH share price" href="./share_price/symbol-SMWH">WH Smith</a></td>
			<td title="SMWH share price"align="right" style="color:blue">1,348.00</td>
			<td align="right" style="color:blue">0.45%</td>
				<td align="right">1,360.00</td>
			<td align="right">1,331.58</td>
		</tr>
				<tr>
			<td><a title="Whitbread share price WTB share price" href="./share_price/symbol-WTB">Whitbread</a></td>
			<td title="WTB share price"align="right" style="color:blue">4,795.00</td>
			<td align="right" style="color:blue">0.44%</td>
				<td align="right">4,803.10</td>
			<td align="right">4,748.88</td>
		</tr>
				<tr>
			<td><a title="William Hill share price WMH share price" href="./share_price/symbol-WMH">William Hill</a></td>
			<td title="WMH share price"align="right" style="color:blue">362.30</td>
			<td align="right" style="color:blue">2.20%</td>
				<td align="right">368.70</td>
			<td align="right">353.85</td>
		</tr>
				<tr>
			<td><a title="Witan Inv Trust share price WTAN share price" href="./share_price/symbol-WTAN">Witan Inv Trust</a></td>
			<td title="WTAN share price"align="right" style="color:blue">770.00</td>
			<td align="right" style="color:blue">1.45%</td>
				<td align="right">770.00</td>
			<td align="right">758.90</td>
		</tr>
				<tr>
			<td><a title="Wolseley share price WOS share price" href="./share_price/symbol-WOS">Wolseley</a></td>
			<td title="WOS share price"align="right" style="color:blue">3,675.00</td>
			<td align="right" style="color:blue">0.30%</td>
				<td align="right">3,690.00</td>
			<td align="right">3,634.00</td>
		</tr>
				<tr>
			<td><a title="Wood Group (John) share price WG. share price" href="./share_price/symbol-WG.">Wood Group (John)</a></td>
			<td title="WG. share price"align="right" style="color:red">538.00</td>
			<td align="right" style="color:red">-0.19%</td>
				<td align="right">543.00</td>
			<td align="right">529.50</td>
		</tr>
				<tr>
			<td><a title="Workspace Group share price WKP share price" href="./share_price/symbol-WKP">Workspace Group</a></td>
			<td title="WKP share price"align="right" style="color:blue">770.00</td>
			<td align="right" style="color:blue">0.65%</td>
				<td align="right">773.00</td>
			<td align="right">763.84</td>
		</tr>
				<tr>
			<td><a title="Worldwide Healthcare Trust share price WWH share price" href="./share_price/symbol-WWH">Worldwide Healthcare Trust</a></td>
			<td title="WWH share price"align="right" style="color:blue">1,780.00</td>
			<td align="right" style="color:blue">0.68%</td>
				<td align="right">1,790.00</td>
			<td align="right">1,758.00</td>
		</tr>
				<tr>
			<td><a title="WPP share price WPP share price" href="./share_price/symbol-WPP">WPP</a></td>
			<td title="WPP share price"align="right" style="color:blue">1,405.00</td>
			<td align="right" style="color:blue">1.81%</td>
				<td align="right">1,410.00</td>
			<td align="right">1,376.00</td>
		</tr>
				<tr>
			<td><a title="Yule Catto & Co share price YULC share price" href="./share_price/symbol-YULC">Yule Catto & Co</a></td>
			<td title="YULC share price"align="right" style="color:blue">178.90</td>
			<td align="right" style="color:blue">0.96%</td>
				<td align="right">180.00</td>
			<td align="right">173.10</td>
		</tr>
				<tr>
			<td><a title="Zoopla Property Group (WI) share price ZPLA share price" href="./share_price/symbol-ZPLA">Zoopla Property Group (WI)</a></td>
			<td title="ZPLA share price"align="right" style="color:red">167.20</td>
			<td align="right" style="color:red">-3.63%</td>
				<td align="right">174.00</td>
			<td align="right">164.90</td>
		</tr>
			</table>
	
       <br/><br/>

       <a href="http://www.livecharts.co.uk/share_prices/share_price.php" title="share prices">Back to share prices</a>

  <br/><br/>
	   <a href="http://www.livecharts.co.uk/MarketCharts/Futsee.php" title="ftse 100">FTSE 100</a>

<br/>
	
	</div>
	</div>
	

    <div class="clearfix"></div>

  
	 <br />
	        <script type="text/javascript"><!--
               google_ad_client = "pub-7932798795997603";
               /* 468x60, created 21/07/09 */
               google_ad_slot = "8784327525";
               google_ad_width = 468;
               google_ad_height = 60;
               //-->
            </script>
            <script type="text/javascript"
             src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
            </script>
			

	
	    <div class="pricedata_profile">
              <br/>Data is provided by <a href="http://corporate.digitallook.com/" rel="nofollow">Digital Look Corporate Solutions</a> incorporating their share prices, data, news and fundamentals on this site.
               Please read the <a href="http://static.digitallook.com/digitalcorporate/partner_terms.html" rel="nofollow">terms and conditions</a> of useage of this data. This data is provided for information purposes only and will be delayed by at least 15 minutes at most times.
        </div>
	 <div class="clearfix"></div>
<div class="lc_sharebottom">	 
        <h5 style="font-size: 13px;color: #2a2a72;">Get a free widget for your website with our latest headlines.</h5>
		 <p>You can now add our live prices and new headlines to your website.The news widget features quotes for Oil prices, spot Gold price and Indices plus a choice of news channel for healines.</p>
		 <p><img style="padding-right: 8px;" src="/images/widgetarrow.gif" /><a style="line-height: 12px;" href="/stockmarketwidget.php" title="news widget">Visit our widget builder</a></p>
		 </div>
      
	   
	<div style="width:460px;">
                    <!-- Possible good ads replace to below links-->
                  <table><tr><td>  <ul class="additional_links">
                        <li><a href="http://www.livecharts.co.uk/share_prices/share_price.php" title="uk share prices">Share prices</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=1" title="uk market news">UK Market news</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=3" title="International markets">International markets</a></li>
                    <li><a href="http://www.livecharts.co.uk/livewire" title="financial articles">Financial articles</a></li>
                        <li><a href="http://www.livecharts.co.uk/articles/commoditytrading.php" title="commodity trading">Commodity trading</a></li>
                        <li><a href="http://www.forexrate.co.uk/" title="forex">Forex</a></li>
                        <li><a href="http://www.forexrate.co.uk/forexcharts.php" title="forex charts">Forex charts</a></li>
                        <li><a href="http://www.livecharts.co.uk/capitalspreads/spread-betting.php">Spread betting</a></li>
                        <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=7" title="share tips">Broker tips</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_chart.php" title="share price charts">Share price charts</a></li>
                    </ul></td>
                    <td><iframe width="300" height="250" marginheight=0 marginwidth=0 frameborder=0 vspace=0 hspace=0 scrolling=NO src="http://www.dianomioffers.co.uk/smartads.epl?id=245"></iframe></td></tr></table>

                </div>


 
	
	
	</div>

   <!-- right shares content -->


      <div class="fl_right">
     <div><a href="http://www.livecharts.co.uk/share_prices/members_features.php"><img src="/images/register_free_button_300px.gif" width="300px" height="80px" border="0" style="margin-bottom:2px;"  alt="register"></a>
 </div>

    <div align="left" style="margin-bottom: 5px;">
                                <script>window.jQuery || document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><\/script>');</script>
		<script type="text/javascript">
		function requestIndexesOrAZ(fndNumber) {
			if ( $('#combo_security_type').val() == "share" ) {
				$('#container_az').show();
				$('#container_indexes').hide();
				$('#container_shares').hide();
				requestAZ();
			}
			else if ( $('#combo_security_type').val() == "index" ) {
				$('#container_az').hide()
				$('#container_indexes').show();
				$('#container_shares').hide();
				requestIndexes();
			}
		}

		function requestIndexes() {
            $.ajax({
                url: 'http://www.livecharts.co.uk/share_prices/symbol_finder_ajax.php',
                data: {
                    toLoad : "indexes"
                },
                success: function(data) {
                    $('#combo_indexes').html(data);
                }
            });    
		}
		function requestAZ() {
            $.ajax({
                url: 'http://www.livecharts.co.uk/share_prices/symbol_finder_ajax.php',
                data: {
                    toLoad : "az"
                },
                success: function(data) {
                    $('#combo_az').html(data);
                }
            });    
		}
		function requestShares() {
			if ( $('#combo_az').val() == "" ) {
				return;
			}
			$('#container_shares').show();
            
            $.ajax({
                url: 'http://www.livecharts.co.uk/share_prices/symbol_finder_ajax.php',
                data: {
                    toLoad : "shares", 
                    letter : $('#combo_az').val()
                },
                success: function(data) {
                    $('#combo_shares').html(data);
                }
            });    
		}

		function goToSharePrice(){
			document.location='http://www.livecharts.co.uk/share_prices/share_price/symbol-' + $('#symbol').val();
			return false;
		}
		function changeIndexHandler() {
							document.location='http://www.livecharts.co.uk/share_prices/share_price/symbol-' + $('#combo_indexes').val();
						}
		function changeShareHandler() {
							document.location='http://www.livecharts.co.uk/share_prices/share_price/symbol-' + $('#combo_shares').val();
						}
		function enterSymbolHandler() {
							document.location='http://www.livecharts.co.uk/share_prices/share_price/symbol-' + $('#symbol').val().toUpperCase();
							return false;
		}

		</script>
				<table bgcolor="#a2a2a2" cellpadding="0" cellspacing="0" border="0" style="height: 38px; width:300px;border:1px solid #000000;padding-top:3px; margin-bottom:5px;">
					<tr>
						<td style="padding-left:10px;padding-bottom:3px;">
						<form method="GET" onsubmit="return enterSymbolHandler()">
								<input type="text" id="symbol" name="symbol" style="width:90px;" value="  Insert ticker" onclick="this.value=''" />
								<input type="submit" value="Go"/>
						</form></td><td> <span style="font-size:1.1em;color: #FFFFFF;font-weight: bold;">Enter share symbol</span>
						</td>
					</tr>
				</table>

				<table bgcolor="#a2a2a2" cellpadding="0" cellspacing="0" style="height: 50px; width:300px;border:1px solid #000000; margin-top:1px;">
					<tr >
						<td style="padding-left:10px;padding-bottom:5px;font-size:1.1em;color: #FFFFFF;font-weight: bold;">Search share prices A to Z
						<select id="combo_security_type" onchange="requestIndexesOrAZ()" style="width:170px;">
							<option value="">- - Share / Index - -</option>
							<option value="share">Share</option>
							<option value="index">Index</option>
						</select>
						</td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-bottom:5px;display:none" id="container_az">
							<select id="combo_az" style="width:170px;" onchange="requestShares()">

							</select>
						</td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-bottom:5px;display:none" id="container_indexes">
							<select id="combo_indexes" style="width:170px;"
								onchange="changeIndexHandler()"
								>
							</select>
						</td>
					</tr>
					<tr>
						<td style="padding-left:10px;padding-bottom:5px;display:none" id="container_shares">
							<select id="combo_shares" style="width:170px;"
								onchange="changeShareHandler()"
							>

							</select>
						</td>
					</tr>
				</table>



                </div>

    <div>
    <script type="text/javascript">adnxs.megatag.load({ size: '300x250', promoSizes: ['1x1'] });</script>

</div>
  <div class="box_area">


<table id="sidetable">
	<thead>
				<tr>
					<th colspan="6"><strong>Latest news headlines</strong></th>
				</tr>
			
			</thead>	
			<tbody>
				<tr>
				    <td colspan="6"><div class="news_area">



	    <span style="width:auto"><a href="http://www.livecharts.co.uk/share_prices/December-UK-retail-sales-much-worse-than-expe-news22384231.html">December UK retail sales much worse than expected, says BRC-KPMG</a></span><br/>
        <span style="width:auto"><a href="http://www.livecharts.co.uk/share_prices/London-travel-chaos-as-27-000-bus-drivers-str-news22383524.html">London travel chaos as 27,000 bus drivers strike over pay</a></span><br/>
        <span style="width:auto"><a href="http://www.livecharts.co.uk/share_prices/More-than-half-pensioners-to-miss-out-on-rece-news22381996.html">More than half pensioners to miss out on receiving new state pension in full</a></span><br/>
        <span style="width:auto"><a href="http://www.livecharts.co.uk/share_prices/UK-trade-deficit-shrinks-in-November-news22377916.html">UK trade deficit shrinks in November</a></span><br/>
    

</div>
				  </td>
                  </tr>	
	
		
							</tbody>
						
		</table>

 <div class="share_box_links" style="height: 50px">
  <ul>
  <li style="color:#444;"><strong>Sponsored Financial Centres</strong></li>
  <li><a href="http://www.livecharts.co.uk/CFD/index.php">CFD Centre in Association with City Index</a></li>

  </ul>
  </div>

<div> <script type="text/javascript"><!--
google_ad_client = "pub-7932798795997603";
/* 300x250, created 21/07/09 */
google_ad_slot = "1620662103";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></div>
  <br />
 <div> <script type="text/javascript"><!--
google_ad_client = "pub-7932798795997603";
/* 300x250, created 21/07/09 */
google_ad_slot = "1620662103";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></div>

  
    <!-- End shares box area -->

    </div>

     <!-- End shares right content -->





 </div>



   <!-- End shares content -->



                <div class="clearfix"></div>


        </div>


        <!-- Left navigation links -->
        <div class="left_col">

             

 <h2 title="uk shares">Top Shares pages</h2>
            <ul>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price.php" title="share price quotes">Share price quotes</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/stock-chart-technical-analysis" title="share charts">Share charts</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_watch_list.php" title="share watch list">Share watch list</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/resultscalendar.php" title="company results">Company Results Calendar</a></li>
<li><a href="http://www.livecharts.co.uk/share_prices/FTSE_100_shares.php" title="ftse 100 share prices">UK 100 Shares</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=1" title="stock market news">Stock market news</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=2" title="company news">Company news</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=7" title="share tips">Share tips</a></li>
                   <li><a href="http://www.livecharts.co.uk/share_prices/company_look_up.php" title="company search">A-Z company search</a></li>

            </ul>

             <h2 title="share price tools">More share features</h2>


           <div id="menu">
	<ul id="toplevel">



	                 <li><a class="fly" href="http://www.livecharts.co.uk/share_prices/members_features.php" title="share tools">Members features &#187;<!--[if gte IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->

			<ul>
			<li><a href="http://www.livecharts.co.uk/share_prices/share_watch_list.php" title="share watch list">Share watch list</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/share_data_filter.php" title="data filter">Share filter</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/share_trends.php" title="chart signals">Chart signals</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_members/threads.php" title="share chat">Share chat room</a></li>
                    <li><a href="http://www.livecharts.co.uk/membersarea/tradingtools/tradingtools.php" title="data tools">Tools and data</a></li>
			</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->


		<li><a class="fly" href="http://www.livecharts.co.uk/share_prices/index_list.php" title="stock markets">UK Index components &#187;<!--[if gte IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->

			<ul>
				<li><a href="http://www.livecharts.co.uk/share_prices/FTSE_100_shares.php" title="ftse 100">FTSE 100 shares</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/FTSE_250_shares.php" title="ftse 250">FTSE 250 shares</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/FTSE_350_shares.php" title="ftse 350">FTSE 350 shares</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/MID300_shares.php" title="mid 300">MID 300 shares</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/AIM50_shares.php" title="aim 50">AIM 50 shares</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/AIM350_shares.php" title="aim 100">AIM 100 shares</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/AIM100_shares.php" title="aim 350">AIM 350 shares</a></li>
			</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

		<li><a class="fly" href="http://www.livecharts.co.uk/share_prices/market_news.php" title="share news">Stock Market News &#187;<!--[if gte IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
 
			<ul>
				<li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=1" title="stock market news">Stock market news</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=3" title="stock market news">World market news</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=4" title="economic news">Economic news</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=6" title="forex news">FX and Commodity news</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=2" title="company news">Company share news</a></li>
                    <li><a href="http://www.livecharts.co.uk/share_prices/share_news_more.php?id=7" title="share tips">Broker share tips</a></li>
			</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

                 <li><a class="fly" rel="nofollow" href="http://www.dianomioffers.co.uk/partner/livecharts/brochures.epl?partner=228&campaign=557">Free financial brochures &#187;<!--[if gte IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->

			<ul>
				<li><a rel="nofollow"href="http://www.onlinetradingservices.co.uk/partner/livecharts/brochures.epl?offer=15476&campaign=557" title="free online trading brochure">Online trading brochures</a></li>
                    <li><a rel="nofollow" href="http://www.moneyguides.co.uk/partner/livecharts/brochures.epl?partner=228&campaign=557&offer=15467" title="investing guides">Investing guides</a></li>
                    <li><a rel="nofollow" href="http://www.isabrochures.co.uk/partner/livecharts/brochures.epl?campaign=557&offer=15415" title="ISA brochures">ISA brochures</a></li>
			        <li><a rel="nofollow" href="http://www.wealthmanagement.dianomioffers.co.uk/partner/livecharts/brochures.epl?partner=228&campaign=557&smartreferer=http%3A%2F%2Fwww%2Elivecharts%2Eco%2Euk%2Fshare_prices%2Ftesting_template%2Ephp&offer=15554" title="wealth management">Wealth management</a></li>
			        <li><a rel="nofollow" href="http://www.savingforchildren.co.uk/partner/livecharts/brochures.epl?partner=228&campaign=557&smartreferer=http%3A%2F%2Fwww%2Elivecharts%2Eco%2Euk%2Fshare_prices%2Ftesting_template%2Ephp&offer=15407" title="Child saving plans">Child saving plans</a></li>
			        <li><a rel="nofollow" href="http://www.savingforretirement.co.uk/partner/livecharts/brochures.epl?partner=228&campaign=557&smartreferer=http%3A%2F%2Fwww%2Elivecharts%2Eco%2Euk%2Fshare_prices%2Ftesting_template%2Ephp&offer=15484" title="SIPPS brochures">SIPPS brochures</a></li>
			</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

            <li><a class="fly" rel="nofollow" href="http://www.capitalspreads.com/agentdirect.php?url=/demoregistration&agentid=247" title="spread betting">Spread Betting &#187;<!--[if gte IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->

			<ul>
				<li><a rel="nofollow" href="http://www.capitalspreads.com/agentdirect.php?url=/demoregistration&agentid=247" title="trading account">Capital Spreads</a></li>
			</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

		        <li><a class="fly" rel="nofollow" href="" title="financial centres">Financial centres &#187;<!--[if gte IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->

			<ul>
			
			    <li><a href="http://www.livecharts.co.uk/CFD/index.php" title="CFDs">City Index CFD's</a></li>
			</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->

		 </ul>

					</div>



<h2>POPULAR Share Prices</h2>
            <ul>
				<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-RMG" title="royal mail">Royal Mail</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-LLOY" title="lloyds">Lloyds share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-HSBA" title="hsbc">HSBC share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-BARC" title="barclays">Barclays share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-PRU" title="prudential">Prudential share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-DGE" title="diageo">Diageo share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-BP." title="bp">BP share price</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-VOD" title="vodafone">Vodafone share price</a></li>
			<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-IAG" title="british airways">British Airways</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-CNA" title="centrica">Centrica share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-TSCO" title="Tesco">Tesco share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-NG." title="national grid">National Grid</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-RBS" title="Rbs">RBS share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-GSK" title="gsk">GSK share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-MKS" title="marks and spencer">Marks and Spencer</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-RR." title="rolls royce">Rolls Royce</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-BNC" title="banco santander">Banco Santander price</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-DLG" title="direct line">Direct Line</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-RIO" title="rio tinto">Rio Tinto</a></li>
				<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-AMEC" title="amec">Amec</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-CRA" title="corac">Corac</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-LOOK" title="lookers">Lookers</a></li>
                 <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-TEP" title="telecom plus">Telecom plus</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-KIE" title="kier">Kier share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-PUB" title="punch">Punch taverns</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-BLNX" title="blinkx">Blinkx share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-TAN" title="tan">Tan share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-YELL" title="yell">Yell share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-RSA" title="rsa">Rsa share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-PDG" title="pendragon">Pendragon share price</a></li>
                 <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-LOG" title="logica">Logica share price</a></li>
                <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-BATS" title="bat">Bat share price</a></li>
               <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-BSY" title="sky">Sky share price</a></li>
               <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-KGF" title="kingfisher">Kingfisher share price</a></li>
            <li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-DGO" title="dragon oil">Dragon Oil share price</a></li>
			<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-DES" title="Desire Petroleum">Desire Petroleum share price</a></li>
			<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-RRL" title="rrl">RRL share price</a></li>
			<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-BPC" title="bpc">BPC share price</a></li>
			<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-VOG" title="vog">VOG share price</a></li>
			<li><a href="http://www.livecharts.co.uk/share_prices/share_price/symbol-SAR" title="sar">SAR share price</a></li>
			</ul>
			
			<br/>
			
        </div>  <div class="clearfix"></div>
        <!-- Banner on the right panel -->


    </div>
</div>

     <div class="clearfix"></div>
<!-- Footer -->
<div class="footer">
    <div class="main_footer"> <a href="http://www.livecharts.co.uk/tradingcommunity/daytradingcommunity.php">Day Trading Community</a> | <a href="http://www.livecharts.co.uk/articles/dowjones.php">Dow
            Jones</a> | <a href="http://www.livecharts.co.uk/articles/crudeoil.php">Crude Oil</a> | <a href="http://www.livecharts.co.uk/articles/spreadbetting.php">Spreadbetting</a> | <a href="http://www.livecharts.co.uk/articles/daytrading.php">Daytrading</a> | <a href="http://www.livecharts.co.uk/LongTerm/menu.php">Futures charts</a> | <a href="http://www.livecharts.co.uk/terms.php">Terms/Risk</a> | <a href="/contact/contact.php">Contact Live Charts</a> | <a href="http://www.livecharts.co.uk/directory.php">Links </a>
            <br /><a href="/share_map.php?letter=0-9">0-9</a> |
        <a href="/share_map.php?letter=a" title="share price">A</a> |
        <a href="/share_map.php?letter=b" title="share price">B</a> |
        <a href="/share_map.php?letter=c" title="share price">C</a> |
        <a href="/share_map.php?letter=d" title="share price">D</a> |
        <a href="/share_map.php?letter=e" title="share price">E</a> |
        <a href="/share_map.php?letter=f" title="share price">F</a> |
        <a href="/share_map.php?letter=g" title="share price">G</a> |
        <a href="/share_map.php?letter=h" title="share price">H</a> |
        <a href="/share_map.php?letter=i" title="share price">I</a> |
        <a href="/share_map.php?letter=j" title="share price">J</a> |
        <a href="/share_map.php?letter=k" title="share price">K</a> |
        <a href="/share_map.php?letter=l" title="share price">L</a> |
        <a href="/share_map.php?letter=m" title="share price">M</a> |
        <a href="/share_map.php?letter=n" title="share price">N</a> |
        <a href="/share_map.php?letter=o" title="share price">O</a> |
        <a href="/share_map.php?letter=p" title="share price">P</a> |
        <a href="/share_map.php?letter=q" title="share price">Q</a> |
        <a href="/share_map.php?letter=r" title="share price">R</a> |
        <a href="/share_map.php?letter=s" title="share price">S</a> |
        <a href="/share_map.php?letter=t" title="share price">T</a> |
        <a href="/share_map.php?letter=u" title="share price">U</a> |
        <a href="/share_map.php?letter=v" title="share price">V</a> |
        <a href="/share_map.php?letter=w" title="share price">W</a> |
        <a href="/share_map.php?letter=x" title="share price">X</a> |
        <a href="/share_map.php?letter=y" title="share price">Y</a> |
        <a href="/share_map.php?letter=z" title="share price">Z</a> </div>


   <div class="secondary_footer"> <a href="http://www.livecharts.co.uk/daily_charts/daily_charts.php">futures charts</a> | <a href="http://www.nysestocks.co.uk/">NYSE
            Stocks</a> | <a href="http://www.forexrate.co.uk/news/">Currency Trading News</a> | <a href="http://www.forexrate.co.uk/">Forex
            Rate</a> | <a href="http://www.nysestocks.co.uk/stockcharts.php">Stock Charts</a></div>

    <p><p>Copyright &copy; <a href="http://www.livecharts.co.uk"><font color="#888888">Live Charts UK</font></a>. All rights reserved.<br />
        Designed inhouse by Live Charts | Special Thanks to <a href="http://www.forexrate.co.uk/"><font color="#888888">Forex Rate</font></a> | <a href="http://www.livecharts.co.uk/sitemap.html"><font color="#888888">Site Map</font></a></p>
        <br class="clearfix" />
   </div>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f67b5dd5a2796e2"></script>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2011715-1";
urchinTracker();
</script>

<SCRIPT SRC="http://ib.adnxs.com/ttj?id=967200" TYPE="text/javascript"></SCRIPT>

<script>wbds_ads.load();</script>



</body>


</html>
