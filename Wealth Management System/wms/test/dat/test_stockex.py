from stockex import stockwrapper as sw

data = sw.YahooData()

# Print Current data of a Stock
print(data.get_current(['GOOG']))

# Print historical data of a Stock
print(data.get_historical("GOOG"))

# Trivial formatting
print("Google stock: Date and Price")
for item in data.get_historical("GOOG"):
    print(item['Date'] + '\t' + item['Close'])


# Other methods:

print("# Do a custom YQL query to Yahoo! Finance YQL API:")
print(data.enquire('select * from yahoo.finance.quotes where symbol in ("GOOG", "C")'))

print("# Get news feed of a Company")
print(data.get_news_feed("GOOG"))

print("# Get options data")
print(data.get_options_info("GOOG"))

print("# Get industry ids")
print(data.get_industry_ids())

print("# Get industry index from a given id")
print(data.get_industry_index('914'))

