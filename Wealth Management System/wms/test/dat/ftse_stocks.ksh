
# ftse350
url="http://www.livecharts.co.uk/share_prices/FTSE_350_shares.php"

hxnormalize -x -e -d -s -L -l 1000 $url | grep "share_price\/symbol" | grep "title" | grep -v "<li>"  > ftse350.dat

cat ftse350.dat | cut -d- -s -f2 | cut -d\" -s -f1  > ftse_stocks.dat


# mid300
url="http://www.livecharts.co.uk/share_prices/MID300_shares.php"

hxnormalize -x -e -d -s -L -l 1000 $url | grep "share_price\/symbol" | grep "title" | grep -v "<li>"  > mid300.dat

cat mid300.dat | cut -d- -s -f2 | cut -d\" -s -f1  >> ftse_stocks.dat



# aim350
url="http://www.livecharts.co.uk/share_prices/AIM350_shares.php"

hxnormalize -x -e -d -s -L -l 1000 $url | grep "share_price\/symbol" | grep "title" | grep -v "<li>"  > aim350.dat

cat aim350.dat | cut -d- -s -f2 | cut -d\" -s -f1  >> ftse_stocks.dat



