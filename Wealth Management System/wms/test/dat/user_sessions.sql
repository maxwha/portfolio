--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-04-01 05:03:06 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 3087 (class 0 OID 28067)
-- Dependencies: 218
-- Data for Name: user_sessions; Type: TABLE DATA; Schema: public; Owner: fuxiadmin
--

INSERT INTO user_sessions (id, user_id, created_at, updated_at, expired_at) VALUES (1, 1, '2015-04-01 02:34:06.679', '2015-04-01 02:34:06.679', '2015-04-01 01:43:20.085805');
INSERT INTO user_sessions (id, user_id, created_at, updated_at, expired_at) VALUES (2, 1, '2015-04-01 01:43:20.117925', '2015-04-01 01:43:20.117938', '2015-04-01 02:42:25.48');
INSERT INTO user_sessions (id, user_id, created_at, updated_at, expired_at) VALUES (3, 1, '2015-04-01 02:42:26.161', '2015-04-01 02:42:26.161', '2015-04-01 02:42:30.825');
INSERT INTO user_sessions (id, user_id, created_at, updated_at, expired_at) VALUES (4, 1, '2015-04-01 02:42:31.519', '2015-04-01 02:42:31.519', '2015-04-01 01:55:03.922477');
INSERT INTO user_sessions (id, user_id, created_at, updated_at, expired_at) VALUES (5, 1, '2015-04-01 01:55:03.937088', '2015-04-01 01:55:03.937099', '2015-04-01 02:56:19.424');
INSERT INTO user_sessions (id, user_id, created_at, updated_at, expired_at) VALUES (6, 1, '2015-04-01 02:56:20.406', '2015-04-01 02:56:20.406', '2015-04-01 02:57:24.614');
INSERT INTO user_sessions (id, user_id, created_at, updated_at, expired_at) VALUES (7, 1, '2015-04-01 02:57:54.096', '2015-04-01 02:57:54.096', NULL);


--
-- TOC entry 3093 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_sessions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fuxiadmin
--

SELECT pg_catalog.setval('user_sessions_id_seq', 7, true);


-- Completed on 2015-04-01 05:03:06 UTC

--
-- PostgreSQL database dump complete
--

