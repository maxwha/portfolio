--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-04-01 05:03:07 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 3085 (class 0 OID 28450)
-- Dependencies: 256
-- Data for Name: user_watchlists; Type: TABLE DATA; Schema: public; Owner: fuxiadmin
--



--
-- TOC entry 3091 (class 0 OID 0)
-- Dependencies: 255
-- Name: user_watchlists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fuxiadmin
--

SELECT pg_catalog.setval('user_watchlists_id_seq', 1, false);


-- Completed on 2015-04-01 05:03:07 UTC

--
-- PostgreSQL database dump complete
--

