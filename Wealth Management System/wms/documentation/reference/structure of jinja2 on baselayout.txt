This main structure of jinja2 on the baselayout template is:

{% block head_meta %}
    ... HTML Meta
{% endblock %}
{% block head_css %}
    ... CSS imports (bootstrap, fontAwesome, select2, fab specific etc...
{% endblock %}
{% block head_js %}
    ... JS imports (JQuery, fab specific)
{% endblock %}
{% block body %}
    {% block navbar %}
        ... The navigation bar (Menu)
    {% endblock %}
     {% block messages %}
        ... Where the flask flash messages are shown ("Added row", etc)
      {% endblock %}
      {% block content %}
        ... All the content goes here, forms, lists, index, charts etc..
      {% endblock %}
    {% block footer %}
        ... The footer, by default its almost empty.
    {% endblock %}
{% block tail_js %}
{% endblock %}