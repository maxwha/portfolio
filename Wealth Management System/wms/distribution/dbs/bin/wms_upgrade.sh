#!/bin/bash
#################################################
#
#  --dbname=wms update script
#
#################################################

	if	[[ "${DB_DBTYPE}" != "POSTGRESQL" 
||	  "( ${DB_SERVER}" = ""  && ${DB_HOST}" = "" ) 
||	  "${DB_PORT}" = "" 
||	  "${DB_DB}" = "" 
||	  "${DB_SA_USER}" = "" 
||	  "${DB_SA_PASS}" = "" 
||	  "${DB_USER}" = "" 
||	  "${DB_PASS}" = "" 
	]]
then
	echo	"#!/bin/bash "
	echo	"# The following environment variables need to be set and exported"
	echo	"# Run this script again and redirect to a file, where a template will be written"
	echo	"" 
	echo	"# Sample values for these variables."
	echo	"#	DB_DBTYPE="POSTGRESQL"" 
	echo	"#	DB_SERVER="CTAS"" 
	echo	"#	DB_HOST="localhost"" 
	echo	"#	DB_PORT="5432"" 
	echo	"#	DB_DB="wms"" 
	echo	"#	DB_SA_USER="postgres"" 
	echo	"#	DB_SA_PASS=""" 
	echo	"#	DB_USER="admin"" 
	echo	"#	DB_PASS=""" 
	echo	"" 
	echo	"# Populate these values suitable for your environment and execute this script when entering it."
	echo	"# Any existing values have been pre-populated for you."
	echo	"export	DB_DBTYPE=\"${DB_DBTYPE}\"" 
	echo	"export	DB_SERVER=\"${DB_SERVER}\"" 
	echo	"export	DB_HOST=\"${DB_HOST}\"" 
	echo	"export	DB_PORT=\"${DB_PORT}\"" 
	echo	"export	DB_DB=\"${DB_DB}\"" 
	echo	"export	DB_SA_USER=\"${DB_SA_USER}\"" 
	echo	"export	DB_SA_PASS=\"${DB_SA_PASS}\"" 
	echo	"export	DB_USER=\"${DB_USER}\"" 
	echo	"export	DB_PASS=\"${DB_PASS}\"" 
	echo	""
	exit
fi

_SQL="psql " 
_OPTIONS="--no-readline --quiet --no-align " 
_TERM=";" 

_SERVER="" 
_HOST="" 
_PORT="" 
_DB="" 
_USER="" 
_PASS="" 
_SA_USER="" 
_SA_PASS="" 

if	[ "${DB_SERVER}" != "" ] && [ "" != "" ]
then
	_SERVER="${DB_SERVER}"
fi

if	[ "${DB_HOST}" != "" ] && [ "--host=" != "" ]
then
	_HOST="--host=${DB_HOST}"
fi

if	[ "${DB_PORT}" != "" ] && [ "--port=" != "" ]
then
	_PORT="--port=${DB_PORT}"
fi

if	[ "${DB_DB}" != "" ] && [ "--dbname=" != "" ]
then
	_DB="--dbname=${DB_DB}"
fi

if	[ "${DB_USER}" != "" ] && [ "--username=" != "" ]
then
	_USER="--username=${DB_USER}"
fi

if	[ "${DB_PASS}" != "" ] && [ "PGPASSWORD=" != "" ]
then
	_PASS="PGPASSWORD=${DB_PASS}"
fi

if	[ "${DB_SA_USER}" != "" ] && [ "--username=" != "" ]
then
	_SA_USER="--username=${DB_SA_USER}"
fi

if	[ "${DB_SA_PASS}" != "" ] && [ "PGPASSWORD=" != "" ]
then
	_SA_PASS="PGPASSWORD=${DB_SA_PASS}"
fi


eval ${_SA_PASS} ${_SQL} ${_OPTIONS} ${_PORT} ${_HOST} ${_SERVER} ${_SA_USER} <<!!
!!


eval ${_PASS} ${_SQL} ${_OPTIONS} ${_PORT} ${_HOST} ${_SERVER} ${_USER} ${_DB} <<"!!"
/**************************************************************
 *
 *	PARSE_OBJECT_NAME
 *
 *	Parses a partial or complete object identifier and returns its
 *	constituent parts in a single row table.
 *
 *	Should handle 'table' , 'schema.table', 'db..table', 'db.schema.table'
 *
 *	Usage
 *
	select	database_name
	,	schema_name
	,	object_name
	into	v_database_name
	,	v_schema_name
	,	v_object_name
	from	parse_object_name( 'db.schema.table')
 *
 *
 *************************************************************/

DROP FUNCTION IF EXISTS PARSE_OBJECT_NAME ( VARCHAR );

CREATE OR REPLACE FUNCTION PARSE_OBJECT_NAME
(	P_NAME  VARCHAR( 255 )
)
RETURNS	table
(	database_name	VARCHAR
,	schema_name	VARCHAR
,	object_name	VARCHAR
)
AS $$


DECLARE

V_RETURN	RECORD;

V_NAME		VARCHAR(255);
V_TYPE		VARCHAR(255);

V_DOT		integer;
V_DOT2		integer;

V_PART_NAME	VARCHAR(255);
V_DATABASE_NAME	VARCHAR(255);
V_SCHEMA_NAME	VARCHAR(255);
V_OBJECT_NAME	VARCHAR(255);

BEGIN
	V_NAME		:= LOWER(P_NAME);
--	V_DOT		:= INSTR(P_NAME,'.');
	V_DOT		:= position('.' in P_NAME);

	IF	( V_DOT > 1 )
	THEN
		V_PART_NAME	:= substring(V_NAME from (V_DOT+1) );
		V_DOT2		:= position('.' in V_PART_NAME );
		IF	( V_DOT2 > 0 )
		THEN
			V_DATABASE_NAME		= SUBSTRING(V_NAME from 1 for (V_DOT-1) );
			IF	( V_DOT2 > 1 )
			THEN
				V_SCHEMA_NAME	= SUBSTRING(V_PART_NAME from 1 for (V_DOT2-1) );
			ELSE
				V_SCHEMA_NAME	= current_schema();
			END IF;
			V_OBJECT_NAME		= SUBSTRING(V_PART_NAME from (V_DOT2+1) );
		ELSE
			V_DATABASE_NAME		= current_database();
			V_SCHEMA_NAME		= SUBSTRING(V_NAME from 1 for (V_DOT-1) );
			V_OBJECT_NAME		= SUBSTRING(V_NAME from (V_DOT+1) );
		END IF;
	ELSE
		V_DATABASE_NAME	= current_database();
		V_SCHEMA_NAME	= current_schema();
		V_OBJECT_NAME	= SUBSTRING(V_NAME from (V_DOT+1) );
	END IF;

-- raise NOTICE ' P_NAME=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_OBJECT_NAME=%', P_NAME, V_DATABASE_NAME, V_SCHEMA_NAME, V_OBJECT_NAME;


	RETURN	QUERY
	SELECT	V_DATABASE_NAME
	,	V_SCHEMA_NAME
	,	V_OBJECT_NAME
	;

END
$$
LANGUAGE PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'PARSE_OBJECT_NAME', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "PARSE_OBJECT_NAME" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "PARSE_OBJECT_NAME" >>>'
	END
AS Result ;

-- Tests
--
-- select	PARSE_OBJECT_NAME( 'wms..users' ) ;
-- select	PARSE_OBJECT_NAME( 'wms.public.users' );
-- select	PARSE_OBJECT_NAME( 'wms.pub.users' );
-- select	PARSE_OBJECT_NAME( 'wms.users' );
-- select	PARSE_OBJECT_NAME( 'public.users' );
-- select	PARSE_OBJECT_NAME( 'users' );

CREATE OR REPLACE FUNCTION OBJECT_EXISTS
(	P_NAME  VARCHAR( 255 )
,	P_TYPE  VARCHAR( 255 )
)
RETURNS Boolean
AS $$


DECLARE

V_RETURN_VALUE	Boolean		:= FALSE;

V_NAME		VARCHAR(255);
V_TYPE		VARCHAR(255);

V_DOT		integer;
V_DOT2		integer;

V_PART_NAME	VARCHAR(255);
V_DATABASE_NAME	VARCHAR(255);
V_SCHEMA_NAME	VARCHAR(255);
V_OBJECT_NAME	VARCHAR(255);

BEGIN

	SELECT	database_name
	,	schema_name
	,	object_name
	INTO	V_DATABASE_NAME
	,	V_SCHEMA_NAME
	,	V_OBJECT_NAME
	FROM	PARSE_OBJECT_NAME( LOWER( P_NAME ))
	;

-- raise NOTICE ' P_NAME=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_OBJECT_NAME=%', P_NAME, V_DATABASE_NAME, V_SCHEMA_NAME, V_OBJECT_NAME;

	V_TYPE		= COALESCE( LOWER(P_TYPE), 'routine' );

	IF	( V_TYPE = 'table' )
	THEN
		SELECT	TRUE
		INTO	V_RETURN_VALUE
		FROM	information_schema.tables
		WHERE	( table_catalog		= V_DATABASE_NAME
		AND	table_schema		= V_SCHEMA_NAME
		AND	table_name		= V_OBJECT_NAME
		AND	table_type		= 'BASE TABLE'
			)
		OR	( table_catalog		= V_DATABASE_NAME
		AND	table_name		= V_OBJECT_NAME
		AND	table_type		= 'LOCAL TEMPORARY'
			)
		;
	ELSEIF	( V_TYPE = 'view' )
	THEN
		SELECT	TRUE
		INTO	V_RETURN_VALUE
		FROM	information_schema.views
		WHERE	table_catalog		= V_DATABASE_NAME
		AND	table_schema		= V_SCHEMA_NAME
		AND	table_name		= V_OBJECT_NAME
		;
	ELSEIF	( V_TYPE = 'routine' )
	THEN
		SELECT	TRUE
		INTO	V_RETURN_VALUE
		FROM	information_schema.routines
		WHERE	routine_catalog		= V_DATABASE_NAME
		AND	routine_schema		= V_SCHEMA_NAME
		AND	routine_name		= V_OBJECT_NAME
		AND	routine_type		= 'FUNCTION'
		;
	ELSE 
		SELECT	FALSE
		INTO	V_RETURN_VALUE
		;
	END IF;

	RETURN 	COALESCE( V_RETURN_VALUE, FALSE);

END
$$
LANGUAGE PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'OBJECT_EXISTS', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "OBJECT_EXISTS" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "OBJECT_EXISTS" >>>'
	END
AS Result ;

-- Tests
--
-- select	OBJECT_EXISTS( 'OBJECT_EXIST', 'ROUTINE' );
-- select	OBJECT_EXISTS( 'OBJECT_EXISTS', 'ROUTINE' );
-- select	OBJECT_EXISTS( 'wms..OBJECT_EXISTS', 'ROUTINE' ) ;
-- select	OBJECT_EXISTS( 'wms.public.OBJECT_EXISTS', 'ROUTINE' );
-- select	OBJECT_EXISTS( 'wms.pub.OBJECT_EXISTS', 'ROUTINE' );
-- select	OBJECT_EXISTS( 'wms.OBJECT_EXISTS', 'ROUTINE' );
-- select	OBJECT_EXISTS( 'public.OBJECT_EXISTS', 'ROUTINE' );

CREATE OR REPLACE FUNCTION COLUMN_EXISTS
(	P_TABLE  VARCHAR( 255 )
,	P_COLUMN  VARCHAR( 255 )
)
RETURNS Boolean
AS $$

DECLARE

V_RETURN_VALUE	boolean		:=	FALSE;

V_TABLE		VARCHAR(255);
V_COLUMN	VARCHAR(255);

V_DOT		integer;
V_DOT2		integer;

V_PART_NAME	VARCHAR(255);

V_DATABASE_NAME	VARCHAR(255);
V_SCHEMA_NAME	VARCHAR(255);
V_TABLE_NAME	VARCHAR(255);


BEGIN
	V_TABLE		:= LOWER(P_TABLE);
	V_COLUMN	:= LOWER(P_COLUMN);
--	V_DOT		:= INSTR(P_TABLE,'.');
	V_DOT		:= position('.' in P_TABLE);

	IF	( V_DOT > 1 )
	THEN
		V_PART_NAME	:= substring(V_TABLE from (V_DOT+1) );
		V_DOT2		:= position('.' in V_PART_NAME );
		IF	( V_DOT2 > 0 )
		THEN
			V_DATABASE_NAME		= SUBSTRING(V_TABLE from 1 for (V_DOT-1) );
			IF	( V_DOT2 > 1 )
			THEN
				V_SCHEMA_NAME	= SUBSTRING(V_PART_NAME from 1 for (V_DOT2-1) );
			ELSE
				V_SCHEMA_NAME	= current_schema();
			END IF;
			V_TABLE_NAME		= SUBSTRING(V_PART_NAME from (V_DOT2+1) );
		ELSE
			V_DATABASE_NAME		= current_database();
			V_SCHEMA_NAME		= SUBSTRING(V_TABLE from 1 for (V_DOT-1) );
			V_TABLE_NAME		= SUBSTRING(V_TABLE from (V_DOT+1) );
		END IF;
	ELSE
		V_DATABASE_NAME	= current_database();
		V_SCHEMA_NAME	= current_schema();
		V_TABLE_NAME	= SUBSTRING(V_TABLE from (V_DOT+1) );
	END IF;

-- raise NOTICE ' P_TABLE=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_TABLE_NAME=%', P_TABLE, V_DATABASE_NAME, V_SCHEMA_NAME, V_TABLE_NAME;

	IF	EXISTS
	(
		SELECT	NULL
		FROM	information_schema.columns
		WHERE	table_catalog		= V_DATABASE_NAME
		AND	table_schema		= V_SCHEMA_NAME
		AND	table_name		= V_TABLE_NAME
		AND	column_name		= V_COLUMN
	)
	THEN
		V_RETURN_VALUE		=	TRUE;
	ELSE
		V_RETURN_VALUE		=	FALSE;
	END IF;

	RETURN 	COALESCE( V_RETURN_VALUE, FALSE );

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'COLUMN_EXISTS', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "COLUMN_EXISTS" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "COLUMN_EXISTS" >>>'
	END
AS Result ;

-- Tests
--
-- select	COLUMN_EXISTS( 'wms..t1', 'c1' );
-- select	COLUMN_EXISTS( 'wms..t1', 'c2' );


DROP TABLE IF EXISTS tree_nodes CASCADE 
; 

DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'tree_nodes', 'TABLE' )
THEN

CREATE TABLE  tree_nodes
(	id			SERIAL
,	parent			VARCHAR(255)
				NULL
,	child			VARCHAR(255)
				NULL
,	is_leaf			INT
				NULL
,	is_home			INT
				NULL
,	level			INT
				NULL
,	is_mutable		INT
				NULL
,	parent_object		VARCHAR(255)
				NULL
,	parent_id		INT
				NULL
,	child_object		VARCHAR(255)
				NULL
,	child_id		INT
				NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT tree_nodes_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('tree_nodes','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE tree_nodes >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE tree_nodes >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE tree_nodes ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_nodes', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "tree_nodes" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "tree_nodes" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'tree_nodes', 'TABLE' )
THEN

CREATE TABLE  tree_nodes
(	id			SERIAL
,	parent			VARCHAR(255)
				NULL
,	child			VARCHAR(255)
				NULL
,	is_leaf			INT
				NULL
,	is_home			INT
				NULL
,	level			INT
				NULL
,	is_mutable		INT
				NULL
,	parent_object		VARCHAR(255)
				NULL
,	parent_id		INT
				NULL
,	child_object		VARCHAR(255)
				NULL
,	child_id		INT
				NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT tree_nodes_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('tree_nodes','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE tree_nodes >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE tree_nodes >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE tree_nodes ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_nodes', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "tree_nodes" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "tree_nodes" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'data', 'TABLE' )
THEN
CREATE TABLE  data
(	id			SERIAL
,	data_id			INT
				NULL
,	data_type		VARCHAR(255)
				NULL
,	source_system		VARCHAR(255)
				NULL
,	source_reference	VARCHAR(8000)
				NULL
,	tag			VARCHAR(8000)
				NULL
,	value			VARCHAR(8000)
				NULL
,	value_sequence		int
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT data_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('data','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE data >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE data >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE data ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'data', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "data" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "data" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'feed', 'TABLE' )
THEN
CREATE TABLE  feed
(	id			SERIAL
,	feed_id			INT
				NULL
,	feed_type		VARCHAR(255)
				NULL
,	source_system		VARCHAR(255)
				NULL
,	source_reference	VARCHAR(8000)
				NULL
,	tag			VARCHAR(8000)
				NULL
,	value			VARCHAR(8000)
				NULL
,	value_sequence		int
				NULL
,	state			int		DEFAULT 100
				NULL
,	target_state		int		DEFAULT	200
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT feed_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('feed','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'feed', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "feed" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "feed" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'logs', 'TABLE' )
THEN
	CREATE TABLE  logs
	(	id			SERIAL
	,	message			VARCHAR(16384)
					DEFAULT ''
					NULL
	,	message_source		VARCHAR(255)
					NULL
	,	message_depth		INT
					DEFAULT 0
					NULL
	,	message_severity	VARCHAR(30)
					DEFAULT	'INFO'
					NULL
	,	message_date		TIMESTAMP
					DEFAULT current_timestamp
					NULL
	,	message_user		VARCHAR(100)
					DEFAULT	current_user
					NULL
	,	logs_date		TIMESTAMP
					DEFAULT current_timestamp
					NULL
	,	user_id			VARCHAR(100)
					DEFAULT	current_user
					NULL
	,	CONSTRAINT logs_pk PRIMARY KEY (id)
	);
	
	IF	OBJECT_EXISTS('logs','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE logs >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE logs >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE logs ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'logs', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "logs" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "logs" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'messages', 'TABLE' )
THEN
CREATE TABLE  messages
(	id			SERIAL
,	message_header_id	INT
				NULL
,	tag			VARCHAR(8000)
				NULL
,	value			VARCHAR(8000)
				NULL
,	value_sequence		int
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT messages_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('messages','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE messages >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE messages >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE messages ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'messages', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "messages" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "messages" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'message_headers', 'TABLE' )
THEN
CREATE TABLE  message_headers
(	id			SERIAL
,	source_system		VARCHAR(255)
				NULL
,	source_reference	VARCHAR(255)
				NULL
,	message_type		VARCHAR(8000)
				NULL
,	message_id		INT
				NULL
,	state			INT
				NULL
,	target_state		INT
				NULL
,	source_status		VARCHAR(255)
				NULL
,	message_status		VARCHAR(255)
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT message_headers_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('message_headers','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE message_headers >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE message_headers >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE message_headers ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'message_headers', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "message_headers" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "message_headers" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'message_tags', 'TABLE' )
THEN
CREATE TABLE  message_tags
(	id			SERIAL
,	name			VARCHAR(8000)
				NULL
,	type			VARCHAR(255)
				NULL
,	size			int
				NULL
,	precision		int
				NULL
,	scale			int
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT message_tags_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('message_tags','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE message_tags >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE message_tags >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE message_tags ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'message_tags', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "message_tags" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "message_tags" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'trees', 'TABLE' )
THEN
CREATE TABLE  trees
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	shortname		VARCHAR(50)
				NULL
,	object_name		VARCHAR(255)
				NULL
,	object_name_id		VARCHAR(255)
				NULL
,	xref_name		VARCHAR(255)
				NULL
,	xref_name_id		VARCHAR(255)
				NULL
,	xref_xref_id		VARCHAR(255)
				NULL
,	leaf_name		VARCHAR(255)
				NULL
,	leaf_name_id		VARCHAR(255)
				NULL
,	bref_name		VARCHAR(255)
				NULL
,	bref_name_id		VARCHAR(255)
				NULL
,	bref_xref_id		VARCHAR(255)
				NULL
,	bud_name		VARCHAR(255)
				NULL
,	bud_name_id		VARCHAR(255)
				NULL
,	notes			VARCHAR(2000)
				NULL

-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT trees_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('trees','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE trees >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE trees >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE trees ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'trees', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "trees" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "trees" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'tree_structures', 'TABLE' )
THEN

CREATE	TABLE	tree_structures
(	id			SERIAL
,	tree_id			INT		NULL
,	nbr			INT		NULL 
,	object_id		INT		NULL
,	object_name		VARCHAR(50)	NULL
-- ,	object_name_id		VARCHAR(50)	NULL		-- No Longer Necessary, Lookup on objects table
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT tree_structures_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('tree_structures','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE tree_structures >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE tree_structures >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE tree_structures ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_structures', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "tree_structures" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "tree_structures" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'tree_nodes', 'TABLE' )
THEN

CREATE TABLE  tree_nodes
(	id			SERIAL
,	parent			VARCHAR(255)
				NULL
,	child			VARCHAR(255)
				NULL
,	is_leaf			INT
				NULL
,	is_home			INT
				NULL
,	level			INT
				NULL
,	is_mutable		INT
				NULL
,	parent_object		VARCHAR(255)
				NULL
,	parent_id		INT
				NULL
,	child_object		VARCHAR(255)
				NULL
,	child_id		INT
				NULL
/*
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
,	CONSTRAINT tree_nodes_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('tree_nodes','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE tree_nodes >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE tree_nodes >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE tree_nodes ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_nodes', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "tree_nodes" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "tree_nodes" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'columns', 'TABLE' )
THEN

CREATE TABLE  columns
(	id			SERIAL
,	name			VARCHAR(8000)
				NULL
,	title			VARCHAR(8000)
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT columns_pk PRIMARY KEY (id)
,	CONSTRAINT columns_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('columns','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE columns >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE columns >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE columns ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'columns', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "columns" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "columns" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'cross_references', 'TABLE' )
THEN

CREATE TABLE  cross_references
(	id			SERIAL
,	object_type		VARCHAR(255)
				NULL
,	object_id		integer	
				NULL
,	xref_type		VARCHAR(255)
				NULL
,	xref_value		VARCHAR(255)
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT cross_references_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('cross_references','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE cross_references >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE cross_references >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE cross_references ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'cross_references', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "cross_references" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "cross_references" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'feed_mappings', 'TABLE' )
THEN

CREATE TABLE  feed_mappings
(	id			SERIAL
,	feed_type		VARCHAR(8000)	NULL
,	tag			VARCHAR(8000)	NULL
,	table_name		VARCHAR(8000)	NULL
,	column_name		VARCHAR(8000)	NULL
,	column_type		VARCHAR(8000)	NULL
,	default_value		VARCHAR(8000)	NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT feed_mappings_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('feed_mappings','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_mappings >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_mappings >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_mappings ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_mappings', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "feed_mappings" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "feed_mappings" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'globals', 'TABLE' )
THEN

CREATE TABLE  globals
(	id			SERIAL
,	code			VARCHAR(8000)	NULL
,	value			VARCHAR(8000)	NULL
,	datatype		VARCHAR(8000)	DEFAULT 'VARCHAR'
						NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT globals_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('globals','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE globals >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE globals >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE globals ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'globals', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "globals" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "globals" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'logins', 'TABLE' )
THEN
CREATE TABLE  logins
(	id			SERIAL
,	name			VARCHAR(255)
				NOT NULL
,	title			VARCHAR(255)
				NOT NULL
,	username		VARCHAR(255)
				NOT NULL
,	password		VARCHAR(255)
				NOT NULL
,	user_id			INTEGER
				NULL
,	role_id			integer
				NULL
,	last_login		timestamp
				NULL
,	one_off			timestamp
				NULL
,	is_suspended		CHAR(1)
				NOT NULL
				DEFAULT 'N'
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT logins_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('logins','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE logins >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE logins >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE logins ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'logins', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "logins" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "logins" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'lookup_codes', 'TABLE' )
THEN
CREATE TABLE  lookup_codes
(	id			SERIAL
,	type			VARCHAR(255)
				NOT NULL
,	code			VARCHAR(255)
				NOT NULL
,	value			VARCHAR(255)
				NOT NULL
,	name			VARCHAR(255)
				NULL	
,	shortname		VARCHAR(30)
				NULL
,	sequence		INT
				NULL
				DEFAULT		0
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT lookup_codes_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('lookup_codes','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE lookup_codes >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE lookup_codes >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE lookup_codes ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'lookup_codes', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "lookup_codes" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "lookup_codes" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;





DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'message_header_rights', 'TABLE' )
THEN

CREATE TABLE  message_header_rights
(	id			SERIAL
,	message_type		VARCHAR(8000)	NULL
,	role_id			int		NULL
,	access_rights		integer		default 0
						NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT message_header_rights_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('message_header_rights','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE message_header_rights >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE message_header_rights >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE message_header_rights ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'message_header_rights', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "message_header_rights" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "message_header_rights" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'message_rights', 'TABLE' )
THEN

CREATE TABLE  message_rights
(	id			SERIAL
,	message_type		VARCHAR(255)	NULL
,	tag			VARCHAR(8000)	NULL
,	role_id			integer		null
,	access_rights		integer		null
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT message_rights_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('message_rights','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE message_rights >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE message_rights >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE message_rights ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'message_rights', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "message_rights" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "message_rights" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'objects', 'TABLE' )
THEN

CREATE TABLE  objects
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	id_name			VARCHAR(255)
				NULL
,	value_name		VARCHAR(255)
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT objects_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('objects','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE objects >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE objects >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE objects ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'objects', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "objects" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "objects" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'roles', 'TABLE' )
THEN

CREATE TABLE  roles
(	id			SERIAL
,	role			VARCHAR(50)
				NOT NULL
,	name			VARCHAR(255)
				NOT NULL
,	title			VARCHAR(255)
				NOT NULL
,	shortname		VARCHAR(50)
				NOT NULL
,	precedence		INTEGER
				NULL
				DEFAULT	0
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT roles_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('roles','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE roles >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE roles >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE roles ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'roles', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "roles" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "roles" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'role_rights', 'TABLE' )
THEN

CREATE TABLE  role_rights
(	id			SERIAL
,	role_id			INTEGER
				NULL
,	object_type		VARCHAR(255)
				NULL
,	object_name		VARCHAR(255)
				NULL
,	object_subname		VARCHAR(255)
				NULL
,	access_rights		INT
				DEFAULT	0
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT role_rights_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('role_rights','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE role_rights >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE role_rights >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE role_rights ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'role_rights', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "role_rights" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "role_rights" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'tags', 'TABLE' )
THEN

CREATE TABLE  tags
(	id			SERIAL
,	tag			VARCHAR(8000)	NULL
,	title			VARCHAR(8000)	NULL
,	summary			VARCHAR(8000)	NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT tags_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('tags','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE tags >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE tags >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE tags ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'tags', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "tags" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "tags" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'users', 'TABLE' )
THEN

CREATE TABLE  users
(	id			SERIAL
,	name			VARCHAR(255)	NULL
,	title			VARCHAR(255)	NULL
,	shortname		VARCHAR(50)	NULL
,	user_type		VARCHAR(50)	NULL
,	type_id			INT		NULL	DEFAULT 0
,	badge_number		INT		NULL	DEFAULT 0
,	is_suspended		INT		NULL	DEFAULT 0
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT users_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('users','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE users >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE users >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE users ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'users', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "users" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "users" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'user_columns', 'TABLE' )
THEN

CREATE TABLE  user_columns
(	id			SERIAL
,	user_id			integer		NULL
,	object_name		VARCHAR(8000)	NULL
,	column_name		VARCHAR(8000)	NULL
,	column_title		VARCHAR(8000)	NULL
,	column_order		integer		NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT user_columns_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('user_columns','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE user_columns >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE user_columns >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE user_columns ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'user_columns', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "user_columns" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "user_columns" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'user_roles', 'TABLE' )
THEN

CREATE TABLE  user_roles
(	id			SERIAL
,	user_id			INTEGER
				NULL
,	role_id			INTEGER
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT user_roles_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('user_roles','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE user_roles >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE user_roles >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE user_roles ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'user_roles', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "user_roles" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "user_roles" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'user_tags', 'TABLE' )
THEN

CREATE TABLE  user_tags
(	id			SERIAL
,	user_id			integer		NULL
,	message_type		VARCHAR(255)	NULL
,	tag			VARCHAR(8000)	NULL
,	tag_title		VARCHAR(8000)	NULL
,	tag_order		integer		NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT user_tags_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('user_tags','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE user_tags >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE user_tags >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE user_tags ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'user_tags', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "user_tags" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "user_tags" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


/*

This table allows us to provide a standard interface to any procedure that needs lookups , Lists of Vales etc.

	
	type		says what type of code we want
	code		says what lookup_code we want
	basetype	describes the lookup datatype or source
				INT, CHAR, NUMBER	are basic datatypes
				LIST			LOVs sourced from lookup_codes
				KEY			LOVs sourced from table in table_name using key in table_key
	table_name	the source table to find the lookup
			lookup_codes is a special case
	table_key	the key field(s) to use to source the lookup
			if basetype is list table_name is lookup_codes then this is the type in that table
	value		then name of the column that provides the value
			if table_name is lookup_codes then this is the value column
	shortname	then name of the column that provides the shortname
			if table_name is lookup_codes then this is the shortname column
	name		then name of the column that provides the name
			if table_name is lookup_codes then this is the name column
	title		a title for this data

*/


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'lookups', 'TABLE' )
THEN

CREATE TABLE  lookups
(	id			SERIAL
,	type			VARCHAR(255)
				NULL
,	code			VARCHAR(255)
				NULL
,	basetype		VARCHAR(255)
				NULL
,	table_name		VARCHAR(255)
				NULL	
,	table_key		VARCHAR(255)
				NULL	
,	value			VARCHAR(255)
				NULL	
,	shortname		VARCHAR(255)
				NULL
,	name			VARCHAR(255)
				NULL	
,	title			VARCHAR(255)
				NULL	
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT lookups_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('lookups','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE lookups >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE lookups >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE lookups ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'lookups', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "lookups" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "lookups" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


/*

	This table allows us to change the precedence of preference groups

	A Priority name identifies the grouping that is used to determine what preferences take priority 
	when deciding which preference value should be used for a particlular preference.

	These groupings are hard coded in the get_preferences() and get_values() SPs.

	They could be for example:

		member_id			
		coach_id
		team_id
		fixture_id
		player_id
		game_id
		system		

	The values should be a power of 2 and in DESCENDING ORDER and will form a bit mask

	For example:
		player		32
		member		16
		coach		8
		team		4
		game		2
		fixture		1
		system		0	( SYSTEM DEFAULT )

	When added together each a unique number will occur for each combination

	Therefor a preference set for member in a team has precedence over a member only one.
		
	An alternative could be VALUES in ASCENDING ORDER

		fixture		1
		team		2
		coach		4
		member		8
		system		MAX_INT	( SYSTEM DEFAULT )

	Then a preference set for member only has precedence over an member in a team.
			


	
	name			says what type of code we want
	priority		the priority

*/


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'preference_priorities', 'TABLE' )
THEN

CREATE TABLE  preference_priorities
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	priority		INT
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT preference_priorities_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('preference_priorities','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE preference_priorities >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE preference_priorities >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE preference_priorities ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'preference_priorities', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "preference_priorities" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "preference_priorities" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'query_detail_columns', 'TABLE' )
THEN
CREATE TABLE  query_detail_columns
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	column_sequence		INTEGER
				NULL
				DEFAULT	0
,	column_name		VARCHAR(50)
				NULL
,	column_title		VARCHAR(50)
				NULL
,	column_summary		VARCHAR(50)
				NULL
,	column_precedence	INTEGER
				NULL
				DEFAULT	0
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT query_detail_columns_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('query_detail_columns','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE query_detail_columns >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE query_detail_columns >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE query_detail_columns ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'query_detail_columns', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "query_detail_columns" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "query_detail_columns" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'diaries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "adviser_id" IN diaries >>';
	IF NOT COLUMN_EXISTS( 'diaries', 'adviser_id' )
	THEN
		RAISE NOTICE	'<<< ADDING "adviser_id" TO diaries >>';
		ALTER TABLE IF EXISTS diaries
		ADD	adviser_id		INTEGER	NULL
		;
		IF COLUMN_EXISTS( 'diaries', 'adviser_id' )
		THEN
			RAISE NOTICE	'<<< ADDED "adviser_id" TO diaries >>';
		ELSE
			RAISE NOTICE	'<<< FAILED TO ADD "adviser_id" TO diaries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "adviser_id" EXISTS IN diaries >>';
	END IF;
END IF;

END

$$;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'advisers', 'TABLE' )
THEN

CREATE TABLE  ADVISERS
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	forenames		VARCHAR(255)
				NULL
,	surname			VARCHAR(255)
				NULL
,	phone			VARCHAR(255)
				NULL
,	email			VARCHAR(255)
				NULL
,	managed_by_id		INT
				NULL
,	user_id			INT
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT advisers_pk PRIMARY KEY (id)
,	CONSTRAINT advisers_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('advisers','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE advisers >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE advisers >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE advisers ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'advisers', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "advisers" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "advisers" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF OBJECT_EXISTS( 'adviser_diaries', 'TABLE' )
THEN
	RAISE NOTICE '<<< DROPPING TABLE "adviser_diaries" >>';
	DROP TABLE IF EXISTS adviser_diaries CASCADE;
	IF	OBJECT_EXISTS('adviser_diaries','TABLE')
	THEN
		RAISE NOTICE '<<< FAILED TO DROP TABLE "adviser_diaries" >>';
	ELSE
		RAISE NOTICE '<<< DROPPED TABLE "adviser_diaries" >>';
	END IF;
ELSE
	RAISE NOTICE '<<< NO TABLE TO DROP "adviser_diaries" >>';
END IF;

END

$$;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	NOT OBJECT_EXISTS( 'clients', 'TABLE' )
THEN

CREATE TABLE  clients
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	forename		VARCHAR(255)
				NULL
,	surname			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	shortname		VARCHAR(50)
				NULL
,	adviser_id		INT
				NULL
,	date_of_birth		DATE
				NULL
,	ni_number		VARCHAR(10)
				NULL
,	image_id		INT
				NULL
--,	contact_id		int
--				NULL
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
,	CONSTRAINT clients_pk PRIMARY KEY (id)
,	CONSTRAINT clients_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('clients','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE clients >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE clients >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE clients ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "date_of_birth" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'date_of_birth' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	date_of_birth		DATE	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'date_of_birth' )
		THEN
			RAISE NOTICE	'<<< ADDED date_of_birth TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING date_of_birth TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "ni_number" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'ni_number' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	ni_number		VARCHAR(10)	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'ni_number' )
		THEN
			RAISE NOTICE	'<<< ADDED ni_number TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING ni_number TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "image_id" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'image_id' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	image_id		VARCHAR(10)	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'image_id' )
		THEN
			RAISE NOTICE	'<<< ADDED image_id TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING image_id TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;





select	CASE
	WHEN OBJECT_EXISTS( 'clients', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "clients" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "clients" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



/*
DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'clients', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "contact_id" IN clients >>';
	IF NOT COLUMN_EXISTS( 'clients', 'contact_id' )
	THEN
		ALTER TABLE IF EXISTS clients
		ADD	contact_id		VARCHAR(10)	NULL
		;
		IF COLUMN_EXISTS( 'clients', 'contact_id' )
		THEN
			RAISE NOTICE	'<<< ADDED contact_id TO clients >>';
		ELSE
			RAISE NOTICE	'<<< FAILED ADDING contact_id TO clients >>';
		END IF;
	END IF;
END IF;

END

$$;

*/


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'client_contacts', 'TABLE' )
THEN

CREATE TABLE  client_contacts
(	id				SERIAL
,	client_id			INT
					NULL
,	contact_id			INT
					NULL
,	sequence			INT		DEFAULT 0
					NULL
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
,	CONSTRAINT client_contacts_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('client_contacts','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE client_contacts >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE client_contacts >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE client_contacts ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'client_contacts', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "client_contacts" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "client_contacts" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF OBJECT_EXISTS( 'client_diaries', 'TABLE' )
THEN
	RAISE NOTICE '<<< DROPPING TABLE "client_diaries" >>';
	DROP TABLE IF EXISTS client_diaries CASCADE;
	IF	OBJECT_EXISTS('client_diaries','TABLE')
	THEN
		RAISE NOTICE '<<< FAILED TO DROP TABLE "client_diaries" >>';
	ELSE
		RAISE NOTICE '<<< DROPPED TABLE "client_diaries" >>';
	END IF;
ELSE
	RAISE NOTICE '<<< NO TABLE TO DROP "client_diaries" >>';
END IF;

END

$$;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF OBJECT_EXISTS( 'client_social_networks', 'TABLE' )
THEN
	RAISE NOTICE '<<< DROPPING TABLE "client_social_networks" >>';
	DROP TABLE IF EXISTS client_social_networks CASCADE;
	IF	OBJECT_EXISTS('client_social_networks','TABLE')
	THEN
		RAISE NOTICE '<<< FAILED TO DROP TABLE "client_social_networks" >>';
	ELSE
		RAISE NOTICE '<<< DROPPED TABLE "client_social_networks" >>';
	END IF;
ELSE
	RAISE NOTICE '<<< NO TABLE TO DROP "client_social_networks" >>';
END IF;

END

$$;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'contacts', 'TABLE' )
THEN

CREATE TABLE  contacts
(	id				SERIAL
,	name				VARCHAR(255)
					NULL
,	title				VARCHAR(255)
					NULL
,	position			VARCHAR(255)
					NULL
,	forename			VARCHAR(255)
					NULL
,	surname				VARCHAR(255)
					NULL
,	honours				VARCHAR(255)
					NULL
,	phone				VARCHAR(255)
					NULL
,	email				VARCHAR(255)
					NULL
,	address_name			VARCHAR(255)
					NULL
,	address_no			VARCHAR(255)
					NULL
,	address_street			VARCHAR(255)
					NULL
,	address_district		vARCHAR(255)
					NULL
,	address_town			VARCHAR(255)
					NULL
,	address_city			VARCHAR(255)
					NULL
,	address_county			VARCHAR(255)
					NULL
,	address_country			VARCHAR(255)
					NULL
,	address_postcode		VARCHAR(255)
					NULL
,	client_id			INT
					NULL
,	sequence			int		default 0
					null
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
,	CONSTRAINT contacts_pk PRIMARY KEY (id)
,	CONSTRAINT contacts_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('contacts','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE contacts >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE contacts >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE contacts ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'contacts', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "contacts" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "contacts" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF OBJECT_EXISTS( 'contact_diaries', 'TABLE' )
THEN
	RAISE NOTICE '<<< DROPPING TABLE "contact_diaries" >>';
	DROP TABLE IF EXISTS contact_diaries CASCADE;
	IF	OBJECT_EXISTS('contact_diaries','TABLE')
	THEN
		RAISE NOTICE '<<< FAILED TO DROP TABLE "contact_diaries" >>';
	ELSE
		RAISE NOTICE '<<< DROPPED TABLE "contact_diaries" >>';
	END IF;
ELSE
	RAISE NOTICE '<<< NO TABLE TO DROP "contact_diaries" >>';
END IF;

END

$$;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'contact_social_networks', 'TABLE' )
THEN

CREATE TABLE  contact_social_networks
(	id				SERIAL
,	contact_id			INT
					NULL
,	social_network_id		INT
					NULL
,	identifier			VARCHAR(255)
					NULL
,	sequence			INT		DEFAULT 0
					NULL
-- Standard Columns
,	owner_id			integer		NULL
,	effective_from			timestamp	NULL
,	effective_to			timestamp	NULL
,	version				int		DEFAULT	1
							NULL
,	create_user			varchar(255)	DEFAULT user
							NULL
,	create_date			timestamp	DEFAULT current_timestamp
							NULL
,	update_user			varchar(255)	DEFAULT user
							NULL
,	update_date			timestamp	DEFAULT current_timestamp
							NULL
,	pid				int		null
,	CONSTRAINT contact_social_networks_pk PRIMARY KEY (id)
,	CONSTRAINT contact_social_networks_uk UNIQUE (contact_id,social_network_id,identifier)
);
	IF	OBJECT_EXISTS('contact_social_networks','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE contact_social_networks >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE contact_social_networks >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE contact_social_networks ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'contact_social_networks', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "contact_social_networks" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "contact_social_networks" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'diaries', 'TABLE' )
THEN

CREATE TABLE  diaries
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	adviser_id		INT
				NULL
,	user_id			INT
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT diaries_pk PRIMARY KEY (id)
,	CONSTRAINT diaries_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('diaries','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE diaries >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE diaries >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE diaries ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'diaries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "adviser_id" IN diaries >>';
	IF NOT COLUMN_EXISTS( 'diaries', 'adviser_id' )
	THEN
		RAISE NOTICE	'<<< ADDING "adviser_id" TO diaries >>';
		ALTER TABLE IF EXISTS diaries
		ADD	adviser_id		INTEGER	NULL
		;
		IF COLUMN_EXISTS( 'diaries', 'adviser_id' )
		THEN
			RAISE NOTICE	'<<< ADDED "adviser_id" TO diaries >>';
		ELSE
			RAISE NOTICE	'<<< FAILED TO ADD "adviser_id" TO diaries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "adviser_id" EXISTS IN diaries >>';
	END IF;
END IF;

END

$$;




select	CASE
	WHEN OBJECT_EXISTS( 'diaries', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "diaries" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "diaries" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'diary_entries', 'TABLE' )
THEN

CREATE TABLE  diary_entries
(	id			SERIAL
,	diary_id		INT
				NULL
,	subject			VARCHAR(255)
				NULL
,	location		VARCHAR(255)
				NULL
,	start_time		TIMESTAMP
				NULL
,	end_time		TIMESTAMP
				NULL
,	details			TEXT
				NULL
,	notes			tEXT
				NULL
,	user_id			INT
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT diary_entries_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('diary_entries','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE diary_entries >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE diary_entries >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE diary_entries ALREADY EXISTS >>';
END IF;

END

$$;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'diary_entries', 'TABLE' )
THEN
	RAISE NOTICE	'<<< CHECKING "client_id" IN diary_entries >>';
	IF NOT COLUMN_EXISTS( 'diary_entries', 'client_id' )
	THEN
		RAISE NOTICE	'<<< ADDING "client_id" TO diary_entries >>';
		ALTER TABLE IF EXISTS diary_entries
		ADD	client_id		INTEGER	NULL
		;
		IF COLUMN_EXISTS( 'diary_entries', 'client_id' )
		THEN
			RAISE NOTICE	'<<< ADDED "client_id" TO diary_entries >>';
		ELSE
			RAISE NOTICE	'<<< FAILED TO ADD "client_id" TO diary_entries >>';
		END IF;
	ELSE
		RAISE NOTICE	'<<< COLUMN "client_id" EXISTS IN diary_entries >>';
	END IF;
END IF;

END

$$;


select	CASE
	WHEN OBJECT_EXISTS( 'diary_entries', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "diary_entries" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "diary_entries" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'historic_prices', 'TABLE' )
THEN

CREATE TABLE  historic_prices
(	id			SERIAL
,	product_id		integer
				NULL
,	price_date		timestamp	DEFAULT current_timestamp
				NULL
,	open			NUMERIC(36,8)
				NULL
,	high			NUMERIC(36,8)
				NULL
,	low			NUMERIC(36,8)
				NULL
,	close			NUMERIC(36,8)
				NULL
,	volume			NUMERIC(36,8)
				NULL
,	adj_close		NUMERIC(36,8)
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT historic_prices_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('historic_prices','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE historic_prices >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE historic_prices >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE historic_prices ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'historic_prices', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "historic_prices" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "historic_prices" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
THEN

CREATE TABLE  historic_quotes
(	id			SERIAL
,	product_id		INT
				NULL
,	latest_value		NUMERIC(36,8)
				NULL
,	open			NUMERIC(36,8)
				NULL
,	high			NUMERIC(36,8)
				NULL
,	low			NUMERIC(36,8)
				NULL
,	close			NUMERIC(36,8)
				NULL
,	quote_date		timestamp	DEFAULT current_timestamp
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT historic_quotes_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('historic_quotes','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE historic_quotes >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE historic_quotes >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE historic_quotes ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'historic_quotes', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "historic_quotes" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "historic_quotes" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'portfolios', 'TABLE' )
THEN

CREATE TABLE  portfolios
(	id			SERIAL
,	name			VARCHAR(50)
				NULL
,	title			VARCHAR(50)
				NULL
,	client_id		integer
				NULL
,	nickname		VARCHAR(50)
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT portfolios_pk PRIMARY KEY (id)
,	CONSTRAINT portfolios_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('portfolios','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE portfolios >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE portfolios >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE portfolios ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'portfolios', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "portfolios" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "portfolios" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
THEN

CREATE TABLE  portfolio_positions
(	id			SERIAL
,	portfolio_id		INT
				NULL
,	product_id		INT
				NULL
,	currency		VARCHAR(3)
				NULL
,	quantity		INT
				NULL
,	price			NUMERIC( 36,8 )
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT portfolio_positions_pk PRIMARY KEY (id)
,	CONSTRAINT portfolio_positions_uk UNIQUE (portfolio_id, product_id)
);
	IF	OBJECT_EXISTS('portfolio_positions','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE portfolio_positions >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE portfolio_positions >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE portfolio_positions ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'portfolio_positions', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "portfolio_positions" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "portfolio_positions" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'quotes', 'TABLE' )
THEN

CREATE TABLE  quotes
(	id			SERIAL
,	product_id		INTEGER
				NULL
,	latest_value		NUMERIC(36,8)
				NULL
,	open			NUMERIC(36,8)
				NULL
,	high			NUMERIC(36,8)
				NULL
,	low			NUMERIC(36,8)
				NULL
,	mid			NUMERIC(36,8)
				NULL
,	close			NUMERIC(36,8)
				NULL
,	bid			NUMERIC(36,8)
				NULL
,	offer			NUMERIC(36,8)
				NULL
,	quote_date		timestamp	DEFAULT current_timestamp
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT quotes_pk PRIMARY KEY (id)
);
	IF	OBJECT_EXISTS('quotes','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE quotes >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE quotes >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE quotes ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'quotes', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "quotes" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "quotes" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_amex_security', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_amex_security CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_amex_security', 'TABLE' )
	THEN
	CREATE TABLE feed_amex_security
	(	id			SERIAL
	,	symbol			VARCHAR(255)	NULL
	,	name			VARCHAR(255)	NULL
	,	lastsale		VARCHAR(255)	NULL
	,	marketcap		VARCHAR(255)	NULL
	,	ipoyear			VARCHAR(255)	NULL
	,	sector			VARCHAR(255)	NULL
	,	industry		VARCHAR(255)	NULL
	,	summaryquote		VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_amex_security_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_amex_security','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_amex_security >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_amex_security >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_amex_security ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_amex_security', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_amex_security" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_amex_security" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_countries', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_countries CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_countries', 'TABLE' )
	THEN
	CREATE TABLE feed_countries
	(	id			SERIAL
	,	country			VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_countries_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_countries','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_countries >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_countries >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_countries ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_countries', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_countries" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_countries" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_currencies', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_currencies CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_currencies', 'TABLE' )
	THEN
	CREATE TABLE feed_currencies
	(	id			SERIAL
	,	name			VARCHAR(255)	NULL
	,	title			VARCHAR(255)	NULL
	,	entity			VARCHAR(255)	NULL
	,	country_code		VARCHAR(255)	NULL
	,	currency_title		VARCHAR(255)	NULL
	,	alpha_code		VARCHAR(255)	NULL
	,	num_code		VARCHAR(255)	NULL
	,	minor_unit		VARCHAR(10)	NULL
	,	minor_unit_int		int		NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_currencies_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_currencies','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_currencies >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_currencies >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_currencies ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_currencies', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_currencies" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_currencies" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_exchanges', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_exchanges CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_exchanges', 'TABLE' )
	THEN
	CREATE TABLE feed_exchanges
	(	id			SERIAL
	,	country			VARCHAR(255)	NULL
	,	exchange		VARCHAR(255)	NULL
	,	symbol			VARCHAR(255)	NULL
	,	ticker_suffix		VARCHAR(255)	NULL
	,	ticker			VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_exchanges_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_exchanges','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_exchanges >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_exchanges >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_exchanges ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_exchanges', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_exchanges" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_exchanges" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_index_components', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_index_components CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_index_components', 'TABLE' )
	THEN
	CREATE TABLE feed_index_components
	(	id			SERIAL
	,	index_name		VARCHAR(255)	NULL
	,	product_symbol_type	VARCHAR(255)	NULL
	,	product_symbol		VARCHAR(255)	NULL
	,	product_name		VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_index_components_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_index_components','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_index_components >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_index_components >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_index_components ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_index_components', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_index_components" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_index_components" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_lse_security', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_lse_security CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_lse_security', 'TABLE' )
	THEN
	CREATE TABLE feed_lse_security
	(	id			SERIAL
	,	symbol			VARCHAR(255)	NULL
	,	name			VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_lse_security_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_lse_security','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_lse_security >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_lse_security >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_lse_security ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_lse_security', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_lse_security" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_lse_security" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_nasdaq_security', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_nasdaq_security CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_nasdaq_security', 'TABLE' )
	THEN
	CREATE TABLE feed_nasdaq_security
	(	id			SERIAL
	,	symbol			VARCHAR(255)	NULL
	,	name			VARCHAR(255)	NULL
	,	lastsale		VARCHAR(255)	NULL
	,	marketcap		VARCHAR(255)	NULL
	,	ipoyear			VARCHAR(255)	NULL
	,	sector			VARCHAR(255)	NULL
	,	industry		VARCHAR(255)	NULL
	,	summaryquote		VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_nasdaq_security_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_nasdaq_security','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_nasdaq_security >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_nasdaq_security >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_nasdaq_security ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_nasdaq_security', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_nasdaq_security" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_nasdaq_security" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_nyse_security', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_nyse_security CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_nyse_security', 'TABLE' )
	THEN
	CREATE TABLE feed_nyse_security
	(	id			SERIAL
	,	symbol			VARCHAR(255)	NULL
	,	name			VARCHAR(255)	NULL
	,	lastsale		VARCHAR(255)	NULL
	,	marketcap		VARCHAR(255)	NULL
	,	ipoyear			VARCHAR(255)	NULL
	,	sector			VARCHAR(255)	NULL
	,	industry		VARCHAR(255)	NULL
	,	summaryquote		VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_nyse_security_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_nyse_security','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_nyse_security >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_nyse_security >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_nyse_security ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_nyse_security', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_nyse_security" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_nyse_security" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;

DO $$

BEGIN

	IF OBJECT_EXISTS( 'feed_yahoo_security', 'TABLE' )
	THEN
		DROP TABLE IF EXISTS feed_yahoo_security CASCADE;
	END IF;

	IF NOT OBJECT_EXISTS( 'feed_yahoo_security', 'TABLE' )
	THEN
	CREATE TABLE feed_yahoo_security
	(	id			SERIAL
	,	ticker			VARCHAR(255)	NULL
	,	name			VARCHAR(255)	NULL
	,	exchange		VARCHAR(255)	NULL
	,	category_name		VARCHAR(255)	NULL
	,	category_code		VARCHAR(255)	NULL
-- Standard Columns
/*
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
*/
	,	CONSTRAINT feed_yahoo_security_pk PRIMARY KEY (id)
	);

	IF	OBJECT_EXISTS('feed_yahoo_security','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE feed_yahoo_security >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE feed_yahoo_security >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE feed_yahoo_security ALREADY EXISTS >>';
END IF;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'feed_yahoo_security', 'TABLE' )
	THEN '<<< CREATED TABLE "feed_yahoo_security" >>>'
	ELSE '<<< FAILED TO CREATE TABLE "feed_yahoo_security" >>>'
	END
AS Result ;

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'exchanges', 'TABLE' )
THEN

CREATE TABLE  exchanges
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	symbol			VARCHAR(50)
				NULL
,	currency_id		INTEGER
				NULL
,	country_id		INTEGER
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT exchanges_pk PRIMARY KEY (id)
,	CONSTRAINT exchanges_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('exchanges','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE exchanges >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE exchanges >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE exchanges ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'exchanges', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "exchanges" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "exchanges" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'index_components', 'TABLE' )
THEN

CREATE TABLE  index_components
(	id			SERIAL
,	index_id		INT
				NULL
,	product_id		INT
				NULL
,	sequence		INT		DEFAULT 0
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT index_components_pk PRIMARY KEY (id)
,	CONSTRAINT index_components_uk UNIQUE (product_id, index_id)
);
	IF	OBJECT_EXISTS('index_components','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE index_components >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE index_components >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE index_components ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'index_components', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "index_components" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "index_components" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'instruments', 'TABLE' )
THEN

CREATE TABLE  instruments
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	symbol			VARCHAR(50)
				NULL
,	currency		VARCHAR(3)
				NULL
,	instrument_type_id	INT
				NULL
,	instrument_category_id	INT
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT instruments_pk PRIMARY KEY (id)
,	CONSTRAINT instruments_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('instruments','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE instruments >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE instruments >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE instruments ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'instruments', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "instruments" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "instruments" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'instrument_types', 'TABLE' )
THEN

CREATE TABLE  instrument_types
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT instrument_types_pk PRIMARY KEY (id)
,	CONSTRAINT instrument_types_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('instrument_types','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE instrument_types >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE instrument_types >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE instrument_types ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'instrument_types', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "instrument_types" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "instrument_types" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'products', 'TABLE' )
THEN

CREATE TABLE  products
(	id			SERIAL
,	exchange_id		INT
				NULL
,	instrument_id		INT
				NULL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	symbol			VARCHAR(50)
				NULL
,	currency_id		int
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT products_pk PRIMARY KEY (id)
,	CONSTRAINT products_uk UNIQUE (name)
,	CONSTRAINT products_uk1 UNIQUE (exchange_id,instrument_id)
);
	IF	OBJECT_EXISTS('products','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE products >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE products >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE products ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'products', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "products" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "products" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'social_networks', 'TABLE' )
THEN

CREATE TABLE  social_networks
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	website			VARCHAR(255)
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT social_networks_pk PRIMARY KEY (id)
,	CONSTRAINT social_networks_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('social_networks','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE social_networks >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE social_networks >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE social_networks ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'social_networks', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "social_networks" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "social_networks" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'countries', 'TABLE' )
THEN

CREATE TABLE  countries
(	id			SERIAL
,	name			VARCHAR(255)
				NULL
,	title			VARCHAR(255)
				NULL
,	nickname		VARCHAR(50)
				NULL
,	currency_id		INTEGER
				NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT countries_pk PRIMARY KEY (id)
,	CONSTRAINT countries_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('countries','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE countries >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE countries >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE countries ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'countries', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "countries" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "countries" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'currencies', 'TABLE' )
THEN

CREATE TABLE  currencies
(	id			SERIAL
,	name			VARCHAR(255)	NULL
,	title			VARCHAR(255)	NULL
,	nickname		VARCHAR(50)	NULL
,	minor_unit		int		NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT currencies_pk PRIMARY KEY (id)
,	CONSTRAINT currencies_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('currencies','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE currencies >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE currencies >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE currencies ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'currencies', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "currencies" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "currencies" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'indices', 'TABLE' )
THEN

CREATE TABLE  indices
(	id			SERIAL
,	name			VARCHAR(255)	NULL
,	title			VARCHAR(255)	NULL
,	nickname		VARCHAR(50)	NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT indices_pk PRIMARY KEY (id)
,	CONSTRAINT indices_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('indices','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE indices >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE indices >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE indices ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'indices', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "indices" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "indices" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




DO $$
	
BEGIN

IF NOT OBJECT_EXISTS( 'instrument_categories', 'TABLE' )
THEN

CREATE TABLE  instrument_categories
(	id			SERIAL
,	name			VARCHAR(255)	NULL
,	title			VARCHAR(255)	NULL
-- Standard Columns
,	owner_id		integer		NULL
,	effective_from		timestamp	NULL
,	effective_to		timestamp	NULL
,	version			int		DEFAULT	1
						NULL
,	create_user		varchar(255)	DEFAULT user
						NULL
,	create_date		timestamp	DEFAULT current_timestamp
						NULL
,	update_user		varchar(255)	DEFAULT user
						NULL
,	update_date		timestamp	DEFAULT current_timestamp
						NULL
,	pid			int		null
,	CONSTRAINT instrument_categories_pk PRIMARY KEY (id)
,	CONSTRAINT instrument_categories_uk UNIQUE (name)
);
	IF	OBJECT_EXISTS('instrument_categories','TABLE')
	THEN
		RAISE NOTICE	'<<< CREATED TABLE instrument_categories >>';
	ELSE
		RAISE NOTICE	'<<< FAILED CREATING TABLE instrument_categories >>';
	END IF;
ELSE
	RAISE NOTICE	'<<< TABLE instrument_categories ALREADY EXISTS >>';
END IF;

END

$$;

select	CASE
	WHEN OBJECT_EXISTS( 'instrument_categories', 'TABLE' )
	THEN '<<< CREATED/UPDATED TABLE "instrument_categories" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE TABLE "instrument_categories" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;




drop index if exists instruments_i1 CASCADE;

create unique index instruments_i1 on instruments ( name );



ALTER TABLE logins
ADD	CONSTRAINT logins_user_id_fk FOREIGN KEY (user_id)
	REFERENCES users (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE logins
ADD	CONSTRAINT logins_role_id_fk FOREIGN KEY (role_id)
	REFERENCES roles (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE message_header_rights
ADD	CONSTRAINT message_header_rights_role_id_fk FOREIGN KEY (role_id)
	REFERENCES roles (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE role_rights
ADD	CONSTRAINT role_rights_role_id_fk FOREIGN KEY (role_id)
	REFERENCES roles (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE tree_structures
ADD	CONSTRAINT tree_structures_tree_id_fk FOREIGN KEY (tree_id)
	REFERENCES trees (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE tree_structures
ADD	CONSTRAINT tree_structures_object_id_fk FOREIGN KEY (object_id)
	REFERENCES objects (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE user_roles
ADD	CONSTRAINT user_roles_user_id_fk FOREIGN KEY (user_id)
	REFERENCES users (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE user_roles
ADD	CONSTRAINT user_roles_role_id_fk FOREIGN KEY (role_id)
	REFERENCES roles (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE ADVISERS
ADD	CONSTRAINT advisers_user_id_fk FOREIGN KEY (user_id)
	REFERENCES users (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE ADVISERS
ADD	CONSTRAINT advisers_managed_by_id_fk FOREIGN KEY (managed_by_id)
	REFERENCES advisers (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE CLIENTS
ADD	CONSTRAINT clients_adviser_id_fk FOREIGN KEY (adviser_id)
	REFERENCES advisers (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;


ALTER TABLE contacts
ADD	CONSTRAINT contacts_client_id_fk FOREIGN KEY (client_id)
	REFERENCES clients (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE contact_social_networks
DROP	CONSTRAINT contact_social_networks_social_networks_id_fk
;

ALTER TABLE contact_social_networks
ADD	CONSTRAINT contact_social_networks_contact_id_fk FOREIGN KEY (contact_id)
	REFERENCES contacts (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE contact_social_networks
ADD	CONSTRAINT contact_social_networks_social_network_id_fk FOREIGN KEY (social_network_id)
	REFERENCES social_networks (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE countries
ADD	CONSTRAINT countries_currency_id_fk FOREIGN KEY (currency_id)
	REFERENCES currencies (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE DIARIES
ADD	CONSTRAINT diaries_adviser_id_fk FOREIGN KEY (adviser_id)
	REFERENCES advisers (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE DIARY_ENTRIES
ADD	CONSTRAINT diary_entries_diary_id FOREIGN KEY (diary_id)
	REFERENCES diaries (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE DIARY_ENTRIES
ADD	CONSTRAINT diary_entries_client_id FOREIGN KEY (client_id)
	REFERENCES clients (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE exchanges
ADD	CONSTRAINT exchanges_currency_id_fk FOREIGN KEY (currency_id)
	REFERENCES currencies (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE exchanges
ADD	CONSTRAINT exchanges_country_id_fk FOREIGN KEY (country_id)
	REFERENCES countries (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE historic_prices
ADD	CONSTRAINT historic_prices_product_id_fk FOREIGN KEY (product_id)
	REFERENCES products (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;ALTER TABLE historic_quotes
ADD	CONSTRAINT historic_quotes_product_id_fk FOREIGN KEY (product_id)
	REFERENCES products (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;ALTER TABLE index_components
ADD	CONSTRAINT index_components_product_id_fk FOREIGN KEY (product_id)
	REFERENCES products (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE index_components
ADD	CONSTRAINT index_components_index_id_fk FOREIGN KEY (index_id)
	REFERENCES indices (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE instruments
ADD	CONSTRAINT instruments_instrument_type_id_fk FOREIGN KEY (instrument_type_id)
	REFERENCES instrument_types (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE instruments
ADD	CONSTRAINT instruments_instrument_category_id_fk FOREIGN KEY (instrument_category_id)
	REFERENCES instrument_categories (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE PORTFOLIOS
ADD	CONSTRAINT portfolios_client_id_fk FOREIGN KEY (client_id)
	REFERENCES clients (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE portfolio_positions
ADD	CONSTRAINT portfolio_positions_portfolio_id_fk FOREIGN KEY (portfolio_id)
	REFERENCES portfolios (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE portfolio_positions
ADD	CONSTRAINT portfolio_positions_product_id_fk FOREIGN KEY (product_id)
	REFERENCES products (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE PRODUCTS
ADD	CONSTRAINT products_instrument_id_fk FOREIGN KEY (instrument_id)
	REFERENCES instruments (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE PRODUCTS
ADD	CONSTRAINT products_exchange_id_fk FOREIGN KEY (exchange_id)
	REFERENCES exchanges(id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;


ALTER TABLE PRODUCTS
ADD	CONSTRAINT products_currency_id_fk FOREIGN KEY (currency_id)
	REFERENCES currencies(id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE quotes
ADD	CONSTRAINT quotes_product_id_fk FOREIGN KEY (product_id)
	REFERENCES products (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION
;


DO $$
	
BEGIN

RAISE NOTICE	'<<< REPLACING VIEW available_columns >>';
IF	OBJECT_EXISTS( 'available_columns', 'VIEW' )
THEN
	DROP VIEW IF EXISTS available_columns CASCADE;
	IF	NOT OBJECT_EXISTS('available_columns','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW available_columns >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW available_columns  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	available_columns 
AS
SELECT	so.table_name				"object_name"
,	sc.column_name				"column_name"
,	COALESCE( c.title, sc.column_name )	"column_title"
,	sc.ordinal_position			"column_id"
,	sc.udt_name				"column_type"
,	sc.data_type				"column_usertype"
FROM	information_schema.tables	so
JOIN	information_schema.columns	sc
ON	sc.table_catalog		=	so.table_catalog
AND	sc.table_schema			=	so.table_schema
AND	sc.table_name			=	so.table_name
LEFT OUTER JOIN
	COLUMNS				c
ON	c.name				=	sc.column_name
WHERE	so.table_catalog		=	CURRENT_DATABASE()
AND	so.table_schema			=	CURRENT_SCHEMA()
AND	so.table_type	IN	( 'BASE TABLE', 'VIEW' )
--AND	sc.name		NOT IN	( 'id', 'version', 'update_date', 'update_user', 'pid', 'state', 'target_state')

;

select	CASE
	WHEN OBJECT_EXISTS( 'available_columns', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "available_columns" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "available_columns" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'available_tags', 'VIEW' )
THEN
	DROP VIEW available_tags CASCADE;
	IF	NOT OBJECT_EXISTS('available_tags','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW available_tags >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW available_tags  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	available_tags 
AS
SELECT	mh.message_type			"message_type"
,	m.tag				"tag"
,	COALESCE(t.title,m.tag)		"tag_title"
FROM	message_headers			mh
JOIN	messages			m
ON	m.message_header_id		=	mh.id
LEFT OUTER JOIN
	tags				t
ON	t.tag				=	m.tag
UNION
SELECT	mr.message_type			"message_type"
,	mr.tag				"tag"
,	COALESCE(t.title,mr.tag)	"tag_title"
FROM	message_rights			mr
LEFT OUTER JOIN
	tags				t
ON	t.tag				=	mr.tag

;


select	CASE
	WHEN OBJECT_EXISTS( 'available_tags', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "available_tags" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "available_tags" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree', 'VIEW' )
THEN
	DROP VIEW tree CASCADE;
	IF	NOT OBJECT_EXISTS('tree','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree  >>';
	END IF;

END IF;

END

$$;


CREATE OR REPLACE VIEW 
	tree
AS
SELECT	tree_id			"tree_id"
,	object_id		"object_id"
,	MAX(object_name)	"object_name"
,	MIN(nbr)		"lft"
,	MAX(nbr)		"rgt"
FROM	tree_structures
GROUP BY
	tree_id
,	object_id
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_root', 'VIEW' )
THEN
	DROP VIEW tree_root CASCADE;
	IF	NOT OBJECT_EXISTS('tree_root','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_root >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_root  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_root ( tree_id, object_id , object_name )
AS
SELECT	ts.tree_id
,	ts.object_id
,	ts.object_name
FROM	tree_structures		ts
WHERE	ts.nbr			=	(
					SELECT	MIN(nbr)
					FROM	tree_structures		ts1
					WHERE	ts1.tree_id		=	ts.tree_id
					)
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_root', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_root" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_root" >>>'
	END
AS "Result" ;			


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'authorised_columns', 'VIEW' )
THEN
	DROP VIEW authorised_columns CASCADE;
	IF	NOT OBJECT_EXISTS('authorised_columns','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW authorised_columns >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW authorised_columns  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	authorised_columns 
AS
SELECT	DISTINCT
	u.id			"user_id"
,	avail.object_name	"object_name"
,	avail.column_name	"column_name"
,	avail.column_title	"column_title"
,	avail.column_id		"column_id"
,	avail.column_type	"column_type"
,	avail.column_usertype	"column_usertype"
FROM	available_columns	avail
JOIN	users			u
ON	u.id			=	u.id
JOIN	user_roles		ur
ON	ur.user_id		=	u.id
JOIN	roles			r
ON	r.id			=	ur.role_id
WHERE	0			<	(
					SELECT	COALESCE(MAX( 1 & access_rights ), 0)
					FROM	role_rights
					WHERE	role_id			=	r.id
					AND	object_type		=	'dbobject'
					AND	object_name		=	avail.object_name
					and	object_subname		=	avail.column_name
					)
UNION
SELECT	( SELECT id FROM users WHERE name = 'admin' )	"user_id"
,	avail.object_name		"object_name"
,	avail.column_name		"column_name"
,	avail.column_title		"column_title"
,	avail.column_id			"column_id"
,	avail.column_type		"column_type"
,	avail.column_usertype		"column_usertype"
FROM	available_columns		avail

;

select	CASE
	WHEN OBJECT_EXISTS( 'authorised_columns', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "authorised_columns" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "authorised_columns" >>>'
	END
AS "Result" ;			



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'authorised_tags', 'VIEW' )
THEN
	DROP VIEW authorised_tags CASCADE;
	IF	NOT OBJECT_EXISTS('authorised_tags','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW authorised_tags >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW authorised_tags  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	authorised_tags 
AS
SELECT	DISTINCT
	u.id					"user_id"
,	avail.message_type			"message_type"
,	avail.tag				"tag"
,	COALESCE(avail.tag_title, avail.tag )	"tag_title"
FROM	available_tags				avail
JOIN	users					u
ON	u.id					=	u.id
JOIN	user_roles				ur
ON	ur.user_id				=	u.id
JOIN	roles					r
ON	r.id					=	ur.role_id
where	0					<	(
							SELECT	COALESCE(MAX( 1 & access_rights ), 0)
							FROM	role_rights
							WHERE	role_id			=	r.id
							AND	object_type		=	'message'
							AND	object_name		=	avail.message_type
							and	object_subname		=	avail.tag
							)
UNION
SELECT	( SELECT id FROM users WHERE name = 'admin' )	"user_id"
,	avail.message_type				"message_type"
,	avail.tag					"tag"
,	avail.tag_title					"tag_title"
FROM	available_tags		avail

;


select	CASE
	WHEN OBJECT_EXISTS( 'authorised_tags', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "authorised_tags" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "authorised_tags" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_depth', 'VIEW' )
THEN
	DROP VIEW tree_depth CASCADE;
	IF	NOT OBJECT_EXISTS('tree_depth','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_depth >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_depth  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_depth 
AS
SELECT  child.tree_id					"tree_id"
,	child.object_id					"object_id"
,	MAX(child.object_name)				"object_name"
,       COUNT(parent.object_id)				"depth"
,	MIN(child.lft)					"lft"
,	MAX(child.rgt)					"rgt"
FROM    tree		parent
,       tree		child
where	child.tree_id	=	parent.tree_id
and	( child.lft	>=	parent.lft
and	  child.lft	<=	parent.rgt
	)
GROUP BY child.tree_id, child.object_id
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_depth', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_depth" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_depth" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_leaves', 'VIEW' )
THEN
	DROP VIEW tree_leaves CASCADE;
	IF	NOT OBJECT_EXISTS('tree_leaves','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_leaves >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_leaves  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_leaves
AS
SELECT	tree_id
,	object_id
,	object_name
,	lft
,	rgt
FROM	tree
WHERE	lft	=	rgt - 1
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_leaves', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_leaves" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_leaves" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_ancestors', 'VIEW' )
THEN
	DROP VIEW tree_ancestors CASCADE;
	IF	NOT OBJECT_EXISTS('tree_ancestors','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_ancestors >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_ancestors  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_ancestors ( tree_id, object_id, object_name, parent_object_id, parent_object_name, lft, rgt )
AS
SELECT	child.tree_id				"tree_id"
,	child.object_id				"object_id"
,	child.object_name			"object_name"
,	parent.object_id			"parent_object_id"
,	parent.object_name			"parent_object_name"
,	parent.lft				"lft"
,	parent.rgt				"rgt"
FROM	tree_depth	parent
,	tree_depth	child
WHERE	parent.tree_id	=	child.tree_id
AND	( parent.lft	<=	child.lft
AND	  parent.rgt	>=	child.rgt
	)
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_ancestors', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_ancestors" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_ancestors" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_children', 'VIEW' )
THEN
	DROP VIEW tree_children CASCADE;
	IF	NOT OBJECT_EXISTS('tree_children','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_children >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_children  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_children ( tree_id, object_id, object_name, sub_object_id, sub_object_name, lft, rgt )
AS
SELECT	parent.tree_id					"tree_id"
,	parent.object_id				"object_id"
,	parent.object_name				"object_name"
,	child.object_id					"child_object_id"
,	child.object_name				"child_object_name"
,	child.lft					"lft"
,	child.rgt					"rgt"
FROM	tree_depth parent
,	tree_depth child
WHERE	child.tree_id	=	parent.tree_id
AND	( child.lft	>=	parent.lft
AND	  child.rgt	<=	parent.rgt
	)
AND	child.lft <> parent.lft  	-- Don't include it's self
AND	NOT EXISTS(
	SELECT	NULL
	FROM	tree		mid
	WHERE	( mid.tree_id	=	parent.tree_id
	AND	  mid.lft	>=	parent.lft
	AND	  mid.lft	<=	parent.rgt
		)
	AND	( child.tree_id	=	mid.tree_id
	AND	  child.lft	>=	mid.lft
	AND	  child.lft	<=	mid.rgt
		)
	AND	mid.object_id NOT IN ( child.object_id, parent.object_id )
	)
;


select	CASE
	WHEN OBJECT_EXISTS( 'tree_children', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_children" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_children" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_descendants', 'VIEW' )
THEN
	DROP VIEW tree_descendants CASCADE;
	IF	NOT OBJECT_EXISTS('tree_descendants','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_descendants >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_descendants  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW
	tree_decendants
AS
SELECT	child.tree_id					"tree_id"
,	parent.object_id				"object_id"
,	parent.object_name				"object_name"
,	child.object_id					"child_object_id"
,	child.object_name				"child_object_name"
,	child.lft					"lft"
,	child.rgt					"rgt"
FROM	tree_depth	parent
JOIN	tree_depth	child
ON	child.tree_id	=	parent.tree_id
WHERE	( child.lft	>=	parent.lft
AND	  child.rgt	<=	parent.rgt
	)
;


select	CASE
	WHEN OBJECT_EXISTS( 'tree_descendants', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_descendants" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_descendants" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_parents', 'VIEW' )
THEN
	DROP VIEW tree_parents CASCADE;
	IF	NOT OBJECT_EXISTS('tree_parents','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_parents >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_parents  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_parents ( tree_id, object_id, object_name, parent_object_id, parent_object_name, lft, rgt )
AS
SELECT	child.tree_id					"tree_id"
,	child.object_id					"object_id"
,	child.object_name				"object_name"
,	parent.object_id				"parent_object_id"
,	parent.object_name				"parent_object_name"
,	parent.lft					"lft"
,	parent.rgt					"rgt"
FROM	tree_depth	parent
,	tree_depth	child
WHERE	parent.tree_id	=	child.tree_id
AND	( parent.lft	<=	child.lft
AND	  parent.rgt	>=	child.rgt
	)
AND	parent.lft	<>	child.lft  	-- Don't include it's self
AND	NOT EXISTS(
	SELECT	NULL
	FROM	tree		mid
	WHERE	mid.tree_id	>=	parent.tree_id
	AND	( mid.lft	>=	parent.lft
	AND	  mid.lft	<=	parent.rgt
		)
	AND	( child.lft	>=	mid.lft
	AND	  child.lft	<=	mid.rgt
		)
	AND	mid.object_id NOT IN ( child.object_id, parent.object_id )
	)
;


select	CASE
	WHEN OBJECT_EXISTS( 'tree_parents', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_parents" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_parents" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'user_authorised_columns', 'VIEW' )
THEN
	DROP VIEW user_authorised_columns CASCADE;
	IF	NOT OBJECT_EXISTS('user_authorised_columns','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW user_authorised_columns >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW user_authorised_columns  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	user_authorised_columns 
AS
SELECT	u.id						"user_id"
,	uc.object_name					"object_name"
,	uc.column_name					"column_name"
,	COALESCE(uc.column_title, auth.column_title)	"column_title"
,	auth.column_id					"column_id"
,	auth.column_type				"column_type"
,	auth.column_usertype				"coulumn_usertype"
FROM	users		u
JOIN	user_columns	uc
ON	uc.user_id	=	u.id
JOIN	authorised_columns	auth
ON	auth.user_id	=	u.id
UNION
SELECT	u.id
,	auth.object_name	"object_name"
,	auth.column_name	"column_name"
,	auth.column_name	"column_title"
,	auth.column_id		"column_id"
,	auth.column_type	"column_type"
,	auth.column_usertype	"coulumn_usertype"
FROM	users		u
JOIN	authorised_columns	auth
ON	auth.user_id	=	u.id
WHERE	NOT EXISTS	(
			SELECT	NULL
			FROM	user_columns
			WHERE	user_id		=	u.id
			AND	object_name	=	auth.object_name
			)

;


select	CASE
	WHEN OBJECT_EXISTS( 'user_authorised_columns', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "user_authorised_columns" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "user_authorised_columns" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'user_authorised_tags', 'VIEW' )
THEN
	DROP VIEW user_authorised_tags CASCADE;
	IF	NOT OBJECT_EXISTS('user_authorised_tags','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW user_authorised_tags >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW user_authorised_tags  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	user_authorised_tags 
AS
SELECT	u.id						"user_id"
,	ut.message_type					"message_type"
,	ut.tag						"tag"
,	COALESCE( ut.tag_title, auth.tag_title)		"tag_title"
FROM	users			u
JOIN	user_tags		ut
ON	ut.user_id		=	u.id
JOIN	authorised_tags		auth
ON	auth.user_id		=	u.id
AND	auth.message_type	=	ut.message_type
AND	auth.tag		=	ut.tag
UNION
SELECT	u.id
,	auth.message_type
,	auth.tag
,	auth.tag_title
FROM	users			u
JOIN	authorised_tags		auth
ON	auth.user_id		=	u.id
WHERE	NOT EXISTS		(
				SELECT	NULL
				FROM	user_tags
				WHERE	user_id		=	u.id
				AND	message_type	=	auth.message_type
				)

;


select	CASE
	WHEN OBJECT_EXISTS( 'user_authorised_tags', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "user_authorised_tags" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "user_authorised_tags" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'authorisation', 'VIEW' )
THEN
	DROP VIEW authorisation CASCADE;
	IF	NOT OBJECT_EXISTS('authorisation','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW authorisation >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW authorisation  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	authorisation
AS
SELECT	DISTINCT
	u.id			"user_id"
,	u.name			"user_name"
,	r.id			"role_id"
,	r.name			"role_name"
FROM	roles			r
JOIN	logins			l
JOIN	users			u
ON	u.id			=	COALESCE(l.user_id, GET_USER_ID(l.username))
JOIN	user_roles		ur
ON	ur.user_id		=	u.id
JOIN	roles			urr
ON	urr.id			=	ur.role_id
AND	urr.precedence		>=	r.precedence
WHERE	u.is_suspended		=	0
AND	( l.last_login		>=	current_time - interval '15 minute'
OR	  l.one_off		>=	current_time - interval '5 minute'
	)
;


select	CASE
	WHEN OBJECT_EXISTS( 'authorisation', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "authorisation" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "authorisation" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'cross_reference_mappings', 'VIEW' )
THEN
	DROP VIEW cross_reference_mappings CASCADE;
	IF	NOT OBJECT_EXISTS('cross_reference_mappings','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW cross_reference_mappings >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW cross_reference_mappings  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	cross_reference_mappings
AS
SELECT	DISTINCT
	cr1.object_type			"user_id"
,	cr1.object_id			"user_name"
,	cr1.xref_type			"from_type"
,	cr1.xref_value			"from_value"
,	cr2.xref_type			"to_type"
,	cr2.xref_value			"to_value"
FROM	cross_references	cr1
JOIN	cross_references	cr2
ON	cr2.object_type		=	cr1.object_type
AND	cr2.object_id		=	cr1.object_id
AND	cr2.xref_type		!=	cr1.xref_type

;

select	CASE
	WHEN OBJECT_EXISTS( 'cross_reference_mappings', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "cross_reference_mappings" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "cross_reference_mappings" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'lookup_types', 'VIEW' )
THEN
	DROP VIEW lookup_types CASCADE;
	IF	NOT OBJECT_EXISTS('lookup_types','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW lookup_types >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW lookup_types  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	lookup_types 
AS
SELECT	type			"type"
,	code			"code"
FROM	lookups
UNION
SELECT	type
,	code
FROM	lookup_codes

;

select	CASE
	WHEN OBJECT_EXISTS( 'lookup_types', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "lookup_types" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "lookup_types" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'relationships', 'VIEW' )
THEN
	DROP VIEW relationships CASCADE;
	IF	NOT OBJECT_EXISTS('relationships','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW relationships >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW relationships  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	relationships
AS
	select	DISTINCT
		st.table_catalog			AS	"table_catalog"
	,	st.table_schema				AS	"table_schema"
	,	st.table_name				AS	"table_name"
	,	st.table_type				AS	"table_type"
	,	c.column_name				AS	"column_name"
	,	kcu.constraint_name			AS	"key_name"
	,	ccu.table_catalog			AS	"related_table_catalog"
	,	ccu.table_schema			AS	"related_table_schema"
	,	ccu.table_name				AS	"related_table_name"
	,	ccu.column_name				AS	"related_column_name"
	,	ccu.constraint_name			AS	"related_key_name"
	,	parse_constraint_name( st.table_name, kcu.constraint_name )
							AS	"relationship_name"
	FROM	information_schema.tables			st
	JOIN
		information_schema.columns			c
	ON	c.table_catalog					=	st.table_catalog
	AND	c.table_schema					=	st.table_schema
	AND	c.table_name					=	st.table_name
--	AND	c.column_name					=	c.column_name
	JOIN
		information_schema.key_column_usage		kcu
	ON	kcu.table_catalog				=	c.table_catalog
	AND	kcu.table_schema				=	c.table_schema
	AND	kcu.table_name					=	c.table_name
	AND	kcu.column_name					=	c.column_name
	JOIN
		information_schema.table_constraints		tc
	ON	tc.constraint_catalog				=	kcu.constraint_catalog
	AND	tc.constraint_schema				=	kcu.constraint_schema
	AND	tc.constraint_name				=	kcu.constraint_name
	AND	tc.constraint_type				=	'FOREIGN KEY'
	JOIN
		information_schema.referential_constraints	rc
	ON	rc.constraint_catalog				=	tc.constraint_catalog
	AND	rc.constraint_schema				=	tc.constraint_schema
	AND	rc.constraint_name				=	tc.constraint_name

	LEFT OUTER JOIN
		information_schema.constraint_column_usage		ccu
	ON	ccu.constraint_catalog				=	rc.unique_constraint_catalog
	AND	ccu.constraint_schema				=	rc.unique_constraint_schema
	AND	ccu.constraint_name				=	rc.unique_constraint_name
--	AND	ccu.column_name					=	c.column_name
	WHERE	ccu.table_name					IS NOT NULL
--	AND	st.table_type					in	( 'BASE TABLE', 'VIEW' )
	AND	st.table_type					in	( 'BASE TABLE' )
	;

select	CASE
	WHEN OBJECT_EXISTS( 'relationships', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "relationships" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "relationships" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'user_rights', 'VIEW' )
THEN
	DROP VIEW user_rights CASCADE;
	IF	NOT OBJECT_EXISTS('user_rights','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW user_rights >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW user_rights  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	user_rights
AS
SELECT	DISTINCT
	u.id			"user_id"
,	u.name			"user_name"
,	r.id			"role_id"
,	r.name			"role_name"
FROM	logins			l
JOIN	users			u
ON	u.id			=	COALESCE(l.user_id, GET_USERS_ID(l.username))
JOIN	user_roles		ur
ON	ur.user_id		=	u.id
JOIN	roles			r
ON	r.id		=	ur.role_id
/*
-- SqlServer
WHERE	( l.last_login		>=	DATE_SUB( NOW(), INTERVAL 15 MINUTE )
OR	  l.one_off		>=	DATE_SUB( NOW(), INTERVAL 5 MINUTE )
	)
-- Sybase
WHERE	( l.last_login		>=	DATEADD( mi , -15, GETDATE() )
OR	  l.one_off		>=	DATEADD( mi , -5, GETDATE() )
	)
*/
-- Postgresql
WHERE	( l.last_login		>=	current_timestamp - interval '15 minute'
OR	  l.one_off		>=	current_timestamp - interval '5 minute'
	)
;

select	CASE
	WHEN OBJECT_EXISTS( 'user_rights', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "user_rights" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "user_rights" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DROP FUNCTION IF EXISTS generate_flask_model( varchar);

CREATE OR REPLACE FUNCTION generate_flask_model
(       p_table_name          VARCHAR
)
RETURNS	TABLE
(	script_line	varchar
)
AS
$$

DECLARE

r		RECORD;
t		RECORD;
columns		varchar;
list		varchar;
prefix		varchar;
py_tab		varchar(4);
py_quote	varchar(1);

v_database_name	varchar;
v_schema_name	varchar;
v_object_name	varchar;
v_table_name	varchar;
v_table_title	varchar;

num_rows	int;
column_count	int;

v_date_format	varchar;

BEGIN
	py_tab		=	'    ';
	py_quote	=	'''';
	v_date_format	=	'\%Y-\%m-\%d';

	select	database_name
	,	schema_name
	,	object_name
	into	v_database_name
	,	v_schema_name
	,	v_object_name
	from	parse_object_name( p_table_name )
	;

-- raise NOTICE ' P_TABLE_NAME=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_OBJECT_NAME=%', P_TABLE_NAME, V_DATABASE_NAME, V_SCHEMA_NAME, V_object_NAME;

	drop  table if exists table_list
	;
		
	create	temporary table table_list
	(	id			serial
	,	table_catalog		varchar
	,	table_schema		varchar
	,	table_name		varchar
	)
	;

	insert	into
		table_list
	(	table_catalog
	,	table_schema
	,	table_name
	)
	select	distinct
		st.table_catalog
	,	st.table_schema
	,	st.table_name
	FROM	information_schema.tables	st
	WHERE	st.table_name			not like 'ab_%'
	AND	st.table_name			not like 'feed_%'
	AND	st.table_name			not like 'message%'
	AND	st.table_name			not like 'tree%'
	AND	st.table_name			not like '%tag%'
	AND	st.table_catalog		=	V_DATABASE_NAME
	AND	st.table_schema			=	V_SCHEMA_NAME
--	AND	st.table_type			in	( 'BASE TABLE', 'VIEW' )
	AND	st.table_type			in	( 'BASE TABLE' )
	AND	st.table_name			like	V_OBJECT_NAME
	;


	drop  table if exists relations
	;

	create	temporary table relations
	(	id			serial
	,	table_name		varchar
	,	related_table_name	varchar
	,	relationship_name	varchar
	)
	;

	insert	into
		relations
	(	table_name
	,	related_table_name
	,	relationship_name
	)
	select	DISTINCT
		st.table_name				AS	"table_name"
	,	rel.related_table_name			AS	"related_table_name"
	,	rel.relationship_name			AS	"relationship_name"
	FROM	table_list				st
	JOIN	relationships				rel
	ON	rel.table_catalog			=	st.table_catalog
	AND	rel.table_schema			=	st.table_schema
	AND	rel.table_name				=	st.table_name
	AND	EXISTS
	(
		SELECT	NULL
		FROM	table_list			rtl
		WHERE	rtl.table_catalog		=	rel.related_table_catalog
		AND	rtl.table_schema		=	rel.related_table_schema
		AND	rtl.table_name			=	rel.related_table_name
	)
	;


	drop table if exists table_order
	;
		
	create	temporary table table_order
		(	id			serial
		,	table_name		varchar
		)
	;

-- Add the tables with no child relationships first

	insert	into
		table_order
	(	table_name )
	select	distinct
		st.table_name
	FROM	table_list			st
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	relations		rel
		WHERE	rel.table_name		=	st.table_name
	)
	;

-- then add the others chunk at a time
--
-- TODO: Had a problem with two tables with a 2-way relationship ( not M-M ) , have to consider this.
--	Got rid of one of the relationships for now.
--

	LOOP
		insert	into
		table_order
		(	table_name )
		select	distinct
			r1.table_name
			-- select *
		from	relations		r1
		WHERE	not exists
		(	select	null
			from	table_order	t1
			where	t1.table_name	= r1.table_name
		)
		and	not exists
		(
			select	null
			from	relations 		r2
			where	r2.table_name		= r1.table_name
			AND	r2.table_name		!= r2.related_table_name
			and	not exists
			(
				select	null
				from	table_order	t2
				where	t2.table_name	= r2.related_table_name
			)
		)
		;

		GET DIAGNOSTICS num_rows = ROW_COUNT;

		exit when num_rows < 1;

	END LOOP;

-- Start the script

	if	object_exists ( 'script', 'TABLE' )
	then
		drop table if exists script;
	end if;

	create	local temp table script
	(	id	serial
	,	line	varchar
	)
	;

	insert into script (line) values ( format( 'import datetime' ));
	insert into script (line) values ( format( '#from sqlalchemy import Table, Column, Integer, String, ForeignKey, Date, Text' ));
	insert into script (line) values ( format( 'from sqlalchemy import BigInteger, Boolean, Column, Date, DateTime, ForeignKey, Index, Integer, Numeric, String, Table, Text, text' ));
	insert into script (line) values ( format( 'from sqlalchemy.orm import relationship' ));
	insert into script (line) values ( format( 'from flask.ext.appbuilder import Model' ));
	insert into script (line) values ( format( 'from flask.ext.appbuilder.models.mixins import AuditMixin, FileColumn, ImageColumn' ));
	insert into script (line) values ( format( '' ));
	insert into script (line) values ( format( '# this seems to be used for views support' ));
	insert into script (line) values ( format( 'from sqlalchemy.ext.declarative import declarative_base' ));
	insert into script (line) values ( format( 'Base = declarative_base()' ));
	insert into script (line) values ( format( 'metadata = Base.metadata' ));
	insert into script (line) values ( format( '' ));

	insert into script (line) values ( format( 'def today():' ));
	insert into script (line) values ( format( '    return datetime.datetime.today().strftime(''%s'')', v_date_format ));
	insert into script (line) values ( format( '' ));


	FOR	t in
		SELECT	o.table_name			AS	"table_name"
		,	FORMAT_TITLE_CASE(o.table_name)	AS	"table_title"
		FROM	table_order			o
		order by
			id
	LOOP

	v_table_name	=	t.table_name;
	v_table_title	=	t.table_title;


	insert into script (line) values ( format( 'class %s(Model):', v_table_title ));
	insert into script (line) values ( format( '    __tablename__ = ''%s''', v_table_name ));
	insert into script (line) values ( format( '' ));

	FOR 	r	IN 
		SELECT	FORMAT_FLASK_COLUMN(c.table_catalog,c.table_schema,c.table_name,c.column_name)	AS	"flask_column"
		FROM	information_schema.columns	c
		WHERE	c.TABLE_CATALOG			= v_database_name
		AND	c.TABLE_SCHEMA			= v_schema_name
		AND	c.TABLE_NAME			= v_table_name
		AND	c.column_name not in ( 'owner_id' , 'version', 'create_user', 'create_date', 'update_user', 'update_date', 'pid' )
		ORDER BY c.ORDINAL_POSITION
	LOOP

	insert into script (line) values ( format( '    %s ', r.flask_column ));

	END LOOP;

	insert into script (line) values ( format( '' ));
--
--	TODO: Change this to handle multiple column FKs
--		CLUE: use the FK SQL in FORMAT_FLASK_COLUMN()
--

	FOR	r	IN 
		SELECT	rel.relationship_name				AS	"relationship_name"
		,	FORMAT_TITLE_CASE(rel.related_table_name)	AS	"related_table_title"
		FROM	relations					rel
		WHERE	rel.table_name					=	v_table_name
		AND	COALESCE(rel.relationship_name,'')		!=	''
	LOOP

	insert into script (line) values ( format( '    %s = relationship("%s") ', r.relationship_name, r.related_table_title ));

	END LOOP;

	insert into script (line) values ( format( '' ));

	if	exists
	(
		SELECT	NULL
		FROM	information_schema.columns	c
		WHERE	c.TABLE_CATALOG			= v_database_name
		AND	c.TABLE_SCHEMA			= v_schema_name
		AND	c.TABLE_NAME			= v_table_name
		AND	c.COLUMN_NAME			= 'name'
	)
	THEN

	insert into script (line) values ( format( '    def __repr__(self):' ));
	insert into script (line) values ( format( '        return self.name' ));
	insert into script (line) values ( format( '' ));

	END IF;

	END LOOP;

	insert into script (line) values ( format( '' ));

	drop table if exists table_list;
	drop table if exists relations;
	drop table if exists table_order;
		
	return	query
	select	line
	from	script
	order by
		id
	;

END
$$
LANGUAGE        PLPGSQL;

select  CASE
        WHEN OBJECT_EXISTS( 'generate_flask_model', 'ROUTINE' )
        THEN '<<< CREATED ROUTINE "generate_flask_model" >>>'
        ELSE '<<< FAILED TO CREATE ROUTINE "generate_flask_model" >>>'
        END
AS Result ;

DROP FUNCTION IF EXISTS generate_flask_view( varchar);

CREATE OR REPLACE FUNCTION generate_flask_view
(       p_table_name          VARCHAR
)
RETURNS	TABLE
(	script_line	varchar
)
AS
$$

DECLARE

r		RECORD;
t		RECORD;
columns		varchar;
list		varchar;
prefix		varchar;
py_tab		varchar(4);
py_quote	varchar(1);

v_database_name	varchar;
v_schema_name	varchar;
v_object_name	varchar;
v_table_name	varchar;
v_table_title	varchar;

num_rows	int;
column_count	int;

BEGIN
	py_tab		=	'    ';
	py_quote	=	'''';

	select	database_name
	,	schema_name
	,	object_name
	into	v_database_name
	,	v_schema_name
	,	v_object_name
	from	parse_object_name( p_table_name )
	;

-- raise NOTICE ' P_TABLE_NAME=%, V_DATABASE_NAME=%, V_SCHEMA_NAME=%, V_OBJECT_NAME=%', P_TABLE_NAME, V_DATABASE_NAME, V_SCHEMA_NAME, V_object_NAME;

	drop  table if exists table_list
	;
		
	create	temporary table table_list
	(	id			serial
	,	table_catalog		varchar
	,	table_schema		varchar
	,	table_name		varchar
	)
	;

	insert	into
		table_list
	(	table_catalog
	,	table_schema
	,	table_name
	)
	select	distinct
		st.table_catalog
	,	st.table_schema
	,	st.table_name
	FROM	information_schema.tables	st
	WHERE	st.table_name			not like 'ab_%'
	AND	st.table_name			not like 'feed_%'
	AND	st.table_name			not like 'message%'
	AND	st.table_name			not like 'tree%'
	AND	st.table_name			not like '%tag%'
	AND	st.table_catalog		=	V_DATABASE_NAME
	AND	st.table_schema			=	V_SCHEMA_NAME
--	AND	st.table_type			in	( 'BASE TABLE', 'VIEW' )
	AND	st.table_type			in	( 'BASE TABLE' )
	AND	st.table_name			like	V_OBJECT_NAME
	;


	drop  table if exists relations
	;
		
	create	temporary table relations
	(	id			serial
	,	table_name		varchar
	,	related_table_name	varchar
	,	relationship_name	varchar
	)
	;

	insert	into
		relations
	(	table_name
	,	related_table_name
	,	relationship_name
	)
	select	DISTINCT
		st.table_name				AS	"table_name"
	,	rel.related_table_name			AS	"related_table_name"
	,	rel.relationship_name			AS	"relationship_name"
	FROM	table_list				st
	JOIN	relationships				rel
	ON	rel.table_catalog			=	st.table_catalog
	AND	rel.table_schema			=	st.table_schema
	AND	rel.table_name				=	st.table_name
	AND	EXISTS
	(
		SELECT	NULL
		FROM	table_list			rtl
		WHERE	rtl.table_catalog		=	rel.related_table_catalog
		AND	rtl.table_schema		=	rel.related_table_schema
		AND	rtl.table_name			=	rel.related_table_name
	)
	;

	drop table if exists table_order
	;
		
	create	temporary table table_order
		(	id			serial
		,	table_name		varchar
		)
	;

-- Add the tables with no child relationships first

	insert	into
		table_order
	(	table_name )
	select	distinct
		st.table_name
	FROM	table_list			st
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	relations		rel
		WHERE	rel.table_name		=	st.table_name
	)
	;

-- then add the others chunk at a time
--
-- TODO: Had a problem with two tables with a 2-way relationship ( not M-M ) , have to consider this.
--	Got rid of one of the relationships for now.
--

	LOOP
		insert	into
		table_order
		(	table_name )
		select	distinct
			r1.table_name
			-- select *
		from	relations		r1
		WHERE	r1.related_table_name	IS NOT NULL
		AND	not exists
		(	select	null
			from	table_order	t1
			where	t1.table_name	= r1.table_name
		)
		and	not exists
		(
			select	null
			from	relations 		r2
			where	r2.table_name		= r1.table_name
			AND	r2.table_name		!= r2.related_table_name
			and	not exists
			(
				select	null
				from	table_order	t2
				where	t2.table_name	= r2.related_table_name
			)
		)
		;

		GET DIAGNOSTICS num_rows = ROW_COUNT;

		exit when num_rows < 1;

	END LOOP;

-- Start the script

	if	object_exists ( 'script', 'TABLE' )
	then
		drop table if exists script;
	end if;

	create	local temp table script
	(	id	serial
	,	line	varchar
	)
	;

	insert into script (line) values ( format( 'from flask_appbuilder import ModelView' ));
	insert into script (line) values ( format( 'from flask_appbuilder.fieldwidgets import Select2Widget' ));
	insert into script (line) values ( format( 'from flask.ext.appbuilder.models.sqla.interface import SQLAInterface' ));
	insert into script (line) values ( format( '#from flask.ext.appbuilder.models.datamodel import SQLAModel' ));
	insert into script (line) values ( format( 'from wtforms.ext.sqlalchemy.fields import QuerySelectField' ));
	insert into script (line) values ( format( 'from app import appbuilder, db' ));
	insert into script (line) values ( format( '' ));
	insert into script (line) values ( format( 'from flask.ext.appbuilder.views import CompactCRUDMixin' ));
	insert into script (line) values ( format( '' ));
	insert into script (line) values ( format( 'from .models import *' ));
	insert into script (line) values ( format( '' ));

	FOR	t in
		SELECT	o.table_name			AS	"table_name"
		,	FORMAT_TITLE_CASE(o.table_name)	AS	"table_title"
		FROM	table_order			o
		order by
			id
	LOOP

	v_table_name	=	t.table_name;
	v_table_title	=	t.table_title;

	select	count(*)
	INTO	column_count
	from	information_schema.columns	c
	where	c.table_catalog		= V_DATABASE_NAME
	AND	c.table_schema		= V_SCHEMA_NAME
	AND	c.table_name		= v_table_name
	;

--	IF	( exists ( select null from relations where related_table_name = v_table_name ) )
--	THEN
--		insert into script (line) values ( format( 'class %sView(CompactCRUDMixin, ModelView):', v_table_title ));
--	ELSE
--		insert into script (line) values ( format( 'class %sView(ModelView):', v_table_title ));
--	END IF;

	insert into script (line) values ( format( 'class %sView(ModelView):', v_table_title ));
	insert into script (line) values ( format( '    datamodel = SQLAInterface(%s)', v_table_title ));
	insert into script (line) values ( format( '' ));


	list =	'';
	prefix	=	'[ ';

-- put the foreign keys at the top

	FOR	r	IN 
		SELECT	rel.relationship_name		AS	"column_name"
		FROM	information_schema.columns	c
		JOIN
			relationships			rel
		ON	rel.table_catalog		=	c.table_catalog
		AND	rel.table_schema		=	c.table_schema
		AND	rel.table_name			=	c.table_name
		AND	rel.column_name			=	c.column_name
		WHERE	c.table_catalog			=	v_database_name
		AND	c.table_schema			=	v_schema_name
		AND	c.table_name			=	v_table_name
		AND	c.column_name			not in ( 'id', 'owner_id', 'version', 'create_user', 'create_date', 'update_user', 'update_date', 'pid' )
		ORDER BY ORDINAL_POSITION
	LOOP
		list	=	list || prefix || py_quote || r.column_name || py_quote;
		prefix  =	', ';
	END LOOP;

-- now the rest of the columns

	FOR	r	IN 
		SELECT	c.column_name			AS	"column_name"
		FROM	information_schema.columns	c
		WHERE	c.table_catalog			= v_database_name
		AND	c.table_schema			= v_schema_name
		AND	c.table_name			= v_table_name
		AND	c.column_name			not in ( 'id', 'owner_id', 'version', 'create_user', 'create_date', 'update_user', 'update_date', 'pid' )
		AND	NOT EXISTS
		(
			SELECT	NULL
			FROM	relationships		rel
			WHERE	rel.table_catalog	=	c.table_catalog
			AND	rel.table_schema	=	c.table_schema
			AND	rel.table_name		=	c.table_name
			AND	rel.column_name		=	c.column_name
		)
		ORDER BY ORDINAL_POSITION
	LOOP
		list	=	list || prefix || py_quote || r.column_name || py_quote;
		prefix  =	', ';
	END LOOP;

	IF	( list != '' )
	THEN
		list	=	list || ' ];';
	insert into script (line) values ( format( '    list_columns = %s ', list ));
	insert into script (line) values ( format( '    show_columns = %s ', list ));
	insert into script (line) values ( format( '    edit_columns = %s ', list ));
	insert into script (line) values ( format( '    add_columns = %s ', list ));
	insert into script (line) values ( format( '' ));
	END IF;


	list =	'';
	prefix	=	'[ ';

	FOR	r	IN 
		SELECT	FORMAT_TITLE_CASE( rel.related_table_name )
							AS	"related_table_name"
		FROM	relations			rel
		WHERE	rel.table_name			=	v_table_name
		AND	rel.related_table_name		!=	v_table_name
	LOOP
		list	=	list || prefix || r.related_table_name || 'View ';
		prefix  =	', ';
	END LOOP;


	IF	( list != '' )
	THEN
		list	=	list || ' ];';
	insert into script (line) values ( format( '    related_views = %s ', list ));
	insert into script (line) values ( format( '' ));

	END IF;



	insert into script (line) values ( format( '#    show_template = ''appbuilder/general/model/show_cascade.html'' '));
	insert into script (line) values ( format( '#    edit_template = ''appbuilder/general/model/edit_cascade.html'' '));
	insert into script (line) values ( format( '' ));


	insert into script (line) values ( format( '' ));

	END LOOP;

/*
	FOR	t in
		SELECT	o.table_name			AS	"table_name"
		,	FORMAT_TITLE_CASE(o.table_name)	AS	"table_title"
		FROM	table_order			o
		order by
			id
	LOOP
	insert into script (line) values ( format( 'appbuilder.add_view_no_menu(%sView)', t.table_title ));
	END LOOP;
*/

	insert into script (line) values ( format( '' ));

	drop table if exists table_list;
	drop table if exists relations;
	drop table if exists table_order;

	return	query
	select	line
	from	script
	order by
		id
	;

END
$$
LANGUAGE        PLPGSQL;

select  CASE
        WHEN OBJECT_EXISTS( 'generate_flask_view', 'ROUTINE' )
        THEN '<<< CREATED ROUTINE "generate_flask_view" >>>'
        ELSE '<<< FAILED TO CREATE ROUTINE "generate_flask_view" >>>'
        END
AS Result ;

CREATE OR REPLACE FUNCTION	process_feed_amex_security
(
--	P_TABLE  VARCHAR( 255 )
--,	P_COLUMN  VARCHAR( 255 )
)
RETURNS void
AS $$

DECLARE

exchange_id	int;

BEGIN

	SELECT	exchange_id = get_exchanges_id('AMEX');

	IF	( exchange_id IS NULL )
	THEN
		INSERT	INTO EXHANGES
		(	name
		,	title
		,	nickname
		,	symbol
		,	currency
		)
		VALUES
		(	'AMEX'
		,	'AMEX'
		,	'AMEX'
		,	'AMEX'
		,	'USD'
		)
		;

		select	exchange_id	= get_exchanges_id('AMEX');
	END IF
	;

	INSERT	INTO	INSTRUMENTS
	(	name
	,	title
	,	symbol
	,	nickname
	,	currency
	,	instrument_type
	)
	SELECT	fas.symbol
	,	fas.name
	,	fas.symbol
	,	fas.symbol
	,	'USD'
	,	get_instrument_types_id('EQUITY')
	FROM	feed_amex_security	fas
	WHERE	get_instruments_id(fas.symbol) IS NOT NULL
	;
	
	INSERT	INTO	PRODUCTS
	(	exchange_id
	,	instrument_id
	,	name
	,	title
	,	symbol
	,	nickname
	,	currency
	)
	SELECT	exchange_id
	,	get_instruments_id(fas.symbol)
	,	fas.symbol
	,	fas.name
	,	fas.symbol
	,	fas.symbol
	,	'USD'
	FROM	feed_amex_security	fas
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	products	p
		where	p.exchange_id	=	exchange_id
		and	p.instrument_id	=	get_instruments_id(fas.symbol)
		)
	;

--
--	Exchange Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	'exchanges'
	,	exchange_id
	,	'TICKER'
	,	'AMEX'
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'exchange'
		AND	cr.object_id		=	exchange_id
		AND	cr.xref_type		=	'TICKER'
	)
	;

--
--	Instruments Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'instruments'
	,	get_instruments_id(fas.symbol)
	,	'TICKER'
	,	fas.symbol
	FROM	feed_amex_security	fas
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'instruments'
		AND	cr.object_id		=	get_instruments_id(fas.symbol)
		AND	cr.xref_type		=	'TICKER'
	)
	;


--
--	Product Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'products'
	,	p.id
	,	'TICKER'
	,	fas.symbol
	FROM	feed_amex_security	fas
	JOIN	products		p
	ON	p.exchange_id		=	exchange_id
	AND	p.instrument_id		=	get_instruments_id(fas.symbol)
	
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'instruments'
		AND	cr.object_id		=	get_instruments_id(fas.symbol)
		AND	cr.xref_type		=	'TICKER'
	)
	;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_amex_security', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_amex_security" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_amex_security" >>>'
	END
AS Result ;

CREATE OR REPLACE FUNCTION	process_feed_currencies
(
)
RETURNS void
AS $$

DECLARE

exchange_id	int;

BEGIN
/*
	update	feed_currencies
	set	minor_unit_int	=	cast ( minor_unit as int )
	;
*/

	INSERT	INTO currencies
	(	name
	,	title
--	,	minor_unit
	)
	SELECT	DISTINCT
		fc.alpha_code
	,	fc.currency_title
--	,	cast( COALESCE(nullif(fc.minor_unit,''),'0') as int )
--	,	fc.minor_unit::int
	FROM	feed_currencies	fc
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	currencies	c
		where	c.id	=	get_currencies_id(fc.alpha_code)
	)
	;


	INSERT	INTO countries
	(	name
	,	title
	,	currency_id
	)
	SELECT	DISTINCT
		fc.name
	,	MAX(fc.title)
	,	MAX(get_currencies_id(fc.alpha_code))
	FROM	feed_currencies	fc
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	countries	c
		where	c.id	=	get_countries_id(fc.name)
	)
	GROUP BY
		fc.name
	;


--
--	Currency Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'currencies'
	,	c.id
	,	'ISO ALPHA 2'
	,	fc.alpha_code
	FROM	feed_currencies	fc
	JOIN	currencies	c
	ON	c.id				=	get_currencies_id(fc.alpha_code)
	WHERE	COALESCE(fc.alpha_code,'')	!=	''
	AND	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'currencies'
		AND	cr.object_id		=	c.id
		AND	cr.xref_type		=	'ISO ALPHA 2'
	)
	;

	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'currencies'
	,	c.id
	,	'ISO NUMERIC 3'
	,	fc.num_code
	FROM	feed_currencies	fc
	JOIN	currencies	c
	ON	c.id				=	get_currencies_id(fc.alpha_code)
	WHERE	COALESCE(fc.num_code,'')		!=	''
	AND	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'currencies'
		AND	cr.object_id		=	c.id
		AND	cr.xref_type		=	'ISO NUMERIC 3'
	)
	;



--
--	Country Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'countries'
	,	c.id
	,	'ISO ALPHA 2'
	,	fc.country_code
	FROM	feed_currencies	fc
	JOIN	currencies	c
	ON	c.id				=	get_countries_id(fc.alpha_code)
	WHERE	COALESCE(fc.country_code,'')	!=	''
	AND	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'countries'
		AND	cr.object_id		=	c.id
		AND	cr.xref_type		=	'ISO ALPHA 2'
	)
	;


END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_currencies', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_currencies" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_currencies" >>>'
	END
AS Result ;

CREATE OR REPLACE FUNCTION	process_feed_exchanges
(
--	P_TABLE  VARCHAR( 255 )
--,	P_COLUMN  VARCHAR( 255 )
)
RETURNS void
AS $$

DECLARE

exchange_id	int;

BEGIN

	INSERT	INTO exchanges
	(	name
	,	title
	,	nickname
	,	symbol
	,	country_id
	)
	SELECT	fe.symbol
	,	fe.exchange
	,	fe.symbol
	,	fe.symbol
	,	get_countries_id(fe.country)
	FROM	feed_exchanges	fe
	WHERE	NOT EXISTS
	(
		SELECT	NULL
		FROM	exchanges	e
		where	e.id	=	get_exchanges_id(fe.symbol)
	)
	;


--
--	Exchange Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT	DISTINCT
		'exchanges'
	,	e.id
	,	'TICKER_SUFFIX'
	,	fe.suffix
	FROM	feed_exchanges	fe
	JOIN	exchanges	e
	ON	e.id		=	get_exchanges_id(fe.symbol)
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'exchanges'
		AND	cr.object_id		=	e.id
		AND	cr.xref_type		=	'TICKER_SUFFIX'
	)
	;

END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_exchanges', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_exchanges" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_exchanges" >>>'
	END
AS Result ;

CREATE OR REPLACE FUNCTION	process_feed_yahoo_security
(
--	P_TABLE  VARCHAR( 255 )
--,	P_COLUMN  VARCHAR( 255 )
)
RETURNS void
AS $$

DECLARE

exchange_id	int;

BEGIN

/*
 *
 *	indices

drop index if exists feed_yahoo_security_i1 cascade;
drop index if exists feed_yahoo_security_i2 cascade;
drop index if exists feed_yahoo_security_i3 cascade;

create index feed_yahoo_security_i1 on feed_yahoo_security ( exchange, ticker );
create index feed_yahoo_security_i2 on feed_yahoo_security ( category_code );
create index feed_yahoo_security_i3 on feed_yahoo_security ( ticker );

 *
 *
 */

	INSERT	INTO	exchanges
	(	name
	,	title
	,	nickname
	,	symbol
	)
	SELECT	DISTINCT
		fys.exchange
	,	MAX(fys.exchange)
	,	MAX(fys.exchange)
	,	MAX(fys.exchange)
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	exchanges	e
		WHERE	e.id		=	get_exchanges_id(fys.exchange)
		)
	GROUP BY
		fys.exchange
	;


	INSERT	INTO	instrument_categories
	(	name
	,	title
	)
	SELECT
		fys.category_code
	,	MAX(fys.category_name)
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	instrument_categories	ic
		WHERE	ic.name		=	fys.category_code
		)
	GROUP BY
		fys.category_code
	;


	INSERT	INTO	INSTRUMENTS
	(	name
	,	title
	,	symbol
	,	nickname
	,	instrument_type_id
	,	instrument_category_id
	)
	SELECT
		get_ric_from_ticker(fys.ticker)
	,	MAX(fys.name)
	,	MAX(get_ric_from_ticker(fys.ticker))
	,	MAX(get_ric_from_ticker(fys.ticker))
	,	MAX(get_instrument_types_id('EQUITY'))
	,	MAX(get_instrument_categories_id(fys.category_code))
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	instruments	i
		where	i.id		=	get_instruments_id(get_ric_from_ticker(fys.ticker))
		)
	GROUP BY
		get_ric_from_ticker(fys.ticker)
	;


	INSERT	INTO	PRODUCTS
	(	exchange_id
	,	instrument_id
	,	name
	,	title
	,	symbol
	)
	SELECT	get_exchanges_id(fys.exchange)
	,	get_instruments_id(get_ric_from_ticker(fys.ticker))
	,	max(fys.ticker)
	,	max(fys.name)
	,	max(fys.ticker)
	FROM	feed_yahoo_security	fys
	WHERE	NOT EXISTS
		(
		SELECT	NULL
		FROM	products	p
		WHERE	p.exchange_id	=	get_exchanges_id(fys.exchange)
		AND	p.instrument_id	=	get_instruments_id(get_ric_from_ticker(fys.ticker))
		)
	GROUP BY
		get_exchanges_id(fys.exchange)
	,	get_instruments_id(get_ric_from_ticker(fys.ticker))
	;


--
--	Exchange Cross References
--
--
--	Instruments Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT
		'instruments'
	,	i.id
	,	'TICKER'
	,	i.symbol
	FROM	instruments		i
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'instruments'
		AND	cr.object_id		=	i.id
		AND	cr.xref_type		=	'TICKER'
	)
	;


--
--	Product Cross References
--
	INSERT	INTO	cross_references
	(	object_type
	,	object_id
	,	xref_type
	,	xref_value
	)
	SELECT
		'products'
	,	p.id
	,	'TICKER'
	,	p.symbol
	FROM	products		p
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr
		WHERE	cr.object_type		=	'products'
		AND	cr.object_id		=	p.id
		AND	cr.xref_type		=	'TICKER'
	)
	;



	INSERT	INTO	cross_references
	(	xref_value
	,	object_id
	,	object_type
	,	xref_type
	)
	SELECT	fys.ticker
	,	MAX(cr.object_id)
	,	'products'
	,	'YAHOO'
	FROM	feed_yahoo_security	fys
	JOIN	cross_references	cr
	ON	cr.object_type		=	'products'
	AND	cr.xref_type		=	'TICKER'
	AND	cr.xref_value		=	fys.ticker
	WHERE	NOT EXISTS
	(	SELECT	NULL
		FROM	cross_references	cr2
		WHERE	cr2.object_type		=	'products'
		AND	cr2.object_id		=	cr.id
		AND	cr2.xref_type		=	'YAHOO'
	)
	GROUP BY
		fys.ticker
	;



END
$$
LANGUAGE	PLPGSQL;

select	CASE
	WHEN OBJECT_EXISTS( 'process_feed_yahoo_security', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "process_feed_yahoo_security" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "process_feed_yahoo_security" >>>'
	END
AS Result ;

CREATE OR REPLACE FUNCTION get_advisers_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_adviser_id	int;

BEGIN

	select	id
	into	v_adviser_id
	from	advisers
	where	name	=	p_name
	;

	return	v_adviser_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_advisers_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_advisers_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_advisers_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_clients_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_client_id	int;

BEGIN

	select	id
	into	v_client_id
	from	clients
	where	name	=	p_name
	;

	return	v_client_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_clients_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_clients_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_clients_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_contacts_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_contacts_id	int;

BEGIN

	select	id
	into	v_contacts_id
	from	contacts
	where	name	=	p_name
	;

	return	v_contacts_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_contacts_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_contacts_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_contacts_id" >>>'
	END
AS Result ;


CREATE OR REPLACE FUNCTION get_countries_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_country_id	int;

BEGIN

	select	id
	into	v_country_id
	from	countries
	where	name	=	p_name
	;

	return	v_country_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_countries_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_countries_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_countries_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_currencies_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_country_id	int;

BEGIN

	select	id
	into	v_country_id
	from	currencies
	where	name	=	p_name
	;

	return	v_country_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_currencies_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_currencies_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_currencies_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_diaries_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_diaries_id	int;

BEGIN

	select	id
	into	v_diaries_id
	from	diaries
	where	name	=	p_name
	;

	return	v_diaries_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_diaries_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_diaries_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_diaries_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_exchanges_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_exchange_id	int;

BEGIN

	select	id
	into	v_exchange_id
	from	exchanges
	where	name	=	p_name
	;

	return	v_exchange_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_exchanges_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_exchanges_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_exchanges_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_instruments_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_instrument_id	int;

BEGIN

	select	id
	into	v_instrument_id
	from	instruments
	where	name	=	p_name
	;

	return	v_instrument_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_instruments_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_instruments_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_instruments_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_instrument_categories_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_instrument_category_id	int;

BEGIN

	select	id
	into	v_instrument_category_id
	from	instrument_categories
	where	name	=	p_name
	;

	return	v_instrument_category_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_instrument_categories_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_instrument_categories_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_instrument_categories_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_instrument_types_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_instrument_type_id	int;

BEGIN

	select	id
	into	v_instrument_type_id
	from	instrument_types
	where	name	=	p_name
	;

	return	v_instrument_type_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_instrument_types_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_instrument_types_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_instrument_types_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_products_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_product_id	int;

BEGIN

	select	id
	into	v_product_id
	from	products
	where	name	=	p_name
	;

	return	v_product_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_products_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_products_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_products_id" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_ric_from_ticker
(	p_ticker	VARCHAR
)
RETURNS	varchar
AS
$$
DECLARE

v_ric	varchar;

BEGIN

	SELECT	case
		when	( strpos(p_ticker,'.') = 0 )
		then	p_ticker
		else	left( p_ticker, strpos(p_ticker,'.') -1 )
		end
	into	v_ric
	;

	return	v_ric;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_ric_from_ticker', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_ric_from_ticker" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_ric_from_ticker" >>>'
	END
AS Result ;



CREATE OR REPLACE FUNCTION get_users_id
(	p_name		VARCHAR
)
RETURNS	integer
AS
$$
DECLARE

v_user_id	int;

BEGIN

	select	id
	into	v_user_id
	from	users
	where	name	=	p_name
	;

	return	v_user_id;
END
$$
LANGUAGE	PLPGSQL;


select	CASE
	WHEN OBJECT_EXISTS( 'get_users_id', 'ROUTINE' )
	THEN '<<< CREATED ROUTINE "get_users_id" >>>'
	ELSE '<<< FAILED TO CREATE ROUTINE "get_users_id" >>>'
	END
AS Result ;




DO $$
	
BEGIN

RAISE NOTICE	'<<< REPLACING VIEW available_columns >>';
IF	OBJECT_EXISTS( 'available_columns', 'VIEW' )
THEN
	DROP VIEW IF EXISTS available_columns CASCADE;
	IF	NOT OBJECT_EXISTS('available_columns','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW available_columns >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW available_columns  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	available_columns 
AS
SELECT	so.table_name				"object_name"
,	sc.column_name				"column_name"
,	COALESCE( c.title, sc.column_name )	"column_title"
,	sc.ordinal_position			"column_id"
,	sc.udt_name				"column_type"
,	sc.data_type				"column_usertype"
FROM	information_schema.tables	so
JOIN	information_schema.columns	sc
ON	sc.table_catalog		=	so.table_catalog
AND	sc.table_schema			=	so.table_schema
AND	sc.table_name			=	so.table_name
LEFT OUTER JOIN
	COLUMNS				c
ON	c.name				=	sc.column_name
WHERE	so.table_catalog		=	CURRENT_DATABASE()
AND	so.table_schema			=	CURRENT_SCHEMA()
AND	so.table_type	IN	( 'BASE TABLE', 'VIEW' )
--AND	sc.name		NOT IN	( 'id', 'version', 'update_date', 'update_user', 'pid', 'state', 'target_state')

;

select	CASE
	WHEN OBJECT_EXISTS( 'available_columns', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "available_columns" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "available_columns" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'available_tags', 'VIEW' )
THEN
	DROP VIEW available_tags CASCADE;
	IF	NOT OBJECT_EXISTS('available_tags','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW available_tags >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW available_tags  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	available_tags 
AS
SELECT	mh.message_type			"message_type"
,	m.tag				"tag"
,	COALESCE(t.title,m.tag)		"tag_title"
FROM	message_headers			mh
JOIN	messages			m
ON	m.message_header_id		=	mh.id
LEFT OUTER JOIN
	tags				t
ON	t.tag				=	m.tag
UNION
SELECT	mr.message_type			"message_type"
,	mr.tag				"tag"
,	COALESCE(t.title,mr.tag)	"tag_title"
FROM	message_rights			mr
LEFT OUTER JOIN
	tags				t
ON	t.tag				=	mr.tag

;


select	CASE
	WHEN OBJECT_EXISTS( 'available_tags', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "available_tags" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "available_tags" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree', 'VIEW' )
THEN
	DROP VIEW tree CASCADE;
	IF	NOT OBJECT_EXISTS('tree','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree  >>';
	END IF;

END IF;

END

$$;


CREATE OR REPLACE VIEW 
	tree
AS
SELECT	tree_id			"tree_id"
,	object_id		"object_id"
,	MAX(object_name)	"object_name"
,	MIN(nbr)		"lft"
,	MAX(nbr)		"rgt"
FROM	tree_structures
GROUP BY
	tree_id
,	object_id
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_root', 'VIEW' )
THEN
	DROP VIEW tree_root CASCADE;
	IF	NOT OBJECT_EXISTS('tree_root','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_root >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_root  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_root ( tree_id, object_id , object_name )
AS
SELECT	ts.tree_id
,	ts.object_id
,	ts.object_name
FROM	tree_structures		ts
WHERE	ts.nbr			=	(
					SELECT	MIN(nbr)
					FROM	tree_structures		ts1
					WHERE	ts1.tree_id		=	ts.tree_id
					)
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_root', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_root" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_root" >>>'
	END
AS "Result" ;			


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'authorised_columns', 'VIEW' )
THEN
	DROP VIEW authorised_columns CASCADE;
	IF	NOT OBJECT_EXISTS('authorised_columns','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW authorised_columns >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW authorised_columns  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	authorised_columns 
AS
SELECT	DISTINCT
	u.id			"user_id"
,	avail.object_name	"object_name"
,	avail.column_name	"column_name"
,	avail.column_title	"column_title"
,	avail.column_id		"column_id"
,	avail.column_type	"column_type"
,	avail.column_usertype	"column_usertype"
FROM	available_columns	avail
JOIN	users			u
ON	u.id			=	u.id
JOIN	user_roles		ur
ON	ur.user_id		=	u.id
JOIN	roles			r
ON	r.id			=	ur.role_id
WHERE	0			<	(
					SELECT	COALESCE(MAX( 1 & access_rights ), 0)
					FROM	role_rights
					WHERE	role_id			=	r.id
					AND	object_type		=	'dbobject'
					AND	object_name		=	avail.object_name
					and	object_subname		=	avail.column_name
					)
UNION
SELECT	( SELECT id FROM users WHERE name = 'admin' )	"user_id"
,	avail.object_name		"object_name"
,	avail.column_name		"column_name"
,	avail.column_title		"column_title"
,	avail.column_id			"column_id"
,	avail.column_type		"column_type"
,	avail.column_usertype		"column_usertype"
FROM	available_columns		avail

;

select	CASE
	WHEN OBJECT_EXISTS( 'authorised_columns', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "authorised_columns" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "authorised_columns" >>>'
	END
AS "Result" ;			



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'authorised_tags', 'VIEW' )
THEN
	DROP VIEW authorised_tags CASCADE;
	IF	NOT OBJECT_EXISTS('authorised_tags','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW authorised_tags >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW authorised_tags  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	authorised_tags 
AS
SELECT	DISTINCT
	u.id					"user_id"
,	avail.message_type			"message_type"
,	avail.tag				"tag"
,	COALESCE(avail.tag_title, avail.tag )	"tag_title"
FROM	available_tags				avail
JOIN	users					u
ON	u.id					=	u.id
JOIN	user_roles				ur
ON	ur.user_id				=	u.id
JOIN	roles					r
ON	r.id					=	ur.role_id
where	0					<	(
							SELECT	COALESCE(MAX( 1 & access_rights ), 0)
							FROM	role_rights
							WHERE	role_id			=	r.id
							AND	object_type		=	'message'
							AND	object_name		=	avail.message_type
							and	object_subname		=	avail.tag
							)
UNION
SELECT	( SELECT id FROM users WHERE name = 'admin' )	"user_id"
,	avail.message_type				"message_type"
,	avail.tag					"tag"
,	avail.tag_title					"tag_title"
FROM	available_tags		avail

;


select	CASE
	WHEN OBJECT_EXISTS( 'authorised_tags', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "authorised_tags" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "authorised_tags" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_depth', 'VIEW' )
THEN
	DROP VIEW tree_depth CASCADE;
	IF	NOT OBJECT_EXISTS('tree_depth','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_depth >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_depth  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_depth 
AS
SELECT  child.tree_id					"tree_id"
,	child.object_id					"object_id"
,	MAX(child.object_name)				"object_name"
,       COUNT(parent.object_id)				"depth"
,	MIN(child.lft)					"lft"
,	MAX(child.rgt)					"rgt"
FROM    tree		parent
,       tree		child
where	child.tree_id	=	parent.tree_id
and	( child.lft	>=	parent.lft
and	  child.lft	<=	parent.rgt
	)
GROUP BY child.tree_id, child.object_id
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_depth', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_depth" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_depth" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_leaves', 'VIEW' )
THEN
	DROP VIEW tree_leaves CASCADE;
	IF	NOT OBJECT_EXISTS('tree_leaves','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_leaves >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_leaves  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_leaves
AS
SELECT	tree_id
,	object_id
,	object_name
,	lft
,	rgt
FROM	tree
WHERE	lft	=	rgt - 1
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_leaves', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_leaves" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_leaves" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_ancestors', 'VIEW' )
THEN
	DROP VIEW tree_ancestors CASCADE;
	IF	NOT OBJECT_EXISTS('tree_ancestors','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_ancestors >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_ancestors  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_ancestors ( tree_id, object_id, object_name, parent_object_id, parent_object_name, lft, rgt )
AS
SELECT	child.tree_id				"tree_id"
,	child.object_id				"object_id"
,	child.object_name			"object_name"
,	parent.object_id			"parent_object_id"
,	parent.object_name			"parent_object_name"
,	parent.lft				"lft"
,	parent.rgt				"rgt"
FROM	tree_depth	parent
,	tree_depth	child
WHERE	parent.tree_id	=	child.tree_id
AND	( parent.lft	<=	child.lft
AND	  parent.rgt	>=	child.rgt
	)
;

select	CASE
	WHEN OBJECT_EXISTS( 'tree_ancestors', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_ancestors" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_ancestors" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_children', 'VIEW' )
THEN
	DROP VIEW tree_children CASCADE;
	IF	NOT OBJECT_EXISTS('tree_children','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_children >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_children  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_children ( tree_id, object_id, object_name, sub_object_id, sub_object_name, lft, rgt )
AS
SELECT	parent.tree_id					"tree_id"
,	parent.object_id				"object_id"
,	parent.object_name				"object_name"
,	child.object_id					"child_object_id"
,	child.object_name				"child_object_name"
,	child.lft					"lft"
,	child.rgt					"rgt"
FROM	tree_depth parent
,	tree_depth child
WHERE	child.tree_id	=	parent.tree_id
AND	( child.lft	>=	parent.lft
AND	  child.rgt	<=	parent.rgt
	)
AND	child.lft <> parent.lft  	-- Don't include it's self
AND	NOT EXISTS(
	SELECT	NULL
	FROM	tree		mid
	WHERE	( mid.tree_id	=	parent.tree_id
	AND	  mid.lft	>=	parent.lft
	AND	  mid.lft	<=	parent.rgt
		)
	AND	( child.tree_id	=	mid.tree_id
	AND	  child.lft	>=	mid.lft
	AND	  child.lft	<=	mid.rgt
		)
	AND	mid.object_id NOT IN ( child.object_id, parent.object_id )
	)
;


select	CASE
	WHEN OBJECT_EXISTS( 'tree_children', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_children" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_children" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_descendants', 'VIEW' )
THEN
	DROP VIEW tree_descendants CASCADE;
	IF	NOT OBJECT_EXISTS('tree_descendants','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_descendants >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_descendants  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW
	tree_decendants
AS
SELECT	child.tree_id					"tree_id"
,	parent.object_id				"object_id"
,	parent.object_name				"object_name"
,	child.object_id					"child_object_id"
,	child.object_name				"child_object_name"
,	child.lft					"lft"
,	child.rgt					"rgt"
FROM	tree_depth	parent
JOIN	tree_depth	child
ON	child.tree_id	=	parent.tree_id
WHERE	( child.lft	>=	parent.lft
AND	  child.rgt	<=	parent.rgt
	)
;


select	CASE
	WHEN OBJECT_EXISTS( 'tree_descendants', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_descendants" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_descendants" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'tree_parents', 'VIEW' )
THEN
	DROP VIEW tree_parents CASCADE;
	IF	NOT OBJECT_EXISTS('tree_parents','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW tree_parents >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW tree_parents  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	tree_parents ( tree_id, object_id, object_name, parent_object_id, parent_object_name, lft, rgt )
AS
SELECT	child.tree_id					"tree_id"
,	child.object_id					"object_id"
,	child.object_name				"object_name"
,	parent.object_id				"parent_object_id"
,	parent.object_name				"parent_object_name"
,	parent.lft					"lft"
,	parent.rgt					"rgt"
FROM	tree_depth	parent
,	tree_depth	child
WHERE	parent.tree_id	=	child.tree_id
AND	( parent.lft	<=	child.lft
AND	  parent.rgt	>=	child.rgt
	)
AND	parent.lft	<>	child.lft  	-- Don't include it's self
AND	NOT EXISTS(
	SELECT	NULL
	FROM	tree		mid
	WHERE	mid.tree_id	>=	parent.tree_id
	AND	( mid.lft	>=	parent.lft
	AND	  mid.lft	<=	parent.rgt
		)
	AND	( child.lft	>=	mid.lft
	AND	  child.lft	<=	mid.rgt
		)
	AND	mid.object_id NOT IN ( child.object_id, parent.object_id )
	)
;


select	CASE
	WHEN OBJECT_EXISTS( 'tree_parents', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "tree_parents" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "tree_parents" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'user_authorised_columns', 'VIEW' )
THEN
	DROP VIEW user_authorised_columns CASCADE;
	IF	NOT OBJECT_EXISTS('user_authorised_columns','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW user_authorised_columns >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW user_authorised_columns  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	user_authorised_columns 
AS
SELECT	u.id						"user_id"
,	uc.object_name					"object_name"
,	uc.column_name					"column_name"
,	COALESCE(uc.column_title, auth.column_title)	"column_title"
,	auth.column_id					"column_id"
,	auth.column_type				"column_type"
,	auth.column_usertype				"coulumn_usertype"
FROM	users		u
JOIN	user_columns	uc
ON	uc.user_id	=	u.id
JOIN	authorised_columns	auth
ON	auth.user_id	=	u.id
UNION
SELECT	u.id
,	auth.object_name	"object_name"
,	auth.column_name	"column_name"
,	auth.column_name	"column_title"
,	auth.column_id		"column_id"
,	auth.column_type	"column_type"
,	auth.column_usertype	"coulumn_usertype"
FROM	users		u
JOIN	authorised_columns	auth
ON	auth.user_id	=	u.id
WHERE	NOT EXISTS	(
			SELECT	NULL
			FROM	user_columns
			WHERE	user_id		=	u.id
			AND	object_name	=	auth.object_name
			)

;


select	CASE
	WHEN OBJECT_EXISTS( 'user_authorised_columns', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "user_authorised_columns" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "user_authorised_columns" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'user_authorised_tags', 'VIEW' )
THEN
	DROP VIEW user_authorised_tags CASCADE;
	IF	NOT OBJECT_EXISTS('user_authorised_tags','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW user_authorised_tags >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW user_authorised_tags  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	user_authorised_tags 
AS
SELECT	u.id						"user_id"
,	ut.message_type					"message_type"
,	ut.tag						"tag"
,	COALESCE( ut.tag_title, auth.tag_title)		"tag_title"
FROM	users			u
JOIN	user_tags		ut
ON	ut.user_id		=	u.id
JOIN	authorised_tags		auth
ON	auth.user_id		=	u.id
AND	auth.message_type	=	ut.message_type
AND	auth.tag		=	ut.tag
UNION
SELECT	u.id
,	auth.message_type
,	auth.tag
,	auth.tag_title
FROM	users			u
JOIN	authorised_tags		auth
ON	auth.user_id		=	u.id
WHERE	NOT EXISTS		(
				SELECT	NULL
				FROM	user_tags
				WHERE	user_id		=	u.id
				AND	message_type	=	auth.message_type
				)

;


select	CASE
	WHEN OBJECT_EXISTS( 'user_authorised_tags', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "user_authorised_tags" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "user_authorised_tags" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'authorisation', 'VIEW' )
THEN
	DROP VIEW authorisation CASCADE;
	IF	NOT OBJECT_EXISTS('authorisation','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW authorisation >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW authorisation  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	authorisation
AS
SELECT	DISTINCT
	u.id			"user_id"
,	u.name			"user_name"
,	r.id			"role_id"
,	r.name			"role_name"
FROM	roles			r
JOIN	logins			l
JOIN	users			u
ON	u.id			=	COALESCE(l.user_id, GET_USER_ID(l.username))
JOIN	user_roles		ur
ON	ur.user_id		=	u.id
JOIN	roles			urr
ON	urr.id			=	ur.role_id
AND	urr.precedence		>=	r.precedence
WHERE	u.is_suspended		=	0
AND	( l.last_login		>=	current_time - interval '15 minute'
OR	  l.one_off		>=	current_time - interval '5 minute'
	)
;


select	CASE
	WHEN OBJECT_EXISTS( 'authorisation', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "authorisation" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "authorisation" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'cross_reference_mappings', 'VIEW' )
THEN
	DROP VIEW cross_reference_mappings CASCADE;
	IF	NOT OBJECT_EXISTS('cross_reference_mappings','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW cross_reference_mappings >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW cross_reference_mappings  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	cross_reference_mappings
AS
SELECT	DISTINCT
	cr1.object_type			"user_id"
,	cr1.object_id			"user_name"
,	cr1.xref_type			"from_type"
,	cr1.xref_value			"from_value"
,	cr2.xref_type			"to_type"
,	cr2.xref_value			"to_value"
FROM	cross_references	cr1
JOIN	cross_references	cr2
ON	cr2.object_type		=	cr1.object_type
AND	cr2.object_id		=	cr1.object_id
AND	cr2.xref_type		!=	cr1.xref_type

;

select	CASE
	WHEN OBJECT_EXISTS( 'cross_reference_mappings', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "cross_reference_mappings" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "cross_reference_mappings" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'lookup_types', 'VIEW' )
THEN
	DROP VIEW lookup_types CASCADE;
	IF	NOT OBJECT_EXISTS('lookup_types','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW lookup_types >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW lookup_types  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	lookup_types 
AS
SELECT	type			"type"
,	code			"code"
FROM	lookups
UNION
SELECT	type
,	code
FROM	lookup_codes

;

select	CASE
	WHEN OBJECT_EXISTS( 'lookup_types', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "lookup_types" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "lookup_types" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'relationships', 'VIEW' )
THEN
	DROP VIEW relationships CASCADE;
	IF	NOT OBJECT_EXISTS('relationships','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW relationships >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW relationships  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	relationships
AS
	select	DISTINCT
		st.table_catalog			AS	"table_catalog"
	,	st.table_schema				AS	"table_schema"
	,	st.table_name				AS	"table_name"
	,	st.table_type				AS	"table_type"
	,	c.column_name				AS	"column_name"
	,	kcu.constraint_name			AS	"key_name"
	,	ccu.table_catalog			AS	"related_table_catalog"
	,	ccu.table_schema			AS	"related_table_schema"
	,	ccu.table_name				AS	"related_table_name"
	,	ccu.column_name				AS	"related_column_name"
	,	ccu.constraint_name			AS	"related_key_name"
	,	parse_constraint_name( st.table_name, kcu.constraint_name )
							AS	"relationship_name"
	FROM	information_schema.tables			st
	JOIN
		information_schema.columns			c
	ON	c.table_catalog					=	st.table_catalog
	AND	c.table_schema					=	st.table_schema
	AND	c.table_name					=	st.table_name
--	AND	c.column_name					=	c.column_name
	JOIN
		information_schema.key_column_usage		kcu
	ON	kcu.table_catalog				=	c.table_catalog
	AND	kcu.table_schema				=	c.table_schema
	AND	kcu.table_name					=	c.table_name
	AND	kcu.column_name					=	c.column_name
	JOIN
		information_schema.table_constraints		tc
	ON	tc.constraint_catalog				=	kcu.constraint_catalog
	AND	tc.constraint_schema				=	kcu.constraint_schema
	AND	tc.constraint_name				=	kcu.constraint_name
	AND	tc.constraint_type				=	'FOREIGN KEY'
	JOIN
		information_schema.referential_constraints	rc
	ON	rc.constraint_catalog				=	tc.constraint_catalog
	AND	rc.constraint_schema				=	tc.constraint_schema
	AND	rc.constraint_name				=	tc.constraint_name

	LEFT OUTER JOIN
		information_schema.constraint_column_usage		ccu
	ON	ccu.constraint_catalog				=	rc.unique_constraint_catalog
	AND	ccu.constraint_schema				=	rc.unique_constraint_schema
	AND	ccu.constraint_name				=	rc.unique_constraint_name
--	AND	ccu.column_name					=	c.column_name
	WHERE	ccu.table_name					IS NOT NULL
--	AND	st.table_type					in	( 'BASE TABLE', 'VIEW' )
	AND	st.table_type					in	( 'BASE TABLE' )
	;

select	CASE
	WHEN OBJECT_EXISTS( 'relationships', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "relationships" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "relationships" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;



DO $$
	
BEGIN

IF	OBJECT_EXISTS( 'user_rights', 'VIEW' )
THEN
	DROP VIEW user_rights CASCADE;
	IF	NOT OBJECT_EXISTS('user_rights','VIEW')
	THEN
		RAISE NOTICE	'<<< DROPPED VIEW user_rights >>';
	ELSE
		RAISE NOTICE	'<<< FAILED TO DROP VIEW user_rights  >>';
	END IF;

END IF;

END

$$;

CREATE OR REPLACE VIEW 
	user_rights
AS
SELECT	DISTINCT
	u.id			"user_id"
,	u.name			"user_name"
,	r.id			"role_id"
,	r.name			"role_name"
FROM	logins			l
JOIN	users			u
ON	u.id			=	COALESCE(l.user_id, GET_USERS_ID(l.username))
JOIN	user_roles		ur
ON	ur.user_id		=	u.id
JOIN	roles			r
ON	r.id		=	ur.role_id
/*
-- SqlServer
WHERE	( l.last_login		>=	DATE_SUB( NOW(), INTERVAL 15 MINUTE )
OR	  l.one_off		>=	DATE_SUB( NOW(), INTERVAL 5 MINUTE )
	)
-- Sybase
WHERE	( l.last_login		>=	DATEADD( mi , -15, GETDATE() )
OR	  l.one_off		>=	DATEADD( mi , -5, GETDATE() )
	)
*/
-- Postgresql
WHERE	( l.last_login		>=	current_timestamp - interval '15 minute'
OR	  l.one_off		>=	current_timestamp - interval '5 minute'
	)
;

select	CASE
	WHEN OBJECT_EXISTS( 'user_rights', 'VIEW' )
	THEN '<<< CREATED/UPDATED VIEW "user_rights" >>>'
	ELSE '<<< FAILED TO CREATE/UPDATE VIEW "user_rights" >>>'
	END
AS "Result" ;			

SELECT	current_database()	as	"Database"
,	current_schema()	as	"Schema"
,	current_timestamp	as	"Time Stamp"
;


!!

