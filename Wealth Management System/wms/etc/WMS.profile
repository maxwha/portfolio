#!/bin/ksh
#-------------------------
#
#	WMS.profile
#
#=========================


if	[ "${WMS_HOME}" = "" ]
then
	echo	"WMS_HOME not set"
	echo	" ... checking if default HOME exists"
	if	[ -d "${BUILD_HOME}/wms" ]
	then
		echo	" ... default HOME exists, setting WMS_HOME"
		export	WMS_HOME=${BUILD_HOME}/wms
	fi
fi

if	[ -d "${WMS_HOME}" ]
then

#export	WMS_DB_HOME="$WMS_HOME/dbs/wms"

export	WMS_DB=${WMS_DB:-"wms"}

export	WMS_WMS_DB=${WMS_WMS_DB:-"wms"}
export	WMS_WMS_DBTYPE=${WMS_WMS_DBTYPE:-"POSTGRESQL"}
export	WMS_WMS_USER=${WMS_WMS_USER:-"admin"}
export	WMS_WMS_PASS=${WMS_WMS_PASS:-"admin"}
export	WMS_WMS_HOST=${WMS_WMS_HOST:-"localhost"}
export	WMS_WMS_SERVER=${WMS_WMS_SERVER:-${DB_SERVER}}
export	WMS_WMS_PORT=${WMS_WMS_PORT:-"5432"}
export	WMS_WMS_SA_USER=${WMS_WMS_SA_USER:-"postgres"}
export	WMS_WMS_SA_PASS=${WMS_WMS_SA_PASS:-"casper"}

export	WMS_WMS_DB_ROOT=${WMS_WMS_DB_ROOT:-"${WMS_HOME}/dbs"}
export	WMS_WMS_SERVER_ROOT=${WMS_WMS_SERVER_ROOT:-${DB_SERVER_ROOT}}
export	WMS_WMS_PATH_SEPARATOR=${WMS_WMS_PATH_SEPARATOR:-${DB_PATH_SEPARATOR}}
export	WMS_WMS_SQLTERM=${WMS_WMS_SQLTERM:-";"}


#export	WMS_WMS_JDBC_DRIVER_CLASS=${WMS_WMS_JDBC_DRIVER_CLASS:-${POSTGRES_JDBC_DRIVER_CLASS}}
#export	WMS_WMS_JDBC_DRIVER=${WMS_WMS_JDBC_DRIVER:-${POSTGRES_JDBC_DRIVER}}
#export	WMS_WMS_JDBC_URL=${WMS_WMS_JDBC_URL:-${WMS_WMS_JDBC_DRIVER}://${WMS_WMS_HOST}:${WMS_WMS_PORT}/${WMS_WMS_DB}}
#

# set any extra variables

	export	WMS_SVN_ROOT="https://secure2.svnrepository.com/s_stpf/wms"
	export	WMS_SVN_USER=${SVN_USER}
	export	WMS_SVN_PASS=${SVN_PASS}

fi



