this directory is refered to as <root>

Building
--------
1) svn update
2) ant or ./build.sh (depending on whether you have apache Ant installed or not.
3) If the build has been successful you will see BUILD SUCCESSFUL.
4) All output is in the <root>/dist directory

Running
-------
1) Edit all your settings in <root>/prop/<project>.prop (the run script assumes that this is where your props file lives)
2) cd into <root>/dist directory
3) ./run.sh

Hey Presto!
