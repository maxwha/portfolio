
import	java.io.File;
import	java.io.*;
import	java.util.*;

public class spell 
{

//	private	static	Dictionary	dictionary	=	new ListDictionary();	// Use this for comparison tests
	private	static	Dictionary	dictionary	=	new HashDictionary( new StringHashCode(), (float)0.5 );
	private	static	Dictionary	corrections;
   
	private	static	StringBuilder	alphabet	=	new StringBuilder("abcdefghijklmnopqrstuvwxyz");

	public static void loadDict(BufferedInputStream dict)
	{
		try
		{
			FileWordRead	words	= new FileWordRead(dict);

			while	( words.hasNextWord() )
			{
				String	nextWord = words.nextWord();
//				System.out.println("Dictionary Entry = " + nextWord );
				dictionary.insert(nextWord);
			}
         	}
	        catch	(IOException e)
		{
			// catch exceptions caused by file input/output errors
			System.out.println("Error reading Dictionary");
			System.exit(0);
		}
	}

	public static void checkFile(BufferedInputStream file)
	{
		try
		{
			FileWordRead	words	= new FileWordRead(file);

			while	( words.hasNextWord() )
			{
				String	nextWord = words.nextWord();
//				System.out.println("Check Word = " + nextWord );
				if (!dictionary.find(nextWord))
				{
					checkWords(nextWord);
				}
			}
		}
	        catch	(IOException e)
		{
			// catch exceptions caused by file input/output errors
			System.out.println("Error reading Check File");
			System.exit(0);
		}
	}

	private static void checkWords(String word)
	{
		System.out.println("Checking word = " + word );

		Boolean	newLineNeeded	=	false;

		corrections	=	new ListDictionary();

		checkSubstitution(new StringBuilder(word));
		checkOmission(new StringBuilder(word));
		checkInsertion(new StringBuilder(word));
		checkReversal(new StringBuilder(word));

		Iterator	correctionList = corrections.elements();
		String prefix = word + " => " ;
		while ( correctionList.hasNext() )
		{
			System.out.print( prefix + correctionList.next() );
			prefix = ", ";
			newLineNeeded = true;
		}

		if	( newLineNeeded )
		{
			System.out.println("");
		}

	}

	private static void checkCorrection(String word)
	{
//		System.out.println("Checking correction = " + word );
		try
		{
			if	( dictionary.find(word) )
			{
				corrections.insert( word );
			}
		}
	        catch	(DictionaryException e)
		{
		}
	}

	private static void checkSubstitution(StringBuilder word)
	{
//		System.out.println("Checking substitution = " + word );
		for	( int i = 0 ; i < word.length(); i++ )
		{
			for	( int j = 0 ; j < alphabet.length(); j++ )
			{
				if	( !(word.charAt(i) == alphabet.charAt(j)) )
				{
					String	newWord;
					if	( i == 0 )
					{
						newWord	=	alphabet.substring(j,j+1)
							+	word.substring(i+1)
							;
					}
					else if	( i == word.length() )
					{
						newWord	=	word.substring(0,i)
							+	alphabet.substring(j,j+1)
							;
					}
					else
					{
						newWord	=	word.substring(0,i)
							+	alphabet.substring(j,j+1)
							+	word.substring(i+1)
							;
					}
					checkCorrection( newWord );
				}
			}
		}
	}

	private static void checkOmission(StringBuilder word)
	{
//		System.out.println("Checking omission = " + word );
		for	( int i = 0 ; i < word.length(); i++ )
		{
			String	newWord;
			if	( i == 0 )
			{
				newWord	=	word.substring(i+1)
					;
			}
			else if	( i == word.length() )
			{
				newWord	=	word.substring(0,i)
					;
			}
			else
			{
				newWord	=	word.substring(0,i)
					+	word.substring(i+1)
					;
			}
			checkCorrection( newWord );
		}
	}

	private static void checkInsertion(StringBuilder word)
	{
//		System.out.println("Checking insertion = " + word );
		for	( int i = 0 ; i <= word.length(); i++ )
		{
			for	( int j = 0 ; j < alphabet.length(); j++ )
			{
				String	newWord;
				if	( i == 0 )
				{
					newWord	=	alphabet.substring(j,j+1)
						+	word.substring(i)
						;
				}
				else if	( i == word.length() )
				{
					newWord	=	word.substring(0)
						+	alphabet.substring(j,j+1)
						;
				}
				else
				{
					newWord	=	word.substring(0,i)
						+	alphabet.substring(j,j+1)
						+	word.substring(i)
						;
				}
				checkCorrection( newWord );
			}
		}
	}

	private static void checkReversal(StringBuilder word)
	{
//		System.out.println("Checking reversal = " + word );
		for	( int i = 0 ; i < (word.length() - 1); i++ )
		{
			String	newWord	=	word.substring(0,i)
					+	word.substring(i+1,i+2)
					+	word.substring(i,i+1)
					+	word.substring(i+2)
					;
			checkCorrection( newWord );
		}
	}


          
    public static void main(String[] args) 
	    throws java.io.IOException
    {
         if (args.length != 2 ) 
	 {
            System.out.println("Usage: spell dictionaryFile.txt inputFile.txt ");
            System.exit(0);
         }
         BufferedInputStream dict,file;

	long	startTime;
	long	finishTime;

	double	time;
         
	startTime	=	System.currentTimeMillis();

         try
	 {
           
            dict  = new BufferedInputStream(new FileInputStream(args[0]));
            file  = new BufferedInputStream(new FileInputStream(args[1]));
	    // To read from specific files, comment the 2 lines above and 
            // uncomment 2 lines below 
            //dict  = new BufferedInputStream(new FileInputStream("C:\\dictionary.txt"));
	    //file  = new BufferedInputStream(new FileInputStream("C:\\checkText.txt"));
	    loadDict(dict);
	    checkFile(file);
	   
         }
         catch (IOException e)
	 {
	    // catch exceptions caused by file input/output errors
            System.out.println("Check your file name");
            System.exit(0);
        }
  
	finishTime	=	System.currentTimeMillis();
	time		= 	finishTime - startTime;
	System.out.println("Time in milseconds is "+ time);
    }
}

