import java.io.*;

import java.awt.*;
import javax.swing.*;

public final class winspell extends JFrame
{	
	private static final String NAME = "winspell";
	private static final String DESCRIPTION = "Spell Checker";
	private static final String COPYWRITE = "(c)2013 Harry G. Maxwell ";

	private	static	BufferedInputStream dict,file;

	private static void usage(PrintStream out)
	{
		out.println( "usage: " + NAME + " dictionary document " );
	}

	private static void help(PrintStream out)
	{
		out.println();
	}

	private winspell()
	{
		super( DESCRIPTION + " - " + COPYWRITE );


		setDefaultCloseOperation	( EXIT_ON_CLOSE 		);
		getContentPane().setLayout	( new BorderLayout()	);
		getContentPane().add( new JPanel(), BorderLayout.CENTER);

		pack();
		setVisible( true );

		spell.loadDict(dict);
		spell.checkFile(file);

	}

	public static void main( String[] arguments )
	{
		if	( arguments.length < 2 )
		{
			usage( System.err );
			help( System.err );
			System.exit( -1 );
		}

        	 try
		 {
           
			dict  = new BufferedInputStream(new FileInputStream(arguments[0]));
			file  = new BufferedInputStream(new FileInputStream(arguments[1]));
			// To read from specific files, comment the 2 lines above and 
			// uncomment 2 lines below 
			//dict  = new BufferedInputStream(new FileInputStream("C:\\dictionary.txt"));
			//file  = new BufferedInputStream(new FileInputStream("C:\\checkText.txt"));
		}
		catch (IOException e)
		{
			// catch exceptions caused by file input/output errors
			System.out.println("Check your file name");
			System.exit(0);
		}
		new winspell();
	}

}

