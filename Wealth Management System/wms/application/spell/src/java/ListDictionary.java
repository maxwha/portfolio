
import	java.util.Iterator;
import	java.util.LinkedList;

public class ListDictionary
	implements	Dictionary
{
	private	LinkedList	list		=	new LinkedList();
   
	public	ListDictionary()
	throws	DictionaryException
	{
	}

	public void insert(String key)
	{
		list.add(key);
	}

	public boolean find(String key)
	{
		return	list.contains(key);
	}

	public Iterator elements()
	{
		return	list.iterator();
	}

	public void remove(String key)
		throws	DictionaryException
	{
		if	( !list.remove(key) )
		{
			throw	new DictionaryException();
		}
	}
	
}
