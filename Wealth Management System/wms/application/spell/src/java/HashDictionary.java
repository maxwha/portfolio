/*
	The Hash is implemented as a simple array, in this case an array of types implementing the Entry interface
	
	The implementation uses a prime number circular file technique.

	e.g.
		if the array has 7 entries, hashValue = 3 ( 0 - 6 ), offset = 2 ( 1 - 6 )

		first probe							=	3
		next probe ( previous + offset )	=	3 + 2 % 7	=	5	
		next probe ( previous + offset )	=	5 + 2 % 7	=	0	
		next probe ( previous + offset )	=	0 + 2 % 7	=	2	
		next probe ( previous + offset )	=	2 + 2 % 7	=	4	
		next probe ( previous + offset )	=	4 + 2 % 7	=	6	
		next probe ( previous + offset )	=	6 + 2 % 7	=	1	
		next probe ( previous + offset )	=	1 + 2 % 7	=	3 ( stops Here)

	because the size of the set is a prime number not divisible by any other number than 1 or itself it will visit every entry before looping back to the start point.
	

	The set size needs to be prime number, this allows the search routing, by adding the secondary hash, to search through the whole set if needs be to find a match or not.

	The StringHashCode object is stored in hashCode and the giveCode method returns an integer representation of the string(key)

	The serach function calls 
	The hash() function then uses this code to provide a first entry point in to the set
	the Hash2() function provides an offset to add to the this and subsequent searches

	the search point has the offset added every loop until either an entry is found (found) or a null entry is found (not found)

	if entries were just deleted ( set to null ) when removed from the set then the search would not work there after it may stop on a null, that wasn't there before, and not find the true entry on the next offset.
	Therefore a placeholder is required.

	To facilitate this a helper class is devised to encapsulate the actual keys and the placeholder and treat them as the same type of object
	The method of doing this is to define an interface and then have an object implement that interface.

	The remove() method therefore must replace the entry with the placeholder and not null


	The findKey() method returns 

		>= 0 if and entry was found and 
		-1 if not found and no room 
		>-1 if not found and there is room , the first available position is negated and offset by -1, this must be reversed if used


*/


import	java.util.*;

public class HashDictionary
	implements	Dictionary
{

	private	int		noEntries		=	0;
	private	int		size			=	0;
	private	int		scale;
	private	int		shift;

	private	HashCode	hashCode;

	private	float		loadFactor;
	private	float		probeCount;
	private	float		opCount;




/*
	A list of prime numbers, the one prior to each power of 2, this gives a good range for extension if the hash table needs to extend and also makes sure its a prime number.
	primePointer is maintained to assist the rehash function.

*/

   
	private	int[]		primes = { 7,31,61,127,251,509,1021,2039,4093,8191,16381,32749,65521,131071,262139,524287,1048773,2097143,4194301,8388593,16777213,33554393,67108859,134217689,268435399, 536870909,1073741789,2147483647};
	private	int		primePointer;


/*
	The 

/*
	The placeholder when an entry is removed
*/
	private	Entry		AVAILABLE		=	new HashEntry(null);

/*
	The set
*/

	private	Entry[]		keys;


	interface	Entry<K>
	{
		K	getKey();
		boolean	equals(Object o );
	}

	private	class	HashEntry<K>
			implements	Entry
	{
		private	K	key;

		public	HashEntry(K key)
		{
			this.key	=	key;
		}

		public	K	getKey()
		{
			return	key;
		}

		public	boolean	equals( Object o )
		{
			HashEntry	entry;
			try
			{
				entry	=	(HashEntry)o;
			}
			catch	( ClassCastException e )
			{
				return	false;
			}
			return	( entry.getKey() == key );
		}

	}



	public	HashDictionary()
	throws	DictionaryException
	{
		throw	new DictionaryException();
	}

 
	public	HashDictionary(HashCode inputCode, float inputLoadFactor )
	{
		hashCode	=	inputCode;
		loadFactor	=	inputLoadFactor;
		primePointer	=	0;
		size		=	primes[primePointer];
		keys		=	new Entry[size];

		Random	random	=	new Random();
		scale		=	random.nextInt( size - 1 ) + 1;
		shift		=	random.nextInt( size ) ;
	}


/*
	Searches through the set as described 

*/	
	private	int	findKey( String key )
	{
		int	avail		=	-1;
		int	hashValue1	=	hash(key);
		int	hashValue2	=	hash2(key);

		int	hashValue	=	hashValue1;

		do
		{
			++probeCount;
			Entry	entry	=	keys[hashValue];
			if	( entry == null )
			{
				if	( avail < 0 )
				{
					avail	=	hashValue;
				}
				break;
			}
			if	( key.equals( entry.getKey() ) )
			{
				return	hashValue;
			}
			if	( entry == AVAILABLE )
			{
				if	( avail < 0 )
				{
					avail	=	hashValue;
				}
			}

			hashValue	=	( ( hashValue + hashValue2 ) % size ) ; // find the next probe point
		}
		while	( hashValue != hashValue1 );	//Stops if it gets back to the start point

		return	-( avail + 1 );
	}



/*
	This function takes the hash code return from the HashCode object and compresses it to ensure it fits in the current set
	the compression function uses MAD ( Multiply, Add and Divide, the scale and shift values are randomly calculated on object creation.
*/

	private	int	hash( String key )
	{
		int	hashValue	=	( Math.abs((( hashCode.giveCode(key) * scale ) + shift ) % size ) );
		return	hashValue;
	}



/*
	This function uses the hash code return from the HashCode object to source an offset, it compresses it to make sure it fits in the current set, it also ensures that the offset > 0 and < the set size, otherwise the search will stop after the first probe, ie the next search point will = the initial search point

*/

	private	int	hash2( String key )
	{
		int	hashValue2	=	( size - ( Math.abs( hashCode.giveCode(key) % ( size - 2 ) ) + 1 ) ) ; 
		return	hashValue2;
	}




/*
	makes a copy of the existing set
	uses the primes set to get the new size
	inserts the active entries in the new set
*/

	private	void	rehash()
	{
		Entry[]	oldKeys	=	keys;
		int	oldSize	=	size;

		size		=	primes[++primePointer];
		keys		=	new	Entry[size];

		for	( int i = 0; i < oldSize; i++ )
		{
			Entry	entry	=	oldKeys[i];
			if	(( entry != null )
			&&	 ( entry != AVAILABLE )
				)
			{
				insert	( entry.getKey().toString() );
			}
		}
	}


	public void insert(String key)
	{
		++opCount;
		if	( ( noEntries / size ) > loadFactor )
		{
			rehash();
		}

		int	i	=	findKey(key);
		if	( i >= 0 )
		{
			return;		// found
		}
		keys	[ -i-1 ]	=	new	HashEntry( key );
		++noEntries;

		return;
	}


	public boolean find(String key)
	{
		++opCount;
		if	( findKey(key) >= 0 )
		{
			return	true;
		}
		return	false;
	}


	public void remove(String key)
		throws	DictionaryException
	{
		++opCount;
		int	i	=	findKey(key);
		if	( i < 0 )
		{
			throw	new DictionaryException();
		}
		keys[i] = AVAILABLE;
		--noEntries;
	}



	
/*
	This method uses a vector to make a safe copy of the set and utilises its iterator method
*/

	public Iterator elements()
	{

		Vector<String>	entries	=	new Vector<String>();

		for	( int i = 0; i < size; i++ )
		{
			Entry	entry	=	keys[i];
			if	(( entry != null )
			&&	 ( entry != AVAILABLE )
				)
			{
				entries.add(entry.getKey().toString());
			}
		}

		return	entries.iterator();
	}



	public float averNumProbes()
	{
		return	( (float) probeCount ) / (float) ( opCount );
	}

}


