import	java.lang.*;
import	java.math.*;

public class StringHashCode
	implements	HashCode
{

	private static	int	HASH_CONSTANT	=	33;	// ( 33,37,39,41 ) 
   
	private	int	hashCode;
 
	// For an input object, computes and returns its HashCode as an integer
	public int giveCode(Object key)
	{
		String	str	=	key.toString();

		double	dblHash	=	0;

		for	( int i = 0 ; i < str.length(); i++ )
		{
			dblHash	=	dblHash
					+	Math.pow(Character.getNumericValue(str.charAt(i)), HASH_CONSTANT )
						;
		}
		hashCode	=	Double.valueOf(dblHash).intValue();

		return	hashCode;
	}
}
