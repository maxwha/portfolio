#from flask_appbuilder import ModelView , IndexView
from flask_appbuilder.fieldwidgets import Select2Widget
from flask.ext.appbuilder.models.sqla.interface import SQLAInterface
from flask.ext.appbuilder.models.datamodel import SQLAModel
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from app import appbuilder, db

from flask.ext.appbuilder.views import ModelView, IndexView, CompactCRUDMixin, MasterDetailView

from .models import *

class GlobalsView(ModelView):
    datamodel = SQLAInterface(Globals)

    list_columns = [ 'code', 'value', 'datatype', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'code', 'value', 'datatype', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'code', 'value', 'datatype', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'code', 'value', 'datatype', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class LookupsView(ModelView):
    datamodel = SQLAInterface(Lookups)

    list_columns = [ 'type', 'code', 'basetype', 'table_name', 'table_key', 'value', 'shortname', 'name', 'title', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'type', 'code', 'basetype', 'table_name', 'table_key', 'value', 'shortname', 'name', 'title', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'type', 'code', 'basetype', 'table_name', 'table_key', 'value', 'shortname', 'name', 'title', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'type', 'code', 'basetype', 'table_name', 'table_key', 'value', 'shortname', 'name', 'title', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class LookupCodesView(ModelView):
    datamodel = SQLAInterface(LookupCodes)

    list_columns = [ 'type', 'code', 'value', 'name', 'shortname', 'sequence', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'type', 'code', 'value', 'name', 'shortname', 'sequence', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'type', 'code', 'value', 'name', 'shortname', 'sequence', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'type', 'code', 'value', 'name', 'shortname', 'sequence', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class LogsView(ModelView):
    datamodel = SQLAInterface(Logs)

    list_columns = [ 'message', 'message_source', 'message_depth', 'message_severity', 'message_date', 'message_user', 'logs_date', 'user_id' ]; 
    show_columns = [ 'message', 'message_source', 'message_depth', 'message_severity', 'message_date', 'message_user', 'logs_date', 'user_id' ]; 
    edit_columns = [ 'message', 'message_source', 'message_depth', 'message_severity', 'message_date', 'message_user', 'logs_date', 'user_id' ]; 
    add_columns = [ 'message', 'message_source', 'message_depth', 'message_severity', 'message_date', 'message_user', 'logs_date', 'user_id' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class QueryDetailColumnsView(ModelView):
    datamodel = SQLAInterface(QueryDetailColumns)

    list_columns = [ 'name', 'column_sequence', 'column_name', 'column_title', 'column_summary', 'column_precedence', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'column_sequence', 'column_name', 'column_title', 'column_summary', 'column_precedence', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'column_sequence', 'column_name', 'column_title', 'column_summary', 'column_precedence', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'column_sequence', 'column_name', 'column_title', 'column_summary', 'column_precedence', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class ObjectsView(ModelView):
    datamodel = SQLAInterface(Objects)

    list_columns = [ 'name', 'id_name', 'value_name', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'id_name', 'value_name', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'id_name', 'value_name', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'id_name', 'value_name', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class CrossReferencesView(ModelView):
    datamodel = SQLAInterface(CrossReferences)

    list_columns = [ 'object_type', 'object_id', 'xref_type', 'xref_value', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'object_type', 'object_id', 'xref_type', 'xref_value', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'object_type', 'object_id', 'xref_type', 'xref_value', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'object_type', 'object_id', 'xref_type', 'xref_value', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class ClientContactsView(ModelView):
#    datamodel = SQLAInterface(ClientContacts)
    datamodel = SQLAModel(ClientContacts)

    list_columns = [ 'client_id', 'contact_id', 'sequence', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'client_id', 'contact_id', 'sequence', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'client_id', 'contact_id', 'sequence', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'client_id', 'contact_id', 'sequence', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class ColumnsView(ModelView):
    datamodel = SQLAInterface(Columns)

    list_columns = [ 'name', 'title', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'title', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'title', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'title', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class UserColumnsView(ModelView):
    datamodel = SQLAInterface(UserColumns)

    list_columns = [ 'user_id', 'object_name', 'column_name', 'column_title', 'column_order', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'user_id', 'object_name', 'column_name', 'column_title', 'column_order', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'user_id', 'object_name', 'column_name', 'column_title', 'column_order', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'user_id', 'object_name', 'column_name', 'column_title', 'column_order', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class FeedView(ModelView):
    datamodel = SQLAInterface(Feed)

    list_columns = [ 'feed_id', 'feed_type', 'source_system', 'source_reference', 'tag', 'value', 'value_sequence', 'state', 'target_state', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'feed_id', 'feed_type', 'source_system', 'source_reference', 'tag', 'value', 'value_sequence', 'state', 'target_state', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'feed_id', 'feed_type', 'source_system', 'source_reference', 'tag', 'value', 'value_sequence', 'state', 'target_state', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'feed_id', 'feed_type', 'source_system', 'source_reference', 'tag', 'value', 'value_sequence', 'state', 'target_state', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class DataView(ModelView):
    datamodel = SQLAInterface(Data)

    list_columns = [ 'data_id', 'data_type', 'source_system', 'source_reference', 'tag', 'value', 'value_sequence', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'data_id', 'data_type', 'source_system', 'source_reference', 'tag', 'value', 'value_sequence', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'data_id', 'data_type', 'source_system', 'source_reference', 'tag', 'value', 'value_sequence', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'data_id', 'data_type', 'source_system', 'source_reference', 'tag', 'value', 'value_sequence', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class PreferencePrioritiesView(ModelView):
    datamodel = SQLAInterface(PreferencePriorities)

    list_columns = [ 'name', 'priority', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'priority', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'priority', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'priority', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class IndexComponentsView(ModelView):
    datamodel = SQLAInterface(IndexComponents)

    list_columns = [ 'index', 'product', 'sequence', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'index', 'product', 'sequence', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'index', 'product', 'sequence', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'index', 'product', 'sequence', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class ContactSocialNetworksView(ModelView):
    datamodel = SQLAInterface(ContactSocialNetworks)

    list_columns = [ 'contact', 'social_network', 'identifier', 'sequence', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'contact', 'social_network', 'identifier', 'sequence', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'contact', 'social_network', 'identifier', 'sequence', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'contact', 'social_network', 'identifier', 'sequence', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class UserRolesView(ModelView):
    datamodel = SQLAInterface(UserRoles)

    list_columns = [ 'user', 'role', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'user', 'role', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'user', 'role', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'user', 'role', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class HistoricQuotesView(ModelView):
    datamodel = SQLAInterface(HistoricQuotes)

    list_columns = [ 'product', 'latest_value', 'open', 'high', 'low', 'close', 'quote_date', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'product', 'latest_value', 'open', 'high', 'low', 'close', 'quote_date', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'product', 'latest_value', 'open', 'high', 'low', 'close', 'quote_date', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'product', 'latest_value', 'open', 'high', 'low', 'close', 'quote_date', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class PortfolioPositionsView(ModelView):
    datamodel = SQLAInterface(PortfolioPositions)

    list_columns = [ 'portfolio', 'product', 'currency', 'quantity', 'price', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'portfolio', 'product', 'currency', 'quantity', 'price', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'portfolio', 'product', 'currency', 'quantity', 'price', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'portfolio', 'product', 'currency', 'quantity', 'price', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class RoleRightsView(ModelView):
    datamodel = SQLAInterface(RoleRights)

    list_columns = [ 'role', 'object_type', 'object_name', 'object_subname', 'access_rights', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'role', 'object_type', 'object_name', 'object_subname', 'access_rights', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'role', 'object_type', 'object_name', 'object_subname', 'access_rights', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'role', 'object_type', 'object_name', 'object_subname', 'access_rights', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class HistoricPricesView(ModelView):
    datamodel = SQLAInterface(HistoricPrices)

    list_columns = [ 'product', 'price_date', 'open', 'high', 'low', 'close', 'volume', 'adj_close', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'product', 'price_date', 'open', 'high', 'low', 'close', 'volume', 'adj_close', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'product', 'price_date', 'open', 'high', 'low', 'close', 'volume', 'adj_close', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'product', 'price_date', 'open', 'high', 'low', 'close', 'volume', 'adj_close', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class DiaryEntriesView(ModelView):
#    datamodel = SQLAInterface(DiaryEntries)
    datamodel = SQLAModel(DiaryEntries)

    list_columns = [ 'diary', 'client', 'subject', 'location', 'start_time', 'end_time', 'details', 'notes', 'user_id', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'diary', 'client', 'subject', 'location', 'start_time', 'end_time', 'details', 'notes', 'user_id', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'diary', 'client', 'subject', 'location', 'start_time', 'end_time', 'details', 'notes', 'user_id', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'diary', 'client', 'subject', 'location', 'start_time', 'end_time', 'details', 'notes', 'user_id', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class LoginsView(ModelView):
    datamodel = SQLAInterface(Logins)

    list_columns = [ 'user', 'role', 'name', 'title', 'username', 'password', 'last_login', 'one_off', 'is_suspended', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'user', 'role', 'name', 'title', 'username', 'password', 'last_login', 'one_off', 'is_suspended', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'user', 'role', 'name', 'title', 'username', 'password', 'last_login', 'one_off', 'is_suspended', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'user', 'role', 'name', 'title', 'username', 'password', 'last_login', 'one_off', 'is_suspended', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class QuotesView(ModelView):
    datamodel = SQLAInterface(Quotes)

    list_columns = [ 'product', 'latest_value', 'open', 'high', 'low', 'mid', 'close', 'bid', 'offer', 'quote_date', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'product', 'latest_value', 'open', 'high', 'low', 'mid', 'close', 'bid', 'offer', 'quote_date', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'product', 'latest_value', 'open', 'high', 'low', 'mid', 'close', 'bid', 'offer', 'quote_date', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'product', 'latest_value', 'open', 'high', 'low', 'mid', 'close', 'bid', 'offer', 'quote_date', 'effective_from', 'effective_to' ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class ProductsView(ModelView):
    datamodel = SQLAInterface(Products)

    list_columns = [ 'exchange', 'instrument', 'currency', 'name', 'title', 'symbol', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'exchange', 'instrument', 'currency', 'name', 'title', 'symbol', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'exchange', 'instrument', 'currency', 'name', 'title', 'symbol', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'exchange', 'instrument', 'currency', 'name', 'title', 'symbol', 'effective_from', 'effective_to' ]; 

    related_views = [ IndexComponentsView , HistoricQuotesView , PortfolioPositionsView , QuotesView , HistoricPricesView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class ContactsView(ModelView):
    datamodel = SQLAInterface(Contacts)

    list_columns = [ 'client', 'name', 'title', 'position', 'forename', 'surname', 'honours', 'phone', 'email', 'address_name', 'address_no', 'address_street', 'address_district', 'address_town', 'address_city', 'address_county', 'address_country', 'address_postcode', 'sequence', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'client', 'name', 'title', 'position', 'forename', 'surname', 'honours', 'phone', 'email', 'address_name', 'address_no', 'address_street', 'address_district', 'address_town', 'address_city', 'address_county', 'address_country', 'address_postcode', 'sequence', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'client', 'name', 'title', 'position', 'forename', 'surname', 'honours', 'phone', 'email', 'address_name', 'address_no', 'address_street', 'address_district', 'address_town', 'address_city', 'address_county', 'address_country', 'address_postcode', 'sequence', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'client', 'name', 'title', 'position', 'forename', 'surname', 'honours', 'phone', 'email', 'address_name', 'address_no', 'address_street', 'address_district', 'address_town', 'address_city', 'address_county', 'address_country', 'address_postcode', 'sequence', 'effective_from', 'effective_to' ]; 

    related_views = [ ContactSocialNetworksView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class DiariesView(ModelView):
    datamodel = SQLAInterface(Diaries)

    list_columns = [ 'adviser', 'name', 'title', 'nickname', 'user_id', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'adviser', 'name', 'title', 'nickname', 'user_id', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'adviser', 'name', 'title', 'nickname', 'user_id', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'adviser', 'name', 'title', 'nickname', 'user_id', 'effective_from', 'effective_to' ]; 

    related_views = [ DiaryEntriesView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class SocialNetworksView(ModelView):
    datamodel = SQLAInterface(SocialNetworks)

    list_columns = [ 'name', 'title', 'nickname', 'website', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'title', 'nickname', 'website', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'title', 'nickname', 'website', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'title', 'nickname', 'website', 'effective_from', 'effective_to' ]; 

    related_views = [ ContactSocialNetworksView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class RolesView(ModelView):
    datamodel = SQLAInterface(Roles)

    list_columns = [ 'role', 'name', 'title', 'shortname', 'precedence', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'role', 'name', 'title', 'shortname', 'precedence', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'role', 'name', 'title', 'shortname', 'precedence', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'role', 'name', 'title', 'shortname', 'precedence', 'effective_from', 'effective_to' ]; 

    related_views = [ RoleRightsView , UserRolesView , LoginsView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class IndicesView(ModelView):
    datamodel = SQLAInterface(Indices)

    list_columns = [ 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 

    related_views = [ IndexComponentsView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class PortfoliosView(ModelView):
#    datamodel = SQLAInterface(Portfolios)
    datamodel = SQLAModel(Portfolios)

    list_columns = [ 'client', 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'client', 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'client', 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'client', 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 

    related_views = [ PortfolioPositionsView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class ClientsView(ModelView):
#    datamodel = SQLAInterface(Clients)
    datamodel = SQLAModel(Clients)

#    list_columns = [ 'adviser', 'name', 'forename', 'surname', 'title', 'shortname', 'date_of_birth', 'ni_number', 'image_id', 'effective_from', 'effective_to' ]; 
#    show_columns = [ 'adviser', 'name', 'forename', 'surname', 'title', 'shortname', 'date_of_birth', 'ni_number', 'image_id', 'effective_from', 'effective_to' ]; 
#    edit_columns = [ 'adviser', 'name', 'forename', 'surname', 'title', 'shortname', 'date_of_birth', 'ni_number', 'image_id', 'effective_from', 'effective_to' ]; 
#    add_columns = [ 'adviser', 'name', 'forename', 'surname', 'title', 'shortname', 'date_of_birth', 'ni_number', 'image_id', 'effective_from', 'effective_to' ]; 

    related_views = [ ContactsView , DiaryEntriesView , PortfoliosView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class InstrumentsView(ModelView):
    datamodel = SQLAInterface(Instruments)

    list_columns = [ 'instrument_type', 'instrument_category', 'name', 'title', 'nickname', 'symbol', 'currency', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'instrument_type', 'instrument_category', 'name', 'title', 'nickname', 'symbol', 'currency', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'instrument_type', 'instrument_category', 'name', 'title', 'nickname', 'symbol', 'currency', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'instrument_type', 'instrument_category', 'name', 'title', 'nickname', 'symbol', 'currency', 'effective_from', 'effective_to' ]; 

    related_views = [ ProductsView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class ExchangesView(ModelView):
    datamodel = SQLAInterface(Exchanges)

    list_columns = [ 'currency', 'country', 'name', 'title', 'nickname', 'symbol', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'currency', 'country', 'name', 'title', 'nickname', 'symbol', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'currency', 'country', 'name', 'title', 'nickname', 'symbol', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'currency', 'country', 'name', 'title', 'nickname', 'symbol', 'effective_from', 'effective_to' ]; 

    related_views = [ ProductsView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class CountriesView(ModelView):
    datamodel = SQLAInterface(Countries)

    list_columns = [ 'currency', 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'currency', 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'currency', 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'currency', 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 

    related_views = [ ExchangesView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class AdvisersView(ModelView):
    datamodel = SQLAInterface(Advisers)

    list_columns = [ 'managed_by', 'user', 'name', 'title', 'nickname', 'forenames', 'surname', 'phone', 'email', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'managed_by', 'user', 'name', 'title', 'nickname', 'forenames', 'surname', 'phone', 'email', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'managed_by', 'user', 'name', 'title', 'nickname', 'forenames', 'surname', 'phone', 'email', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'managed_by', 'user', 'name', 'title', 'nickname', 'forenames', 'surname', 'phone', 'email', 'effective_from', 'effective_to' ]; 

    related_views = [ ClientsView , DiariesView ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class InstrumentTypesView(ModelView):
    datamodel = SQLAInterface(InstrumentTypes)

    list_columns = [ 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'title', 'nickname', 'effective_from', 'effective_to' ]; 

    related_views = [ InstrumentsView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class InstrumentCategoriesView(ModelView):
    datamodel = SQLAInterface(InstrumentCategories)

    list_columns = [ 'name', 'title', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'title', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'title', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'title', 'effective_from', 'effective_to' ]; 

    related_views = [ InstrumentsView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class CurrenciesView(ModelView):
    datamodel = SQLAInterface(Currencies)

    list_columns = [ 'name', 'title', 'nickname', 'minor_unit', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'title', 'nickname', 'minor_unit', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'title', 'nickname', 'minor_unit', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'title', 'nickname', 'minor_unit', 'effective_from', 'effective_to' ]; 

    related_views = [ ExchangesView , ProductsView , CountriesView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


class UsersView(ModelView):
    datamodel = SQLAInterface(Users)

    list_columns = [ 'name', 'title', 'shortname', 'user_type', 'type_id', 'badge_number', 'is_suspended', 'effective_from', 'effective_to' ]; 
    show_columns = [ 'name', 'title', 'shortname', 'user_type', 'type_id', 'badge_number', 'is_suspended', 'effective_from', 'effective_to' ]; 
    edit_columns = [ 'name', 'title', 'shortname', 'user_type', 'type_id', 'badge_number', 'is_suspended', 'effective_from', 'effective_to' ]; 
    add_columns = [ 'name', 'title', 'shortname', 'user_type', 'type_id', 'badge_number', 'is_suspended', 'effective_from', 'effective_to' ]; 

    related_views = [ AdvisersView , LoginsView , UserRolesView  ]; 

#    show_template = 'appbuilder/general/model/show_cascade.html' 
#    edit_template = 'appbuilder/general/model/edit_cascade.html' 


######################################################################

class MyIndexView(IndexView):
    index_template = 'appbuilder/index.html'



######################################################################


#appbuilder.add_view_no_menu(UserTagsView)
#appbuilder.add_view_no_menu(TagsView)
appbuilder.add_view_no_menu(DiaryEntriesView)
appbuilder.add_view_no_menu(ColumnsView)
appbuilder.add_view_no_menu(IndexComponentsView)
appbuilder.add_view_no_menu(PortfolioPositionsView)
appbuilder.add_view_no_menu(DataView)
appbuilder.add_view_no_menu(FeedView)
appbuilder.add_view_no_menu(UserColumnsView)
appbuilder.add_view_no_menu(ContactSocialNetworksView)
appbuilder.add_view_no_menu(RoleRightsView)
appbuilder.add_view_no_menu(UserRolesView)
appbuilder.add_view_no_menu(RolesView)
appbuilder.add_view_no_menu(UsersView)



appbuilder.add_view(AdvisersView, "Advisers", icon="fa-users", category="WMS", category_icon = "fa-server")
appbuilder.add_separator("Advisers")
appbuilder.add_view(DiariesView, "Diaries", icon="fa-folder-open-o", category="WMS")
appbuilder.add_separator("Advisers")
appbuilder.add_view(ClientsView, "Clients", icon="fa-users", category="WMS")
appbuilder.add_separator("Advisers")
appbuilder.add_view(PortfoliosView, "Portfolios",icon = "fa-envelope-o",category = "WMS")
appbuilder.add_view(ContactsView, "Contacts",icon = "folder-open-o",category = "WMS")


appbuilder.add_view(ExchangesView, "Exchanges",icon = "fa-bank",category = "Market Data", category_icon = "fa-server")
appbuilder.add_view(InstrumentsView, "Instruments",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(ProductsView, "Products",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(IndicesView, "Indices",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(QuotesView, "Quotes",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(HistoricQuotesView, "Historic Quotes",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(HistoricPricesView, "Historic Prices",icon = "fa-envelope",category = "Market Data")

appbuilder.add_view(CountriesView, "Countries",icon = "fa-envelope",category = "Static Data", category_icon = "fa-server" )
appbuilder.add_view(CurrenciesView, "Currencies",icon = "fa-envelope",category = "Static Data")
appbuilder.add_view(InstrumentTypesView, "Instrument Types",icon = "fa-envelope",category = "Static Data" )
appbuilder.add_view(InstrumentCategoriesView, "Instrument Categories",icon = "fa-envelope",category = "Static Data")
appbuilder.add_view(SocialNetworksView, "SocialNetworks",icon = "fa-envelope",category = "Static Data")

appbuilder.add_view(GlobalsView, "Properties",icon = "fa-bank",category = "Management", category_icon = "fa-database")
appbuilder.add_view(LogsView, "Logs",icon = "fa-bank",category = "Management")
appbuilder.add_view(LookupsView, "Lookups",icon = "fa-bank",category = "Management")
appbuilder.add_view(LookupCodesView, "Lookup codes",icon = "fa-bank",category = "Management")
appbuilder.add_view(CrossReferencesView, "Cross reference",icon = "fa-bank",category = "Management")
appbuilder.add_view(QueryDetailColumnsView, "Query Columns",icon = "fa-bank",category = "Management")



