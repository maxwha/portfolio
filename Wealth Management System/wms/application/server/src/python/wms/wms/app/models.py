import datetime
#from sqlalchemy import Table, Column, Integer, String, ForeignKey, Date, Text
from sqlalchemy import BigInteger, Boolean, Column, Date, DateTime, ForeignKey, Index, Integer, Numeric, String, Table, Text, text
from sqlalchemy.orm import relationship
from flask.ext.appbuilder import Model
from flask.ext.appbuilder.models.mixins import AuditMixin, FileColumn, ImageColumn

# this seems to be used for views support
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
metadata = Base.metadata

def today():
    return datetime.datetime.today().strftime('\%Y-\%m-\%d')

class Globals(Model):
    __tablename__ = 'globals'

    id = Column(Integer, primary_key=True , nullable=False) 
    code = Column(String(8000), nullable=True) 
    value = Column(String(8000), nullable=True) 
    datatype = Column(String(8000), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


class Lookups(Model):
    __tablename__ = 'lookups'

    id = Column(Integer, primary_key=True , nullable=False) 
    type = Column(String(255), nullable=True) 
    code = Column(String(255), nullable=True) 
    basetype = Column(String(255), nullable=True) 
    table_name = Column(String(255), nullable=True) 
    table_key = Column(String(255), nullable=True) 
    value = Column(String(255), nullable=True) 
    shortname = Column(String(255), nullable=True) 
    name = Column(String(255), nullable=True) 
    title = Column(String(255), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class LookupCodes(Model):
    __tablename__ = 'lookup_codes'

    id = Column(Integer, primary_key=True , nullable=False) 
    type = Column(String(255), nullable=False) 
    code = Column(String(255), nullable=False) 
    value = Column(String(255), nullable=False) 
    name = Column(String(255), nullable=True) 
    shortname = Column(String(30), nullable=True) 
    sequence = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class Logs(Model):
    __tablename__ = 'logs'

    id = Column(Integer, primary_key=True , nullable=False) 
    message = Column(String(16384), nullable=True) 
    message_source = Column(String(255), nullable=True) 
    message_depth = Column(Integer, nullable=True) 
    message_severity = Column(String(30), nullable=True) 
    message_date = Column(DateTime, nullable=True) 
    message_user = Column(String(100), nullable=True) 
    logs_date = Column(DateTime, nullable=True) 
    user_id = Column(String(100), nullable=True) 


class QueryDetailColumns(Model):
    __tablename__ = 'query_detail_columns'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), nullable=True) 
    column_sequence = Column(Integer, nullable=True) 
    column_name = Column(String(50), nullable=True) 
    column_title = Column(String(50), nullable=True) 
    column_summary = Column(String(50), nullable=True) 
    column_precedence = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class Objects(Model):
    __tablename__ = 'objects'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), nullable=True) 
    id_name = Column(String(255), nullable=True) 
    value_name = Column(String(255), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class CrossReferences(Model):
    __tablename__ = 'cross_references'

    id = Column(Integer, primary_key=True , nullable=False) 
    object_type = Column(String(255), nullable=True) 
    object_id = Column(Integer, nullable=True) 
    xref_type = Column(String(255), nullable=True) 
    xref_value = Column(String(255), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


class ClientContacts(Model):
    __tablename__ = 'client_contacts'

    id = Column(Integer, primary_key=True , nullable=False) 
    client_id = Column(Integer, nullable=True) 
    contact_id = Column(Integer, nullable=True) 
    sequence = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


class Columns(Model):
    __tablename__ = 'columns'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(8000), unique=True , nullable=True) 
    title = Column(String(8000), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class UserColumns(Model):
    __tablename__ = 'user_columns'

    id = Column(Integer, primary_key=True , nullable=False) 
    user_id = Column(Integer, nullable=True) 
    object_name = Column(String(8000), nullable=True) 
    column_name = Column(String(8000), nullable=True) 
    column_title = Column(String(8000), nullable=True) 
    column_order = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


class Feed(Model):
    __tablename__ = 'feed'

    id = Column(Integer, primary_key=True , nullable=False) 
    feed_id = Column(Integer, nullable=True) 
    feed_type = Column(String(255), nullable=True) 
    source_system = Column(String(255), nullable=True) 
    source_reference = Column(String(8000), nullable=True) 
    tag = Column(String(8000), nullable=True) 
    value = Column(String(8000), nullable=True) 
    value_sequence = Column(Integer, nullable=True) 
    state = Column(Integer, nullable=True) 
    target_state = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


class Data(Model):
    __tablename__ = 'data'

    id = Column(Integer, primary_key=True , nullable=False) 
    data_id = Column(Integer, nullable=True) 
    data_type = Column(String(255), nullable=True) 
    source_system = Column(String(255), nullable=True) 
    source_reference = Column(String(8000), nullable=True) 
    tag = Column(String(8000), nullable=True) 
    value = Column(String(8000), nullable=True) 
    value_sequence = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


class PreferencePriorities(Model):
    __tablename__ = 'preference_priorities'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), nullable=True) 
    priority = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class IndexComponents(Model):
    __tablename__ = 'index_components'

    id = Column(Integer, primary_key=True , nullable=False) 
    index_id = Column(Integer, ForeignKey('indices.id'), nullable=True) 
    product_id = Column(Integer, ForeignKey('products.id'), nullable=True) 
    sequence = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    product = relationship("Products") 
    index = relationship("Indices") 

class ContactSocialNetworks(Model):
    __tablename__ = 'contact_social_networks'

    id = Column(Integer, primary_key=True , nullable=False) 
    contact_id = Column(Integer, ForeignKey('contacts.id'), nullable=True) 
    social_network_id = Column(Integer, ForeignKey('social_networks.id'), nullable=True) 
    identifier = Column(String(255), unique=True , nullable=True) 
    sequence = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    social_network = relationship("SocialNetworks") 
    contact = relationship("Contacts") 

class UserRoles(Model):
    __tablename__ = 'user_roles'

    id = Column(Integer, primary_key=True , nullable=False) 
    user_id = Column(Integer, ForeignKey('users.id'), nullable=True) 
    role_id = Column(Integer, ForeignKey('roles.id'), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    role = relationship("Roles") 
    user = relationship("Users") 

class HistoricQuotes(Model):
    __tablename__ = 'historic_quotes'

    id = Column(Integer, primary_key=True , nullable=False) 
    product_id = Column(Integer, ForeignKey('products.id'), nullable=True) 
    latest_value = Column(Numeric(36,8), nullable=True) 
    open = Column(Numeric(36,8), nullable=True) 
    high = Column(Numeric(36,8), nullable=True) 
    low = Column(Numeric(36,8), nullable=True) 
    close = Column(Numeric(36,8), nullable=True) 
    quote_date = Column(DateTime, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    product = relationship("Products") 

class PortfolioPositions(Model):
    __tablename__ = 'portfolio_positions'

    id = Column(Integer, primary_key=True , nullable=False) 
    portfolio_id = Column(Integer, ForeignKey('portfolios.id'), nullable=True) 
    product_id = Column(Integer, ForeignKey('products.id'), nullable=True) 
    currency = Column(String(3), nullable=True) 
    quantity = Column(Integer, nullable=True) 
    price = Column(Numeric(36,8), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    product = relationship("Products") 
    portfolio = relationship("Portfolios") 

class RoleRights(Model):
    __tablename__ = 'role_rights'

    id = Column(Integer, primary_key=True , nullable=False) 
    role_id = Column(Integer, ForeignKey('roles.id'), nullable=True) 
    object_type = Column(String(255), nullable=True) 
    object_name = Column(String(255), nullable=True) 
    object_subname = Column(String(255), nullable=True) 
    access_rights = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    role = relationship("Roles") 

class HistoricPrices(Model):
    __tablename__ = 'historic_prices'

    id = Column(Integer, primary_key=True , nullable=False) 
    product_id = Column(Integer, ForeignKey('products.id'), nullable=True) 
    price_date = Column(DateTime, nullable=True) 
    open = Column(Numeric(36,8), nullable=True) 
    high = Column(Numeric(36,8), nullable=True) 
    low = Column(Numeric(36,8), nullable=True) 
    close = Column(Numeric(36,8), nullable=True) 
    volume = Column(Numeric(36,8), nullable=True) 
    adj_close = Column(Numeric(36,8), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    product = relationship("Products") 

class DiaryEntries(Model):
    __tablename__ = 'diary_entries'

    id = Column(Integer, primary_key=True , nullable=False) 
    diary_id = Column(Integer, ForeignKey('diaries.id'), nullable=True) 
    subject = Column(String(255), nullable=True) 
    location = Column(String(255), nullable=True) 
    start_time = Column(DateTime, nullable=True) 
    end_time = Column(DateTime, nullable=True) 
    details = Column(Text, nullable=True) 
    notes = Column(Text, nullable=True) 
    user_id = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 
    client_id = Column(Integer, ForeignKey('clients.id'), nullable=True) 

    client = relationship("Clients") 
    diary = relationship("Diaries") 

class Logins(Model):
    __tablename__ = 'logins'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), nullable=False) 
    title = Column(String(255), nullable=False) 
    username = Column(String(255), nullable=False) 
    password = Column(String(255), nullable=False) 
    user_id = Column(Integer, ForeignKey('users.id'), nullable=True) 
    role_id = Column(Integer, ForeignKey('roles.id'), nullable=True) 
    last_login = Column(DateTime, nullable=True) 
    one_off = Column(DateTime, nullable=True) 
    is_suspended = Column(String(1), nullable=False) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    user = relationship("Users") 
    role = relationship("Roles") 

    def __repr__(self):
        return self.name

class Quotes(Model):
    __tablename__ = 'quotes'

    id = Column(Integer, primary_key=True , nullable=False) 
    product_id = Column(Integer, ForeignKey('products.id'), nullable=True) 
    latest_value = Column(Numeric(36,8), nullable=True) 
    open = Column(Numeric(36,8), nullable=True) 
    high = Column(Numeric(36,8), nullable=True) 
    low = Column(Numeric(36,8), nullable=True) 
    mid = Column(Numeric(36,8), nullable=True) 
    close = Column(Numeric(36,8), nullable=True) 
    bid = Column(Numeric(36,8), nullable=True) 
    offer = Column(Numeric(36,8), nullable=True) 
    quote_date = Column(DateTime, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    product = relationship("Products") 

class Products(Model):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True , nullable=False) 
    exchange_id = Column(Integer, ForeignKey('exchanges.id'), nullable=True) 
    instrument_id = Column(Integer, ForeignKey('instruments.id'), nullable=True) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    symbol = Column(String(50), nullable=True) 
    currency_id = Column(Integer, ForeignKey('currencies.id'), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    instrument = relationship("Instruments") 
    currency = relationship("Currencies") 
    exchange = relationship("Exchanges") 

    def __repr__(self):
        return self.name

class Contacts(Model):
    __tablename__ = 'contacts'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    position = Column(String(255), nullable=True) 
    forename = Column(String(255), nullable=True) 
    surname = Column(String(255), nullable=True) 
    honours = Column(String(255), nullable=True) 
    phone = Column(String(255), nullable=True) 
    email = Column(String(255), nullable=True) 
    address_name = Column(String(255), nullable=True) 
    address_no = Column(String(255), nullable=True) 
    address_street = Column(String(255), nullable=True) 
    address_district = Column(String(255), nullable=True) 
    address_town = Column(String(255), nullable=True) 
    address_city = Column(String(255), nullable=True) 
    address_county = Column(String(255), nullable=True) 
    address_country = Column(String(255), nullable=True) 
    address_postcode = Column(String(255), nullable=True) 
    client_id = Column(Integer, ForeignKey('clients.id'), nullable=True) 
    sequence = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    client = relationship("Clients") 

    def __repr__(self):
        return self.name

class Diaries(Model):
    __tablename__ = 'diaries'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    adviser_id = Column(Integer, ForeignKey('advisers.id'), nullable=True) 
    user_id = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    adviser = relationship("Advisers") 

    def __repr__(self):
        return self.name

class SocialNetworks(Model):
    __tablename__ = 'social_networks'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    website = Column(String(255), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class Roles(Model):
    __tablename__ = 'roles'

    id = Column(Integer, primary_key=True , nullable=False) 
    role = Column(String(50), nullable=False) 
    name = Column(String(255), nullable=False) 
    title = Column(String(255), nullable=False) 
    shortname = Column(String(50), nullable=False) 
    precedence = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class Indices(Model):
    __tablename__ = 'indices'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class Portfolios(Model):
    __tablename__ = 'portfolios'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(50), unique=True , nullable=True) 
    title = Column(String(50), nullable=True) 
    client_id = Column(Integer, ForeignKey('clients.id'), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    client = relationship("Clients") 

    def __repr__(self):
        return self.name

class Clients(Model):
    __tablename__ = 'clients'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    forename = Column(String(255), nullable=True) 
    surname = Column(String(255), nullable=True) 
    title = Column(String(255), nullable=True) 
    shortname = Column(String(50), nullable=True) 
    adviser_id = Column(Integer, ForeignKey('advisers.id'), nullable=True) 
    date_of_birth = Column(Date, nullable=True) 
    ni_number = Column(String(10), nullable=True) 
    image_id = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    adviser = relationship("Advisers") 

    def __repr__(self):
        return self.name

class Instruments(Model):
    __tablename__ = 'instruments'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    symbol = Column(String(50), nullable=True) 
    currency = Column(String(3), nullable=True) 
    instrument_type_id = Column(Integer, ForeignKey('instrument_types.id'), nullable=True) 
    instrument_category_id = Column(Integer, ForeignKey('instrument_categories.id'), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    instrument_category = relationship("InstrumentCategories") 
    instrument_type = relationship("InstrumentTypes") 

    def __repr__(self):
        return self.name

class Exchanges(Model):
    __tablename__ = 'exchanges'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    symbol = Column(String(50), nullable=True) 
    currency_id = Column(Integer, ForeignKey('currencies.id'), nullable=True) 
    country_id = Column(Integer, ForeignKey('countries.id'), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    currency = relationship("Currencies") 
    country = relationship("Countries") 

    def __repr__(self):
        return self.name

class Countries(Model):
    __tablename__ = 'countries'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    currency_id = Column(Integer, ForeignKey('currencies.id'), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    currency = relationship("Currencies") 

    def __repr__(self):
        return self.name

class Advisers(Model):
    __tablename__ = 'advisers'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    forenames = Column(String(255), nullable=True) 
    surname = Column(String(255), nullable=True) 
    phone = Column(String(255), nullable=True) 
    email = Column(String(255), nullable=True) 
    managed_by_id = Column(Integer, ForeignKey('advisers.id'), nullable=True) 
    user_id = Column(Integer, ForeignKey('users.id'), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 

    user = relationship("Users") 
    managed_by = relationship("Advisers") 

    def __repr__(self):
        return self.name

class InstrumentTypes(Model):
    __tablename__ = 'instrument_types'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class InstrumentCategories(Model):
    __tablename__ = 'instrument_categories'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class Currencies(Model):
    __tablename__ = 'currencies'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), unique=True , nullable=True) 
    title = Column(String(255), nullable=True) 
    nickname = Column(String(50), nullable=True) 
    minor_unit = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name

class Users(Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True , nullable=False) 
    name = Column(String(255), nullable=True) 
    title = Column(String(255), nullable=True) 
    shortname = Column(String(50), nullable=True) 
    user_type = Column(String(50), nullable=True) 
    type_id = Column(Integer, nullable=True) 
    badge_number = Column(Integer, nullable=True) 
    is_suspended = Column(Integer, nullable=True) 
    effective_from = Column(DateTime, nullable=True) 
    effective_to = Column(DateTime, nullable=True) 


    def __repr__(self):
        return self.name


