
#appbuilder.add_view_no_menu(UserTagsView)
#appbuilder.add_view_no_menu(TagsView)
appbuilder.add_view_no_menu(DiaryEntriesView)
appbuilder.add_view_no_menu(ColumnsView)
appbuilder.add_view_no_menu(IndexComponentsView)
appbuilder.add_view_no_menu(PortfolioPositionsView)
appbuilder.add_view_no_menu(DataView)
appbuilder.add_view_no_menu(FeedView)
appbuilder.add_view_no_menu(UserColumnsView)
appbuilder.add_view_no_menu(ContactSocialNetworksView)
appbuilder.add_view_no_menu(RoleRightsView)
appbuilder.add_view_no_menu(UserRolesView)
appbuilder.add_view_no_menu(RolesView)
appbuilder.add_view_no_menu(UsersView)



appbuilder.add_view(AdvisersView, "Advisers", icon="fa-users", category="WMS", category_icon = "fa-server")
appbuilder.add_separator("Advisers")
appbuilder.add_view(DiariesView, "Diaries", icon="fa-folder-open-o", category="WMS")
appbuilder.add_separator("Advisers")
appbuilder.add_view(ClientsView, "Clients", icon="fa-users", category="WMS")
appbuilder.add_separator("Advisers")
appbuilder.add_view(PortfoliosView, "Portfolios",icon = "fa-envelope-o",category = "WMS")
appbuilder.add_view(ContactsView, "Contacts",icon = "folder-open-o",category = "WMS")


appbuilder.add_view(ExchangesView, "Exchanges",icon = "fa-bank",category = "Market Data", category_icon = "fa-server")
appbuilder.add_view(InstrumentsView, "Instruments",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(ProductsView, "Products",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(IndicesView, "Indices",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(QuotesView, "Quotes",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(HistoricQuotesView, "Historic Quotes",icon = "fa-envelope",category = "Market Data")
appbuilder.add_view(HistoricPricesView, "Historic Prices",icon = "fa-envelope",category = "Market Data")

appbuilder.add_view(CountriesView, "Countries",icon = "fa-envelope",category = "Static Data", category_icon = "fa-server" )
appbuilder.add_view(CurrenciesView, "Currencies",icon = "fa-envelope",category = "Static Data")
appbuilder.add_view(InstrumentTypesView, "Instrument Types",icon = "fa-envelope",category = "Static Data" )
appbuilder.add_view(InstrumentCategoriesView, "Instrument Categories",icon = "fa-envelope",category = "Static Data")
appbuilder.add_view(SocialNetworksView, "SocialNetworks",icon = "fa-envelope",category = "Static Data")

appbuilder.add_view(GlobalsView, "Properties",icon = "fa-bank",category = "Management", category_icon = "fa-database")
appbuilder.add_view(LogsView, "Logs",icon = "fa-bank",category = "Management")
appbuilder.add_view(LookupsView, "Lookups",icon = "fa-bank",category = "Management")
appbuilder.add_view(LookupCodesView, "Lookup codes",icon = "fa-bank",category = "Management")
appbuilder.add_view(CrossReferencesView, "Cross reference",icon = "fa-bank",category = "Management")
appbuilder.add_view(QueryDetailColumnsView, "Query Columns",icon = "fa-bank",category = "Management")



