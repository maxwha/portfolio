import os
import sys
import csv
import ystockquote
import psycopg2
from pprint import pprint

if len(sys.argv) < 3:
    print 'usage: ' + sys.argv[0] + ' start_date["YYYY-MM-DD"] end_date["YYYY-MM-DD"]' 
    quit()

start_date = sys.argv[1]
end_date = sys.argv[2]

db_user= os.environ['DB_USER']
db_pass= os.environ['DB_PASS']
db_host = os.environ['DB_HOST']
db_db = os.environ['DB_DB']
db_db = os.environ['DB_DB']
db_port = os.environ['DB_PORT']

con = psycopg2.connect(database=db_db, user=db_user, password=db_pass, host=db_host, port=db_port)

cur = con.cursor()
cur.execute('SELECT version()')
ver = cur.fetchone()
print ver

cur.execute('SELECT distinct p.symbol, p.id FROM products p ')
products = cur.fetchall()
#start_date = '2000-01-01'
#end_date = '2015-03-08'

for p in products:
    ticker = p[0]
    id = p[1]

#    stock_exchange = ystockquote.get_stock_exchange(ticker)
#    change = ystockquote.get_change(ticker)
#    price = ystockquote.get_price(ticker)
#    market_cap = ystockquote.get_market_cap(ticker)
#    _52_week_high = ystockquote.get_52_week_high(ticker)
#    _52_week_low = ystockquote.get_52_week_low(ticker)
#    avg_volume = ystockquote.get_avg_daily_volume(ticker)
#    volume = ystockquote.get_volume(ticker)

#    print (ticker + " (" + change + ") ")
#    print (stock_exchange.strip('"'))
#    print ("Share Price: " + price)
#    print ("Market Cap: " + market_cap)
#    print ("52 Week High/Low: " + _52_week_high + "/" + _52_week_low)
#    print ("Trading Volume: " + volume)
#    print ("Average Trading Volume(3m): " + avg_volume)

    try:

        historic_prices = ystockquote.get_historical_prices(ticker, start_date, end_date)

        for date in historic_prices:
            detail = historic_prices[date]

            try:
                cur.execute(("INSERT INTO historic_prices (product_id, price_date, open, high, low, close, volume, adj_close ) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' ) ") % (id, date, detail['Open'], detail['High'], detail['Low'], detail['Close'], detail['Volume'], detail['Adj Close'] ))

            except Exception, e:
                print(ticker, str(e) )
                con.rollback()

        con.commit()

    except Exception, e:
        print(ticker, str(e) )
        con.rollback()

