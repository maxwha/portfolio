# README #

## Contents ##

CV.pdf - Latest CV.

Computer Games Animation.pdf - Code Listing for animating in C++.

SQL to NoSQL Transfer.pdf - Report on transferring data from a MySQL database to a MongoDB database.

### Dissertation ###

PeltierApp - Application used to communicate with with a Peltier device using bluetooth.

html - Website used to display images during experiment.

Summary Poster - Summary of my dissertation.

Cool Side of the Pillow - Complete dissertation. 

### Mobile Applications ###

Content Provider - Contains source code and report for creating a content provider application.

Quiz - Contains source code and report for creating a quiz application.

### Wealth Management System ###

wms/dbs/wms - PostgreSQL database source used as the backend for the wealth management system.

wms/dbs/wms/doc - scripts to generate PostgreSQL database.

wms/dbs/wms/doc/awswms/index.html - Overview of the database

wms/bin - Support shell scripts.

wms/distribution/dbs/bin - Shell scripts to generate database.

wms/application/server/src/python/wms/wms/app - Python application using a restful interface and flask framework. 

Application Design and Implementation.pdf - Report on creating a wealth management system using YahooFinance API. Unfortunately the application can no longer be viewed.

wms.pdf - PostgreSQL database Schema.

[AWS Database Schema](https://bitbucket.org/maxwha/portfolio/src/89a4be5567893cb4c5aaf905b6e100d7badba97a/Wealth%20Management%20System/wms/dbs/wms/doc/awswms/index.html)

**Owner: **Maxwha